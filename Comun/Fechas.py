#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

"""
DESCRIPCIÓN
===========

Algunas utilidades para el manejo de fechas (L{FECHA}), intervalos
(L{TSDELTA}) y marcas de tiempo (L{TIMESTAMP}).

ÚLTIMAS MODIFICACIONES
======================

 - 15/2/2015 Añadidas funciones new_fecha y new_timestamp.
 - 30/11/2002 22:47 - Añadido el método L{FECHA.AsShortText}
"""

import sys
import re
import time
import calendar
import datetime
import types
from cgi import escape

from Libreria.Comun import numerals
from Libreria.Comun import Colores

Version = 1.4
Modificado = '13/08/01 0:17'

MESES = ['',  # No Existe mes 0
    'enero' , 'febrero' , 'marzo',
    'abril' , 'mayo' , 'junio',
    'julio' , 'agosto' , 'septiembre',
    'octubre' , 'noviembre' , 'diciembre',
    ]

TOPE_MESES = [0 # No Existe mes 0
    , 31  # enero
    , 28  # febrero
    , 31  # marzo
    , 30  # abril
    , 31  # mayo
    , 30  # junio
    , 31  # julio
    , 31  # agosto
    , 30  # septiembre
    , 31  # octubre
    , 30  # noviembre
    , 31  # diciembre
    ]

WEEKDAY = ['L','M','X','J','V','S','D']
FULLWEEKDAY = ['lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado', 'domingo']
ABBRWEEKDAY = ['lun', 'mar', 'mié', 'jue', 'vie', 'sáb', 'dom']

SECS_IN_DAY = 86400 # 864000 segundos es un dia


patFecha = re.compile('(\d+)[\\\/:\.,\- ]+(\d+)[\\\/:\.,\- ](\d\d\d\d)')


class ErrorFormatoFecha(Exception):
    pass


def hoy(): # PEP8 funstion name style
    """Retorna una variable de tipo L{FECHA} con el valor del día actual.

    @return: La fecha actual, como una instacia de la clase L{FECHA}
    @rtype: L{FECHA}
    @see: L{Ayer}, L{Ahora}
    """
    return FECHA(time.time())
Hoy = hoy # Compatibilidad hacia atras

def ayer(): # PEP8 funstion name style
    """Retorna una variable de tipo FECHA con el valor del día anterior.

    @return: La fecha de ayer, como una instacia de
        la clase L{FECHA}
    @rtype: L{FECHA}
    @see: L{Hoy}, L{Ahora}
    """
    return FECHA(time.time()) - 1
Ayer = ayer # Compatibilidad hacia atras

def ahora(): # PEP8 funstion name style
    """Retorna una variable de tipo TIMESTAMP con el valor del momento actual.

    @return: La fecha y hora actual, como una instacia de
        la clase L{TIMESTAMP}
    @rtype: L{TIMESTAMP}
    @see: L{Hoy}, L{Ayer}
    """
    return TIMESTAMP()
Ahora = ahora # Compatibilidad hacia atras

def edad(f):
    today = hoy()
    result = today.year - f.year
    if (today.month, today.day) > (f.month, f.day):
        result += 1
    return result



class TIMEBASE(object):
    """Clase base compartida por L{FECHA} y L{TIMESTAMP}
    """
    ReverseMonth = {
        'Jan':1, 'January':1, 'Ene':1, 'Enero': 1
        , 'Feb': 2, 'February':2, 'Febrero':2
        , 'Mar': 3, 'March': 3, 'Marzo': 3
        , 'Apr': 4, 'April': 4, 'Abr': 4, 'Abril':4
        , 'May': 5, 'Mayo':5
        , 'Jun': 6, 'June':6, 'Junio': 6
        , 'Jul': 7, 'July':7, 'Julio':7
        , 'Aug': 8, 'August':8, 'Ago':8, 'Agosto':8
        , 'Sep': 9, 'September':9, 'Septiembre': 9
        , 'Oct': 10, 'October': 10, 'Octubre': 10
        , 'Nov': 11, 'November': 11, 'Noviembre':11
        , 'Dec': 12, 'December':12, 'Dic':12, 'Diciembre':12
        }

    patRFC822 = re.compile('(\w\w\w, )?(\d+) (\w+) (\d+) (\d+):(\d+):(\d+).+')
    patAMD = re.compile('^(\d{4})[\-/](\d{1,2})[\-/](\d{1,2})$')
    patDMA = re.compile('(\d+)[\\\/:\.,\- ]+([a-zA-Z0-9]+)[\\\/:\.,\- ](\d\d\d\d)')
    patFechaCompleta = re.compile('(\d+) de ([a-zA-Z]+) de (\d+)')
    patFechaHora = re.compile('(\d+)-(\d+)-(\d+) \d\d:\d\d:\d\d.\d\d')
    patAAAAMMDDhhmmss = re.compile('(\d\d\d\d)[/\-](\d\d?)[/\-](\d\d?)[ T](\d+):(\d+):(\d+)(?:[\+\-]\d\d:\d\d)?')
    patDDMMAAAAhhmmss = re.compile('(\d\d?)[/\-](\d\d?)[/\-](\d\d\d\d) (\d+):(\d+):(\d+)')
    patDDMMAAhhmmss = re.compile('(\d\d?)[/\-](\d\d?)[/\-](\d\d) (\d+):(\d+):(\d+)')
    patWeekDayMonthDDhhmmssAAAA = re.compile('... (...) (\d+) (\d\d):(\d\d):(\d\d) (\d\d\d\d)')

class TIMESTAMP(TIMEBASE):
    """Clase para trabajar con marcas temporal, con precisión hasta segundos.
    """

    def raises_format_error(self, s):
        raise ValueError(
            'TIMESTAMP: El formato de entrada: {}'
            ' es incorrecto'.format(s)
            )

    def __init__(self, ts=None):
        """
        Constructor para la clase TIMESTAMP.

        @param ts: Tiempo de referencia, en milisegundos, o bien una string
               indicando la fecha y hora en el formato "dd/mm/[aa]aa hh:mm:ss",
               por ejemplo "09/17/2003 15:19:36". Si no se incluye
               se tomará el tiempo de referencia del sistema.
        @raise ValueError: Si el constructor no puede formar un TIMESTAMP
            correcto a partir de los parámetros suministrados.
        """
        if ts == None:
            self.TS = time.localtime(time.time())
        elif type(ts) == type(0.0):
            self.TS = time.localtime(ts)
        elif type(ts) == type(''):
            ts = ts.strip()
            if TIMESTAMP.patAAAAMMDDhhmmss.match(ts):
                self.parseTimestampAAAAMMDDhhmmss(ts)
            elif TIMESTAMP.patDDMMAAAAhhmmss.match(ts):
                self.parseTimestampDDMMAAAAhhmmss(ts)
            elif TIMESTAMP.patDDMMAAhhmmss.match(ts):
                self.parseTimestampDDMMAAhhmmss(ts)
            elif TIMESTAMP.patWeekDayMonthDDhhmmssAAAA.match(ts):
                self.parseTimestampWeekDayMonthDDhhmmssAAAA(ts)
            else:
                self.raises_format_error(ts)
        elif isinstance(ts, datetime.datetime) or isinstance(ts, datetime.date):
            self.TS = ts.timetuple()
        else:
            # vamos a intentar convertirlo a string y a ver si asi...
            ts = str(ts).strip()
            if TIMESTAMP.patAAAAMMDDhhmmss.match(ts):
                self.parseTimestampAAAAMMDDhhmmss(ts)
            elif TIMESTAMP.patDDMMAAAAhhmmss.match(ts):
                self.parseTimestampDDMMAAAAhhmmss(ts)
            elif TIMESTAMP.patDDMMAAhhmmss.match(ts):
                self.parseTimestampDDMMAAhhmmss(ts)
            elif TIMESTAMP.patWeekDayMonthDDhhmmssAAAA.match(ts):
                self.parseTimestampWeekDayMonthDDhhmmssAAAA(ts)
            else:
                self.raises_format_error(ts)

    def getDiaSemana(self):
        """Retorna el dia de la semana.


        En caso de necesitar el día del mes, vease
        el método L{getDia}

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> f = Fechas.TIMESTAMP('19/9/2003 23:12:22')
            >>> assert f.getDiaSemana() == 4

        @return: un número indicando el día de la semana, en
                 el rango [0-6] (0=Lunes)
        @rtype: integer
        """
        return self.TS[6]

    def AsDatetime(self):
        return datetime.datetime.fromtimestamp(time.mktime(self.TS))

    def parseTimestampWeekDayMonthDDhhmmssAAAA(self, ts):
        """
        Intenta interpretar una string que tenga
        el formato: Fri Mar 19 10:33:29 2010

        @raise ValueError: Si el parámetro suministrado no es correcto.
        """
        m = FECHA.patWeekDayMonthDDhhmmssAAAA.match(ts)
        if m:
            (NombreMes, Dia, Hora, Minutos, Segundos, Anio) = m.groups()
            Mes = self.ReverseMonth.get(NombreMes)
            Dia = int(Dia)
            Hora, Minutos, Segundos = int(Hora), int(Minutos), int(Segundos)
            Anio = int(Anio)
            tupla = (Anio, Mes , Dia, Hora, Minutos, Segundos, 0,0,-1)
            self.TS = time.localtime(time.mktime(tupla))
        else:
            self.raises_format_error(ts)

    def parseTimestampAAAAMMDDhhmmss(self, ts):
        """
        Intenta interpretar una string que tenga
        el formato: AAAA-MM-DD hh:mm:ss[+/-hh:mm]

            >>> from Libreria.Comun import Fechas
            >>> ts = Fechas.TIMESTAMP()
            >>> ts.parseTimestampAAAAMMDDhhmmss('1976-6-30 12:23:41')
            >>> assert str(ts) == '30/06/1976 12:23:41'

        @raise ValueError: Si el parámetro suministrado no es correcto.
        """
        m = FECHA.patAAAAMMDDhhmmss.match(ts)
        if m:
            (A, M, D, h, m, s) = [int(x) for x in m.groups()]
            tupla = (A, M , D, h, m, s, 0,0,-1)
            self.TS = time.localtime(time.mktime(tupla))
        else:
            self.raises_format_error(ts)

    def setDMAHMS(self, dia, mes, anio, horas, minutos, segundos):
        tupla = (anio, mes, dia, horas, minutos, segundos, 0, 0, -1)
        self.TS = time.localtime(time.mktime(tupla))


    def parseTimestampDDMMAAAAhhmmss(self, ts):
        """
        Intenta interpretar una string que tenga
        el formato: DD-MM-AAAA hh:mm:ss

            >>> from Libreria.Comun import Fechas
            >>> ts = Fechas.TIMESTAMP()
            >>> ts.parseTimestampDDMMAAAAhhmmss('30-10-1976 12:23:45')
            >>> assert str(ts) == '30/10/1976 12:23:45'

        @raise ValueError: Si el parámetro suministrado no es correcto.
        """
        m = TIMESTAMP.patDDMMAAAAhhmmss.match(ts)
        if m:
            (D, M, A, h, m, s) = [int(x) for x in m.groups()]
            tupla = (A, M , D, h, m, s, 0,0,-1)
            self.TS = time.localtime(time.mktime(tupla))
        else:
            self.raises_format_error(ts)

    def parseTimestampDDMMAAhhmmss(self, ts):
        """
        Intenta interpretar una string que tenga
        el formato: DD-MM-AA hh:mm:ss

            >>> from Libreria.Comun import Fechas
            >>> ts = Fechas.TIMESTAMP()
            >>> ts.parseTimestampDDMMAAhhmmss('30-10-76 12:23:45')
            >>> assert ts == '30/10/1976 12:23:45'

        @raise ValueError: Si el parámetro suministrado no es correcto.
        """
        m = FECHA.patDDMMAAhhmmss.match(ts)
        if m:
            (D, M, A, h, m, s) = [int(x) for x in m.groups()]
            if 71 < A < 100:
                A += 1900
            elif 0 < A < 72:
                A += 2000
            tupla = (A, M , D, h, m, s, 0,0,-1)
            self.TS = time.localtime(time.mktime(tupla))
        else:
            self.raises_format_error(ts)

    def __str__(self):
        """
        Representación textual del timestamp, en formato
        B{dia/mes/año hora:minutos:segundos}.

        Ejemplo:

            >>> from Libreria.Comun.Fechas import TIMESTAMP
            >>> expected = '10/02/2004 13:36:25'
            >>> assert str(TIMESTAMP(1076420185.761)) == expected

        @return: Representación textual de la fecha
        @rtype: string
        """
        return '%02d/%02d/%04d %02d:%02d:%02d' % (self.TS[2], self.TS[1], self.TS[0],
            self.TS[3], self.TS[4], self.TS[5])

    def __repr__(self):
        return '{}("{}")'.format(self.__class__.__name__, self)

    def isoformat(self):
        return '%04d-%02d-%02dT%02d:%02d:%02d' % (self.TS[0], self.TS[1], self.TS[2],
            self.TS[3], self.TS[4], self.TS[5])

    def as_short_text(self, weekday=True):
        """Retorna una descripción textual de la fecha (sin hora).

        :param weekday: indica si incluir o no el nombre
                        del día de la semana (por defecto se incluye)
        :return: Una descripcion de texto de la fecha.
        :rtype: string
        """

        if weekday:
            d = datetime.date(self.year, self.month, self.day)
            fwd = FULLWEEKDAY[d.weekday()]
            return u"%s, %d de %s de %d" % (fwd, self.day, MESES[self.month], self.year)
        else:
            return u"%d de %s de %d" % (self.day, MESES[self.month], self.year)

    def AsFullText(self, weekday=True):
        """Retorna una descripción textual del timestamp.

        Ejemplo:

            >>> from Libreria.Comun import Fechas
            >>> f = Fechas.TIMESTAMP('26/9/2003 23:12:36')
            >>> assert f.AsFullText() == 'viernes, 26 de septiembre de 2003,' \
                                         ' a las 23:12:36'
            >>> assert f.AsFullText(weekday=False) == '26 de septiembre de' \
                                                      ' 2003, a las 23:12:36'

        @param weekday: indica si incluir o no el nombre
                        del día de la semana (por defecto se incluye)
        @return: Una descripcion de texto del timestamp.
        @rtype: string
        """
        anio = self.TS[0]
        mes = self.TS[1]
        dia = self.TS[2]
        hora = self.TS[3]
        minutos = self.TS[4]
        segundos  = self.TS[5]

        if weekday:
            fwd = FULLWEEKDAY[self.TS[6]]
            return "%s, %d de %s de %d, a las %02d:%02d:%02d" % (
                fwd
                , dia
                , MESES[mes]
                , anio
                , hora
                , minutos
                ,segundos
                )
        else:
            return "%d de %s de %d, a las %02d:%02d:%02d" % (
                dia
                , MESES[mes]
                , anio
                , hora
                , minutos
                ,segundos
                )
    as_full_text = AsFullText


    def AsFloat(self):
        return time.mktime(self.TS)

    def AsRFC822(self):
        """
        Representacion del timestamp, en el formato definido en el RFC822,
        usado, entre otras cosas, para las cabeceras de fechas en los
        mensajes de correo electrónico.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F1 = Fechas.TIMESTAMP('12/5/2005 23:22:12')
            >>> assert F1.AsRFC822() == 'Thu, 12 May 2005 23:22:12 +0000'

        @return: La fecha, en formato RFC822
        @rtype: string
        @see L{parseRFC822}
        """
        return time.strftime("%a, %d %b %Y %H:%M:%S +0000", self.TS)

    def AsRFC1123(self):
        """
        Representacion del timestamp, en el formato definido en el RFC1123

        @return: La fecha, en formato RFC1123
        @rtype: string
        @see L{parseRFC822}
        """
        return time.strftime("%a, %d %b %Y %H:%M:%S GMT", self.TS)

    def parseRFC822(self, text):
        """
        Analiza una string de texto, que debe estar en formato RFC822, y
        adopta los valores definidas en la misma.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.TIMESTAMP()
            >>> F.parseRFC822('Thu, 12 May 2005 23:22:12 +0000')
            >>> assert str(F) == '12/05/2005 23:22:12'

        @see: L{AsRFC822}
        @raise ValueError: Si el parámetro es incorrecto.
        """
        m = TIMESTAMP.patRFC822.match(text)
        if m:
            anio  = int(m.group(4))
            mes = TIMESTAMP.ReverseMonth[m.group(3)]
            dia = int(m.group(2))
            hora = int(m.group(5))
            minuto = int(m.group(6))
            segundo = int(m.group(7))
            tupla = (anio, mes , dia, hora, minuto, segundo, 0,0,-1)
            self.TS = time.localtime(time.mktime(tupla))
        else:
            self.raises_format_error(text)

    def AsODBC(self):
        """
        Retorna el timestamp en formato ODBC.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.TIMESTAMP('12/5/2005 23:22:12')
            >>> assert F.AsODBC() == "{ ts '2005-05-12 23:22:12' }"

        @see: L{AsOracle}
        @return: El timestmap como una string apta para base de datos ODBC.
        @rtype: string
        """
        return "{ ts '%s' }" % time.strftime('%Y-%m-%d %H:%M:%S', self.TS)

    def AsOracle(self):
        """Retorna el timestamp en formato apto para bases de datos ORACLE.

        @see: L{AsODBC}
        @return: El timestmap como una string apta para base de datos Oracle.
        @rtype: string
        """
        if self.TS == None:
            return 'NULL'
        else:
            return "to_date('{:04d}/{:02d}/{:02d} "  \
                   "{:02d}:{:02d}:{:02d}'"  \
                   ", 'YYYY/MM/DD HH24:MI:SS')".format(
                        self.TS[0], self.TS[1], self.TS[2],
                        self.TS[3], self.TS[4], self.TS[5]
                        )

    def getDia(self):
        """Obtener el día de la fecha, (Día del mes).
        """
        return self.TS[2]

    Dia = property(getDia)
    day = property(getDia)

    def getMes(self):
        """Obtener el mes de la fecha.
        """
        return self.TS[1]

    Mes = property(getMes)
    month = property(getMes)

    def getNombreMes(self):
        global MESES
        mes = self.TS[1]
        result = MESES[mes]
        return result

    def getNombreCortoMes(self):
        global MESES
        mes = self.TS[1]
        result = MESES[mes][0:3]
        return result

    def getAnio(self):
        """Obtener el año de la fecha.
        """
        return self.TS[0]

    Anio = property(getAnio)
    year = property(getAnio)

    def getDMA(self):
        """Devuelve una tupla con los valores de Dia, Mes, Año.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.TIMESTAMP('12/5/2005 23:22:12')
            >>> assert F.getDMA() == (12, 5, 2005)

        @see: L{getHMS}, L{getMDA}, L{getDMAHMS}
        @return: dia, mes y año.
        @rtype: tuple
        """
        return (self.TS[2], self.TS[1], self.TS[0])

    def getHora(self):
        """Obtener la hora.
        """
        return self.TS[3]

    Hora = hour = property(getHora)

    def getMinutos(self):
        """Obtener los minutos.
        """
        return self.TS[4]

    Minutos = minute = property(getMinutos)

    def getSegundos(self):
        """Obtener los segundos.
        """
        return self.TS[5]

    Segundos = second = property(getSegundos)

    def getHMS(self):
        '''
        Devuelve una tupla con los valores de Hora, minutos y segundos.

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.TIMESTAMP('12/5/2005 23:22:12')
            >>> assert F.getHMS() == (23, 22, 12)

        @see: L{getDMA}, L{getDMAHMS}
        @return: hora, minuto y segundo.
        @rtype: tuple
        '''
        return (self.TS[3], self.TS[4], self.TS[5])

    def getDMAHMS(self):
        """Devuelve una tupla con los valores de Dia, Mes, Año, Hora, Minutos
        y Segundos.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.TIMESTAMP('12/5/2005 23:22:12')
            >>> assert F.getDMAHMS() == (12, 5, 2005, 23, 22, 12)

        @see: L{getHMS}, L{getDMA}, L{getMDA}
        @return: dia, mes y año, hora, minutos y segundos.
        @rtype: tuple
        """
        return (self.TS[2], self.TS[1], self.TS[0]
            , self.TS[3], self.TS[4], self.TS[5])

    def getMDA(self):
        """Obtiene una tupla con los valores para el el mes, dia y año
        de una fecha (formato americano).

        Si se quiere obtener los mismos datos, pero en formato
        europeo (día, mes, año) se puede usar la function L{getMDA}.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.TIMESTAMP('12/5/2005 22:55:12')
            >>> assert F.getMDA() == (5, 12, 2005)

        @return: Una tupla con los valores (mes, dia, año)
        @rtype: tuple
        @see: L{getDMA}, L{getDMAHMS}, L{getHMS}
        """
        return (self.TS[1], self.TS[2], self.TS[0])


    def __cmp__(self, other):
        """Operador de Comparación. Permite comprobar si dos timestamps
        tienen el mismo valor o no.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F1 = Fechas.TIMESTAMP('12/5/2005 23:22:12')
            >>> F2 = Fechas.TIMESTAMP('13/6/2006 23:22:12')
            >>> assert not (F1 == F2)
            >>> assert F1 != F2
            >>> assert F2 > F1
            >>> assert F1 < F2

        @return: -1 si C{self} es menor que C{other}, 0 si C{self} es igual
            a C{other}, y 1 si C{self} es mayor que C{other}. Ver
            Basic Customization en
            http://www.parcan.es/doc/python/ref/customization.html
        @rtype: integer
        """
        if other == None or other == {} or other == '': return -1
        other = TIMESTAMP(other)
        if self.TS < other.TS:
            return -1
        elif self.TS > other.TS:
            return 1
        else:
            return 0

    def __add__(self, amount):
        """Operador de suma.

        Permite sumar dias a una instacia de tipo L{TIMESTAMP}

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.TIMESTAMP('26/6/2003 12:25:26')
            >>> F = F + 3
            >>> assert str(F) == '29/06/2003 12:25:26'
            >>> F = F + 2
            >>> assert str(F) == '01/07/2003 12:25:26'

        @rtype: L{TIMESTAMP}
        """
        return TIMESTAMP( self.AsFloat() + (amount * SECS_IN_DAY))

    def __sub__(self, amount):
        """Operador de resta.

        Permite restar días a una instancia de tipo L{TIMESTAMP}

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.TIMESTAMP('26/6/2003 12:25:25')
            >>> F = F - 3
            >>> assert str(F) == '23/06/2003 12:25:25'
            >>> F = F - 23
            >>> assert str(F) == '31/05/2003 12:25:25'

        @rtype: L{TIMESTAMP}
        """
        return TIMESTAMP( self.AsFloat() - (amount * SECS_IN_DAY))

Timestamp = TimeStamp = TIMESTAMP

def new_timestamp(year, month, day, hour=0, minute=0, second=0):
    '''Devuelve un Timestamp creado con los datos pasados como parámetros.

    :param year: Año
    :param year: mes
    :param year: día
    :param year: hora (opcional, valor por defecto 00)
    :param year: minutos (opcional, valor por defecto 00)
    :param year: segundos (opcional, valor por defecto 00)
    :return: El objeto Timestamp con los valores indicados
    :rtype: TIMESTAMP

    '''
    result = TIMESTAMP()
    result.setDMAHMS(day, month, year, hour, minute, second)
    return result


class FECHA(TIMEBASE):
    """Clase para trabajar con fechas.
    """
    def __init__(self, f=None):
        """Constructor.
        Permite crear una instacia de la clase L{FECHA} con los valores que
        deseemos. Pueden usarse distintos formas de suministrar los valores
        iniciales.

        En caso de querer la fecha de hoy, es más sencillo
        resurrir  a la función L{Hoy}

        @param f: Parámetro opcional. Si se omite, la instacia se creará pero
            sin valores asignados. Se puede utilizar para inicializar el
            número de segundos desde 1/1/1970 (tal y como la retorna la
            función L{time.time}), una string en formato dia/mes/año,
            como C{"1/1/1970"}, un objeto tipo C{dbiDate} como los que retornan
            las funciones ODBC u otro objeto de tipo L{FECHA}.

        @raise ValueError: Si el constructor no puede formar una L{FECHA}
            correcta a partir de los parámetros suministrados.
        """
        self.Dia = self.Mes = self.Anio = None
        if f:
            if type(f) == type(0.0):
                tupla = time.localtime(f)
                self.setDMA(tupla[2], tupla[1], tupla[0])
            elif type(f) == type(''):
                f = f.strip()
                if FECHA.patDMA.match(f):
                    self.parseDMA(f)
                elif FECHA.patAMD.match(f):
                    self.parseAMD(f)
                elif FECHA.patRFC822.match(f):
                    self.parseRFC822(f)
                elif FECHA.patAAAAMMDDhhmmss.match(f):
                    self.parseFechaAAAAMMDDhhmmss(f)
                elif FECHA.patDDMMAAAAhhmmss.match(f):
                    self.parseFechaDDMMAAAAhhmmss(f)
                elif FECHA.patFechaCompleta.match(f):
                    self.parseFechaCompleta(f)
                elif FECHA.patFechaHora.match(f):
                    self.parseFechaHora(f)
                elif FECHA.patWeekDayMonthDDhhmmssAAAA.match(f):
                    self.parseTimestampWeekDayMonthDDhhmmssAAAA(ts)
                else:
                    raise ValueError(
                        'El formato de fecha: {}'
                        ' es incorrecto'.format(f)
                        )
            elif type(f) == tuple:
                (self.Dia, self.Mes, self.Anio) = f
            elif isinstance(f, datetime.datetime) or isinstance(f, datetime.date):
                self.setDMA(f.day, f.month, f.year)
            elif isinstance(f, FECHA):
                (self.Dia, self.Mes, self.Anio) = (f.Dia, f.Mes, f.Anio)
            else:
                # vamos a intentar convertirlo a string y a ver si asi...
                f = str(f).strip()
                if FECHA.patDMA.match(f):
                    self.parseDMA(f)
                elif FECHA.patAMD.match(f):
                    self.parseAMD(f)
                elif FECHA.patRFC822.match(f):
                    self.parseRFC822(f)
                elif FECHA.patAAAAMMDDhhmmss.match(f):
                    self.parseFechaAAAAMMDDhhmmss(f)
                elif FECHA.patDDMMAAAAhhmmss.match(f):
                    self.parseFechaDDMMAAAAhhmmss(f)
                elif FECHA.patFechaCompleta.match(f):
                    self.parseFechaCompleta(f)
                elif FECHA.patFechaHora.match(f):
                    self.parseFechaHora(f)
                elif FECHA.patWeekDayMonthDDhhmmssAAAA.match(f):
                    self.parseTimestampWeekDayMonthDDhhmmssAAAA(f)
                else:
                    raise ErrorFormatoFecha(
                        'Formato de fecha incorrecto: "{}"'.format(escape(f))
                        )

    def weekday(self):
        f = datetime.date(self.year, self.month, self.day)
        return f.weekday()

    def today(self):
        return FECHA(time.time())
        
    def as_date(self):
        """Retorna la fecha como un objeto datetime.date.
        """
        return datetime.date(self.year, self.month, self.day)
    AsDate = as_date    
        
    def __hash__(self):
        return long('%04d%02d%02d' % (self.year, self.month, self.day))

    def __str__(self):
        """Representación textual.

        En caso de necesitar una representación mas explicita, se pueden usar
        los métodos L{AsShortText} y L{AsFullText}

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/1996')
            >>> assert str(F) == '12/5/1996'

        @rtype: string
        """
        if self.day and self.month and self.year:
            return '{:d}/{:d}/{:4d}'.format(self.day, self.month, self.year)
        else:
            return ''

    def __repr__(self):
        return "FECHA('{}')".format(str(self))

    def strftime(self,format):
        return self.as_date().strftime(format)
        
    def isoformat(self):
        return '%04d-%02d-%02d' % (self.year, self.month, self.day)




    def AsTodoLetra(self, weekday=True):
        """ Retorna el timestamp con todo en letras, incluido año

        :param weekday: indica si incluir o no el nombre
                        del día de la semana (por defecto se incluye)
        :return: Una descripcion de texto del timestamp.
        :rtype: string
        """

        if weekday:
            fwd = FULLWEEKDAY[self.getDiaSemana()]
            return "%s, %s de %s de %s" % (
                fwd,
                numerals.numeral(self.Dia),
                MESES[self.Mes],
                numerals.numeral(self.Anio)
                )
        else:
            return "%s de %s de %s" % (
                numerals.numeral(dia),
                MESES[self.Mes],
                numerals.numeral(self.Anio),
                )

    def AsRFC822(self):
        """
        Representacion de la Fecha, en el formato definido en el RFC822.
        Usado, entre otras cosas, para las cabeceras de fechas en los
        mensajes de correo electrónico. Como no tenemos datos para la
        hora, se supondrán las 12:00:00

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> assert F.AsRFC822() == 'Thu, 12 May 2005 12:00:00 +0000'

        @return: La fecha, en formato RFC822
        @rtype: string
        @see: L{parseRFC822}
        """
        tupla = (self.Anio, self.Mes, self.Dia, 12, 0, 0, 0, 0, -1)
        tupla = time.localtime(time.mktime(tupla))
        return time.strftime("%a, %d %b %Y %H:%M:%S +0000", tupla)

    def parseTimestampWeekDayMonthDDhhmmssAAAA(self, ts):
        """
        Intenta interpretar una string que tenga
        el formato: Fri Mar 19 10:33:29 2010

        @raise ValueError: Si el parámetro suministrado no es correcto.
        """
        m = FECHA.patWeekDayMonthDDhhmmssAAAA.match(ts)
        if m:
            (NombreMes, Dia, Hora, Minutos, Segundos, Anio) = m.groups()
            Mes = self.ReverseMonth.get(NombreMes)
            Dia = int(Dia)
            Anio = int(Anio)
            self.setDMA(Dia, Mes, Anio)
        else:
            raise ValueError(
                'No entiendo el formato de fecha: {}'.format(ts)
                )
    
    def parseRFC822(self, text):
        """
        Analiza una string de texto, que debe estar en formato RFC822, y
        adopta los valores definidas en la misma.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA()
            >>> F.parseRFC822('Thu, 12 May 2005 23:22:12 +0000')
            >>> assert str(F) == '12/5/2005'

        see: L{AsRFC822}
        @raise ValueError: si el formato es incorrecto.
        """
        m = FECHA.patRFC822.match(text)
        if m:
            anio  = int(m.group(4))
            mes = FECHA.ReverseMonth[m.group(3)]
            dia = int(m.group(2))
            self.setDMA(dia, mes, anio)
        else:
            raise ValueError(
                'El formato de fecha: {}'
                ' es incorrecto'.format(f)
                )

    def parseAMD(self, s):
        """
        Intenta interpretar cualquier string que forme una secuencia
        del tipo año mes y dia, y asimila  sus valores. Se puede usar
        como separador cualquier caracter del conjunto:

            - [C{/}, C{-}]

        Año, mes y dia tienen que venir en formato numérico.

        @raise ValueError: Si el parámetro suministrado no es correcto.
        """
        m = FECHA.patAMD.match(s.strip())
        if m:
            (anio, mes, dia) = (int(x) for x in m.groups())
            self.setDMA(dia, mes, anio)
        else:
            raise ValueError(
                'El formato de fecha: {}'
                ' es incorrecto'.format(f)
                )


    def parseDMA(self, f):
        """
        Intenta interpretar cualquier string que forme una secuencia
        del tipo dia, mes año, y asimila  sus valores. Se puede usar
        como separador cualquier caracter del conjunto:

            - [C{/}, C{\\}, C{:}, C{.}, C{-}]

        Se puede indicar el mes como  un número, como un nombre en
        español o ingles, o como abreviatura (tres letras) del nombre
        en ingles o español.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA()
            >>> F.parseDMA('1-2-2001')
            >>> assert F.day == 1
            >>> assert F.month == 2
            >>> assert F.year == 2001
            >>> f2 = Fechas.FECHA()
            >>> f2.parseDMA('1.Jan.2001')
            >>> assert f2.day == 1
            >>> assert f2.month == 1
            >>> assert f2.year == 2001
            >>> f3 = Fechas.FECHA()
            >>> f3.parseDMA('12/marzo/2001')
            >>> assert str(f3) == '12/3/2001'

        @raise ValueError: Si el parámetro suministrado no es correcto.
        """
        m = FECHA.patDMA.match(f.strip())
        if m:
            (d, m ,a) = m.groups()
            d = int(d)
            m = int(FECHA.ReverseMonth.get(m.capitalize(), m))
            a = int(a)
            self.setDMA(d,m,a)
        else:
            self.raises_format_error(f)

    def parseFechaHora(self, s):
        """
        Intenta interpretar una string que tenga el formato:
        AAAA-MM-DD hh:mm:ss.ss, como por ejemplo
        1976-04-12 14:23:22.63

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA()
            >>> F.parseFechaHora('1976-04-12 14:23:22.63')
            >>> assert str(F) == '12/4/1976'

        @raise ValueError: Si el parámetro suministrado no es correcto.
        """
        m = FECHA.patFechaHora.match(s)
        (a, m, d) = m.groups()
        d = int(d)
        m = int(m)
        a = int(a)
        self.setDMA(d,m,a)

    def parseFechaCompleta(self, f):
        """
        Intenta interpretar una string que tenga el formato:
        B{I{dia} de I{mes} de I{anio}}, como por ejemplo
        12 de abril de 1976.

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA()
            >>> F.parseFechaCompleta('12 de abril de 1976')
            >>> assert str(F) == '12/4/1976'

        @raise ValueError: Si el parámetro suministrado no es correcto.
        """
        m = FECHA.patFechaCompleta.match(f)
        (d, m, a) = m.groups()
        d = int(d)
        m = FECHA.ReverseMonth[m.capitalize()]
        a = int(a)
        self.setDMA(d,m,a)

    def parseFechaAAAAMMDDhhmmss(self, f):
        """
        Intenta interpretar una string que tenga
        el formato: AAAA-MM-DD hh:mm:ss

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA()
            >>> F.parseFechaAAAAMMDDhhmmss('1976-06-30 12:23:41')
            >>> assert str(F) == '30/6/1976'

        @raise ValueError: Si el parámetro suministrado no es correcto.
        """
        m = FECHA.patAAAAMMDDhhmmss.match(f)
        (A, M, D, h, m, s) = [int(x) for x in m.groups()]
        self.setDMA(D, M, A)

    def parseFechaDDMMAAAAhhmmss(self, f):
        """
        Intenta interpretar una string que tenga
        el formato: DD-MM-AAAA hh:mm:ss

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA()
            >>> F.parseFechaDDMMAAAAhhmmss('30-10-1976 12:23:41')
            >>> assert str(F) == '30/10/1976'

        @raise ValueError: Si el parámetro suministrado no es correcto.
        """
        m = FECHA.patDDMMAAAAhhmmss.match(f)
        (D, M, A, h, m, s) = [int(x) for x in m.groups()]
        self.setDMA(D, M, A)


    def validate(self):
        """Validación de los valores permitidos.

        @todo: La validacion es muy pobre, solo comprueba que los días
        esten en el rango [1..31] y los meses en [1..12].

        @raise ValueError: En caso de que no cuadren los valores
        """
        if self.Dia != None:
            if (self.Dia < 1) or (self.Dia > 31):
                msg = 'El valor del dia [%s] no es correcto' % self.Dia
                raise ValueError(msg)
        if self.Mes != None:
            if (self.Mes <1) or (self.Mes > 12):
                msg = 'El valor del mes [%s] no es correcto' % self.Mes
                raise ValueError(msg)
        if self.Dia and self.Mes and self.Anio:
            (self.Dia, self.Mes, self.Anio) = checkDMA(self.Dia, self.Mes, self.Anio)

    Validate = validate # PEP8

    def setDMA(self, d,m,a):
        """Define los valores de la fecha.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA()
            >>> F.setDMA(12, 4, 1976)
            >>> assert str(F) == '12/4/1976'

        @param d: dia
        @param m: mes
        @param a: año
        @Note: Realiza una validación (llama a L{Validate}), por lo que
        puede elevar una excepción ValueError
        @raise ValueError: Alguno de los datos de la fecha no son correctos.
        """
        (self.Dia, self.Mes, self.Anio) = (d, m, a)
        self.Validate()

    def setDia(self, dia):
        '''Establece el dia de la fecha.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/4/1976')
            >>> assert str(F) == '12/4/1976'
            >>> F.setDia(14)
            >>> assert str(F) == '14/4/1976'

        @Note: Realiza una validación (llama a L{Validate}), por lo que
        puede elevar una excepción ValueError
        @raise ValueError: El valor del dia no es correcto.
        '''
        self.Dia = dia
        self.Validate()

    def getDia(self):
        """Obtener el día de la fecha, (Día del mes).

        En caso de necesitar el día de la semana, vease
        el método L{getDiaSemana}

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> assert F.getDia() == 12

        @return: El dia del mes, en el rango [1..31]
        @rtype: integer
        """
        return self.Dia

    day = property(getDia, setDia)

    def setMes(self, mes):
        '''Establece el dia de la fecha.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/4/1976')
            >>> F.setMes(1)
            >>> assert str(F) == '12/1/1976'

        @Note: Realiza una validación (llama a L{Validate}), por lo que
        puede elevar una excepción ValueError
        @raise ValueError: El valor del dia no es correcto.
        '''
        self.Mes = mes
        self.Validate()

    def getMes(self):
        """Obtener el mes de la fecha.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> assert F.getMes() == 5

        @return: El mes, en el rango [1..12]
        @rtype: integer
        """
        return self.Mes

    month = property(getMes, setMes)

    def getNombreMes(self):
        """Obtener el nombre del mes.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> assert F.getNombreMes() == 'mayo'

        @return: Una string con el nombre del mes, en español.
        @rtype: string
        """
        global MESES
        return MESES[self.Mes]

    def getNombreCortoMes(self):
        global MESES
        result = MESES[self.Mes][0:3]
        return result


    NombreMes = property (getNombreMes)

    def getDiasHabiles(self):
        import calendar
        count = 0
        for w in calendar.monthcalendar(self.Anio, self.Mes):
             count += reduce(lambda x,y: x+y, map(lambda x: x>0, w[0:5]))
        return count

    def getDiaSemana(self):
        """Retorna el dia de la semana.


        En caso de necesitar el día del mes, vease
        el método L{getDia}

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('19/9/2003')
            >>> assert F.getDiaSemana() == 4

        @return: un número indicando el día de la semana, en
                 el rango [0-6] (0=Lunes)
        @rtype: integer
        """
        t = time.mktime((self.Anio, self.Mes, self.Dia, 12, 0, 0, 0, 0, -1))
        return time.localtime(t)[6]

    DiaSemana = property (getDiaSemana)

    def setAnio(self, anio):
        self.Anio = anio
        self.validate()

    def getAnio(self):
        """Retorna el año de la fecha.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> assert F.getAnio() == 2005

        @return: El año, como un entero.
        @rtype: integer
        """
        return self.Anio

    year = property(getAnio, setAnio)

    def getDMA(self):
        """Obtiene una tupla con los valores para el día, mes y año de una
        fecha, en ese orden (formato europeo).

        Si se quiere obtener los mismos datos, pero en formato
        americano (mes, dia, año) se puede usar la function L{getMDA}.
        Si se quiere una tupla en orden Año, mes, Dia véase
        L{getAMD}.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> assert F.getDMA() == (12, 5, 2005)

        @return: Una tupla con los valores (dia, mes, año)
        @rtype: tuple
        @see: L{getMDA}, L{getAMD}
        """
        return (self.Dia, self.Mes, self.Anio)

    def getMDA(self):
        """Obtiene una tupla con los valores para el mes, día y año
        de una fecha (formato americano).

        Si se quiere obtener los mismos datos, pero en formato
        europeo (día, mes, año) se puede usar la function L{getDMA}.
        Si se quiere una tupla en orden Año, mes, Dia véase
        L{getAMD}.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> assert F.getMDA() == (5, 12, 2005)

        @return: Una tupla con los valores (mes, dia, año)
        @rtype: tuple
        @see: L{getDMA}, L{getAMD}
        """
        return (self.Mes, self.Dia, self.Anio)

    def getAMD(self):
        """Obtiene una tupla con los valores para el año, mes y día
        de una fecha.

        Si se quiere obtener los mismos datos, pero en formato
        europeo (día, mes, año) se puede usar la function L{getDMA}.
        Para obtener los datos en formato americano (mes, dia, año),
        vease L{getMDA}.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> assert F.getAMD() == (2005, 5, 12)

        @return: Una tupla con los valores (año, mes, dia)
        @rtype: tuple
        @see: L{getDMA}, L{getMDA}
        """
        return (self.Anio, self.Mes, self.Dia)

    def first_day(self):
        """Devuelve una nueva fecha apuntado al primer día del año.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> F = F.first_day()
            >>> assert str(F) == '1/1/2005'
            >>>

        @see: L{last_day}
        """
        return FECHA((1, 1, self.year))

    def last_day(self):
        """Devuelve una nueva fecha apuntado al último día del año.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> F = F.last_day()
            >>> assert str(F) == '31/12/2005'
            >>>

        @see: L{first_day}
        """
        return FECHA((31, 12, self.year))

    def lastDayofMonth(self):
        '''
        Retorna el último día del mes de la fecha.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> assert F.lastDayofMonth() == 31

        @return: Ultimo día del mes actual (tiene en cuenta los años bisiestos)
        @rtype: integer
        '''
        (wd, result) = calendar.monthrange(self.Anio, self.Mes)
        return result

    def nextMonth(self):
        '''Retorna una nueva instancia de L{FECHA} con un valor igual a
        la fecha de referencia más un mes. En caso de estar en diciembre,
        avanzará a enero del siguiente año. Intentará situarnos en
        el mismo dia del que partiamos, pero si no fuera posible
        nos situara en el más cercano. Por ejemplo, si estamos en el
        31 de enero de 2005, al pedirle que avanze un mes nos retornará
        el 28 de febrero de 2005.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('31/1/2005')
            >>> F = F.nextMonth()
            >>> assert str(F) == '28/2/2005'
            >>> F = F.nextMonth()
            >>> assert str(F) == '28/3/2005'

        @see: L{prevMonth}
        @return: Un objeto tipo L{FECHA}, avanzando un mes
        @rtype: L{FECHA}
        '''
        (d, m, a) = (self.Dia, self.Mes, self.Anio)
        m += 1
        if m > 12:
            a += 1
            m = 1
        (wd, result) = calendar.monthrange(a, m)
        if d > result:
            d == result
        result = FECHA()
        result.setDMA(d, m, a)
        return result

    def prevMonth(self):
        '''Retorna una nueva instancia de L{FECHA} con un valor igual a
        la fecha de referencia menos un mes. En caso de estar en enero,
        retrocederá a diciembre del año anterior. Intentará situarnos en
        el mismo dia del que partiamos, pero si no fuera posible
        nos situara en el más cercano. Por ejemplo, si estamos en el
        31 de marzo de 2005, al pedirle que retroceda un mes nos retornará
        el 28 de febrero de 2005.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('31/3/2005')
            >>> F = F.prevMonth()
            >>> assert str(F) == '28/2/2005'
            >>> F = F.prevMonth()
            >>> assert str(F) == '28/1/2005'
            >>> F = F.prevMonth()
            >>> assert str(F) == '28/12/2004'


        @see: L{nextMonth}
        @return: Un objeto tipo L{FECHA}, retrocedido un mes
        @rtype: L{FECHA}
        '''

        (d, m, a) = (self.Dia, self.Mes, self.Anio)
        m -= 1
        if m < 1:
            a -= 1
            m = 12
        (wd, result) = calendar.monthrange(a, m)
        if d > result:
            d == result
        result = FECHA()
        result.setDMA(d, m, a)
        return result

    def AsOracle(self):
        """Formato apto para bases de datos ORACLE.

        Retorna una string que describe la fecha en un formato apto para
        bases de datos de tipo Oracle.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> assert F.AsOracle() == "to_date('12/05/2005','DD/MM/YYYY')"

        @see: L{AsODBC}, L{AsFloat}
        @return: Una string que describe la fecha en ORACLE.
        @rtype: string
        """
        if self.Dia and self.Mes and self.Anio:
            return "to_date('%02d/%02d/%04d','DD/MM/YYYY')" % (self.Dia, self.Mes, self.Anio)
        else:
            return 'NULL'

    def AsODBC(self):
        """Formato apto para bases de datos ODBC.

        Retorna una string que describe la fecha en un formato apto para
        bases de datos de tipo ODBC.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> assert F.AsODBC() == "{ d '2005-05-12' }"

        @see: L{AsOracle}, L{AsFloat}
        @return: Una string que describe la fecha en ODBC.
        @rtype: string
        """
        if self.Dia and self.Mes and self.Anio:
            return "{ d '%04d-%02d-%02d' }" % (self.Anio, self.Mes, self.Dia)
        else:
            return 'NULL'

    def AsFloat(self):
        """Obtener la fecha en formato Float

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('12/5/2005')
            >>> assert F.AsFloat() == 1115895600.0

        @return: La fecha, como el número de segundos
                 transcurridos desde el 1/1/197O.
        @rtype: float
        """
        tupla = (self.Anio, self.Mes, self.Dia,12,0,0,0,0,-1)
        return time.mktime(tupla)

    def AsShortText(self):
        """Retorna una descripción textual de la fecha.

        Ejemplo:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('26/9/2003')
            >>> assert F.AsShortText() == '26/sep/2003'

        @return: Una descripcion de texto de la fecha, en el
                 formato C{dia/nombre del mes/año}, por
                 ejemplo C{12/enero/2005}
        @rtype: string
        """
        global MESES
        return "%d/%s/%d" % (self.Dia, MESES[self.Mes][0:3], self.Anio)
    as_short_text = AsShortText # PEP9


    def AsFullText(self, weekday=True):
        """Retorna una descripción textual de la fecha.

        Ejemplo:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('26/9/2003')
            >>> assert F.AsFullText() == 'viernes, 26 de septiembre de 2003'
            >>> assert F.AsFullText(weekday=False) == '26 de septiembre de 2003'

        @param weekday: indica si incluir o no el nombre
                        del día de la semana (por defecto se incluye)
        @return: Una descripcion de texto de la fecha.
        @rtype: string
        """

        if weekday:
            d = datetime.date(self.year, self.month, self.day)
            fwd = FULLWEEKDAY[d.weekday()]
            return "%s, %d de %s de %d" % (fwd, self.day, MESES[self.month], self.year)
        else:
            return "%d de %s de %d" % (self.day, MESES[self.month], self.year)
    as_full_text = AsFullText #PEP8

    def __add__(self, amount):
        """Operador de suma.

        Permite sumar dias a una instacia de tipo L{FECHA}

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('26/6/2003')
            >>> F = F + 3
            >>> assert str(F) == '29/6/2003'
            >>> F = F + 2
            >>> assert str(F) == '1/7/2003'

        @rtype: L{FECHA}
        """
        amount = amount * SECS_IN_DAY
        actual = self.AsFloat() + amount
        result = FECHA(actual)
        return result

    def __sub__(self, amount):
        """Operador de resta.

        Permite restar dias a una instacia de tipo L{FECHA}

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F = Fechas.FECHA('26/6/2003')
            >>> F = F - 3
            >>> assert str(F) == '23/6/2003'
            >>> F = F - 23
            >>> assert str(F) == '31/5/2003'

        @rtype: L{FECHA}
        """
        amount = amount * SECS_IN_DAY
        actual = self.AsFloat() - amount
        result = FECHA(actual)
        return result

    def __cmp__(self, other):
        """Operador de comparación.
        Permite comparar Fechas

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> F1 = Fechas.FECHA('12/5/2005')
            >>> F2 = Fechas.FECHA('13/6/2006')
            >>> assert F2 > F1
            >>> assert not F2 < F1

        @return: un valor negativo si C{self} es menor que C{other}, 0 si
            C{self} es igual a C{other}, y un valor positivo si C{self} es
            mayor que  C{other}. Ver
            U{Basic Customization<http://www.parcan.es/doc/python/ref/customization.html>}.
        @rtype: integer
        """
        if other == None or other == {}: return -1
        if type(other) == type(self) or isinstance(other, datetime.date):
            result = 0
            result += (self.year - other.year) * 512
            result += (self.month - other.month) * 32
            result += (self.day - other.day)
            return result
        else:
            return -1

    def __eq__(self, other):
        """Operador de comparación.

        determina si una fecha es equivalente a otra dada o
        a un objeto detatime.date.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> import datetime
            >>> F1 = Fechas.FECHA('12/5/2005')
            >>> F2 = Fechas.FECHA('12/5/2005')
            >>> assert F1 == F2
            >>> F3 = datetime.date(2005, 5, 12)
            >>> assert F1 == F3

        @return: un valor negativo si C{self} es menor que C{other}, 0 si
            C{self} es igual a C{other}, y un valor positivo si C{self} es
            mayor que  C{other}. Ver
            U{Basic Customization<http://www.parcan.es/doc/python/ref/customization.html>}.
        @rtype: integer
        """
        if type(other) == type(self) or isinstance(other, datetime.date):
            result = bool(
                self.year == other.year
                and self.month == other.month
                and self.day == other.day
                )
            return result
        else:
            return False

    def __ne__(self, other):
        """Operador de comparación.

        determina si una fecha NO es equivalente a otra dada o
        a un objeto detatime.date.

        Ejemplo de uso:

            >>> from Libreria.Comun import Fechas
            >>> import datetime
            >>> F1 = Fechas.FECHA('12/5/2005')
            >>> F2 = Fechas.FECHA('12/5/2005')
            >>> assert (F1 == F2)
            >>> F3 = datetime.date(2005, 5, 12)
            >>> assert F1 == F3

        @return: un valor negativo si C{self} es menor que C{other}, 0 si
            C{self} es igual a C{other}, y un valor positivo si C{self} es
            mayor que  C{other}. Ver
            U{Basic Customization<http://www.parcan.es/doc/python/ref/customization.html>}.
        @rtype: integer
        """
        if type(other) == type(self) or isinstance(other, datetime.date):
            result = bool(
                self.year != other.year
                or self.month != other.month
                or self.day != other.day
                )
            return result
        else:
            return True

    def as_timestamp(self, hh=0, mm=0, ss=0):
        s = '%02d/%02d/%04d %02d:%02d:%02d' % (
            self.day, self.month, self.year,
            hh, mm, ss
            )
        return TIMESTAMP(s)
Fecha = FECHA

def new_fecha(year, month, day):
    result = FECHA()
    result.setDMA(day, month, year)
    return result


class TimeDelta(object):
    """Imprimir la diferencia entre timestamp con un formato agradable.
    """
    UN_ANIO = 31536000.0            # 60 secs * 60 min * 24 hour * 365 days
    UN_MES = 2628000.0              # Segundos en un mes (media) UN_ANIO / 12
    UN_DIA = 86400.0                # 60 secs * 60 min * 24 hour
    UNA_HORA = 3600                 # 60 secs * 60 min
    UN_MINUTO = 60                  # 60 secs
    HORAS_EN_UN_DIA    = 24
    MINUTOS_EN_UN_DIA  = 1440       # 24 horas * 60 minutos
    SEGUNDOS_EN_UN_DIA = 86400      # 60 secs * 60 min * 24 hour

    def __init__(self, ts, referencia=None):
        """Constructor.

        @param ts: Timestamp a considerar.
        @param referencia: Timestamp de referencia. Si no se indica
                           nada, la hora actual
        """
        if isinstance(ts, (TIMESTAMP, FECHA)):
            ts = ts.AsFloat()
        if isinstance(ts, datetime.datetime) or isinstance(ts, datetime.date):
            ts = time.mktime( ts.timetuple() )
        else:
            ts = float(ts)
        if not referencia:
            referencia = time.time()
        else:
            if isinstance(referencia, TIMESTAMP):
                referencia = referencia.AsFloat()

        self.Delta = referencia - ts


    def AsDiferencia(self):
        '''Retorna una descripción textual del intervalo.
        '''
        Buff = []
        Delta = abs(int(self.Delta))
        if Delta > TSDELTA.SEGUNDOS_EN_UN_DIA:
            Dias = Delta / TSDELTA.SEGUNDOS_EN_UN_DIA
            if Dias == 1:
                Buff.append('un día')
            else:
                Buff.append('%d días' % Dias)
        Resto = Delta % TSDELTA.SEGUNDOS_EN_UN_DIA
        Horas = Resto / 3600
        Minutos = Resto  % (24*60) % 60
        Segundos = Resto % 60
        Buff.append('%02d:%02d:%02d' % (Horas, Minutos, Segundos))
        return ', '.join(Buff)

    def AsFuturo(self):
        '''Retorna una representación textual aproximada del intervalo
        de tiempo, considerado hacia el futuro.

        @return: Descripción del intervalo de tiempo
        @rtype: string
        @see: L{AsPasado}
        '''
        if self.Delta < TSDELTA.UN_DIA/2:
            Dias = round(self.Delta // TSDELTA.UN_DIA)
            if Dias == -1:
                return 'mañana'
            if Dias == -2:
                return 'pasado mañana'
            else:
                return 'dentro de %d días' % -Dias

        if self.Delta <= TSDELTA.UNA_HORA:
            Horas = round(self.Delta // TSDELTA.UNA_HORA)
            if Horas == 1:
                return 'dentro de una hora'
            else:
                return 'dentro de %d horas' % -Horas

        if self.Delta < TSDELTA.UN_MINUTO:
            Minutos = round(self.Delta // TSDELTA.UN_MINUTO)
            if Minutos == 1: return 'dentro de un minuto'
            if Minutos < 6: return 'dentro de %d minutos' % -Minutos
            if Minutos < 20: return 'dentro de un cuarto de hora'
            if Minutos < 40: return 'dentro de media hora'
            return 'tres cuartos de hora'

        if self.Delta < TSDELTA.UN_MES/2:
            Meses = round(self.Delta // TSDELTA.UN_MES)
            if Meses == -11:
                return 'dentro de un mes'
            else:
                return 'dentro de %d meses' % -Meses

        if self.Delta < TSDELTA.UN_ANIO // 2:
            Anios = round(self.Delta // TSDELTA.UN_ANIO)
            if Anios == -1:
                return 'dentro de un año'
            else:
                return 'dentro de %d años' % -Anios
        return ''

    as_futuro = AsFuturo  # retrocompatibilidad

    def AsPasado(self):
        '''Retorna una representación textual aproximada del intervalo
        de tiempo, considerado desde el pasado.

        @return: Descripción del intervalo de tiempo
        @rtype: string
        @see: L{AsFuturo}
        '''
        if self.Delta > TSDELTA.UN_ANIO / 2:
            Anios = round(self.Delta / TSDELTA.UN_ANIO)
            if Anios == 1:
                return 'un año'
            else:
                return '%d años' % Anios

        if self.Delta > TSDELTA.UN_MES/2:
            Meses = round(self.Delta / TSDELTA.UN_MES)
            if Meses == 1:
                return 'un mes'
            else:
                return '%d meses' % Meses

        if self.Delta > TSDELTA.UN_DIA/2:
            Dias = round(self.Delta / TSDELTA.UN_DIA)
            if Dias == 1:
                return 'un día'
            else:
                return '%d días' % Dias

        if self.Delta >= TSDELTA.UNA_HORA:
            Horas = round(self.Delta / TSDELTA.UNA_HORA)
            if Horas == 1:
                return 'una hora'
            else:
                return '%d horas' % Horas

        if self.Delta > TSDELTA.UN_MINUTO:
            Minutos = round(self.Delta / TSDELTA.UN_MINUTO)
            if Minutos == 1: return 'un minuto'
            if Minutos < 6: return '%d minutos' % Minutos
            if Minutos < 20: return 'un cuarto de hora'
            if Minutos < 40: return 'media hora'
            return 'tres cuartos de hora'

        return 'un momento'

    as_pasado = AsPasado  ## Por retrocompatibilidad

    def __str__(self):
        """Repersentacion textual aproximada del intervalo
        de tiempo, considerado desde el pasado si es positivo
        y hacia el futuro si es negativo.

        @return: Descripción del intervalo de tiempo
        @rtype: string
        @see: L{AsFuturo}, L{AsPasado}
        """
        if self.Delta < 0:
            return self.AsFuturo()
        else:
            return self.AsPasado()

TSDELTA = TimeDelta  # Por retrocompatibilidad

def LinkTo(url, text):
    """Genera un hiperenlace.

    Esta función se define aquí para no tener que importar
    L{Libreria.Base.Html.Href}.

    Ejemplo de uso:

        >>> from Libreria.Comun import Fechas
        >>> assert Fechas.LinkTo('http://www.python.org/', 'Python') == \
                '<a target="_top" href="http://www.python.org/">' \
                '<font size="-1">Python</font></a>'

    @param url: URL de destino
    @param text: texto a incluir dentro del enlace
    @return: texto HTML con el enlace.
    @rtype: string
    """
    return '<a target="_top" href="%s"><font size="-1">%s</font></a>' % (url, text)

def PrevMonth(anio, mes, script='Calendario.py'):
    """Hiperenlace para Retroceder un mes.

    @param anio: Año
    @param mes: Mes
    @param script: Script al que se debe enlazar
    @return: Un hiperenlace al mes anterior.
    @rtype: string
    @todo: revisarlo para usar el L{FECHA.prevMonth}
    """
    mes = mes - 1
    if mes < 1:
        anio = anio - 1
        mes = 12
    url = '%s?FECHA=01/%02d/%04d' % (script, mes, anio)
    return LinkTo(url, '<')

def NextMonth(anio, mes, script='Calendario.py'):
    """Hiperenlace para avanzar un mes.

    @param anio: Año
    @param mes: Mes
    @param script: Script al que se debe enlazar
    @return: Un hiperenlace al mes siguiente.
    @rtype: string
    @todo: revisarlo para usar el L{FECHA.nextMonth}
    """
    mes = mes + 1
    if mes > 12:
        anio = anio + 1
        mes = 1
    url = '%s?FECHA=01/%02d/%04d' % (script, mes, anio)
    return LinkTo(url, '>')

class CALENDARIO_BASE:
    """Clase base para trabajar con calendarios

    Especialmente, para representarlos en HTML.
    """
    def __init__(self, fecha, script='Fechas.py'):
        """Constructor

        @param fecha: Fecha a partir de la cual se creará el calendario
        @param script: URL del script que se enlazará cada dia válido del calendario
        """
        self.Fecha = fecha
        self.BACKGROUND = Colores.Indigo
        self.HEADER_BACKGROUD = Colores.Navy
        self.HEADER_FOREGROUND = Colores.White
        self.OTHER_DAY_BACKGROUND = Colores.White
        self.CURRENT_DAY_BACKGROUND = Colores.Siena
        self.Script = script

    def _PrintDay(self, dia, mes, anio, dia_concreto):
        """Imprime el día del calendario.
        """
        if dia_concreto == 0:
            return '<td bgcolor="%s">&nbsp;</td>' % self.BACKGROUND
        F = FECHA()
        F.setDMA(dia_concreto,mes,anio)
        link = '%s?FECHA=%s' % (self.Script, F)
        A = LinkTo(link, dia_concreto)
        if dia_concreto != dia:
            color = self.OTHER_DAY_BACKGROUND
        else:
            color = self.CURRENT_DAY_BACKGROUND
        return '<td bgcolor="%s">%s</td>' % (color, A)

class CALENDARIO(CALENDARIO_BASE):
    """Clase para trabajar con calendarios convencionales
    de tipo cuadrado.

    Especialmente, para representarlos en HTML.
    """

    def __str__(self):
        """Representación textual del calendario.
        """
        (dia, mes, anio) = self.Fecha.getDMA()
        buff = ['<table align=center bgcolor="%s" border=0>' % self.BACKGROUND]
        buff.append('<tr>')
        buff.append('<th width=16>{}</th>'.format(
            PrevMonth(anio,mes, self.Script)
            ))
        buff.append('<th width=80 colspan=5 align="CENTER">{}/{}</th>'.format(
            MESES[mes][0:3], anio
            ))
        buff.append('<th width=16>{}</th>'.format(
            NextMonth(anio, mes, self.Script)
            ))
        buff.append('</tr>')
        buff.append('<tr>')
        for wd in WEEKDAY:
            buff.append('<th width=16 bgcolor="{}">'.format(
                self.HEADER_BACKGROUD
                ))
            buff.append('<font color="{}" size=-2>'.format(
                self.HEADER_FOREGROUND
                ))
            buff.append(str(wd))
            buff.append('</font></th>')
            buff.append('')
        buff.append('</tr>')
        for week in calendar.monthcalendar(anio, mes):
            buff.append('<tr>')
            for day in week:
                buff.append(self._PrintDay(dia,mes,anio, day))
            buff.append('</tr>')
        buff.append('</table>')
        return '\n'.join(buff)

class CALENDARIO_HORIZONTAL(CALENDARIO_BASE):
    """Clase para trabajar con calendarios horizontales

    Especialmente, para representarlos en HTML.
    """

    def _PrintDay(self, dia, mes, anio, dia_concreto):
        """Imprime el día del calendario.
        """
        F = FECHA()
        F.setDMA(dia_concreto,mes,anio)
        link = '%s?FECHA=%s' % (self.Script, F)
        A = LinkTo(link, dia_concreto)
        if dia_concreto != dia:
            klass = 'CALENDARIO DIA'
        else:
            klass = 'CALENDARIO HOY'
        return '\t<td class="%s">%s</td>\n' % (klass, A)

    def __str__(self):
        """Representación textual del calendario.
        """
        (dia, mes, anio) = self.Fecha.getDMA()
        Lineas = ['<table class="CALENDARIO" align=center border="0">\n']
        Lineas.append('<caption class="CALENDARIO">\n')
        Lineas.append(PrevMonth(anio,mes, self.Script))
        Lineas.append('%s / %s' % (MESES[mes], anio))
        Lineas.append(NextMonth(anio,mes, self.Script))
        Lineas.append('</caption>\n')
        SEMANA = ['\t<th class="LABORAL">L</th>\n'
            , '\t<th class="LABORAL">M</th>\n'
            , '\t<th class="LABORAL">X</th>\n'
            , '\t<th class="LABORAL">J</th>\n'
            , '\t<th class="LABORAL">V</th>\n'
            , '\t<th class="FESTIVO">S</th>\n'
            , '\t<th class="FESTIVO">D</th>\n'
            ]
        index = 0
        firstRow = []
        secondRow = []
        for week in calendar.monthcalendar(anio, mes):
            for day in week:
                index = (index + 1) % 7
                if day == 0:
                    continue
                firstRow.append(SEMANA[index])
                secondRow.append(self._PrintDay(dia,mes,anio, day))
        Lineas.append('<tr>%s</tr>' % ''.join(firstRow))
        Lineas.append('<tr>%s</tr>' % ''.join(secondRow))
        Lineas.append('</table>')
        return '\n'.join(Lineas)


def checkDMA(d,m,a):
    '''
    Comprueba la consistencia de los datos suministrados, para comprobar
    que forman una fecha coherente. Por ejemplo, no acepta el 29 de febrero
    a no ser que el año sea bisisesto.

    No genera error ni excepción, sino que silenciosamente arregla el error.

    Ejemplo de uso:

    >>> from Libreria.Comun import Fechas
    >>> assert Fechas.checkDMA(29, 2, 2005) == (28, 2, 2005)
    >>> assert Fechas.checkDMA(29, 2, 2004) == (29, 2, 2004)

    @return: Una tupla con los valores de día, mes y año verificados.
    @rtype: tuple
    '''
    if calendar.isleap(a):
        TOPE_MESES[2] = 29
    else:
        TOPE_MESES[2] = 28
    if m < 1:
        m = 1
    elif m > 12:
        m = 12
    if d < 1:
        d = 1
    if d > TOPE_MESES[m]:
        d = TOPE_MESES[m]
    return (d, m, a)

class ClasificadorActualidad:
    def __init__(self):
        self.hoy = hoy()
        self.ayer = ayer()

    def __call__(self, row, field_name):
        global MESES

        fecha = row[field_name]
        if fecha == self.hoy:
            return 'hoy'
        elif fecha == self.ayer:
            return 'ayer'
        elif fecha.year == self.hoy.year and fecha.month == self.hoy.month: # El mismo mes y año
            return MESES[fecha.month]
        elif fecha.year == self.hoy.year: # El mismo año, diferente mes
            return '{} de {}'.format(MESES[fecha.month], fecha.year)
        else:
            return str(fecha.year)
