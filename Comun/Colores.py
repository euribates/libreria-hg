#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

"""
DESCRIPCIÓN
===========

Variables y Clases utiles para trabajar con colores. Define algunos
nombres utles para determinados colores, como L{Blanco}, L{White},
L{Negro}, L{Black}, L{Navy}, L{Indigo}, L{Verde}, L{Rojo}. También
define la clase L{Pautado}, que representa un ciclo de colores, util
a la hora de realizar un pautado en un listado, por ejemplo.
"""

import random

Blanco = White = '#FFFFFF'
Negro = Black = '#000000'
Rojo = Red = '#990000'
Verde = Green = '#009900'
Azul = Blue = '#000099'

Navy = '#66AADD'
Indigo = '#99CCFF'
Cyan = '#BFDFFD'
DarkBlue = '#000040'
Silver = '#DDDDDD'
Siena = '#FDEFBF'
Siena_Tostado = '#8F7D3E'

Todos = [
    Navy, Indigo, Cyan, DarkBlue,
    Silver, Siena, Siena_Tostado, Verde,
    Rojo, Azul
]


class RandomColor(object):

    def __init__(self):
        global Todos
        self.Opciones = Todos[:]

    def __str__(self):
        if self.Opciones:
            color = random.choice(self.Opciones)
            self.Opciones.remove(color)
            return color
        else:
            r = random.randrange(33, 223)
            g = random.randrange(33, 223)
            b = random.randrange(33, 223)
            return '#%02X%02X%02X' % (r, g, b)


class Pautado:
    """
    DESCRIPCIÓN
    ===========
    Genera colores en alternancia.

    Instanciado una variable de esta clase, podemos obtener distintos
    colores cada vez que se imprima, a partir de un conjunto que
    se le suministra en el constructor (Por defecto utiliza una lista
    de dos colores, L{White} y L{Silver}.
    """

    def __init__(self, colores=None):
        """Constructor

        Ejemplo:

            >>> from Libreria.Comun import Colores
            >>> bgcolor = Colores.Pautado([Colores.Navy
            ...     , Colores.Indigo
            ...     , Colores.Cyan])
            >>> for i in range(4):
            ...        print bgcolor
            #66AADD
            #99CCFF
            #BFDFFD
            #66AADD
            >>>

        @param colores: lista de colores a alternar. Por defecto es
                        [L{White}, L{Silver}], pero se puede especificar
                        distintos colores.
        @type colores: Una lista de cualquier caso que se pueda
                       considerar un color, normalmente
                       strings del tipo C{'#FFCC33'}.
        """
        self.colores = colores or [White, Silver]
        self.index = 0
        self.tope = len(self.colores)

    def __str__(self):
        """Representacion textual.

        devolverá el color que corresponda, según la lista de colores
        definida en el constructor.
        """
        color = self.colores[self.index]
        self.index = (self.index + 1) % self.tope
        return color
