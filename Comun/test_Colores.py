#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

from . import Colores


def test_known_colors():
    assert Colores.Blanco == '#FFFFFF'
    assert Colores.White == '#FFFFFF'
    assert Colores.Negro == '#000000'
    assert Colores.Black == '#000000'
    assert Colores.Navy == '#66AADD'
    assert Colores.Indigo == '#99CCFF'
    assert Colores.Cyan == '#BFDFFD'
    assert Colores.DarkBlue == '#000040'
    assert Colores.Silver == '#DDDDDD'
    assert Colores.Siena == '#FDEFBF'
    assert Colores.Siena_Tostado == '#8F7D3E'
    assert Colores.Verde == '#009900'
    assert Colores.Green == '#009900'
    assert Colores.Rojo == '#990000'
    assert Colores.Red == '#990000'
    assert Colores.Azul == '#000099'
    assert Colores.Blue == '#000099'


def test_pautado():
    Color = Colores.Pautado([Colores.Navy, Colores.Cyan, Colores.White])
    assert str(Color) == Colores.Navy
    assert str(Color) == Colores.Cyan
    assert str(Color) == Colores.White
    assert str(Color) == Colores.Navy
    assert str(Color) == Colores.Cyan
    assert str(Color) == Colores.White
    assert str(Color) == Colores.Navy
