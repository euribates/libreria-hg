#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from .trace import Tron


def test_tron_empty_on_init():
    '''Si no se ha escrito nada, el resultado es una cadena vacía'''
    t = Tron()
    assert str(t) == ''


def test_tron_as_boolean():
    '''Los objetos Tron deben evaluarse siempre como verdaderos'''
    t = Tron()
    assert t
    assert bool(t) is True


def test_example_use_it():
    with Tron() as output:
        print('Hola, mundo')
    assert str(output) == 'Hola, mundo\n'


def test_example_write():
    output = Tron()
    output.write('Hola,')
    output.write(' mundo')
    assert output.read() == 'Hola, mundo'


def test_example_write_and_two_reads():
    t = Tron()
    t.write('hola, mundo')
    assert t.read() == 'hola, mundo'
    assert t.read() == 'hola, mundo'
