#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

from six.moves import StringIO
import sys


class Tron:

    def __init__(self):
        self.buffer = StringIO()
        self.result = ''

    def write(self, s):
        self.buffer.write(str(s))

    def flush(self):
        self.result += self.buffer.getvalue()
        self.buffer = StringIO()

    def read(self):
        self.flush()
        return self.result

    def __str__(self):
        return self.read()

    def __bool__(self):
        return True

    __nonzero__ = __bool__

    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self.buffer
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type or exc_val or exc_tb:
            self.buffer.write('--[ Exception ]---------------------\n')
            self.buffer.write('exc_type: {}\n'.format(exc_type))
            self.buffer.write('exc_val: {}\n'.format(exc_val))
            self.buffer.write('exc_tb: {}\n'.format(exc_tb))
            self.buffer.write('------------------------------------\n')
        sys.stdout = self._stdout
        self.read()
