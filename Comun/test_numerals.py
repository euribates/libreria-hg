#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import pytest
import unittest
from . import numerals


def test_numeral_zero():
    assert numerals.numeral(0) == 'cero'


def test_numeral_negativo():
    assert numerals.numeral(-4) == 'menos cuatro'


_samples = {
    1: 'uno',
    2: 'dos',
    3: 'tres',
    4: 'cuatro',
    5: 'cinco',
    6: 'seis',
    7: 'siete',
    8: 'ocho',
    9: 'nueve',
    10: 'diez',
    11: 'once',
    12: 'doce',
    13: 'trece',
    14: 'catorce',
    15: 'quince',
    16: 'dieciséis',
    17: 'diecisiete',
    18: 'dieciocho',
    19: 'diecinueve',
    20: 'veinte',
    21: 'veintiuno',
    22: 'veintidós',
    23: 'veintitrés',
    24: 'veinticuatro',
    25: 'veinticinco',
    26: 'veintiséis',
    27: 'veintisiete',
    28: 'veintiocho',
    29: 'veintinueve',
    30: 'treinta',
    62: 'sesenta y dos',
    98: 'noventa y ocho',
    99: 'noventa y nueve',
    100: 'cien',
    101: 'ciento uno',
    102: 'ciento dos',
    198: 'ciento noventa y ocho',
    200: 'doscientos',
    201: 'doscientos uno',
    745: 'setecientos cuarenta y cinco',
    998: 'novecientos noventa y ocho',
    999: 'novecientos noventa y nueve',
    1000: 'mil',
    1001: 'mil uno',
    1002: 'mil dos',
    1111: 'mil ciento once',
    1023: 'mil veintitrés',
    1223: 'mil doscientos veintitrés',
    1998: 'mil novecientos noventa y ocho',
    1999: 'mil novecientos noventa y nueve',
    7103: 'siete mil ciento tres',
    7130: 'siete mil ciento treinta',
    8977: 'ocho mil novecientos setenta y siete',
    10000: 'diez mil',
    10001: 'diez mil uno',
    10002: 'diez mil dos',
    42758: 'cuarenta y dos mil setecientos cincuenta y ocho',
    58455: 'cincuenta y ocho mil cuatrocientos cincuenta y cinco',
    19998: 'diecinueve mil novecientos noventa y ocho',
    19999: 'diecinueve mil novecientos noventa y nueve',
    100000: 'cien mil',
    100001: 'cien mil uno',
    100002: 'cien mil dos',
    278482: 'doscientos setenta y ocho mil cuatrocientos ochenta y dos',
    199998: 'ciento noventa y nueve mil novecientos noventa y ocho',
    199999: 'ciento noventa y nueve mil novecientos noventa y nueve',
    }


@pytest.fixture(params=_samples, ids=_samples.values())
def sample(request):
    return request.param, _samples[request.param]


def test_simple(sample):
    num, text = sample
    assert numerals.numeral(num) == text


_femeninos = {
    1: 'una',
    2: 'dos',
    1998: 'mil novecientas noventa y ocho',
    1999: 'mil novecientas noventa y nueve',
    8977: 'ocho mil novecientas setenta y siete',
    42758: 'cuarenta y dos mil setecientas cincuenta y ocho',
    278482: 'doscientas setenta y ocho mil cuatrocientas ochenta y dos',
    }


@pytest.fixture(params=_femeninos, ids=_femeninos.values())
def sample_f(request):
    return request.param, _femeninos[request.param]


def test_numeral_miles_femenino(sample_f):
    num, text = sample_f
    assert numerals.numeral(num, en_femenino=True) == text


class CasosEspeciales(unittest.TestCase):

    def test_millones(self):
        assert numerals.numeral(1000000) == 'un millón'
        assert numerals.numeral(1000001) == 'un millón uno'
        assert numerals.numeral(1000002) == 'un millón dos'
        assert numerals.numeral(2057672) == (
            'dos millones cincuenta y siete mil'
            ' seiscientos setenta y dos'
            )
        assert numerals.numeral(2057672, en_femenino=True) == (
            'dos millones cincuenta y siete mil'
            ' seiscientas setenta y dos'
            )
        assert numerals.numeral(4728272) == (
            'cuatro millones setecientos veintiocho mil'
            ' doscientos setenta y dos'
            )
        assert numerals.numeral(4728272, en_femenino=True) == (
            'cuatro millones setecientas veintiocho mil'
            ' doscientas setenta y dos'
            )
        assert numerals.numeral(1999998) == (
            'un millón novecientos noventa y nueve'
            ' mil novecientos noventa y ocho'
            )
        assert numerals.numeral(1999999) == (
            'un millón novecientos noventa y nueve'
            ' mil novecientos noventa y nueve'
            )

    def test_decenas_de_millones(self):
        self.assertEqual(numerals.numeral(10000000), 'diez millones')
        self.assertEqual(numerals.numeral(10000001), 'diez millones uno')
        self.assertEqual(
            numerals.numeral(21048428),
            'veintiún millones cuarenta y ocho mil'
            ' cuatrocientos veintiocho'
            )
        self.assertEqual(
            numerals.numeral(99999998),
            'noventa y nueve millones'
            ' novecientos noventa y nueve mil'
            ' novecientos noventa y ocho'
            )
        self.assertEqual(
            numerals.numeral(99999999),
            'noventa y nueve millones'
            ' novecientos noventa y nueve mil'
            ' novecientos noventa y nueve'
            )


class TestExcepciones(unittest.TestCase):

    def test_fucking_21(self):
        assert numerals.numeral(21) == 'veintiuno'

    def test_fucking_210(self):
        assert numerals.numeral(210) == 'doscientos diez'

    def test_fucking_2121(self):
        assert numerals.numeral(2110) == 'dos mil ciento diez'

    def test_fucking_21021(self):
        assert numerals.numeral(21021) == 'veintiún mil veintiuno'
        assert numerals.numeral(21021, en_femenino=True) == (
            'veintiuna mil veintiuna'
            )

    def test_fucking_21021021(self):
        assert numerals.numeral(21021021) == (
            'veintiún millones veintiún mil veintiuno'
            )
        assert numerals.numeral(21021, en_femenino=True) == (
            'veintiuna mil veintiuna'
            )

    def test_fucking_21_billions(self):
        assert numerals.numeral(21021021021021) == (
            'veintiún billones veintiún mil veintiún millones'
            ' veintiún mil veintiuno'
            )


def test_as_importe_cero():
    assert numerals.as_importe_euros(0) == 'cero euros'
    assert numerals.as_importe_euros(0.0) == 'cero euros'


_centimos = {
    0.01: 'cero euros con un céntimo',
    0.02: 'cero euros con dos céntimos',
    0.03: 'cero euros con tres céntimos',
    0.04: 'cero euros con cuatro céntimos',
    0.05: 'cero euros con cinco céntimos',
    0.06: 'cero euros con seis céntimos',
    0.07: 'cero euros con siete céntimos',
    0.08: 'cero euros con ocho céntimos',
    0.09: 'cero euros con nueve céntimos',
    0.10: 'cero euros con diez céntimos',
    0.11: 'cero euros con once céntimos',
    0.12: 'cero euros con doce céntimos',
    0.13: 'cero euros con trece céntimos',
    0.14: 'cero euros con catorce céntimos',
    0.15: 'cero euros con quince céntimos',
    0.16: 'cero euros con dieciséis céntimos',
    0.17: 'cero euros con diecisiete céntimos',
    0.18: 'cero euros con dieciocho céntimos',
    0.19: 'cero euros con diecinueve céntimos',
    0.20: 'cero euros con veinte céntimos',
    0.21: 'cero euros con veintiun céntimos',
    0.22: 'cero euros con veintidós céntimos',
    0.23: 'cero euros con veintitrés céntimos',
    0.24: 'cero euros con veinticuatro céntimos',
    0.25: 'cero euros con veinticinco céntimos',
    0.26: 'cero euros con veintiséis céntimos',
    0.27: 'cero euros con veintisiete céntimos',
    0.28: 'cero euros con veintiocho céntimos',
    0.29: 'cero euros con veintinueve céntimos',
    0.30: 'cero euros con treinta céntimos',
    0.31: 'cero euros con treinta y un céntimos',
    0.32: 'cero euros con treinta y dos céntimos',
    0.33: 'cero euros con treinta y tres céntimos',
    0.34: 'cero euros con treinta y cuatro céntimos',
    0.35: 'cero euros con treinta y cinco céntimos',
    0.36: 'cero euros con treinta y seis céntimos',
    0.37: 'cero euros con treinta y siete céntimos',
    0.38: 'cero euros con treinta y ocho céntimos',
    0.39: 'cero euros con treinta y nueve céntimos',
    0.40: 'cero euros con cuarenta céntimos',
    0.41: 'cero euros con cuarenta y un céntimos',
    0.42: 'cero euros con cuarenta y dos céntimos',
    0.43: 'cero euros con cuarenta y tres céntimos',
    0.44: 'cero euros con cuarenta y cuatro céntimos',
    0.45: 'cero euros con cuarenta y cinco céntimos',
    0.46: 'cero euros con cuarenta y seis céntimos',
    0.47: 'cero euros con cuarenta y siete céntimos',
    0.48: 'cero euros con cuarenta y ocho céntimos',
    0.49: 'cero euros con cuarenta y nueve céntimos',
    0.50: 'cero euros con cincuenta céntimos',
    0.51: 'cero euros con cincuenta y un céntimos',
    0.52: 'cero euros con cincuenta y dos céntimos',
    0.53: 'cero euros con cincuenta y tres céntimos',
    0.54: 'cero euros con cincuenta y cuatro céntimos',
    0.55: 'cero euros con cincuenta y cinco céntimos',
    0.56: 'cero euros con cincuenta y seis céntimos',
    0.57: 'cero euros con cincuenta y siete céntimos',
    0.58: 'cero euros con cincuenta y ocho céntimos',
    0.59: 'cero euros con cincuenta y nueve céntimos',
    0.60: 'cero euros con sesenta céntimos',
    0.61: 'cero euros con sesenta y un céntimos',
    0.62: 'cero euros con sesenta y dos céntimos',
    0.63: 'cero euros con sesenta y tres céntimos',
    0.64: 'cero euros con sesenta y cuatro céntimos',
    0.65: 'cero euros con sesenta y cinco céntimos',
    0.66: 'cero euros con sesenta y seis céntimos',
    0.67: 'cero euros con sesenta y siete céntimos',
    0.68: 'cero euros con sesenta y ocho céntimos',
    0.69: 'cero euros con sesenta y nueve céntimos',
    0.70: 'cero euros con setenta céntimos',
    0.71: 'cero euros con setenta y un céntimos',
    0.72: 'cero euros con setenta y dos céntimos',
    0.73: 'cero euros con setenta y tres céntimos',
    0.74: 'cero euros con setenta y cuatro céntimos',
    0.75: 'cero euros con setenta y cinco céntimos',
    0.76: 'cero euros con setenta y seis céntimos',
    0.77: 'cero euros con setenta y siete céntimos',
    0.78: 'cero euros con setenta y ocho céntimos',
    0.79: 'cero euros con setenta y nueve céntimos',
    0.80: 'cero euros con ochenta céntimos',
    0.81: 'cero euros con ochenta y un céntimos',
    0.82: 'cero euros con ochenta y dos céntimos',
    0.83: 'cero euros con ochenta y tres céntimos',
    0.84: 'cero euros con ochenta y cuatro céntimos',
    0.85: 'cero euros con ochenta y cinco céntimos',
    0.86: 'cero euros con ochenta y seis céntimos',
    0.87: 'cero euros con ochenta y siete céntimos',
    0.88: 'cero euros con ochenta y ocho céntimos',
    0.89: 'cero euros con ochenta y nueve céntimos',
    0.90: 'cero euros con noventa céntimos',
    0.91: 'cero euros con noventa y un céntimos',
    0.92: 'cero euros con noventa y dos céntimos',
    0.93: 'cero euros con noventa y tres céntimos',
    0.94: 'cero euros con noventa y cuatro céntimos',
    0.95: 'cero euros con noventa y cinco céntimos',
    0.96: 'cero euros con noventa y seis céntimos',
    0.97: 'cero euros con noventa y siete céntimos',
    0.98: 'cero euros con noventa y ocho céntimos',
    0.99: 'cero euros con noventa y nueve céntimos',
    }


@pytest.fixture(params=_centimos, ids=_centimos.values())
def centimos(request):
    return request.param, _centimos[request.param]


def test_as_importe_with_centimos(centimos):
    num, text = centimos
    assert numerals.as_importe_euros(num) == text


def test_as_importe_known_vaues():
    target = 'cuatrocientos ochenta y cuatro euros con veintitrés céntimos'
    assert numerals.as_importe_euros(484.23) == target
