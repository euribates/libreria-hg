#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import pytest
import unittest
from Libreria.Comun.trace import Tron

from six.moves import StringIO
import sys

from Libreria.Base import Form


def test_div():
    div = Form.DIV('Este es el texto')
    div.Klass = 'aviso'
    div.onClick = """alert('hola');"""
    assert str(div) == (
        '<div'
        ' class="aviso"'
        ' onClick="javascript:alert(\'hola\');">'
        'Este es el texto'
        '</div>'
        )


def test_span():
    span = Form.Span('Este es el texto')
    span.Klass = 'aviso'
    span.onClick = "alert('hola');"
    assert str(span) == (
        '<span'
        ' class="aviso"'
        ' onClick="javascript:alert(\'hola\');">'
        'Este es el texto'
        '</span>'
        )


class TestEventos(unittest.TestCase):

    def test_on_load(self):
        '''Asignando la propiedad onLoad.
        '''
        w = Form.Text('Nombre', 0)
        w.onLoad = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)

    def test_on_unload(self):
        '''Asignando la propiedad OnUnload.
        '''
        w = Form.Text('Nombre', 0)
        w.onUnload = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)

    def test_OnFocus(self):
        '''Asignando la propiedad onFocus.
        '''
        w = Form.Text('Nombre', 0)
        w.onFocus = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)

    def test_OnBlur(self):
        '''Asignando la propiedad onBlur.
        '''
        w = Form.Text('Nombre', 0)
        w.onBlur = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)

    def test_OnMouseOver(self):
        '''Asignando la propiedad onMouseOver.
        '''
        w = Form.Text('Nombre', 0)
        w.onMouseOver = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)

    def test_OnMouseOut(self):
        '''Asignando la propiedad onMouseOut.
        '''
        w = Form.Text('Nombre', 0)
        w.onMouseOut = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)

    def test_OnClick(self):
        '''Asignando la propiedad onClick.
        '''
        w = Form.Text('Nombre', 0)
        w.onClick = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)

    def test_OnMouseDown(self):
        '''Asignando la propiedad onMouseDown.
        '''
        w = Form.Text('Nombre', 0)
        w.onMouseDown = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)

    def test_OnMouseUp(self):
        '''Asignando la propiedad onMouseUp.
        '''
        w = Form.Text('Nombre', 0)
        w.onMouseUp = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)

    def test_OnMouseMove(self):
        '''Asignando la propiedad onMouseMove.
        '''
        w = Form.Text('Nombre', 0)
        w.onMouseMove = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)

    def test_OnKeyDown(self):
        '''Asignando la propiedad onKeyDown.
        '''
        w = Form.Text('Nombre', 0)
        w.onKeyDown = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)

    def test_OnKeyUp(self):
        '''Asignando la propiedad onKeyUp.
        '''
        w = Form.Text('Nombre', 0)
        w.onKeyUp = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)

    def test_OnKeyPress(self):
        '''Asignando la propiedad onKeyPress.
        '''
        w = Form.Text('Nombre', 0)
        w.onKeyPress = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)

    def test_OnResize(self):
        '''Asignando la propiedad onResize.
        '''
        w = Form.Text('Nombre', 0)
        w.onResize = '''alert('hola, mundo');'''
        assert "alert('hola, mundo');" in str(w)


class TestText(unittest.TestCase):

    def test_create_text(self):
        '''Uso basico.
        '''
        widget = Form.Text('nombre', 'valor')
        assert widget.identity == 'text_nombre'
        assert widget.Klass == 'form-control'
        assert str(widget) == (
            '<input type="text" name="nombre" value="valor"'
            ' id="text_nombre" class="form-control"'
            ' size="35">'
            )

    def test_using_identity(self):
        '''Atributo Identificador (Identity), en la clase Text.
        '''
        widget = Form.Text('nombre', 'valor', identity='txt_nombre')
        assert widget.Identity == 'txt_nombre'
        assert widget.Klass == 'form-control'
        assert str(widget) == (
            '<input type="text" name="nombre"'
            ' value="valor"'
            ' id="txt_nombre"'
            ' class="form-control"'
            ' size="35">'
            )

    def test_klass(self):
        '''Usando el parámetro klass en un Text.
        '''
        widget = Form.Text('nombre', 'valor', klass='tururu')
        assert widget.identity == 'text_nombre'
        assert widget.Klass == 'tururu'
        assert str(widget) == (
            '<input type="text" name="nombre" value="valor"'
            ' id="text_nombre"'
            ' class="tururu"'
            ' size="35">'
            )

    def test_constructor(self):
        '''Ejemplo del constructor de Form.Text.
        '''
        txt = Form.Text('Nombre', 'PEPE')
        assert str(txt) == (
            '<input type="text"'
            ' name="Nombre"'
            ' value="PEPE"'
            ' id="text_nombre"'
            ' class="form-control"'
            ' size="35">'
            )

    def test_constructor_size(self):
        '''Ejemplo del constructor de Form.Text con parametro size.
        '''
        txt = Form.Text('Nombre', 'PEPE', size=10)
        assert str(txt) == (
            '<input type="text" name="Nombre" value="PEPE"'
            ' id="text_nombre"'
            ' class="form-control"'
            ' size="10">'
            )

    def test_constructor_size_max_length(self):
        '''Ejemplo del constructor de Form.Text con
        parametro size y maxlength.
        '''
        txt = Form.Text('Nombre', 'PEPE', size=10, maxlength=100)
        assert str(txt) == (
            '<input type="text" name="Nombre" value="PEPE"'
            ' id="text_nombre"'
            ' class="form-control"'
            ' size="10"'
            ' maxlength="100">'
            )

    def test_uso_con_comillas(self):
        '''Ejemplo de uso de Form.Text con comillas.
        '''
        txt = Form.Text('Nombre', 'PEPE "Er cojo"')
        assert str(txt) == (
            '<input type="text" name="Nombre"'
            ' value="PEPE &quot;Er cojo&quot;"'
            ' id="text_nombre"'
            ' class="form-control" size="35">'
            )

    def test_uso_mayor_menor(self):
        '''Ejemplo de uso de Form.Text con simbolos mayor y menor.
        '''
        txt = Form.Text('Formula', 'Sea x > 23 e y < 21')
        assert str(txt) == (
            '<input type="text" name="Formula"'
            ' value="Sea x &gt; 23 e y &lt; 21"'
            ' id="text_formula"'
            ' class="form-control" size="35">'
            )

    def test_uso_ampersand(self):
        '''Ejemplo de uso de Form.Text con el simbolo ampersand.
        '''
        txt = Form.Text('Company', 'Ernst & Young')
        assert str(txt) == (
            '<input type="text" name="Company"'
            ' value="Ernst &amp; Young"'
            ' id="text_company"'
            ' class="form-control" size="35">'
            )


class TestIdentificadores(unittest.TestCase):

    def test_password(self):
        '''Atributo Identificador (Id), en la clase PASSWORD.
        '''
        widget = Form.Password('NOMBRE', 'texto', identity='testigo')
        assert widget.Identity == 'testigo'
        assert 'id="testigo"' in str(widget)

    def test_radio(self):
        '''Atributo Identificador (Id), en la clase Radio.
        '''
        widget = Form.Radio('EsteEsElNombre', 'valor', identity='testigo')
        assert widget.Identity == 'testigo'
        assert 'id="testigo"' in str(widget)

    def test_Checkbox(self):
        '''Atributo Identificador (Id), en la clase CheckBox.
        '''
        widget = Form.CheckBox('nombre', 'valor', identity='testigo')
        assert widget.Identity == 'testigo'
        assert 'id="testigo"' in str(widget)

    def test_file(self):
        '''Atributo Identificador (Id), en la clase FILE.
        '''
        widget = Form.File('NOMBRE', 'valor', identity='testigo')
        assert widget.Identity == 'testigo'
        assert 'id="testigo"' in str(widget)

    def test_image(self):
        '''Atributo Identificador (Id), en la clase IMAGE.
        '''
        widget = Form.Image('NOMBRE', 'valor', identity='testigo')
        assert widget.Identity == 'testigo'
        assert 'id="testigo"' in str(widget)

    def test_select(self):
        '''Atributo Identificador (Id), en la clase Select.
        '''
        widget = Form.Select('NOMBRE', identity='testigo')
        assert widget.Identity == 'testigo'
        assert 'id="testigo"' in str(widget)

    def test_textarea(self):
        '''Atributo Identificador (Id), en la clase TextArea.
        '''
        widget = Form.TextArea('NOMBRE', 'texto', identity='testigo')
        assert widget.Identity == 'testigo'
        assert 'id="testigo"' in str(widget)

    def test_submit(self):
        '''Atributo Identificador (Id), en la clase SUBMIT.
        '''
        widget = Form.Submit('NOMBRE', 'texto', identity='testigo')
        assert widget.Identity == 'testigo'
        assert 'id="testigo"' in str(widget)

    def test_hidden(self):
        '''Atributo Identificador (Id), en la clase Hidden.
        '''
        widget = Form.Hidden('NOMBRE', 'texto', identity='testigo')
        assert widget.Identity == 'testigo'
        assert 'id="testigo"' in str(widget)

    def test_label(self):
        '''Atributo Identificador (Id), en la clase Label.
        '''
        widget = Form.Label('texto', identity='testigo', for_item='txt_texto')
        assert widget.Identity == 'testigo'
        assert 'id="testigo"' in str(widget)
        assert 'for="txt_texto"' in str(widget)


class TestGetRango(unittest.TestCase):

    def test_un_valor(self):
        '''Uso de getRango con un valor.
        '''
        # Simulación del paso de valor CGI
        import cgi
        Form.FORMDATA = {}
        Form.FORMDATA['RANGO'] = cgi.MiniFieldStorage('RANGO', '23')
        (a, b) = Form.getRango('RANGO')
        assert a == 23
        assert b == 0

    def test_tresValores(self):
        '''Uso de getRango con valores incorrectos.
        '''
        # Simulación del paso de valor CGI
        import cgi
        Form.FORMDATA = {}
        Form.FORMDATA['RANGO'] = cgi.MiniFieldStorage('RANGO', '23:12:3')
        with pytest.raises(ValueError):
            Form.getRango('RANGO')

    def test_dos_valores(self):
        '''Uso de getRango con dos valores.
        '''
        # Simulación del paso de valor CGI
        import cgi
        Form.FORMDATA = {}
        Form.FORMDATA['RANGO'] = cgi.MiniFieldStorage('RANGO', '23:12')
        (a, b) = Form.getRango('RANGO')
        assert a == 23
        assert b == 12

        Form.FORMDATA['RANGO'] = cgi.MiniFieldStorage('RANGO', '23  :12')
        (a, b) = Form.getRango('RANGO')
        assert a == 23
        assert b == 12

        Form.FORMDATA['RANGO'] = cgi.MiniFieldStorage('RANGO', '23:   12')
        (a, b) = Form.getRango('RANGO')
        assert a == 23
        assert b == 12

        Form.FORMDATA['RANGO'] = cgi.MiniFieldStorage('RANGO', '23  :  12')
        (a, b) = Form.getRango('RANGO')
        assert a == 23
        assert b == 12

        Form.FORMDATA['RANGO'] = cgi.MiniFieldStorage('RANGO', '23/12')
        (a, b) = Form.getRango('RANGO')
        assert a == 23
        assert b == 12

        Form.FORMDATA['RANGO'] = cgi.MiniFieldStorage('RANGO', '23.12')
        (a, b) = Form.getRango('RANGO')
        assert a == 23
        assert b == 12

        Form.FORMDATA['RANGO'] = cgi.MiniFieldStorage('RANGO', '23*12')
        (a, b) = Form.getRango('RANGO')
        assert a == 23
        assert b == 12

        Form.FORMDATA['RANGO'] = cgi.MiniFieldStorage('RANGO', '23-12')
        (a, b) = Form.getRango('RANGO')
        assert a == 23
        assert b == 12

        Form.FORMDATA['RANGO'] = cgi.MiniFieldStorage('RANGO', '23+12')
        (a, b) = Form.getRango('RANGO')
        assert a == 23
        assert b == 12


class TestGetFecha(unittest.TestCase):

    def test_uso(self):
        '''Uso de getFecha.
        '''
        # Simulación del paso de valor CGI
        import cgi
        Form.FORMDATA = {}
        Form.FORMDATA['F.DIA'] = cgi.MiniFieldStorage('F.DIA', '23')
        Form.FORMDATA['F.MES'] = cgi.MiniFieldStorage('F.MES', '7')
        Form.FORMDATA['F.ANIO'] = cgi.MiniFieldStorage('F.ANIO', '2005')
        # Fin de simulacion
        f = Form.getFecha('F')
        assert f is None
        f = Form.getFecha('F', sep='.')
        assert f.day == 23
        assert f.month == 7
        assert f.year == 2005


def test_get_name_and_set_name():
    '''Comprobar la propiadad Name (getName, setName).
    '''
    w = Form.Widget('hola', 'mundo')
    assert w.Nombre == 'hola'
    assert w.Name == 'hola'
    w.Name = 'Otro'
    assert w.Nombre == 'Otro'
    assert w.Name == 'Otro'
    assert w.Name == w.Nombre


def test_value_property():
    '''Comprobar la propiadad Value (getValue, setValue).
    '''
    w = Form.WIDGET('hola', 'mundo')
    assert w.Valor == 'mundo'
    assert w.Value == 'mundo'
    w.Value = 'Otro'
    assert w.Valor == 'Otro'
    assert w.Value == 'Otro'
    assert w.Valor == w.Value


def test_escape_no_changes():
    assert Form.Escape('pepe') == 'pepe'
    assert Form.Escape('Well Balanced') == 'Well Balanced'
    assert Form.Escape('contiene "comillas') == 'contiene "comillas'


def test_escape_with_changes():
    assert Form.Escape('&') == '&amp;'
    assert Form.Escape('<') == '&lt;'
    assert Form.Escape('>') == '&gt;'
    assert Form.Escape('<b>') == '&lt;b&gt;'
    assert Form.Escape('contiene "comillas', True) == (
        'contiene &quot;comillas'
        )


class Widgets(unittest.TestCase):
    def test_Select(self):
        '''Varias pruebas sobre los objetos de clase Select.
        '''
        cb = Form.Select('name')
        cb.addOption(1, 'Uno')
        cb.addOption(2, 'Dos')
        cb.addOption(0, 'Todos', selected=True)
        s = str(cb)
        assert '<select name="name" id="select_name">' in s
        assert '<option value="1">Uno</option>' in s
        assert '<option value="2">Dos</option>' in s
        assert '<option value="0" selected>Todos</option>' in s
        assert '</select>' in s
        assert cb.Valor == 0


class TestImage(unittest.TestCase):

    def test_constructor(self):
        '''Constructor de IMAGE.
        '''
        img = Form.IMAGE('imagen.gif', '/static/art/escudo.png')
        assert isinstance(img, Form.IMAGE)
        assert issubclass(img.__class__, Form.WIDGET)

    def test_klass(self):
        '''Uso del parametro klass en la clase IMAGE.
        '''
        img = Form.Image(
            'imagen.gif',
            '/static/art/escudo.png',
            klass="tururu",
            )
        assert 'class="tururu"' in str(img)


class File(unittest.TestCase):
    def test_Constructor(self):
        f1 = Form.File('nombre')
        f13 = Form.File('nombre', accept="text/plain")
        f12 = Form.File('nombre', 'valor')
        f123 = Form.File('nombre', 'valor', accept="text/plain")
        for f in [f1, f13, f12, f123]:
            assert isinstance(f, Form.FILE)
            assert issubclass(f.__class__, Form.WIDGET)

    def test_klass_attribute(self):
        '''Uso del parametro klass en la clase FILE.
        '''
        f = Form.File('nombre', klass="tururu")
        assert 'class="tururu"' in str(f)

    def test_accept_attribute(self):
        '''Uso del parametro -accept- en la clase FILE.
        '''
        f = Form.File('nombre', accept="image/*")
        assert 'accept="image/*"' in str(f)


# Test for class Select


def test_select_constructor():
    universes = dict(dc='DC Universe', um='Marvel Universe')
    s1 = Form.Select('Universe')
    s2 = Form.Select('Universe', dictionary=universes)
    s3 = Form.Select('Universe', 'DC', dictionary=universes)
    for s in [s1, s2, s3]:
        assert s.Name == 'Universe'
        assert isinstance(s, Form.Select)
        assert issubclass(s.__class__, Form.Widget)


def test_select_klass():
    s = Form.Select('Universe', klass="tururu")
    assert s.Klass == 'tururu'
    assert 'class="tururu"' in str(s)


def test_select_enable_disable():
    s = Form.Select('Universe', klass="tururu")
    assert not s.disabled
    s.disable()
    assert s.disabled
    s.enable()
    assert not s.disabled


def test_select_disabled_output():
    s = Form.Select('Universe', klass="tururu")
    s.disable()
    output = str(s)
    assert 'disabled="disabled"' in output
    s.enable()
    output = str(s)
    assert 'disabled="disabled"' not in output


class Form_Fila(unittest.TestCase):

    def setUp(self):
        sys.stdout = StringIO()

    def tearDown(self):
        sys.stdout = sys.__stdout__

    def get_output(self):
        return sys.stdout.getvalue().strip()

    def test_separador(self):
        '''Llamada a Form.Fila con un solo parametro.
        '''
        Form.Fila('hola')
        texto = self.get_output()
        assert 'hola' in texto
        assert 'colspan="2"' in texto

    def test_llamada_a_filas(self):
        '''Llamada a Form.Filas.
        '''
        with Tron() as output:
            Form.Filas(
                'Opciones',
                Form.CheckBox('opt1', 'Uno', label="uno"),
                Form.CheckBox('opt2', 'Dos', label="dos")
                )
        texto = str(output)
        assert 'Opciones' in texto
        assert (
            '<input type="checkbox"'
            ' name="opt1"'
            ' value="Uno"'
            ' id="checkbox_opt1">') in texto
        assert '<label for="checkbox_opt1">uno</label>' in texto
        assert (
            '<input type="checkbox"'
            ' name="opt2"'
            ' value="Dos"'
            ' id="checkbox_opt2">') in texto
        assert '<label for="checkbox_opt2">dos</label>' in texto

    def test_uso_normal(self):
        '''Llamada a Form.Fila con dos parametros.
        '''
        Form.Fila('hola', 'mundo')
        texto = self.get_output()
        assert 'hola' in texto
        assert 'mundo' in texto

    def test_comentarios(self):
        '''Llamada a Form.Fila con parametro opcional: comentario.
        '''
        Form.Fila('hola', 'mundo', comentario='que tal')
        texto = self.get_output()
        assert 'hola' in texto
        assert 'mundo' in texto
        assert 'que tal' in texto


class TestTextArea(unittest.TestCase):

    def test_Constructor(self):
        ta1 = Form.TextArea('Nombre')
        ta12 = Form.TextArea('Nombre', 'ejemplo de texto')
        ta1234 = Form.TextArea('Nombre', 'ejemplo de texto', rows=5, cols=75)
        ta13 = Form.TextArea('Nombre', rows=4)
        ta134 = Form.TextArea('Nombre', rows=4, cols=70)
        ta6 = Form.TextArea('Nombre', disabled=True)
        for ta in [ta1, ta12, ta1234, ta13, ta134, ta6]:
            assert isinstance(ta, Form.TextArea)
            assert issubclass(ta.__class__, Form.WIDGET)

    def test_klass(self):
        '''Uso del parametro klass en la clase TextArea.
        '''
        w = Form.TextArea('Nombre', 'ejemplo de texto', klass="tururu")
        assert 'class="tururu"' in str(w)


# Tests for class Radio


def test_radio_constructor_just_name():
    r = Form.Radio('Nombre')
    assert isinstance(r, Form.Radio)
    assert issubclass(r.__class__, Form.WIDGET)
    assert r.Name == 'Nombre'
    assert r.Value is None
    assert not r.Disabled


def test_radio_constructor_name_and_value():
    r = Form.Radio('Nombre', 'valor')
    assert r.Name == 'Nombre'
    assert r.Value == 'valor'
    assert not r.Disabled


def test_radio_constructor_disabled():
    r = Form.Radio('Nombre', 'valor', disabled=True)
    assert r.Disabled


def test_radio_constructor_with_label():
    r = Form.Radio('Nombre', 'valor', label='warning')
    assert r.Label == 'warning'


def test_radio_constructor_disabled_and_with_label():
    r = Form.Radio('Nombre', 'valor', label='warning', disabled=True)
    assert r.Disabled
    assert r.Label == 'warning'


def test_radio_klass():
    r = Form.Radio('Nombre', 'valor', klass="tururu")
    assert r.Klass == 'tururu'


def test_radio_identity():
    r = Form.Radio('Nombre', 'valor', identity="tururu")
    assert r.Identity == 'tururu'


# Tests for class CheckBox


def test_checkbox_klass():
    '''Uso del parametro klass en la clase CheckBox.
    '''
    w = Form.CheckBox('Nombre', 'valor', klass="tururu")
    assert 'class="tururu"' in str(w)


def test_checkbox_identity():
    '''Uso del parametro Identity en la clase CheckBox.
    '''
    w = Form.CheckBox('Nombre', 'valor', identity="tururu")
    assert 'id="tururu"' in str(w)


def test_checkbox_constructor():
    '''Constructor de la clase CheckBox.
    '''
    r1 = Form.CheckBox('Nombre')
    r2 = Form.CheckBox('Nombre', 'valor')
    r3 = Form.CheckBox('Nombre', 'valor', disabled=True)
    r4 = Form.CheckBox('Nombre', 'valor', label='etiqueta')
    r5 = Form.CheckBox(
        'Nombre', 'valor',
        label='etiqueta',
        disabled=True,
        )
    for r in [r1, r2, r3, r4, r5]:
        assert isinstance(r, Form.CheckBox)
        assert issubclass(r.__class__, Form.WIDGET)


class Ejemplo(unittest.TestCase):

    def test_ejemplo_de_uso(self):
        """Ejemplo de uso.
        """
        with Tron() as output:
            Form.Begin('action.py')
            print(Form.Label('etiqueta'))
            print(Form.Hidden('texto oculto', '3'))
            print(Form.Text('Texto', 'valor'))
            print(Form.TextArea(
                'textarea',
                'Este es el texto\nde la <texarea>'
                ))
            txt = Form.TextArea(
                'textarea',
                'Este es el texto\nde la <texarea>'
                )
            txt.setDisabled()
            print(txt)
            items = {'uno': 'I', 'Dos': 'II'}
            print(Form.Select('NUMERO', items))
            print(Form.CheckBox('Confirmar', 'S', checked='Checked'))
            print(Form.Radio('SEXO', 'Varón'))
            print(Form.Radio('SEXO', 'Mujer', checked='Checked'))
            cb = Form.CheckBox('SEXO', 'Mujer', checked='Checked')
            cb.setOnClick("javascript:document.title='Doy el coñazo';")
            print(cb)
            Form.End()
        txt_form = str(output).strip()
        assert txt_form.startswith('<form ')
        assert txt_form.endswith('</form>')


def test_as_financial():
    '''Probando el metodo Form.AsFinancial.
    '''
    assert Form.AsFinancial(1023.45) == '1.023,45'
    assert Form.AsFinancial(1023) == '1.023,00'
    assert Form.AsFinancial(10355.6723) == '10.355,67'
    assert Form.AsFinancial(103955.6723) == '103.955,67'
    assert Form.AsFinancial(10334755.6783) == '10.334.755,68'


def test_boton():
    boton = Form.Boton('go.py', 'Go', Fecha='1/1/2005')
    assert boton == (
        '<form action="go.py" method="POST"'
        ' style="margin: 0" class="FORMULARIO">\n'
        '<input type="hidden" name="FECHA" value="1/1/2005">\n'
        '<input type="submit" name="OK" value="Go">\n'
        '</form>'
        )


# Testing format_as_label


def test_format_as_label_without_id():
    w = Form.Text('name')
    s = Form.format_as_label('nombre', w)
    assert '<label for="text_name">' in s


def test_format_as_label_with_id():
    w = Form.Text('name', identity='id_name')
    s = Form.format_as_label('nombre', w)
    assert '<label for="id_name">' in s
