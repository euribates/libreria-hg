#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import datetime
from six import StringIO
import getopt
import sys
import unittest

from Libreria.Comun.trace import Tron
from . import Html


def test_alert():
    nombre_completo = 'Ulysses Bloodstone'
    with Tron() as salida:
        Html.alert('El señor {} ya está incorporado', nombre_completo)
    salida = str(salida)
    assert salida == (
        '<div class="alert alert-info" role="alert">\n'
        'El señor Ulysses Bloodstone ya está incorporado\n'
        '</div>\n'
        )

def test_alert_danger():
    with Tron() as output:
        Html.alert(
            '¡Atención! Este pedido ya ha sido '
            'entregado en fecha {}',
            'hoy',
            klass='danger'
            )
    salida = str(output)
    assert salida == (
        '<div class="alert alert-danger" role="alert">\n'
        '¡Atención! Este pedido ya ha sido '
        'entregado en fecha hoy\n'
        '</div>\n'
        )


def test_create_img():
    img = Html.IMG('/static/art/escudo.png', 22, 40, 'Escudo')
    assert img.Src == '/static/art/escudo.png'
    assert img.Width == 22
    assert img.Height == 40
    assert img.Alt == 'Escudo'
    assert img.Title == ''
    img.Title = 'Otro'
    assert img.Title == 'Otro'
    assert str(img) == '<img src="/static/art/escudo.png"'  \
                       ' width="22" height="40"'  \
                       ' alt="Escudo" border="0" title="Otro">'


class Comentarios(unittest.TestCase):

    def setUp(self):
        options, args = getopt.getopt(
            sys.argv[1:],
            'hHvq',
            ['help', 'verbose', 'quiet'],
            )
        self.verbosity = 1
        for opt, value in options:
            if opt in ('-q','--quiet'):
                self.verbosity = 0
            if opt in ('-v','--verbose'):
                self.verbosity = 2

    def test(self):
        import doctest
        (num_errores, num_tests) = doctest.testmod(Html)
        if self.verbosity > 1:
            print('Comprobados %d ejemplos en los comentarios (%d errores)' % (
                num_tests, num_errores
                ))
        self.assertEqual(num_errores, 0)


class LlamadasAFunciones(unittest.TestCase):
    def setUp(self):
        self.Salida = StringIO()
        sys.stdout = self.Salida

    def tearDown(self):
        sys.stdout = sys.__stdout__

    def printSalida(self):
        self.Salida.seek(0)
        sys.__stdout__.write('\n--[Salida]------------------------------\n')
        sys.__stdout__.write(self.Salida.read())
        sys.__stdout__.write('\n----------------------------------------\n')

    def testHeader(self):
        '''Llamada a Html.Header sin parametros.
        '''
        Html.Header()

    def testHeaderExtra(self):
        '''Llamada a Html.Header sin parametros.
        '''
        Html.Header('pragma: no-cache')
        # self.printSalida()

    def testDebug(self):
        '''Llamada a Html.Debug.
        '''
        Html.Debug()

    def testRedirect(self):
        '''Llamada a Html.Redirect.
        '''
        Html.Redirect('http://www.python.org/')


    def test_Begin(self):
        '''Llamada a Html.Begin.
        '''
        Html.Begin('Este es el titulo')

    def test_End(self):
        '''Llamada a Html.End.
        '''
        Html.End()

    def test_traceback(self):
        '''Llamada a Html.Traceback().
        '''
        Html.TraceBack('Este es el mensaje')

    def test_Panic(self):
        '''Llamada a Html.Panic.
        '''
        self.assertRaises(SystemExit, Html.Panic, 'Este es el Mensaje')

    def test_Error(self):
        '''Llamada a Html.Error.
        '''
        self.assertRaises(SystemExit, Html.Error, 'Este es el Mensaje')

    def test_Aviso(self):
        '''Llamada a Html.Aviso.
        '''
        self.assertRaises(SystemExit, Html.Aviso, 'Este es el Mensaje')

    def test_h1(self):
        '''Llamada a Html.h1'''
        with Tron() as salida: 
            Html.h1('Texto')
        assert str(salida) == '<h1>Texto</h1>\n'
    
    def test_h2(self):
        '''Llamada a Html.h2 sin argumentos'''
        with Tron() as salida: 
            Html.h2('Texto')
        assert str(salida) == '<h2>Texto</h2>\n'

    def test_h2_con_args(self):
        '''Llamada a Html.h2 coin argumentos pasados por orden'''
        with Tron() as salida: 
            Html.h2('Hola, {0}', 'mundo')
        assert str(salida) == '<h2>Hola, mundo</h2>\n'

    def test_h2_con_kwargs(self):
        '''Llamada a Html.h2 con argumentos pasados por nombre'''
        with Tron() as salida: 
            Html.h2('Hola, {planet}', planet='Earth')
        assert str(salida) == '<h2>Hola, Earth</h2>\n'

    def test_h3(self):
        '''Llamada a Html.h3.
        '''
        with Tron() as salida: 
            Html.h3('Harry Potter y el prisionero de Azkaban')
        assert str(salida) == '<h3>Harry Potter y el prisionero de Azkaban</h3>\n'

    def test_h4(self):
        '''Llamada a Html.h4.
        '''
        with Tron() as salida: 
            Html.h4('Harry Potter y el cáliz de fuego')
        assert str(salida) == '<h4>Harry Potter y el cáliz de fuego</h4>\n'

    def test_h5(self):
        '''Llamada a Html.h5.
        '''
        with Tron() as salida: 
            Html.h5('Texto')
        assert str(salida) == '<h5>Texto</h5>\n'

    def test_h6(self):
        '''Llamada a Html.h6.
        '''
        with Tron() as salida: 
            Html.h6('Harry Potter y la Orden del Fénix')
        assert str(salida) == '<h6>Harry Potter y la Orden del Fénix</h6>\n'

    def test_h6_with_class(self):
        '''Llamada a Html.h6.
        '''
        with Tron() as salida: 
            Html.h6('Harry Potter y la Orden del Fénix', klass='novel')
        assert str(salida) == '<h6 class="novel">Harry Potter y la Orden del Fénix</h6>\n'


    def test_blockquote(self):
        '''Llamada a Html.blockquete.
        '''
        target = '<blockquote>\nTexto\n</blockquote>'
        with Tron() as salida:
            Html.blockquote('Texto')
        assert target == str(salida).strip()

    def test_hr(self):
        '''Lllamada a Html.hr.
        '''
        Html.hr()

    def test_Small(self):
        '''Resultado de Html.Small
        '''
        self.assertEqual(Html.Small('texto'), '<small>texto</small>')

    def test_Big(self):
        '''Resultado de Html.Big
        '''
        self.assertEqual(Html.Big('texto'), '<big>texto</big>')

    def test_p(self):
        '''Llamada a Html.p.
        '''
        Html.p('P&aacute;rrafo')

    def test_a_con_un_solo_parametro(self):
        '''Llamada a Html.a con un solo parametro.
        '''
        self.assertEqual(
            Html.a('http://www.python.org/'),
            '<a href="http://www.python.org/">http://www.python.org/</a>',
            )

    def test_a_con_dos_parametros(self):
        '''Llamada a Html.a con dos parametro.
        '''
        self.assertEqual(
            Html.a('http://www.python.org/', 'Python Homepage'),
            '<a href="http://www.python.org/">Python Homepage</a>',
            )


    def test_Check_CGI_Traceback(self):
        Html.Check_CGI_Traceback()

    def test_includeFile(self):
        '''Llamada a Html.includeFile.
        '''
        Html.includeFile(__file__)

    def test_includeFileFails(self):
        '''Llamada a Html.includeFile con un archivo que no existe.
        '''
        self.assertRaises(Html.HtmlError, Html.includeFile, 'este_fichero_no_existe.tururu.txt')

    def test_Join(self):
        L = 6
        N = '048'
        s = Html.join('images', 'diputados', '%dl' % L, '%s.jpg' % N)
        self.assert_('images' in s)
        self.assert_('diputados' in s)
        self.assert_('6l' in s)
        self.assert_('048.jpg' in s)


class UsoPanic(unittest.TestCase):

    def setUp(self):
        sys.stdout = StringIO()

    def tearDown(self):
        sys.stdout = sys.__stdout__

    def test_call_to_panic(self):
        self.assertRaises(
            SystemExit,
            Html.Panic,
            'Algo fue horriblemente mal',
            )
        buffer = sys.stdout.getvalue()
        self.assert_('Algo fue horriblemente mal' in buffer)


class Uso(unittest.TestCase):

    def setUp(self):
        sys.stdout = StringIO()

    def tearDown(self):
        sys.stdout = sys.__stdout__

    def test(self):
        '''Ejempo de uso.
        '''
        Html.Header()
        Html.Begin('Demostracion')
        print('Probando listas')
        print('\tConstructor')
        ul = Html.UL(['uno','dos','tres'])
        print (ul)
        print ('\tappend')
        ul.append('cuatro')
        print (ul)

        print('Probando <tt>Tabla</tt> - Clase TABLE, Metodos AddCol, AddRow')
        Tabla = Html.TABLE()
        Tabla.TR('uno', 'dos')
        Tabla.TR('tres', 'cuatro')
        print (Tabla)

    def test_tr(self):
        '''Probando funcion tr(*items).
        '''
        self.assertEqual(
            Html.tr('uno', 'dos'),
            '<tr valign="top">\n\t<td>uno</td>\n\t<td>dos</td>\n</tr>'
            )

        self.assertEqual(
            Html.tr('tres', 4),
            '<tr valign="top">\n'
            '\t<td>tres</td>\n'
            '\t<td>4</td>\n'
            '</tr>'
            )
        # self.printSalida()
        Html.End()


class Prueba_HtmlTags(unittest.TestCase):

    def test_Href(self):
        '''Prueba de la clase Href.
        '''
        link = Html.Href('http://www.python.org', 'Python Homepage')
        s = str(link)

    def test_HrefSinURL(self):
        '''Uso de Href con texto y sin URL.
        '''
        link = Html.Href(None, 'Python Homepage')
        self.assertEqual(str(link),  'Python Homepage')

    def test_HrefSinTexto(self):
        '''Uso de Href sin texto y con URL.
        '''
        link = Html.Href('http://www.python.org/')
        self.assertEqual(str(link),  '<a href="http://www.python.org/" target="_top">http://www.python.org/</a>')

class Prueba_Error(unittest.TestCase):
    def setUp(self):
        sys.stdout = StringIO()

    def tearDown(self):
        sys.stdout = sys.__stdout__

    def test_sinParametros(self):
        '''Llamada a Html.Error (sin parámetro opcional.)
        '''
        self.assertRaises(SystemExit, Html.Error, 'Mensaje del error')

    def test_conParametros(self):
        '''Llamada a Html.Error (con parámetro opcional tipo texto.)
        '''
        self.assertRaises(
            SystemExit,
            Html.Error,
            'Mensaje del error',
            'Par&aacute;metro opcional'
            )


class NavBarYear(unittest.TestCase):

    def test_use(self):
        hoy = datetime.date.today()
        s = Html.nav_bar_year('index.py', hoy.year)

    def test_fuera_de_rango(self):
        self.assertRaises(ValueError, Html.nav_bar_year,
            'index.py', 2016, 
            from_year=2015, 
            to_year=2000
            )

    def test_limite_superior(self):
        s = Html.nav_bar_year('index.py', 2015, from_year=2015, to_year=2012)

    def test_limite_inferior(self):
        s = Html.nav_bar_year('index.py', 2012, from_year=2015, to_year=2012)

    def test_podemos_equivocarnos_en_especifiar_el_rango(self):
        s1 = Html.nav_bar_year('index.py', 2016, from_year=2016, to_year=2000)
        s2 = Html.nav_bar_year('index.py', 2016, from_year=2000, to_year=2016)
        self.assertEqual(s1, s2)


# test paneles

def test_base_panel():
    output = ''
    try:
        _stdout = sys.stdout
        sys.stdout = StringIO()

        with Html.PanelBase('test base panel') as p:
            p.print('<p>hola</p>')
        output = sys.stdout.getvalue()
        assert '<div class="panel panel-default">' in output
        assert '<div class="panel-heading">' in output
        assert '<h3 class="panel-title">test base panel</h3>' in output
        assert '<p>hola</p>' in output
    finally:
        sys.stdout = _stdout


def test_panel():
    output = ''
    try:
        _stdout = sys.stdout
        sys.stdout = StringIO()
        with Html.Panel('test panel') as p:
            p.print('<p>hola</p>')
        output = sys.stdout.getvalue()
        assert '<div class="panel panel-default">' in output
        assert '<div class="panel-heading">' in output
        assert '<h3 class="panel-title">test panel</h3>' in output
        assert '<div class="panel-body">' in output
        assert '<p>hola</p>' in output
    finally:
        sys.stdout = _stdout


if __name__ == '__main__': 
    pytest.main()
