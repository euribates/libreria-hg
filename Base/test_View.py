#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import re
import pytest
import unittest
import datetime
from Libreria.Base import View


class MultiplesEnlaces(unittest.TestCase):

    def test_as_multiples_enlaces(self):
        texto = 'Enlaza las palabras Mazinger y Voltron.'
        patrones = [
            ('Mazinger', 'http://es.wikipedia.org/wiki/Mazinger'),
            ('Voltron', 'http://es.wikipedia.org/wiki/Voltron'),
            ]
        self.assertEqual(
            View.as_multiples_enlaces(texto, patrones),
            'Enlaza las palabras ' \
            '<a href="http://es.wikipedia.org/wiki/Mazinger">Mazinger</a> y ' \
            '<a href="http://es.wikipedia.org/wiki/Voltron">Voltron</a>.'
            )

    def test_as_multiples_enlaces_no_matches(self):
        texto = u'Enlaza las palabras Mazinger y Voltron.'
        patrones = [
            (u'Gundam', u'http://es.wikipedia.org/wiki/Gundam'),
            (u'Neon Genesis Evangelion', u'http://es.wikipedia.org/wiki/Evangelion'),
            ]
        self.assertEqual(View.as_multiples_enlaces(texto, patrones), texto)


class Test_AsHtml(unittest.TestCase):

    def test(self):
        self.assertEqual(
            View.AsHtml('hola\nmundo'),
            'hola\n<br>\nmundo\n<br>'
            )

    def test_tabs(self):
        self.assertEqual(
            View.AsHtml('\tmundo'),
            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mundo\n<br>'
            )


class AsAviso(unittest.TestCase):

    def test_message_in_output(self):
        aviso = View.AsAviso('hola, mundo')
        self.assertTrue('hola, mundo' in aviso)

class View_AsAsNumeralEuros(unittest.TestCase):
    def test_numeros(self):
        self.assertEqual(View.AsNumeralEuros(1170), 'mil ciento setenta euros')
        self.assertEqual(View.AsNumeralEuros(1170.34), 'mil ciento setenta euros con treinta y cuatro céntimos')

class View_AsFecha(unittest.TestCase):
    def test_Float(self):
        '''Llamada a View.AsFecha con un valor float.
        '''
        self.assertEqual(
            View.AsFecha(7489574.685)
            , '<i style="color: #AAAAAA;">(28/3/1970)</i>'
            )

    def test_None(self):
        '''Llamada a View.AsFecha con un valor nulo.
        '''
        self.assertEqual(
            View.AsFecha(None)
            , '---'
            )

    def test_String(self):
        '''Llamada a View.AsFecha con un valor de tipo string.
        '''
        self.assertEqual(
            View.AsFecha('28/3/1970')
            , '<i style="color: #AAAAAA;">(28/3/1970)</i>'
            )


# AsTabla


def test_as_tabla_empty():
    '''Llamada a View.AsTabla con un valor nulo o un vector vacio.
    '''
    assert View.AsTabla([]) == ''
    assert View.AsTabla(None) == ''

def test_as_table_passing_list_of_tuples():
    '''Llamada a View.AsTabla con un vactor de tuplas.
    '''
    s = View.AsTabla([(1, 2), (3,4)])
    assert '<table class="table">' in s
    assert '<td valign="top">1</td>' in s
    assert '<td valign="top">2</td>' in s
    assert '<td valign="top">3</td>' in s
    assert '<td valign="top">4</td>' in s
    assert '</table>' in s


# AsRows


def test_as_rows_empty():
    '''Llamada a View.AsRows con un valor nulo o un vector vacio.
    '''
    assert View.AsRows([]) == ''
    assert View.AsRows(None) == ''


def test_as_rows_passing_list_of_tuples():
    '''Llamada a View.AsRows con un vactor de tuplas.
    '''
    s = View.AsRows([(1, 2), (3,4)])
    assert '<table class="table">' in s
    assert '<td valign="top">1</td>' in s
    assert '<td valign="top">2</td>' in s
    assert '<td valign="top">3</td>' in s
    assert '<td valign="top">4</td>' in s
    assert '</table>' in s


class AsLink(unittest.TestCase):

    def test_solo_enlace(self):
        '''View.AsLink con un sólo parámetro (url).
        '''
        assert View.AsLink(u'http://www.python.org/') ==  \
            '<a href="http://www.python.org/">http://www.python.org/</a>'

    def test_enlace_y_descripcion(self):
        '''View.AsLink con dos parámetros (url, descripción).
        '''
        assert  View.AsLink('http://www.python.org/', 'Python') == \
            '<a href="http://www.python.org/">Python</a>'

    def test_enlace_nulo(self):
        '''Llamada a View.AsLink con enlace nulo.'''
        assert View.AsLink('', 'Python') == 'Python'
        assert View.AsLink(None, 'Python') == 'Python'


class Escape(unittest.TestCase):
    '''Pruebas a Libreria.Base.escape.'''

    def test_escape_html_text(self):
        self.assertEqual(
            View.escape('This is <cool> & <fun>'),
            'This is &lt;cool&gt; &amp; &lt;fun&gt;'
            )

    def test_no_change(self):
        assert View.escape('Must pass untouched') == 'Must pass untouched'
    


class AsHora(unittest.TestCase):
    '''Ver un numero de minutos en formato HH:MM.
    '''

    def test_con_una_string(self):
        '''Llamada a View.AsHora() (con una string).'''
        self.assertEqual(View.AsHora('07:34'), '<b class="HORA">07:34</b>')
        self.assertEqual(View.AsHora(u'07:34'), '<b class="HORA">07:34</b>')

    def test_Valores_Conocidos(self):
        '''Llamada a View.AsHora() (con valores positivos).'''
        self.assertEqual(View.AsHora(1), '<b class="HORA">00:01</b>')
        self.assertEqual(View.AsHora(420), '<b class="HORA">07:00</b>')
        self.assertEqual(View.AsHora(1380), '<b class="HORA">23:00</b>')
        self.assertEqual(View.AsHora(1439), '<b class="HORA">23:59</b>')

    def test_Valores_Negativos(self):
        '''Llamada a View.AsHora() (con valores negativos).'''
        self.assertEqual(View.AsHora(-1), '<b class="HORA ENNEGATIVO">-00:01</b>')
        self.assertEqual(View.AsHora(-420), '<b class="HORA ENNEGATIVO">-07:00</b>')
        self.assertEqual(View.AsHora(-1380), '<b class="HORA ENNEGATIVO">-23:00</b>')
        self.assertEqual(View.AsHora(-1439), '<b class="HORA ENNEGATIVO">-23:59</b>')


'''Batería de pruebas de View.AsFilesize().
'''

def test_as_filesize_known_values():
    '''Llamada a View.AsFilesize() (con valores conocidos).
    '''
    assert View.as_filesize(123) == '123 Bytes'
    assert View.as_filesize(420) == '420 Bytes'
    assert View.as_filesize(1380) == '1.35 KB.'
    assert View.as_filesize(14603) == '14.26 KB.'
    assert View.as_filesize(63488) == '62.00 KB.'
    assert View.as_filesize(874943) == '854.44 KB.'
    assert View.as_filesize(1874943) == '1.79 MB.'


'''Batería de pruebas de View.AsNumero().
'''

class View_AsNumero(unittest.TestCase):

    def __init__(self, name):
        self.patHtml = re.compile('<[^>]+>')
        unittest.TestCase.__init__(self, name)

    def cleanHtml(self, s):
        return self.patHtml.sub('', s)

    def test_Valores_Conocidos(self):
        '''Llamada a View.AsNumero (con valores conocidos).
        '''
        self.assertEqual(self.cleanHtml(View.AsNumero(-1)), '-1')
        self.assertEqual(self.cleanHtml(View.AsNumero(0)), '0')
        self.assertEqual(self.cleanHtml(View.AsNumero(10)), '10')
        self.assertEqual(self.cleanHtml(View.AsNumero(100)), '100')
        self.assertEqual(self.cleanHtml(View.AsNumero(1000)), '1.000')
        self.assertEqual(self.cleanHtml(View.AsNumero(10000)), '10.000')
        self.assertEqual(self.cleanHtml(View.AsNumero(100000)), '100.000')
        self.assertEqual(self.cleanHtml(View.AsNumero(1000000)), '1.000.000')
        self.assertEqual(self.cleanHtml(View.AsNumero(10000000)), '10.000.000')
        self.assertEqual(self.cleanHtml(View.AsNumero(100000000)), '100.000.000')
        self.assertEqual(self.cleanHtml(View.AsNumero(1000000000)), '1.000.000.000')
        self.assertEqual(self.cleanHtml(View.AsNumero(49034.45283)), '49.034,45')
        # Sin marcas Html
        self.assertEqual(self.cleanHtml(View.AsNumero(-1)), '-1')
        self.assertEqual(View.AsNumero(0, html_markup=False), '0')
        self.assertEqual(View.AsNumero(10, html_markup=False), '10')
        self.assertEqual(View.AsNumero(100, html_markup=False), '100')
        self.assertEqual(View.AsNumero(1000, html_markup=False), '1.000')
        self.assertEqual(View.AsNumero(10000, html_markup=False), '10.000')
        self.assertEqual(View.AsNumero(100000, html_markup=False), '100.000')
        self.assertEqual(View.AsNumero(1000000, html_markup=False), '1.000.000')
        self.assertEqual(View.AsNumero(10000000, html_markup=False), '10.000.000')
        self.assertEqual(View.AsNumero(100000000, html_markup=False), '100.000.000')
        self.assertEqual(View.AsNumero(1000000000, html_markup=False), '1.000.000.000')
        self.assertEqual(View.AsNumero(49034.45283, html_markup=False), '49.034,45')

class AsBoolean(unittest.TestCase):
    '''Batería de pruebas de View.AsBoolean.
    '''

    patron_si = u'<b style="color:#18442D;">Si</b>' 
    patron_no = u'<b style="color:#321A26;">No</b>'

    def test_False(self):
        '''AsBoolean: El valor False se visualiza como No.'''
        self.assertEqual(View.as_boolean(False), AsBoolean.patron_no)

    def test_True(self):
        '''AsBoolean: El valor True se visualiza como Si.'''
        self.assertEqual(View.as_boolean(True), AsBoolean.patron_si)

    def test_N(self):
        '''AsBoolean: El valor `N` se visualiza como No
        '''
        self.assertTrue('No' in View.as_boolean('N'))

    def test_S(self):
        '''AsBoolean: El valor `S` se visualiza como Si
        '''
        self.assertTrue('Si' in View.AsBoolean('S'))

    def test_None(self):
        '''El valor None se visualiza como No.'''
        self.assertEqual(View.AsBoolean(None), AsBoolean.patron_no)

    def test_Zero(self):
        '''El valor Cero (0) se visualiza como No.'''
        self.assertEqual(View.AsBoolean(0), AsBoolean.patron_no)

    def test_Positive(self):
        '''El valor entero mayor que 0 se visualiza como Si.'''
        self.assertEqual(View.AsBoolean(23), AsBoolean.patron_si)

    def test_Negative(self):
        '''El valor entero menor que 0 se visualiza como Si.'''
        self.assertEqual(View.AsBoolean(-23), AsBoolean.patron_si)

    def test_EmptyString(self):
        '''El valor string vacio se visualiza como No.'''
        self.assertEqual(View.AsBoolean(''), AsBoolean.patron_no)

    def test_NonEmptyString(self):
        '''El valor string no vacio se visualiza como si.'''
        self.assertEqual(View.AsBoolean('hola'), AsBoolean.patron_si)

    def test_EmptyDict(self):
        '''Un diccionario vacio se visualiza como No.'''
        self.assertEqual(View.AsBoolean({}), AsBoolean.patron_no)

    def test_NonEmptyDict(self):
        '''Un diccionario no vacio se visualiza como Si.'''
        self.assertEqual(View.AsBoolean({'uno':1}), AsBoolean.patron_si)

    def test_EmptyArray(self):
        '''Un vector vacio se visualiza como No.'''
        self.assertEqual(View.AsBoolean([]), AsBoolean.patron_no)

    def test_NonEmptyArray(self):
        '''Un vector no vacio se visualiza como Si.'''
        self.assertEqual(View.AsBoolean([1,2]), AsBoolean.patron_si)

    def test_EmptyTuple(self):
        '''Una tupla vacio se visualiza como No.'''
        self.assertEqual(View.AsBoolean(()), AsBoolean.patron_no)

    def test_NonEmptyTupla(self):
        '''Una tupla no vacia se visualiza como Si.'''
        self.assertEqual(View.AsBoolean(('uno',1)), AsBoolean.patron_si)

class IBAN(unittest.TestCase):

    def test_as_iban(self):
        self.assertEqual(
            View.as_iban('ES2114650100722030876293'),
            'ES21 1465 0100 7220 3087 6293',
            )
            

class View_AsNumero(unittest.TestCase):
    '''Batería de pruebas de View.AsFilesize().
    '''

    def __init__(self, name):
        self.patHtml = re.compile('<[^>]+>')
        unittest.TestCase.__init__(self, name)

    def cleanHtml(self, s):
        return self.patHtml.sub('', s)

    def test_Valores_Conocidos(self):
        '''Llamada a View.AsNumero (con valores conocidos).
        '''
        self.assertEqual(self.cleanHtml(View.AsNumero(-1)), '-1,00')
        self.assertEqual(self.cleanHtml(View.AsNumero(0)), '0,00')
        self.assertEqual(self.cleanHtml(View.AsNumero(10)), '10,00')
        self.assertEqual(self.cleanHtml(View.AsNumero(100)), '100,00')
        self.assertEqual(self.cleanHtml(View.AsNumero(1000)), '1.000,00')
        self.assertEqual(self.cleanHtml(View.AsNumero(10000)), '10.000,00')
        self.assertEqual(self.cleanHtml(View.AsNumero(100000)), '100.000,00')
        self.assertEqual(self.cleanHtml(View.AsNumero(1000000)), '1.000.000,00')
        self.assertEqual(self.cleanHtml(View.AsNumero(10000000)), '10.000.000,00')
        self.assertEqual(self.cleanHtml(View.AsNumero(100000000)), '100.000.000,00')
        self.assertEqual(self.cleanHtml(View.AsNumero(1000000000)), '1.000.000.000,00')
        self.assertEqual(self.cleanHtml(View.AsNumero(49034.45283)), '49.034,45')

class Badge(unittest.TestCase):
    def test_as_badge(self):
        assert View.as_badge(100) == '<span class="badge">100</span>'


class AsHora(unittest.TestCase):

    def test_known_values(self):
        self.assertEqual(View.as_hora(0), '00:00')
        self.assertEqual(View.as_hora(1), '00:01')
        self.assertEqual(View.as_hora(59), '00:59')
        self.assertEqual(View.as_hora(60), '01:00')
        self.assertEqual(View.as_hora(61), '01:01')
        self.assertEqual(View.as_hora(719), '11:59')
        self.assertEqual(View.as_hora(720), '12:00')
        self.assertEqual(View.as_hora(721), '12:01')
        self.assertEqual(View.as_hora(1439), '23:59')

    def test_lower_limit(self):
        self.assertRaises(ValueError, View.as_hora, -1)
        self.assertRaises(ValueError, View.as_hora, -1000)

    def test_upper_limit(self):
        self.assertRaises(ValueError, View.as_hora, 1440)

class AsUnbreakable(unittest.TestCase):

    def test_known_values(self):
        self.assertEqual(View.as_unbreakable('Hola, mundo'), 'Hola,&nbsp;mundo')


class Ordinal(unittest.TestCase):

    def test_primero(self):
        assert View.as_ordinal(1) == 'primero'
        assert View.as_ordinal(1, is_fem=True) == 'primera'

    def test_segundo(self):
        assert View.as_ordinal(2) == 'segundo'
        assert View.as_ordinal(2, is_fem=True) == 'segunda'

    def test_tercero(self):
        assert View.as_ordinal(3) == 'tercero'
        assert View.as_ordinal(3, is_fem=True) == 'tercera'

    def test_cuarto(self):
        assert View.as_ordinal(4) == 'cuarto'
        assert View.as_ordinal(4, is_fem=True) == 'cuarta'

    def test_noveno(self):
        assert View.as_ordinal(9) == 'noveno'
        assert View.as_ordinal(9, is_fem=True) == 'novena'

    def test_decimo(self):
        assert View.as_ordinal(10) == 'décimo'
        assert View.as_ordinal(10, is_fem=True) == 'décima'

    def test_undecimo(self):
        assert View.as_ordinal(11) == 'undécimo'
        assert View.as_ordinal(11, is_fem=True) == 'undécima'

    def test_doudecimo(self):
        assert View.as_ordinal(12) == 'duodécimo'
        assert View.as_ordinal(12, is_fem=True) == 'duodécima'

    def test_decimotercero(self):
        assert View.as_ordinal(13) == 'décimotercero'
        assert View.as_ordinal(13, is_fem=True) == 'décimotercera'

    def test_decimooctavo(self):
        assert View.as_ordinal(18) == 'décimoctavo'
        assert View.as_ordinal(18, is_fem=True) == 'décimoctava'

    def test_19(self):
        assert View.as_ordinal(19) == 'décimonoveno'
        assert View.as_ordinal(19, is_fem=True) == 'décimonovena'

    def test_20(self):
        assert View.as_ordinal(20) == 'vigésimo'
        assert View.as_ordinal(20, is_fem=True) == 'vigésima'

    def test_21(self):
        assert View.as_ordinal(21) == 'vigésimo primero'
        assert View.as_ordinal(21, is_fem=True) == 'vigésimo primera'

    def test_28(self):
        assert View.as_ordinal(28) == 'vigésimo octavo'
        assert View.as_ordinal(28, is_fem=True) == 'vigésimo octava'

    def test_29(self):
        assert View.as_ordinal(29) == 'vigésimo noveno'
        assert View.as_ordinal(29, is_fem=True) == 'vigésimo novena'

    def test_30(self):
        assert View.as_ordinal(30) == 'trigésimo'
    
    def test_30_fem(self):
        assert View.as_ordinal(30, is_fem=True) == 'trigésima'


class UsoAsIconDays(unittest.TestCase):

    def setUp(self):
        self.as_icon_days = View.AsIconDays()
    
    def test_one_week_backwards(self):
        f = datetime.date.today() - datetime.timedelta(days=7)
        assert self.as_icon_days(f) == '\u2611'

    def test_yesterday(self):
        f = datetime.date.today() - datetime.timedelta(days=1)
        assert self.as_icon_days(f) == '\u2611'

    def test_today(self):
        hoy = datetime.date.today()
        assert self.as_icon_days(hoy) == '\u2610'

    def test_tomorrow(self):
        f = datetime.date.today() + datetime.timedelta(days=1)
        assert self.as_icon_days(f) == '\u2680'

    def test_today_plus_two(self):
        f = datetime.date.today() + datetime.timedelta(days=2)
        assert self.as_icon_days(f) == '\u2681'

    def test_today_plus_six(self):
        f = datetime.date.today() + datetime.timedelta(days=6)
        assert self.as_icon_days(f) == '\u2685'

    def test_today_plus_seven_or_more(self):
        for num_days in range(7, 21):
            f = datetime.date.today() + datetime.timedelta(days=num_days)
            assert self.as_icon_days(f) == '\u25A3'



if __name__ == '__main__': unittest.main()
