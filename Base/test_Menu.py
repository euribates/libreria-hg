#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import sys
from six.moves import StringIO

import unittest


from . import Menu


class Constructores(unittest.TestCase):

    def test_Menu(self):
        testMenu = Menu.MENU('Menu')
        assert testMenu.Titulo == 'Menu'

    def test_Submenu(self):
        testMenu = Menu.MENU('Menu')
        testSubmenu = Menu.SUBMENU(testMenu, 'Submenu', 'submenu.py')
        self.assertEqual(testSubmenu.Titulo, 'Submenu')

    def test_MenuItem(self):
        testMenu = Menu.MENU('Menu')
        testSubmenu = Menu.SUBMENU(testMenu, 'Submenu', 'submenu.py')
        testMenuItem = Menu.MENUITEM(testSubmenu, 'MenuItem', 'menuitem.py')
        assert testMenuItem.Titulo == 'MenuItem'


class Uso(unittest.TestCase):

    def test(self):
        os.environ['SCRIPT_NAME'] = '/cgi-bin/alta.py'
        testMenu = Menu.MENU('Inventario')
        mnAlta = Menu.SUBMENU(testMenu, 'Altas', 'alta.py')
        mnAltaEquipo = Menu.MENUITEM(mnAlta, 'Equipos', 'altaEquipo.py')
        mnAltaUsuario = Menu.MENUITEM(mnAlta, 'Usuario', 'altaUsuario.py')
        mnAltaTelefono = Menu.MENUITEM(mnAlta, 'Teléfono', 'altaTelefono.py')
        mnConsulta = Menu.SUBMENU(testMenu, 'Consultas', 'consulta.py')
        mnConsultaBusqueda = Menu.MENUITEM(
            mnConsulta, 'Búsquedas', 'busqueda.py'
            )
        mnConsultaEquipo = Menu.MENUITEM(
            mnConsulta, 'Equipos', 'consultaEquipo.py'
            )
        mnConsultaUsuario = Menu.MENUITEM(
            mnConsulta, 'Usuario', 'consultaUsuario.py'
            )
        mnConsultaTelefono = Menu.MENUITEM(
            mnConsulta, 'Teléfono', 'consultaTelefono.py'
            )
        mn_baja = Menu.SUBMENU(testMenu, 'Bajas', 'baja.py')
        assert mn_baja.Titulo == 'Bajas'
        sys.stdout = StringIO()
        testMenu.dump()
        sys.stdout = sys.__stdout__


class Varios_niveles(unittest.TestCase):

    def setUp(self):
        sys.stdout = StringIO()

    def test(self):
        Raiz = Menu.MENU('Raiz')
        Raiz.Activo = True
        Submenu = Menu.SUBMENU(Raiz, 'Submenu', 'submenu.py')
        Submenu.Activo = True
        Subsubmenu = Menu.SUBMENU(Submenu, 'Subsubmenu', 'subsubmenu.py')
        Subsubmenu.Activo = True
        Menuitem = Menu.MENUITEM(Subsubmenu, 'Menuitem', 'menuitem.py')
        Menuitem.Activo = True
        print(Raiz.SubMenus)
        print(Raiz.SubMenus[0].MenuItems)
        print(Raiz)

    def tearDown(self):
        sys.stdout = sys.__stdout__


if __name__ == '__main__':
    unittest.main()
