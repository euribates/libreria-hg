#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import pytest
import RTF


@pytest.fixture
def rtf_doc():
    return RTF.RTF('test_RTF.rtf')


def test_rtf_generation(rtf_doc):
    '''Generación de un documento tipo RTF.
    '''
    rtf_doc.Begin('Ejemplo de RTF.py', 'Módulo Libreria.Base.RTF')
    rtf_doc.Header('Ejemplo de documento generado con RTF.py')
    rtf_doc.h1('Cabecera Nivel 1')
    rtf_doc.h2('Cabecera Nivel 2')
    rtf_doc.h3('Cabecera Nivel 3')
    rtf_doc.h4('Cabecera Nivel 4')
    rtf_doc.h3('Otra cabecera de nivel 3, ahora con esdrújulas')
    rtf_doc.p('Primer párrafo.')
    rtf_doc.p('Segundo párrafo.')
    rtf_doc.p('Este es el tercer párrafo.')
    rtf_doc.p('Esto texto está en {}.'.format(rtf_doc.b('negrita')))
    rtf_doc.p('Esto texto está en {}.'.format(rtf_doc.i('itálica')))
    rtf_doc.End()
