#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import os
import time


class CacheBase:
    """Clase base para las caches de tablas o vistas.

    Las clases L{TableCache} y L{ViewCache} derivan de esta, sólo
    redefiniendo la forma en que obtiens los datos.
    """
    CACHE_DIR = os.path.join(os.path.sep, 'tmp', 'cache', 'web')

    def __init__(self, db, name=''):
        self.kernel = {}
        self.name = name or self.__class__.__name__
        self.db = db
        self.Keys = []
        self.Fields = []
        if not os.path.isdir(CacheBase.CACHE_DIR):
            os.makedirs(CacheBase.CACHE_DIR)
        self.filename = os.path.join(
            CacheBase.CACHE_DIR,
            '{}.cache'.format(self.name)
            )
        self.order = None

    def need_update(self):
        try:
            modificado = time.time() - os.path.getmtime(self.filename)
        except os.error:
            modificado = 360000
        if modificado > 3600 and self.db:  # Una hora: 60s x 60m
            return True
        else:
            return False

    def _checkTimestamp(self, refresh=False):
        if refresh or not os.path.exists(self.filename):
            self.refresh()
            return
        if self.need_update():
            self.refresh()
        else:
            self.retrieve()

    def __getitem__(self, key):
        return self.kernel[key]

    def has_key(self, k):
        return k.upper() in self.Keys

    def store(self):
        with open(self.filename, 'w') as f:
            json.dump(self.kernel, f)

    def retrieve(self):
        with open(self.filename, 'r') as f:
            self.kernel = json.load(f)

    def __len__(self):
        return len(self.kernel)

    def sql_select(self):
        raise NotImplementedError(
            'La clase derivada de CacheBase'
            ' debe sobrescribir sql_select'
            )

    def refresh(self):
        sql = self.sql_select()
        if not self.db.isConnected():
            self.db.Connect()
        keys_limit = len(self.Keys)
        self.kernel = {}
        for row in self.db.get_rows(sql):
            values = list(row.values())
            claves = values[:keys_limit]
            clave = tuple(claves) if len(claves) > 1 else claves[0]
            valores = values[keys_limit:]
            valor = tuple(valores) if len(valores) > 1 else valores[0]
            self.kernel[clave] = valor
        self.store()

    def addKey(self, key_name):
        self.Keys.append(key_name.upper())

    def keys(self):
        return self.Keys

    def items(self):
        return self.kernel.items()

    def values(self):
        return self.kernel.values()

    def addField(self, field_name):
        self.Fields.append(field_name.upper())

    def get(self, key, default=None):
        return self.kernel.get(key, default)


class TableCache(CacheBase):

    def sql_select(self):
        all_fields = ', '.join(self.Keys + self.Fields)
        buff = [
            'SELECT {}'.format(all_fields),
            'FROM {}'.format(self.name),
            ]
        if self.order:
            buff.append('ORDER BY {}'.format(self.order))
        return ' '.join(buff)


class ViewCache(CacheBase):

    def __init__(self, db, name, sql):
        super().__init__(db, name)
        self.sql = sql

    def sql_select(self):
        return self.sql
