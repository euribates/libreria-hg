#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import pytest
import unittest
import sys
import six

from . import Database
from .Cache import CacheBase
from .Cache import TableCache
from .Cache import ViewCache


if six.PY2:
    from mock import Mock
else:
    from unittest.mock import Mock


def test_create_base_cache():
    '''Probamos el constructor Base
    '''
    db = Mock(Database.DB)
    d = CacheBase(db, 'test_table')
    assert d.kernel == {}
    assert d.name == 'test_table'
    assert d.db == db
    assert d.Keys == []
    assert d.Fields == []


class TestOperaciones(unittest.TestCase):

    def setUp(self):
        self.db = Mock(Database.DB)
        self.tabla = TableCache(self.db, 'EMP')
        self.tabla.addKey('EMPNO')
        self.tabla.addField('ENAME')

    def test_SqlSelect(self):
        '''Llamada a la funcion SqlSelect -> String
        '''
        sql = self.tabla.sql_select()
        self.assertEqual(sql.upper(), 'SELECT EMPNO, ENAME FROM EMP')

    def test_refresh(self):
        '''Comprobacion del método refresh()
        '''
        self.db.get_rows = lambda sql: []
        self.db.isConnected = lambda: False
        self.tabla.refresh()
        self.assertTrue(self.db.Connect.called)

    def test_keys(self):
        '''Comprobacion del método keys() -> Array of keys
        '''
        self.assertTrue('EMPNO' in self.tabla.keys())
        self.assertTrue(len(self.tabla.keys()) == 1)

    def test_has_key(self):
        '''Comprobacion del método has_key() -> Boolean
        '''
        self.assertTrue(self.tabla.has_key('EMPNO'))


def test_db():
    fieldnames = ['empno', 'ename', 'job', 'dname']
    db = Mock(Database.DB)
    db.get_rows = lambda sql: [
        Database.Row(fieldnames, (7839, 'KING', 'PRESIDENT', 'ACCOUNTING')),
        Database.Row(fieldnames, (7782, 'CLARK', 'MANAGER', 'ACCOUNTING')),
        Database.Row(fieldnames, (7934, 'MILLER', 'CLERK', 'ACCOUNTING')),
        Database.Row(fieldnames, (7566, 'JONES', 'MANAGER', 'RESEARCH')),
        Database.Row(fieldnames, (7902, 'FORD', 'ANALYST', 'RESEARCH')),
        Database.Row(fieldnames, (7876, 'ADAMS', 'CLERK', 'RESEARCH')),
        Database.Row(fieldnames, (7369, 'SMITH', 'CLERK', 'RESEARCH')),
        Database.Row(fieldnames, (7788, 'SCOTT', 'ANALYST', 'RESEARCH')),
        Database.Row(fieldnames, (7521, 'WARD', 'SALESMAN', 'SALES')),
        Database.Row(fieldnames, (7844, 'TURNER', 'SALESMAN', 'SALES')),
        Database.Row(fieldnames, (7490, 'ALLEN', 'SALESMAN', 'SALES')),
        Database.Row(fieldnames, (7900, 'JAMES', 'CLERK', 'SALES')),
        Database.Row(fieldnames, (7698, 'BLAKE', 'MANAGER', 'SALES')),
        Database.Row(fieldnames, (7654, 'MARTIN', 'SALESMAN', 'SALES')),
        ]
    return db

@pytest.fixture
def view_employees():
    db = test_db()
    sql = Database.Select('empno, ename, job, dname')
    sql.From('emp')
    sql.LeftJoin('dept', 'emp.deptno = dept.deptno')
    vista = ViewCache(db, 'VISTA', sql)
    vista.addKey('empno')
    vista.addField('ename')
    vista.addField('job')
    vista.addField('dname')
    vista.refresh()
    return vista


def test_cache_view(view_employees):
    '''Probando uso de una CacheView
    '''
    assert len(view_employees) == 14
    (name, job, dept) = view_employees[7521]
    assert name == 'WARD'
    assert job == 'SALESMAN'
    assert dept == 'SALES'
        
@pytest.fixture
def table_employee():
    db = test_db()
    table = TableCache(test_db(), 'emp')
    table.addKey('empno')
    table.addField('ename')
    table.addField('job')
    table.addField('dname')
    table.refresh()
    return table


def test_cache_table(table_employee):
    assert len(table_employee) == 14
    (name, job, dept) = table_employee[7521]
    assert name == 'WARD'
    assert job == 'SALESMAN'
    assert dept == 'SALES'
