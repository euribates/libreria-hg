#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import pytest
import unittest

from . import Validar


class Excepciones(unittest.TestCase):

    def test(self):
        err = Validar.ErrorValidacion('nif', 'El valor no puede ser nulo')
        self.assertIn('El', str(err))
        self.assertIn('valor', str(err))
        self.assertIn('no', str(err))
        self.assertIn('puede', str(err))
        self.assertIn('ser', str(err))
        self.assertIn('nulo', str(err))

        err = Validar.ErrorValidacion('nif', 'El {} no puede ser nulo')
        self.assertIn('El', str(err))
        self.assertIn('nif', str(err))
        self.assertIn('no', str(err))
        self.assertIn('puede', str(err))
        self.assertIn('ser', str(err))
        self.assertIn('nulo', str(err))


def test_en_rango_values_out_of_limits():
    with pytest.raises(Validar.ErrorValorIncorrecto):
        Validar.EnRango(3, 4, 6)
        Validar.EnRango(7, 4, 6)


def test_en_rango_values_inside():
    Validar.EnRango(4, 4, 6)
    Validar.EnRango(5, 4, 6)
    Validar.EnRango(6, 4, 6)


def test_en_rango_several_cases():
    Validar.EnRango(3, 0, 10)
    Validar.EnRango(12.34, -100.0, 100.0)
    Validar.EnRango('F', 'A', 'Z')
    with pytest.raises(Validar.ErrorValorIncorrecto):
        Validar.EnRango(-3, 0, 10)
        Validar.EnRango(-30.0, -10.0, 10.0)
        Validar.EnRango('#', 'A', 'Z')


def test_verdadero_true():
    '''Llamada a Validar.verdadero (Con valor verdadero, pasa).
    '''
    Validar.verdadero(True, 'Ejemplo simplón')


def test_verdadero_false():
    '''Llamada a Validar.verdadero (Con valor falso, falla).
    '''
    with pytest.raises(Validar.ErrorValidacion):
        Validar.verdadero(False, 'Ejemplo simplón')


def test_son_iguales_true_cases():
    Validar.SonIguales(2, 2)
    Validar.SonIguales('hola', 'hola')
    Validar.SonIguales(0.0, 0.0)


def test_son_iguales_false_cases():
    with pytest.raises(Validar.ErrorValorIncorrecto):
        Validar.SonIguales(1, 0)
        Validar.SonIguales('hola', 'adios')
        Validar.SonIguales(1.0, 0.0)


def test_mayor_que_cero_true_cases():
    Validar.MayorQueCero(4738232)
    Validar.MayorQueCero(123)
    Validar.MayorQueCero(1)
    Validar.MayorQueCero(1.0)


def test_mayor_que_cero_false_cases():
    with pytest.raises(Validar.ErrorValorIncorrecto):
        Validar.MayorQueCero(0)
        Validar.MayorQueCero(-1)
        Validar.MayorQueCero(-1.0)
        Validar.MayorQueCero(-123)
        Validar.MayorQueCero(-4738232)


def test_no_nulo():
    Validar.NoNulo(4738232)
    Validar.NoNulo('Hola, mundo')
    Validar.NoNulo(1.1)
    Validar.NoNulo([1, 2, 3])
    Validar.NoNulo((1, 2, 3))
    Validar.NoNulo(7, 'Ejemplo con mensaje personalizado')
    with pytest.raises(Validar.ErrorValorNulo):
        Validar.NoNulo(None)


class NoVacio(unittest.TestCase):

    def test_no_vacio(self):
        Validar.NoVacio(4738232, 'ETIQUETA')
        Validar.NoVacio('Hola, mundo', 'ETIQUETA')
        Validar.NoVacio(1.1, 'ETIQUETA')
        Validar.NoVacio([1, 2, 3], 'ETIQUETA')
        Validar.NoVacio((1, 2, 3), 'ETIQUETA')

    def test_no_vacio_with_none(self):
        with pytest.raises(Validar.ErrorValorNulo):
            Validar.NoVacio(None)

    def test_no_vacio_with_zero(self):
        with pytest.raises(Validar.ErrorValorNulo):
            Validar.NoVacio(0)

    def test_NoVacio_StringVacia(self):
        with pytest.raises(Validar.ErrorValorNulo):
            Validar.NoVacio('')

    def test_NoVacio_ArrayVacia(self):
        with pytest.raises(Validar.ErrorValorNulo):
            Validar.NoVacio([])

    def test_NoVacio_DiccionarioVacio(self):
        with pytest.raises(Validar.ErrorValorNulo):
            Validar.NoVacio({})

    def test_NoVacio_TuplaVacia(self):
        '''Llamada a Validar.NoVacio con parámetro tupla vacia (()).'''
        with pytest.raises(Validar.ErrorValorNulo):
            Validar.NoVacio(tuple())


def test_sesion_nula():
    with pytest.raises(Validar.ErrorValorNulo):
        Validar.UsuarioValidado(None)


class IBAN(unittest.TestCase):

    def test_iban_correcto(self):
        Validar.IBAN('ES3421006769722201361562')

    def test_iban_incorrecto(self):
        self.assertRaises(
            Validar.ErrorValidacion,
            Validar.IBAN,
            'ES3421006769722201361563'  # Deberia acabar en 2
            )

    def test_iban_demasiado_corto(self):
        self.assertRaises(
            Validar.ErrorValidacion,
            Validar.IBAN,
            'ES342100676972220136156'
            )

    def test_iban_codigo_de_pais_incorrecto(self):
        self.assertRaises(
            Validar.ErrorValidacion,
            Validar.IBAN,
            'XX3421006769722201361562'
            )


# Tests for Validar.NIF function


def test_nif_handle_spaces_after_and_before():
    assert Validar.NIF("   43622239W") == "43622239W"
    assert Validar.NIF("43622239W    ") == "43622239W"
    assert Validar.NIF("   43622239W    ") == "43622239W"


def test_nif_empty():
    assert Validar.NIF('') == ''
    assert Validar.NIF(None) is None


def test_nif_no_letter():
    with pytest.raises(Validar.ErrorValidacion):
        Validar.NIF("43622239")


def test_nif_wrong_length():
    with pytest.raises(Validar.ErrorValidacion):
        Validar.NIF("4")
        Validar.NIF("4362239W")
        Validar.NIF("4343622239W")
        Validar.NIF("43436254289754782239W")


def test_nif_return_capitals():
    assert Validar.NIF("43622239w") == "43622239W"


def test_nif_wrong_letter():
    assert Validar.NIF("43622239W") == "43622239W"
    with pytest.raises(Validar.ErrorValidacion):
        Validar.NIF("43622239A")
        Validar.NIF("43622239B")
        Validar.NIF("43622239C")
        Validar.NIF("43622239D")
        Validar.NIF("43622239E")
        Validar.NIF("43622239F")
        Validar.NIF("43622239G")
        Validar.NIF("43622239H")
        Validar.NIF("43622239J")
        Validar.NIF("43622239K")
        Validar.NIF("43622239L")
        Validar.NIF("43622239M")
        Validar.NIF("43622239N")
        Validar.NIF("43622239P")
        Validar.NIF("43622239Q")
        Validar.NIF("43622239R")
        Validar.NIF("43622239S")
        Validar.NIF("43622239T")
        Validar.NIF("43622239V")
        Validar.NIF("43622239X")
        Validar.NIF("43622239Y")
        Validar.NIF("43622239Z")


def test_cif_right_values():
    '''Validaciones de CIF correctos.
    '''
    assert Validar.NIF('43622376R') == '43622376R'
    assert Validar.NIF('A58818501') == 'A58818501'
    assert Validar.NIF('B38897831') == 'B38897831'
    assert Validar.NIF('U90786997') == 'U90786997'
    assert Validar.NIF('E14836670') == 'E14836670'
    assert Validar.NIF('V3322602H') == 'V3322602H'
    assert Validar.NIF('W7742810J') == 'W7742810J'
    assert Validar.NIF('J5218843J') == 'J5218843J'
    assert Validar.NIF('R5401928F') == 'R5401928F'
    assert Validar.NIF('L5190465D') == 'L5190465D'
    assert Validar.NIF('U2533110I') == 'U2533110I'
    assert Validar.NIF('E69971661') == 'E69971661'
    assert Validar.NIF('V86341849') == 'V86341849'


def test_cif_wrong_values():
    '''Validaciones de CIF incorrectos.
    '''
    assert Validar.NIF('U90786997') == 'U90786997'
    with pytest.raises(Validar.ErrorValidacion):
        Validar.NIF("U90786990")
        Validar.NIF("U90786991")
        Validar.NIF("U90786992")
        Validar.NIF("U90786993")
        Validar.NIF("U90786994")
        Validar.NIF("U90786995")
        Validar.NIF("U90786996")
        Validar.NIF("U90786998")
        Validar.NIF("U90786999")


def test_nie_right_values():
    assert Validar.NIF('Y9960215X') == 'Y9960215X'
    assert Validar.NIF('Z9376441J') == 'Z9376441J'
    assert Validar.NIF('X5981019F') == 'X5981019F'
    assert Validar.NIF('X9138086W') == 'X9138086W'
    assert Validar.NIF('Z9770825Q') == 'Z9770825Q'
    assert Validar.NIF('Y4398258M') == 'Y4398258M'
    assert Validar.NIF('Z0297921F') == 'Z0297921F'
    assert Validar.NIF('X8538174E') == 'X8538174E'
    assert Validar.NIF('Z7637811E') == 'Z7637811E'
    assert Validar.NIF('X3336379E') == 'X3336379E'
    assert Validar.NIF('Z2835060Q') == 'Z2835060Q'
    assert Validar.NIF('Z2708009V') == 'Z2708009V'
    assert Validar.NIF('X7178663H') == 'X7178663H'
    assert Validar.NIF('Y3880379V') == 'Y3880379V'
    assert Validar.NIF('Y6049770W') == 'Y6049770W'
    assert Validar.NIF('Y1872353Y') == 'Y1872353Y'
    assert Validar.NIF('Y3735811G') == 'Y3735811G'
    assert Validar.NIF('X2858169M') == 'X2858169M'
    assert Validar.NIF('X7520496W') == 'X7520496W'
    assert Validar.NIF('Y0884503Y') == 'Y0884503Y'


def test_nif_right_values():
    assert Validar.NIF("43622239W") == "43622239W"
    assert Validar.NIF("45455934E") == "45455934E"
    assert Validar.NIF("75255374H") == "75255374H"
    assert Validar.NIF("10836680T") == "10836680T"
    assert Validar.NIF("42846272D") == "42846272D"
    assert Validar.NIF("42876901W") == "42876901W"
    assert Validar.NIF("78492126A") == "78492126A"
    assert Validar.NIF("78437330Q") == "78437330Q"
    assert Validar.NIF("78627318R") == "78627318R"
    assert Validar.NIF("42719946E") == "42719946E"
    assert Validar.NIF("45453223W") == "45453223W"
    assert Validar.NIF("76900385T") == "76900385T"
    assert Validar.NIF("52820390Q") == "52820390Q"
    assert Validar.NIF("13708019L") == "13708019L"
    assert Validar.NIF("41858906X") == "41858906X"
    assert Validar.NIF("78443925X") == "78443925X"
    assert Validar.NIF("42872539X") == "42872539X"
    assert Validar.NIF("41910481L") == "41910481L"
    assert Validar.NIF("42664763Q") == "42664763Q"
    assert Validar.NIF("78616203H") == "78616203H"
    assert Validar.NIF("29736254Z") == "29736254Z"
    assert Validar.NIF("42939985C") == "42939985C"
    assert Validar.NIF("78399590L") == "78399590L"
    assert Validar.NIF("41955864T") == "41955864T"
    assert Validar.NIF("41944003F") == "41944003F"
    assert Validar.NIF("42086170B") == "42086170B"
    assert Validar.NIF("78695010G") == "78695010G"
    assert Validar.NIF("00983900Y") == "00983900Y"
    assert Validar.NIF("42775621Z") == "42775621Z"
    assert Validar.NIF("52843288Y") == "52843288Y"
    assert Validar.NIF("42744386J") == "42744386J"
    assert Validar.NIF("42488448L") == "42488448L"
    assert Validar.NIF("42447795F") == "42447795F"
    assert Validar.NIF("42182796Z") == "42182796Z"
    assert Validar.NIF("78502258S") == "78502258S"
    assert Validar.NIF("78401847E") == "78401847E"
    assert Validar.NIF("42086009B") == "42086009B"
    assert Validar.NIF("78447918R") == "78447918R"
    assert Validar.NIF("78606486F") == "78606486F"
    assert Validar.NIF("42165875K") == "42165875K"
    assert Validar.NIF("43806365J") == "43806365J"
    assert Validar.NIF("24139298Q") == "24139298Q"
    assert Validar.NIF("43288864N") == "43288864N"
    assert Validar.NIF("28193510H") == "28193510H"
    assert Validar.NIF("78564777C") == "78564777C"
    assert Validar.NIF("42636501K") == "42636501K"
    assert Validar.NIF("44307944P") == "44307944P"
    assert Validar.NIF("42079671K") == "42079671K"
    assert Validar.NIF("05351208M") == "05351208M"
    assert Validar.NIF("44704576M") == "44704576M"
    assert Validar.NIF("43269051W") == "43269051W"
    assert Validar.NIF("52821799E") == "52821799E"
    assert Validar.NIF("02511023K") == "02511023K"
    assert Validar.NIF("45733796K") == "45733796K"
    assert Validar.NIF("43377401E") == "43377401E"
    assert Validar.NIF("41763113N") == "41763113N"
    assert Validar.NIF("52273555Y") == "52273555Y"
    assert Validar.NIF("42635765K") == "42635765K"
    assert Validar.NIF("43779067Q") == "43779067Q"
    assert Validar.NIF("41858555G") == "41858555G"
    assert Validar.NIF("42618037A") == "42618037A"
    assert Validar.NIF("42609384K") == "42609384K"
    assert Validar.NIF("42730140G") == "42730140G"
    assert Validar.NIF("41817940F") == "41817940F"
    assert Validar.NIF("30466602C") == "30466602C"
    assert Validar.NIF("43765861N") == "43765861N"
    assert Validar.NIF("42807385S") == "42807385S"
    assert Validar.NIF("78609229J") == "78609229J"
    assert Validar.NIF("42654396E") == "42654396E"
    assert Validar.NIF("78615190V") == "78615190V"
    assert Validar.NIF("41806707K") == "41806707K"
    assert Validar.NIF("78396076R") == "78396076R"
    assert Validar.NIF("10385791A") == "10385791A"
    assert Validar.NIF("42810011L") == "42810011L"
    assert Validar.NIF("42741984A") == "42741984A"
    assert Validar.NIF("78638742V") == "78638742V"
    assert Validar.NIF("42811965H") == "42811965H"
    assert Validar.NIF("42078009S") == "42078009S"
    assert Validar.NIF("42427210F") == "42427210F"
    assert Validar.NIF("78555324C") == "78555324C"
    assert Validar.NIF("42759465G") == "42759465G"
    assert Validar.NIF("45456902R") == "45456902R"
    assert Validar.NIF("43266241K") == "43266241K"
    assert Validar.NIF("45707720G") == "45707720G"
    assert Validar.NIF("78621696Z") == "78621696Z"
    assert Validar.NIF("52846801T") == "52846801T"
    assert Validar.NIF("41772998F") == "41772998F"
    assert Validar.NIF("42901912N") == "42901912N"
    assert Validar.NIF("42433332B") == "42433332B"
    assert Validar.NIF("47789127A") == "47789127A"
    assert Validar.NIF("42717857A") == "42717857A"
    assert Validar.NIF("78496738S") == "78496738S"
    assert Validar.NIF("78720501B") == "78720501B"
    assert Validar.NIF("42186746P") == "42186746P"
    assert Validar.NIF("41854942W") == "41854942W"
    assert Validar.NIF("78487668F") == "78487668F"
    assert Validar.NIF("42695428E") == "42695428E"
    assert Validar.NIF("41903681G") == "41903681G"
    assert Validar.NIF("30413225A") == "30413225A"
    assert Validar.NIF("42770348P") == "42770348P"
