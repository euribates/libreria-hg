#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

"""
DESCRIPCIÓN
===========

Acceso a base de datos.

FUNCIONALIDAD
=============

    Funciones y clases para facilitar el
    acceso a base de datos. El objeto más
    importante de este módulo es L{DB},
    representación de la base de datos.
    Normalmente de este objeto se obtiene una única instancia,
    y para  garantizarlo se suele implementar el método
    C{Connect} en los modulos derivadas, que
    incorpore además los parámetros específicos
    de la conexión. Vease como  ejemplo
    L{Libreria.Internet.Database.Connect} o
    L{Libreria.Intranet.Database.Connect}.

    LA CLASE DB
    -----------

        Las métodos más usados de la clase L{DB} son:
        B{L{getRow<DB.getRow>}}, que
        obtiene una única fila de resultados de una consulta SQL (Un
        objeto de tipo L{Row}) y B{L{getRows<DB.getRows>}}, que obtiene una
        lista de filas de resultados de una consulta SQL
        (Una lista de objetos de la clase L{Row}).

        Ademas, para las operaciones de manipulación de
        datos C{UPDATE}, C{INSERT} y C{DELETE}
        se utiliza el método L{Execute<DB.Execute>}.

    LA CLASE Row
    ------------

        El objetivo de la clase L{Row} es mantener una representación
        interna de una fila de datos de una tabla, vista o consulta de la
        base de datos. Por eso los resultados de las operaciones de
        consulta como L{DB.getRow} y L{DB.getRows} devuelven objetos
        L{Row}. Por lo demás, se limita a facilitar el acceso
        a los datos de diferentes maneras: Como si fuera
        un diccionario, mediante el nombre del campo, o como si
        fuera una lista, indexada por orden. Tambien
        se puede acceder (por el momento B{sólo como lectura})
        a los campos como si fueran atributos del
        objeto (Vease el ejemplo en el L{Operador de
        cualificación<Row.__getattr__>}).

    LA CLASE TABLE
    --------------

        La clase L{TABLE} es una representación de una tabla
        de la base de datos. Permite definir subclases o
        instanciar objetos y reflejar en ellos la
        estructura de la tabla subyacente (Usando los
        métodos L{addKey<TABLE.addKey>} y
        L{addField<TABLE.addField>}). De
        este forma se  simplifican las operaciones de
        Insercion, Actualización y borrado (Métodos
        L{Insert<TABLE.Insert>},L{Update<TABLE.Update>} y
        L{Delete<TABLE.Delete>}) porque la clase contiene
        la información suficiente para generar las
        sentencias SQL necesarias para estas operaciones
        (Vease L{createInsert<TABLE.createInsert>},
        L{createUpdate<TABLE.createUpdate>} y
        L{createDelete<TABLE.createDelete>})

    LA CLASE FIELD (Y DERIVADAS)
    ----------------------------

        La clase L{TABLE} se compone de campos, representados
        por derivados de L{FIELD}. Estos campos siguen
        la misma estructura que la tabla o consulta a
        la base de datos que se intenta representar con
        el objeto L{TABLE}.


    ÚLTIMOS CAMBIOS
    ---------------

         - 5/oct/2009 Añadida la clase DB_MYSQL, para
           conectarse a bases de daos MySql

         - 27/06/2005 Añadida la clase L{Select}

         - 18/01/2005 21:01:08 Añadido el método get
           a las clases L{Row} (L{Row.get}) y L{TABLE}
           (L{TABLE.get}).

         - 17/01/2005 14:54:49 Mejorada la documentación.

         - 30/04/2003 14:16 Mejorada la clase L{Row},
           con un nuevo método L{Row.getValue};
           por  necesidades de compatibilidad con el modulo
           L{Libreria.Base.Form}.
           Además, se ha  añadido una unidad de pruebas,
           C{test_Database.py}

         - 28/10/2001 18:54 Añadida la función L{AsString}, Para
           facilitar la creación de SQL dinámico

         - 20/06/2901 18.03 Añadidas las funciones L{setType} y
           L{getType} al módulo, de forma que las fechas
           se formateen como C{ODBC} o C{ORACLE} segun corresponda.

         - La clase interna L{FIELD} se ha derivado en distintas
           clases, para facilitar la retpresentacion
           de sus valores en distintos SGDBR.

"""

import types
import html
import os
import sys
import re
import six
import datetime

from decimal import Decimal

from Libreria.Comun import Fechas

TRON = 0

DATABASE_TYPES = [
    'ODBC', 'ORACLE', 'XMLDB',
    'FIREBIRD', 'SQLSERVER', 'MYSQL',
    'POSTGRESQL', 'SQLITE',
    ]

DATABASE_TYPE = None
DateType = type(datetime.date.today())
DateTimeType = type(datetime.datetime.now())

if six.PY2:
    def memoize(f):
        "Memoization decorator for functions taking one or more arguments."
        class memodict(dict):

            def __init__(self, f):
                self.f = f

            def __call__(self, *args):
                return self[args]

            def __missing__(self, key):
                ret = self[key] = self.f(*key)
                return ret

        return memodict(f)
else:
    from functools import lru_cache
    memoize = lru_cache(maxsize=256)


def get_dsn(name):
    '''
    Obtener una entrada DSN de la tabla global

    :param name: Clave de la entrada en la tabla de DNS global.
    :return: La string DSN correspondiente
    '''
    _global_dsn = {
        'test': 'scott/tiger@oracle',
        }
    if name in _global_dsn:
        return _global_dsn[name]
    else:
        raise ValueError(
            'La entrada a la tabla DSN [{}] es incorrecta'.format(name)
            )


class DATABASE_ERROR(Exception):
    """
    Excepcion específica para errores de este módulo.
    """
    pass


def setType(tipo):
    """
    Definir el tipo de base de datos.

    @param tipo: Tipo de base de datos: Debe ser
                 uno de los valores definidos
                 en DATABASE_TYPES
    @type tipo: string
    """
    global DATABASE_TYPES, DATABASE_TYPE
    assert tipo in DATABASE_TYPES, 'Tipo de DB {} desconocido'.format(tipo)
    DATABASE_TYPE = tipo


set_type = setType  # PEP8


def getType():
    """Obtener el tipo de base de datos.

    @return: Tipo de base de datos: Debe ser
             de tipo C{'ODBC'} o C{'ORACLE'} o
             C{'XMLDB'}
    @rtype: string
    """
    return DATABASE_TYPE


get_type = getType  # PEP8


def AsHtml(text):
    """
    Escapa los caracteres especiales de HTML.

    @param text: Texto a modificar
    @return: Texto modificado
    @rtype: string
    """
    text = str(text)
    text = text.replace('&', '&amp;')
    text = text.replace('"', '&quot;')
    text = text.replace('<', '&lt;')
    text = text.replace('>', '&gt;')
    return text


def Tron():
    """Activa el modo DEBUG.
    """
    global TRON
    TRON = 1


def Troff():
    """Desactiva el modo DEBUG.
    """
    global TRON
    TRON = 0


def date_as_sqlite(fecha):
    if fecha:
        return "'{:04d}-{:02d}-{:02d}'".format(
            fecha.year,
            fecha.month,
            fecha.day,
            )
    else:
        return 'NULL'


def date_as_firebird(fecha):
    return "'{:04d}-{:02d}-{:02d}'".format(
        fecha.year,
        fecha.month,
        fecha.day,
        )


def date_as_mysql(f):
    if not f:
        return 'NULL'
    return "DATE '{:04d}-{:02d}-{:02d}'".format(f.year, f.month, f.day)


def date_as_postgresql(fecha):
    if not fecha:
        return 'NULL'
    return "date '%04d-%02d-%02d'" % fecha.getAMD()


def date_as_sqlserver(fecha):
    if not fecha:
        return 'NULL'
    return "'%02d/%02d/%04d'" % (fecha.month, fecha.day, fecha.year)


def date_as_oracle(fecha):
    """Devuelve una fecha en formato ORACLE.

    @param fecha: La fecha en cuestión, una instacia de la
                  clase L{Libreria.Comun.Fechas.FECHA} o un
                  objeto de tipo datetime.date.
    @return: Una string con la representacion de fecha
             usada por Oracle, por ejemplo:
             C{to_date('12/09/2003','DD/MM/AAAA')}
    @rtype: string
    @see L{Libreria.Comun.Fechas}
    """
    if not fecha:
        return 'NULL'
    if fecha.day and fecha.month and fecha.year:
        return "to_date('{:02d}/{:02d}/{:04d}', 'DD/MM/YYYY')".format(
            fecha.day,
            fecha.month,
            fecha.year,
            )
    else:
        return 'NULL'


def NextVal(db, sequence, tron=False):
    '''Dada una secuencia de oracle, postgresql, o un generador
    de Firebird, obtiene el siguiente valor.
    @param db Base de datos a utilizar, del tipo Database.
    @param sequence Nombre de la Secuencia.
    @return El numero correspondiente al siguiente valor de la secuencia.
    '''
    if db.Tipo == 'FIREBIRD':
        sql = 'SELECT Gen_id(%s, 1) FROM RDB$DATABASE;' % sequence
    elif db.Tipo == 'POSTGRESQL':
        sql = "SELECT nextval('seq_%s')" % sequence.lower()
    else:  # Oracle
        sql = 'SELECT %s.NextVal FROM Dual' % sequence
    if tron:
        print_sql(sql, output=tron)
    result = db.getRow(sql)[0]
    return result or 1


next_val = NextVal  # PEP8


def date_as_odbc(fecha):  # Antes AsODBC
    """Devuelve una fecha en formato ODBC.

    Ejemplo de uso:

    @param fecha: La fecha en cuestión, una instacia de la
                  clase L{Libreria.Comun.Fechas.FECHA}
    @return: Una string con la representacion de fecha usada
              por ODBC, por ejemplo C{{d'2003-09-12'}}
    @rtype: string
    @see Libreria.Comun.Fechas
    """
    if not fecha:
        return 'NULL'
    if fecha.day and fecha.month and fecha.year:
        return "{d '%04d-%02d-%02d'}" % (fecha.year, fecha.month, fecha.day)
    else:
        return 'NULL'


def AsTimestamp(ts):
    """Devuelve un timestamp (fecha+hora) en formato de string adecuada al
    gestor de la base de datos.

    Depende de la variable global C{DATABASE_TYPE}, definida al realizar
    un L{DB.Connect} o mediante las funciones L{setType}/L{getType}

    @param s: El timestamp en cuestión, una instacia de la
                  clase L{Libreria.Comun.Fechas.TIMETSTAMP}
    @return: Una string con la representacion del timestamp, dependiente
             del tipo de base de datos que se haya definido.
    @rtype: string
    @see: L{Libreria.Comun.Fechas}, L{setType}, L{getType}
    @raises ValueError: Si no es capaz de representar la fecha correctamente.
    """
    global DATABASE_TYPE
    if not ts:
        return 'NULL'
    if DATABASE_TYPE == 'ODBC':
        return ts.AsODBC()
    elif DATABASE_TYPE == 'ORACLE':
        return "to_date('{:04d}/{:02d}/{:02d} "  \
                "{:02d}:{:02d}:{:02d}'"  \
                ", 'YYYY/MM/DD HH24:MI:SS')".format(
                    ts.year, ts.month, ts.day,
                    ts.hour, ts.minute, ts.second
                    )
    elif DATABASE_TYPE == 'FIREBIRD':
        return "'%04d-%02d-%02d %02d:%02d:%02d'" % (
            ts.year, ts.month, ts.day,
            ts.hour, ts.minute, ts.second
            )
    elif DATABASE_TYPE == 'POSTGRESQL':
        return "timestamp '%04d-%02d-%02d %02d:%02d:%02d'" % (
            ts.year, ts.month, ts.day,
            ts.hour, ts.minute, ts.second
            )
    elif DATABASE_TYPE == 'MYSQL':
        return "TIMESTAMP '%04d-%02d-%02d %02d:%02d:%02d'" % (
            ts.year, ts.month, ts.day, ts.hour, ts.minute, ts.second
            )
    elif DATABASE_TYPE == 'SQLSERVER':
        return "'%04d-%02d-%02d %02d:%02d:%02d'" % (
            ts.year, ts.month, ts.day, ts.hour, ts.minute, ts.second
            )
    elif DATABASE_TYPE == 'SQLITE':
        return "'%04d-%02d-%02d %02d:%02d:%02d'" % (
            ts.year, ts.month, ts.day, ts.hour, ts.minute, ts.second
            )
    else:
        return "'%04d-%02d-%02d %02d:%02d:%02d'" % (
            ts.year, ts.month, ts.day,
            ts.hour, ts.minute, ts.second
            )


def AsFecha(s):
    """Devuelve una fecha en formato de string entendible por el
    gestor de la base de datos.

    Depende de la variable global C{DATABASE_TYPE}, definida al realizar
    un L{DB.Connect} o mediante las funciones L{setType}/L{getType}

    @param s: La fecha en cuestión, una instacia de la
                  clase L{Libreria.Comun.Fechas.FECHA}
    @return: Una string con la representacion de fecha, dependiente
             del tipo de base de datos que se haya definido.
    @rtype: string
    @see: L{Libreria.Comun.Fechas}, L{date_as_oracle}, L{date_as_odbc}
    @raises ValueError: Si no es capaz de representar la fecha correctamente.
    """
    global DATABASE_TYPE
    if not s:
        return 'NULL'
    if DATABASE_TYPE == 'ODBC':
        return date_as_odbc(s)
    elif DATABASE_TYPE == 'ORACLE':
        return date_as_oracle(s)
    elif DATABASE_TYPE == 'FIREBIRD':
        return date_as_firebird(s)
    elif DATABASE_TYPE == 'POSTGRESQL':
        return date_as_postgresql(s)
    elif DATABASE_TYPE == 'SQLSERVER':
        return date_as_sqlserver(s)
    elif DATABASE_TYPE == 'MYSQL':
        return date_as_mysql(s)
    else:
        raise ValueError(
            'El modelo de base de datos: "{}" '
            'es desconocido'.format(DATABASE_TYPE)
            )


def safe_string(s):
    if isinstance(s, six.binary_type):
        s = s.rstrip()
        s = s.replace(b"\r\n", b"\n")
        # replace cp1252 LEFT/RIGHT DOUBLE QUOTATION MARK
        s = s.replace(b'\x93', b'"')
        s = s.replace(b'\x94', b'"')
        s = s.replace(b"'", b"''")
    else:
        s = s.rstrip()
        s = s.replace(u"\r\n", u"\n")
        s = s.replace(u"'", u"''")
    return s


def as_string(s):
    """
    Retorna la string rodeada con los caracteres de
    comilla simple, o bien C{'NULL'}, si el parámetro
    de entrada es C{None}.

    Ejemplo de uso:

        >>> from Libreria.Base import Database
        >>> print Database.AsString("pepe")
        'pepe'
        >>> print Database.AsString('''Augustus Mc'Pherson''')
        'Augustus Mc''Pherson'
        >>> print Database.AsString(None)
        NULL


    @param s: Cadena de texto a transformar
    @type s: string
    @return: una string preparada para usarse en una sentencia SQL
    @rtype: string
    """
    if s and len(str(s)) > 0:
        return "'%s'" % safe_string(s)
    else:
        return 'NULL'


AsString = as_string  # PEP8


def as_integer(i):
    """Retorna una string con la representacion decimal del
    valor pasado, o C{NULL} si el parámetro de entrada es C{None}.

    @param i: Entero o equivalente
    @type i: integer
    @return: una string preparada para usarse en una sentencia SQL
    @rtype: string
    """
    return 'NULL' if i is None else "{:d}".format(i)


AsInteger = as_integer  # PEP8


def as_integers_list(nums):
    if nums is None:
        return 'NULL'
    else:
        buff = [AsInteger(n) for n in nums]
        return ', '.join(buff)


def as_list(iterable):
    buff = []
    for v in iterable:
        buff.append(str(new_field(v)))
    return ''.join(['(', ', '.join(buff), ')'])


def load_as_dict(db, sql):
    rows = db.get_rows(sql)
    return dict([(_[0], _[1]) for _ in rows])


def AsDecimal(valor, decimales=2):
    """Retorna una string con la representacion flotante del
    valor pasado, o C{NULL} si el parámetro de entrada es C{None}.

    @param valor: Valor a representar
    @type valor: Float, entero o equivalente
    @return: una string preparada para usarse en una sentencia SQL
    @rtype: string
    """
    if valor is not None:
        return str(round(valor, decimales))
    else:
        return 'NULL'


def AsFloat(valor):
    """Retorna una string con la representacion en coma flotante del
    valor pasado, o C{NULL} si el parámetro de entrada es C{None}.

    @param valor: Valor a representar
    @type valor: Float, entero o equivalente
    @return: una string preparada para usarse en una sentencia SQL
    @rtype: string
    """
    if valor is not None:
        return str(valor)
    else:
        return 'NULL'


def AsBoolean(v):
    """
    Retorna el valor 'S' o   'N', rodeada con los caracteres de
    comilla simple, segun el valor de v, o bien C{'NULL'}, si el parámetro
    de entrada es C{None}.

    Ejemplo de uso:

        >>> from Libreria.Base import Database
        >>> print Database.AsBoolean(True)
        'S'
        >>> print Database.AsBoolean(False)
        'N'
        >>> print Database.AsBoolean(None)
        NULL


    @param v: Valor booleano a representar
    @type v: boolean
    @return: una string preparada para usarse en una sentencia SQL
    @rtype: string
    """
    if v is None:
        return 'NULL'
    elif v:
        return "'S'"
    else:
        return "'N'"


class Row(object):
    """
    DESCRIPCIÓN
    ===========

    Clase para la representacion de una fila de datos de resultado. Puede ser
    accedida de múltiples formas, como una lista (Por posición), como un
    diccionario (por el nombre del campo) o simulando un atributo (por
    el nombre del campo.
    """

    def __init__(self, fieldsnames=None, fields=None, **kwargs):
        """Constructor.
        Si se especifica el parámetro C{fieldsnames}, es obligatorio
        incluir C{fields}, y viceversa.

        Ejemplo de uso:

            >>> # Normalmente no hace falta crear objetos tipo C{Row}
            >>> # Son resultado de métodos como L{getRow} o L{getRows}
            >>> from Libreria.Base import Database
            >>> r = Database.Row(['CODIGO', 'DESCRIPCION'], [1, 'Tenerife'])
            >>> print r.Codigo
            1
            >>> print r.Descripcion
            Tenerife
            >>>

        @param fieldsnames: Nombres de los campos que constituiran
                            el registro (opcional)
        @type fieldsnames: list
        @param fields: Valores iniciales de los campos (opcional)
        @type fields: list
        """
        self.Reset()
        if fieldsnames and fields:
            self._Names = [x.upper() for x in fieldsnames]
            self._Values = fields
        elif fieldsnames and fields is None:
            self._Names = [x.upper() for x in fieldsnames]
            self._Values = [None] * len(fieldsnames)
        if kwargs:
            for (name, value) in kwargs.items():
                self._Names.append(name.upper())
                self._Values.append(value)

    def update(self, d):
        for name in d:
            k = name.upper()
            v = d[name]
            if k in self._Names:
                self[k] = v
            else:
                self.addValue(k, v)

    def Reset(self):
        """Borra la definición del registro (En memoria).
        """
        self._Names = []
        self._Values = []

    def items(self):
        return zip(self._Names, self._Values)

    @classmethod
    def from_dict(sef, data):
        return Row(fieldsnames=None, fields=None, **data)

    def as_dict(self, exclude=None):

        def cast_dict(data):
            for name in data:
                if isinstance(data[name], Fechas.TIMESTAMP):
                    data[name] = data[name].AsDatetime()
                elif isinstance(data[name], Fechas.FECHA):
                    data[name] = data[name].AsDate()
                elif isinstance(data[name], six.text_type):
                    data[name] = data[name]
                elif isinstance(data[name], six.binary_type):
                    data[name] = data[name].decode('utf-8')
                elif isinstance(data[name], dict):
                    data[name] = cast_dict(data[name])
            return data

        keys = set([k.lower() for k in self.keys()])
        if exclude:
            keys -= set(exclude)
        data = dict([(k, self.get(k)) for k in keys])
        return cast_dict(data)

    def addValue(self, name, value):
        """Añadir un campo al registro, especificando nombre y valor.

        Por ejemplo, para incluir un parámetro de tipo C{integer}
        llamado C{altura}, y otro de tipo C{string}
        llamado C{nombre}, en el L{Row} C{Edificio}, se usaría:

            >>> import Libreria.Base.Database
            >>> Edificio = Libreria.Base.Database.Row()
            >>> Edificio.addValue('nombre', '')
            >>> Edificio.addValue('altura', 0)

        @param name: Nombre del campo
        @param value: Valor del campo, a partir del cual
                      se obtendrá tambien el tipo.
        """
        name = name.upper()
        self._Names.append(name)
        self._Values.append(value)
    add_value = addValue

    def getValue(self, key):
        """Obtiene el valor de un campo a a partir de su nombre.

        Ejemplo de uso:

            >>> import Libreria.Base.Database
            >>> Edificio = Libreria.Base.Database.Row()
            >>> Edificio.addValue('nombre', '')
            >>> Edificio.addValue('altura', 33)
            >>> print Edificio.getValue('ALTURA')
            33


        @param key: Nombre del campo
        @return: El valor del campo
        @rtype: *

        """
        key = key.upper()
        index = self._Names.index(key)
        return self._Values[index]

    def _find_index(self, key):
        if type(key) == int:
            return key
        else:
            key = key.upper()
            if key in self._Names:
                index = self._Names.index(key)
                return index
            else:
                return -1

    def get(self, key, default=None):
        '''La función get es similar a la usada para acceder a
        la clase como si fuera un diccionario, pero no eleva excepción
        en caso de que no esté definido el campo, sino que devuelve
        el valor definido por el parámetro C{default}.

        Ejemplo de uso:

            >>> import Libreria.Base.Database
            >>> Edificio = Libreria.Base.Database.Row()
            >>> Edificio.addValue('nombre', '')
            >>> Edificio.addValue('altura', 33)
            >>> print Edificio.get('ALTURA')
            33
            >>> print Edificio.get('NOEXISTE')
            None
            >>> print Edificio.get('NOEXISTE', 'Tururú')
            Tururú


        @param key:  El nombre del campo cuyo valor queremos obtener.

        @param default: Valor de retorno a utilizar en caso de que
                        no esté definido el campo C{key}.

        @return: El valor definido para C{key}, o C{default} si el campo
                 C{key} no existe.
        @rtype: *
        @see: L{__getitem__}
        '''
        index = self._find_index(key)
        if index >= 0:
            return self._Values[index]
        else:
            return default

    def pop(self, key, default=None):
        index = self._find_index(key)
        if index >= 0:
            value = self._Values[index]
            del self._Values[index]
            del self._Names[index]
            return value
        else:
            return default

    def setValues(self, **nameargs):
        """Inicializa los valores del registro, especificando los
        nombres y valores como parámetros.

        Por ejemplo, para poner los valores C{nombre} y C{altura} en
        un registro llamado C{edificio}, se podria hacer
        asi:

            >>> import Libreria.Base.Database
            >>> Edificio = Libreria.Base.Database.Row()
            >>> Edificio.setValues(nombre='Empire State Building', altura=87)
            >>> print Edificio.NOMBRE
            Empire State Building
            >>> print Edificio.ALTURA
            87

        @Note: Hace un L{Reset} implicito, por lo que todos los
               nombres y valores almacenados con anterioridad
               se borrarán.
        @param nameargs: diccionario de nombres - valores con el
                         que alimentar el registro
        """
        self.Reset()
        for (name, value) in nameargs.items():
            self.addValue(name, value)

    def has_key(self, key):
        return key.upper() in self._Names

    def __contains__(self, key):
        return key.upper() in self._Names

    def values(self):
        return self._Values

    def keys(self):
        """Retorna los nombres de los campos que constituyen el registro.

        Los nombres siempre se devuelven en mayusculas, independientemente
        de como se definieran:

        Ejemplo de uso:

            >>> import Libreria.Base.Database
            >>> Edificio = Libreria.Base.Database.Row()
            >>> Edificio.addValue('Nombre', '')
            >>> Edificio.addValue('Altura', 0)
            >>> print Edificio.keys()
            ['NOMBRE', 'ALTURA']
        """
        return self._Names

    def __repr__(self):
        """Retorna una descripcion textual del objeto.
        """
        return 'Row([{}], [{}])'.format(
            ', '.join(["'{}'".format(_.lower()) for _ in self._Names]),
            ', '.join([repr(_) for _ in self._Values]),
            )

    def __getitem__(self, key):
        """Permite usar un L{Row} como si fuera un diccionario o una lista.
        @param key:    Nombre o slice o número de la columna
        @see: L{get}
        """
        if isinstance(key, int):
            return self._Values[key]
        elif isinstance(key, slice):
            result = Row()
            result.Reset()
            desde = key.start or 0
            hasta = key.stop or len(self._Values)
            for index in range(desde, hasta):
                name = self._Names[index]
                valor = self._Values[index]
                result.add_value(name, valor)
            return result
        else:
            return self.getValue(key)

    def __setitem__(self, key, value):
        """Permite usar un L{Row} como si fuera un diccionario.

        Ejemplo:
            >>> import Libreria.Base.Database
            >>> Edificio = Libreria.Base.Database.Row()
            >>> Edificio.addValue('Nombre', '')
            >>> Edificio.addValue('Altura', 0)
            >>> Edificio['NOMBRE'] = 'Brooklin Bridge'
            >>> print Edificio['NOMBRE']
            Brooklin Bridge

        @param key: Clave del diccionario
        @param value: Valor a asignar.
        """
        if isinstance(key, int):
            self._Values[key] = value
        else:
            key = key.upper()
            index = self._Names.index(key)
            self._Values[index] = value

    def __eq__(self, other):
        """Permite comparar dos Rows.
        """
        if isinstance(other, Row):
            return (self._Names, self._Values) == (other._Names, other._Values)
        raise ValueError(
            'La clase Row no se puede comparar con {}'.format(type(other))
            )

    def __getattr__(self, nombre):
        """
        Permite usar los objetos L{Row} especificando los
        nombres como si fueran atributos

        Se puede especificar el nombre del campo en mayusculas,
        minúsculas, o cualquier variante, independientemente de
        como se haya definido el campo.

        Ejemplo:

            >>> import Libreria.Base.Database
            >>> Edificio = Libreria.Base.Database.Row()
            >>> Edificio.addValue('Nombre', 'Sears Tower')
            >>> Edificio.addValue('Altura', 442)
            >>> print Edificio.NOMBRE, Edificio.ALTURA
            Sears Tower 442
            >>> print Edificio.Nombre, Edificio.Altura
            Sears Tower 442

        @attention: Por el momento, sólo se puede usar el acceso como
                    atributos para leer los valores, no para modificarlos.
        @param nombre: Nombre del campo.
        @return: Valor del campo
        """
        try:
            index = self._Names.index(nombre.upper())
        except ValueError:
            raise AttributeError(nombre)
        return self._Values[index]

    def __str__(self):
        """Retorna una representacion textual (HTML) del registro.

        Ejemplo de uso:

            >>> import Libreria.Base.Database
            >>> Edificio = Libreria.Base.Database.Row()
            >>> Edificio.addValue('Nombre', 'Sears Tower')
            >>> Edificio.addValue('Altura', 442)
            >>> print Edificio
            <ul>
            <li>NOMBRE=Sears Tower (&lt;type 'str'&gt;)
            <li>ALTURA=442 (&lt;type 'int'&gt;)
            </ul>

        """
        if self._Names == []:
            return 'Empty Row'
        else:
            result = ['<ul>']
            for index in range(0, len(self._Names)):
                Nombre = self._Names[index]
                Valor = self._Values[index]
                result.append(
                    '<li>{}={} ({})'.format(
                        Nombre,
                        Valor,
                        html.escape(str(type(Valor)))
                        )
                    )
            result.append('</ul>')
            return '\n'.join(result)

    def __len__(self):
        """
        Devuelve el numero de campos de un registro.

        @return: Número de campos que constituyen el objeto L{Row}
        @rtype: integer
        """
        return len(self._Names)

    def cast(self, field_name, functor):
        key = field_name.upper()
        index = self._Names.index(key)
        value = self._Values[index]
        if value is not None:
            self._Values[index] = functor(value)

    def exclude(self, *names):
        result = Row()
        names = [_.upper() for _ in names]
        for name in self._Names:
            if name not in names:
                result.addValue(name, self[name])
        return result

    def include(self, *names):
        result = Row()
        names = [_.upper() for _ in names]
        for name in self._Names:
            if name in names:
                result.addValue(name, self[name])
        return result


ROW = Row  # PEP8


class DB(object):
    """
    DESCRIPCIÓN
    ===========

    Representación como objeto de la base de datos.

    Esta es la clase que mantiene la conexión con el gestor de la
    base de datos, y proporciona ciertos métodos que facilitan
    el acceso y manipulación de los datos, a nivel de sentencias
    SQL.

    Los métodos más importantes son:

        - El L{Constructor<__init__>}

        - L{Connect} e L{isConnected}, para la conexión al SGBD.

        - L{getRows} y L{getRow}, para obtenr los resultados de
            las consultas C{SELECT}.

        - L{Execute} para Ejecutar operaciones
            SQL que no sean de consulta, sino de modificación
            de datos, es decir, C{UPDATE}, C{INSERT} o
            C{DELETE}.
    """

    def __init__(self, dsn=None, autoconnect=True, tipo='ODBC'):
        """Constructor.

        @param dsn: I{Data Source Name} de la base de datos
                    a la que queremos conectarnos (Opcional).
        @type  dsn: string
        @param autoconnect: Flag de conexión. Si está a C{True}, y
                            se ha especificado un dsn, se intentará
                            realizar la conexión (Opcional).
        @type  autoconnect: boolean
        @param tipo: Tipo de conexión a la base de datos. (Opcional)
        @type  tipo: string
        @raise DATABASE_ERROR: Si no puede realizar la conexión.
        """
        self.DSN = dsn
        self.DBHandle = None
        self.Tipo = tipo
        if dsn and autoconnect:
            self.Connect(dsn, tipo)
        setType(tipo)

    def isConnected(self):
        '''Indica si el objeto está conectado a una base de
        datos o no.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> db = Database.DB('ORACLE/scott/tiger', autoconnect=False)
            >>> if not db.isConnected():
            ...     print 'No está conectado.'
            No está conectado.

        @return: C{True} si está conectado, C{False} en caso contrario.
        @rtype : boolean
         '''
        return self.DBHandle is not None

    def __str__(self):
        """Representación textual del objeto.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> db = Database.DB('ORACLE/scott/tiger', autoconnect=False)
            >>> print db
            [Clase DB(ODBC) DSN=ORACLE]

        """
        tipo = getType()
        if tipo == 'ODBC':
            return '[Clase DB(ODBC) DSN=%s]' % self.DSN.split('/')[0]
        elif tipo == 'XMLDB':
            (dsn, host) = self.DSN.split('@')
            return '[Clase DB(XMLDB) DSN=%s, HOST=%s]' % (dsn, host)
        elif tipo == 'ORACLE':
            return '[Clase DB(ORACLE) USR=%s]' % self.DSN.split('/')[1]
        elif tipo == 'SQLSERVER':
            return '[Clase DB_SQLSERVER]'
        elif tipo == 'FIREBIRD':
            return '[Clase DB_FIREBIRD DSN=%s]' % self.DSN
        elif tipo == 'MYSQL':
            return '[Clase DB_MYSQL]'
        else:
            return '[Clase DB(Tipo Desconocido)]'

    def Connect(self, dsn=None, tipo=None):
        """
        Realiza una conexión a la base de datos.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> db = Database.DB_ORACLE()
            >>> db.Connect('scott/tiger@oracle')
            >>> print db.isConnected()
            True

        @param dsn: I{Data Source Name} de la base de datos a
                    la que queremos conectarnos
        @type  dsn: string
        @param tipo: Puede ser C{'ODBC'}, C{'ORACLE'} o C{'XMLDB'} (En
                     el caso de usar xmldb, el C{dsn} debe
                     ser C{dsn@maquina}).
        @type  tipo: string
        @raise DATABASE_ERROR: Si no puede establecer la conexión.
        @see: L{Constructor<__init__>}
        """
        tipo = tipo or getType()
        if dsn:
            self.DSN = dsn
        setType(tipo)
        if tipo == 'ODBC':
            import odbc
            try:
                self.DBHandle = odbc.odbc(self.DSN)
            except Exception as err:
                dbname = dsn.split('/')[0]
                msg = '<br>DATABASE Error\n'  \
                      '<br>Error al conectar a {dbname}\n'  \
                      '<hr>\n'  \
                      '<P>Es posible que no se hayan definido'  \
                      ' correctamente los parámetros de'  \
                      ' conexión.\n'  \
                      ' Excepcion adicional: %s'.format(
                        dbname=dbname,
                        err=err,
                        )
                raise DATABASE_ERROR(msg)
        elif tipo == 'XMLDB':
            import xmldb.client
            (dsn, host) = self.DSN.split('@')
            self.DBHandle = xmldb.client.Database(host, dsn)
        elif tipo == 'ORACLE':
            import cx_Oracle
            (src, usr, pwd) = self.DSN.split('/')
            self.DBHandle = cx_Oracle.connect(usr, pwd, src)
        else:
            raise DATABASE_ERROR('''El tipo de conexion solicitada:
                "%s" es desconocido''' % tipo)

    def Disconnect(self):
        """Realiza la desconexión de la base de datos (Si estuviera conectada).

        Normalmente no hace falta llamar a esta función, ya que se encarga
        el L{Destructor<__del__>}.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> db = Database.DB_ORACLE('scott/tiger@oracle')
            >>> db.Disconnect()

        """
        if self.isConnected():
            self.DBHandle.close()
            self.DBHandle = None

    def Commit(self):
        pass
    commit = Commit  # PEP8

    def __del__(self):
        '''Destructor

        Realiza una desconexión de la base de datos, llamando al
        método L{Disconnect}, antes de liberar la variable.
        '''
        self.Disconnect()

    def cursor(self):
        """Retorna un cursor para acceso a la base de datos.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> db = Database.DB_ORACLE('scott/tiger@oracle')
            >>> cur = db.cursor()
            >>> r = cur.execute('Select count(*) from EMP')

        se recomienda, en lo posible, usar las funciones L{getRow},
        L{getRows} y L{Execute} en ves de operar directamente
        con cursores.

        @return: Un cursor asociado a la base de datos.
        @rtype : cursor
        """
        return self.DBHandle.cursor()

    def Execute(self, sql, parametros=[]):
        """Ejecuta una sentencia SQL.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> db = Database.DB_ORACLE('scott/tiger@oracle')
            >>> result = db.Execute('Update EMP set SAL=800 where SAL = 800')

        @param sql: Sentencia sql
        @type  sql: string
        @return: El resultado de la ejecucion
        @rtype:  integer
        """
        if isinstance(sql, six.binary_type):
            sql = sql.decode('utf-8')
        else:
            sql = str(sql)
        cur = self.DBHandle.cursor()
        if parametros:
            sql = self.parametrizaSql(sql)
            result = cur.execute(sql, parametros)
        else:
            result = cur.execute(sql)
        cur.close()
        self.Commit()
        return result

    execute = Execute  # PEP8

    def getRows(self, sql, parametros=[], num_max_rows=0, cast=None):
        """
        Ejecuta una consulta SQL, y retorna una lista de filas (Objetos
        del tipo L{Row}) conteniendo el resultado de la sentencia, o
        C{None} si no hay ningún resultado.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> db = Database.DB_ORACLE('scott/tiger@oracle')
            >>> rows = db.getRows('SELECT * FROM Dept ORDER BY Deptno')
            >>> print len(rows)
            4
            >>> for row in rows:
            ...     print row.Deptno, row.Dname, row.Loc
            10 ACCOUNTING NEW YORK
            20 RESEARCH DALLAS
            30 SALES CHICAGO
            40 OPERATIONS BOSTON

        @todo: El resultado, cuando la consulta no retorna ninguna fila,
               debería ser una lista vacia C{[]}, y no C{None}.
        @param sql: Sentencia sql
        @type sql:  string
        @param num_max_rows: Número máximo de filas que queremos
                            que nos sean devueltas. Es un valor opcional, si
                            no se especifica se supone que los queremos
                            todos (num_max_rows=0)
        @type num_max_rows:  integer
        @return: las filas resultado de la ejecucion, o C{None}
                 si ninguna fila de la base de datos
                 cumple las condiciones de la consulta
        @rtype: list o None.
        """
        sql = str(sql)
        FieldsNames = []
        if parametros:
            sql = self.parametrizaSql(sql)

        cur = self.DBHandle.cursor()
        if parametros:
            cur.execute(sql, parametros)
        else:
            cur.execute(sql)
        for i in cur.description:
            FieldsNames.append(i[0].upper())
        if num_max_rows:
            Buffer = cur.fetchmany(num_max_rows)
        else:
            Buffer = cur.fetchall()
        cur.close()
        cur = None
        self.DBHandle.commit()
        if len(Buffer) == 0:
            return []
        if cast is None:
            result = [Row(FieldsNames, list(tupla)) for tupla in Buffer]
        else:
            result = [cast(Row(FieldsNames, list(tupla))) for tupla in Buffer]
        return result

    get_rows = getRows

    def getRow(self, sql, parametros=None, cast=None):
        """Ejecuta una consulta SQL, y retorna un objeto de tipo
        L{Row}, conteniendo la fila resultante de la consulta,
        o C{None}, si la consulta no retorna ningún resultado.

        En caso de que la consulta devuelva más de una fila, el resultado
        sera sólo la primera.

        @param sql: Sentencia de consulta SQL.
        @type sql:  string
        @return: La fila resultado de la consulta, (En forma de
                objeto L{Row}), o C{None} si ninguna fila
                de la base de datos cumple las condiciones
                de la consulta
        @rtype: L{Row} o None
        """
        sql = str(sql)
        FieldsNames = []
        if parametros:
            sql = self.parametrizaSql(sql)
        cur = self.DBHandle.cursor()
        if parametros:
            cur.execute(sql, parametros)
        else:
            cur.execute(sql)
        fields = cur.fetchone()
        for i in cur.description:
            FieldsNames.append(i[0].upper())

        cur.close()
        cur = None
        self.DBHandle.commit()
        if not fields:
            return None
        else:
            if cast is None:
                return Row(FieldsNames, list(fields))
            else:
                return cast(Row(FieldsNames, list(fields)))

    # PEP8. Also returns {} instead of None
    def get_row(self, sql, parametros=None, cast=None):
        if not parametros:
            parametros = []
        if cast is None:
            return self.getRow(sql, parametros=parametros) or {}
        else:
            return cast(self.getRow(sql, parametros=parametros) or {})

    def parametrizaSql(self, sql):
        sql = sql.replace(r'%s', '?')
        return sql

    def get_escalar(self, sql, parametros=None, default=0):
        row = self.get_row(sql, parametros)
        if not row:
            return default
        return row[0] if row[0] else default

    def as_date(self, f):
        return date_as_odbc(f)


class DB_SQLITE(DB):

    def __init__(self, dsn=None, autoconnect=True):
        super(DB_SQLITE, self).__init__(dsn, autoconnect, 'SQLITE')

    def Connect(self, dsn, tipo='SQLITE'):
        import sqlite3
        self.DBHandle = sqlite3.connect(dsn)

    def as_date(self, f):
        return date_as_sqlite(f)


class DB_ORACLE(DB):

    class Counter(object):
        
        def __init__(self, inicio=0):
            self.Index = inicio
        
        def __call__(self, patron):
            self.Index += 1
            return ':%s' % self.Index

    def __init__(self, dsn=None, autoconnect=True):
        super(DB_ORACLE, self).__init__(dsn, autoconnect, 'ORACLE')
        self.patParametro = re.compile('%s')

    def Connect(self, dsn=None, tipo='ORACLE'):
        """
        Realiza una conexión a la base de datos.

        @param dsn: I{Data Source Name} de la base de datos a
                    la que queremos conectarnos, en forma:
                    user/password@machine
        @type  dsn: string
        @raise DATABASE_ERROR: Si no puede establecer la conexión.
        @see: L{Constructor<__init__>}
        """
        if dsn:
            self.DSN = dsn
            (credenciales, self.Nombre) = self.DSN.split('@')
            (self.Usuario, self.Password) = credenciales.split('/', 1)

        setType(tipo)
        import cx_Oracle
        try:
            self.DBHandle = cx_Oracle.connect(
                self.DSN,
                encoding="UTF-8",
                nencoding = "UTF-8",
                )
        except Exception as err:
            dbname = dsn.split('@')[1]
            msg = '<br>DATABASE Error'             \
                  '<br>Clase: {cls}'               \
                  '<br>Error al conectar a: {db}'  \
                  '<br>Mensaje de error: {err}'.format(
                    cls=self.__class__.__name__,
                    db=dbname,
                    err=err,
                    )
            raise DATABASE_ERROR(msg)

    def __str__(self):
        return 'Base de datos Oracle %s@%s' % (self.Usuario, self.Nombre)

    def next_val(self, sequence):
        sql = 'SELECT %s.NextVal FROM Dual' % sequence
        result = self.get_row(sql)[0]
        return result or 1
        
    def Commit(self):
        self.DBHandle.commit()

    def parametrizaSql(self, sql):
        counter = DB_ORACLE.Counter()
        sql = self.patParametro.sub(counter, sql)
        return sql

    def getRowsWithCLOB(self, sql, parametros=[], num_max_rows=0, tron=False):
        assert num_max_rows >= 0, 'parámetros num_max_rows incorrecto %s' % num_max_rows
        import cx_Oracle
        sql = str(sql)
        if parametros:
            sql = self.parametrizaSql(sql)
        cur = self.DBHandle.cursor()
        cur.execute(sql, parametros)
        firstRow = cur.fetchone()
        if not firstRow:
            return []
        Values = list(firstRow)

        Names = [x[0] for x in cur.description]
        Types = [x[1] for x in cur.description]
        index = 0
        Clob_Columns = []
        for index in range(len(Types)):
            if Types[index] == cx_Oracle.CLOB:
                Clob_Columns.append(index)
        index = 0
        for col in Values:
            if type(col) == cx_Oracle.LOB:
                Clob_Columns.append(index)
                Values[index] = str(Values[index])
            index += 1
        Result = []
        Result.append(Row(Names, Values))
        count = 1
        nextRow = cur.fetchone()
        while nextRow:
            Values = list(nextRow)
            count += 1
            for index in Clob_Columns:
                Values[index] = str(Values[index])
            Result.append(Row(Names, Values))
            if num_max_rows and num_max_rows == count:
                break
            nextRow = cur.fetchone()
        cur.close()
        cur = None
        return Result

    def as_date(self, f):
        return date_as_oracle(f)


    def CallProc(self, name, params):
        cur = self.cursor()
        cur.callproc(name, params)
        self.Commit()


class DB_MYSQL(DB):

    def __init__(self, dsn, autoconnect=True):
        super(DB_MYSQL, self).__init__(dsn, autoconnect, tipo='MYSQL')
        setType('MYSQL')
        if dsn and autoconnect:
            self.Connect(dsn)

    def Connect(self, dsn, tipo='MYSQL'):
        self.DSN = dsn
        if type(self.DSN) == type(''):
            import re
            patDSN = re.compile('Server[=:](.+);Database[=:](.+);Uid[=:](.+);Pwd[=:](.+)', re.IGNORECASE)
            match = patDSN.match(self.DSN)
            if not match:
                raise DATABASE_ERROR(
                    'El origen de datos indicada [%s] no es correcto' % self.DSN
                  , 'Esperaba: Server=[host];Database=[database];Uid=[user];Pwd=[password]'
                    )
            self.Server = match.group(1)
            self.Database = match.group(2)
            self.Usuario = match.group(3)
            self.Password = match.group(4)
        elif type(self.DSN) == type(()):
            (self.Server, self.Database, self.Usuario, self.Password) = self.DSN
        else:
            raise DATABASE_ERROR(
                'El parametro de conexion [%s] es del tipo incorrecto' % self.DSN
              , 'debe ser una string o una tupla y es de tipo %s' % type(self.DSN)
              )
        import MySQLdb
        try:
            self.DBHandle =  MySQLdb.connect(
                host = self.Server
              , user = self.Usuario
              , passwd = self.Password
              , db = self.Database
              )
        except Exception as err:
            raise DATABASE_ERROR(
                'Error al conectar a la base de datos: {srv}/{db}\n'
                'El usuario de conexión es: {usr}\n'
                'Mensaje de error: {err}'.format(
                    srv=self.Server,
                    db=self.Database,
                    usr=self.Usuario, 
                    err=err
                    )
                )

    def parametrizaSql(self, sql):
        return sql

    def as_date(self, f):
        return date_as_mysql(f)


class DB_SQLSERVER(DB):

    def __init__(self, dsn=None, autoconnect=True):
        super(DB_SQLSERVER, self).__init__(dsn, autoconnect, tipo='SQLSERVER')
        setType('SQLSERVER')
        if dsn and autoconnect:
            self.Connect(dsn)

    def Connect(self, dsn, tipo='SQLSERVER'):
        self.DSN = dsn
        if type(self.DSN) in types.StringTypes:
            patDSN = re.compile('Server=(.+);Database=(.+);Uid=(.+);Pwd=(.+)', re.IGNORECASE)
            match = patDSN.match(self.DSN)
            if not match:
                raise DATABASE_ERROR(
                    'El origen de datos indicada [%s] no es correcto' % self.DSN
                  , 'Esperaba: Server=[host];Database=[database];Uid=[user];Pwd=[password]'
                    )
            self.Server = match.group(1)
            self.Database = match.group(2)
            self.Usuario = match.group(3)
            self.Password = match.group(4)
        elif type(self.DSN) == type(()):
            (self.Server, self.Database, self.Usuario, self.Password) = self.DSN
        else:
            raise DATABASE_ERROR(
                'El parámetro de conexión: {dsn}'
                ' es del tipo incorrecto. Debe ser'
                ' una string o una tupla y es '
                ' de tipo {tipo}.'.format(
                    dsn=self.DSN,
                    tipo=str(type(self.DSN)),
                    )
                )
        import pymssql
        try:
            self.DBHandle = pymssql.connect(
              host=self.Server,
              database=self.Database,
              user=self.Usuario,
              password=self.Password,
              charset='UTF-8',
              )
        except Exception as err:
            raise DATABASE_ERROR(
                'Error al conectar a la base de datos: {srv}/{db}\n'
                'El usuario de conexión es: {usr}\n'
                'Mensaje de error: {err}'.format(
                    srv=self.Server,
                    db=self.Database,
                    usr=self.Usuario, 
                    err=err
                    )
                )

    def parametrizaSql(self, sql):
        return sql

    def Commit(self):
        self.DBHandle.commit()

    def as_date(self, f):
        return date_as_sqlserver(f)



class DB_POSTGRESQL(DB):

    def __init__(self, dsn, user, password, host='localhost', autoconnect=True):
        self.dsn = dsn
        self.user = user
        self.password = password
        self.host = host
        super(DB_POSTGRESQL, self).__init__(dsn, autoconnect, tipo='POSTGRESQL')
        if dsn and autoconnect:
            self.Connect(dsn, 'POSTGRESQL')

    def Connect(self, dsn='', tipo='POSIGRESQL'):
        import psycopg2
        if dsn:
            self.dsn = dsn
        try:
            self.DBHandle = psycopg2.connect(
                database = self.dsn,
                user = self.user,
                password = self.password,
                host = self.host
                )
        except Exception as error:
            msg = 'Error al conectar a la base de datos: {dsn}\n'  \
                  'El usuario de conexión es: {usr}\n'             \
                  'Mensaje de error: {err}'.format(
                       dsn=self.dsn,
                       usr=self.user, 
                       err=error,
                       ) 
            raise DATABASE_ERROR(msg)

    def next_val(self, sequence):
        '''Obtiene el siguiente valor de una secuencia postgresql.
        @param sequence Nombre de la Secuencia.
        @return El siguiente valor de la secuencia.
        '''
        sql = "SELECT nextval('seq_{}')".format(sequence.lower())
        return db.get_row(sql)[0] or 1

    def Execute(self, sql, parametros=[]):
        super(DB_POSTGRESQL, self).Execute(sql, parametros)
        self.commit()

    def commit(self):
        self.DBHandle.commit()

    def __str__(self):
        return 'DB_POSTGRESQL(dsn={})'.format(self.dsn)

    def Close(self):
        if self.DBHandle:
            self.DBHandle.close()
    close = Close

    def as_date(self, f):
        return date_as_postgresql(f)


class DB_FIREBIRD(DB):

    def __init__(self, dsn=None, autoconnect=True, charset='ISO8859_1'):
        self.CharSet = charset
        super(DB_FIREBIRD, self).__init__(dsn, autoconnect, tipo='FIREBIRD')
        setType('FIREBIRD')
        if dsn:
            self.Connect(dsn)

    def __str__(self):
        if self.DBHandle:
            return '[Base de datos tipo Firebird, conectada a "%s"]' % (self.Filename)
        else:
            return '[Base de datos tipo Firebird, no conectada]'

    def Disconnect(self):
        pass
    disconnect = Disconnect

    def Connect(self, dsn, tipo='FIREBIRD'):
        """
        Realiza una conexión a la base de datos.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> db = Database.DB_FIREBIRD('file=DBTest.fdb;usr=sysdba;pwd=masterkey')
            >>> print db.isConnected()
            True

        @param dsn: I{Data Source Name} de la base de datos a
                    la que queremos conectarnos, en forma: file=[fichero fdb];usr=[Usuario];pwd=[Contraseña]
        @type  dsn: string
        @raise DATABASE_ERROR: Si no puede establecer la conexión.
        @see: L{Constructor<__init__>}
        """
        self.DSN = dsn
        if type(self.DSN) in types.StringTypes:
            patDSN = re.compile('(file|dsn)[:=](.+);usr[:=](.+);pwd[=:](.+)', re.IGNORECASE)
            match = patDSN.match(self.DSN)
            if not match:
                raise DATABASE_ERROR('La fuente de datos indicada [%s] no está correctamente formateada' % self.DSN)
            self.Filename = match.group(2)
            self.Usuario = match.group(3)
            self.Password = match.group(4)
        elif type(self.DSN) == type(()):
            (self.Filename, self.Usuario, self.Password) = self.DSN
        else:
            raise DATABASE_ERROR('''
El parámetro de conexión [%s] es del
tipo incorrecto: debe ser una string o una tupla y es de tipo %s''' % (str(self.DSN), str(type(self.DSN))))
        import fdb
        try:
            self.DBHandle = fdb.connect(
                dsn=self.Filename, 
                user = self.Usuario, 
                password = self.Password, 
                charset = self.CharSet
                )
        except DATABASE_ERROR as e:
            raise DATABASE_ERROR('Error al conectar a la base de datos [%s], con el usuario [%s]' % (self.Filename, self.Usuario))


    def next_val(self, sequence):
        sql = 'SELECT Gen_id(%s, 1) FROM RDB$DATABASE;' % sequence
        return db.get_row(sql)[0] or 1

    def StartTransaction(self):
        self.DBHandle.begin()

    def Rollback(self):
        self.DBHandle.rollback()
    rollback = Rollback

    def Commit(self):
        self.DBHandle.commit()
    commit = Commit

    def Close(self):
        if self.DBHandle:
            self.DBHandle.close()
    close = Close

    def ExecuteTransaction(self, statments):
        self.StartTransaction()
        try:
            cur = self.DBHandle.cursor()
            for Sql in statments:
                cur.execute(Sql)
            self.Commit()
        except Exception as err:
            self.Rollback()
            raise(err)

    def as_date(self, f):
        return date_as_firebird(f) 


class FIELD(object):
    """
    DESCRIPCIÓN
    ===========

    Clase base para la descripción de los campos de una tabla.

    La clase L{TABLE} se compone de campos L{FIELD}, siguiendo
    la misma estructura que la tabla o consulta a la base de datos
    de la cual se haya formado la L{tabla<TABLE>}.

    Las responsabilidades de cada objeto L{FIELD} son las siguientes:

        - Saber acerca del tipo de datos del campo que representa.

        - Contener el valor asignado al campo, o C{None}
          si en la base de datos está a C{NULL} (Atributo C{Value}).

        - Ser capaz de representar dicho valor de una forma apta para
          sentencias C{SQL} (Método L{__str__} y métodos derivados
          L{AsSQL}.)

        - Ser capaz de representar el valor en forma de celda de tabla
          Html (Método L{TD}, usado básicamente para depurado).

        - Ser capaz de interpretar una representación textual del valor
          (método L{parse}), y reconstruir el valor a partir de la misma.
          Los tipos derivados de C{FIELD}, como L{FIELD_INTEGER} o
          L{FIELD_DATE} normalmente también sobreescriben
          este método.

    @see: L{TABLE}
    """
    def __init__(self, value=None):
        """Constructor.

        Normalmente no se usa el constructor a pelo, sino el método
        factoría L{new_field}.

        @param value: Valor del campo. A partir del valor se interpreta
                    el tipo de dato.
        """
        self.Value = value
        self.Tipo = type(value)

    def parse(self, s):
        '''
        Interpreta una string intentando obtener el valor
        correspondiente. La implementación por defecto sólo
        es válida para el tipo string, las demas clases derivadas
        tendran que sobreescribirla.

        @param s: string que representa el valor
        @attention: Nomrmalmente, se sobreescribe en las clases derivadas.
        '''
        self.Valor = str(s)

    def __str__(self):
        """
        Representación textual.

        Devuelve una representación textual apta para una operación SQL.
        Depende del método virtual L{AsSQL}, por lo que todas las
        clases derivadas deben sobreescribir L{AsSQL}.

        @return: Representación textual apta para una operación SQL.
        @rtype: string
        @see: L{AsSQL}.
        """
        if self.Value == None:
            return 'NULL'
        else:
            return self.AsSQL()

    def AsSQL(self):
        """
        Representación textual apta para una operación SQL.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> F = Database.FIELD(23)
            >>> try:
            ...     print F.AsSQL()
            ... except NotImplementedError, e:
            ...     print 'Te pillé'
            Te pillé

        @return: Representación textual apta para una operación SQL.
        @rtype:  string
        @see: L{__str__}
        @attention: Método virtual. Debe ser implementeado por
                    B{todas} las chases derivadas. Si se intenta
                    invocar directamente desde un objeto tipo
                    L{FIELD} puro, elevará una excepción

        @raise NotImplementedError: Si se intenta invocar desde
                                    la clase base.
        """
        raise NotImplementedError(
            'La clase FIELD es una clase base.\n'
            'Se ha intentado invocar AsSQL, un método abstracto.'
            )

    def TD(self):
        """Representación como celda HTML.

        Básicamente para depurado. Representa el valor del campo
        rodeado de las marcas HTML de Celda <td> y </td>.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> F = Database.FIELD(23)
            >>> print F.TD()
            <td class="DATA" align="LEFT">23</td>

        @return: Una string en formato HTML representado el valor del campo
        @rtype:  string
        """
        return '<td class="DATA" align="LEFT">%s</td>' % self.Value

class FIELD_INTEGER(FIELD):
    """
    DESCRIPCIÓN
    ===========

    Descripción de un campo C{integer} de una C{TABLE}.
    """
    def __init__(self, value=0):
        """Constructor.

        Normalmente no se usa el constructor a pelo, sino el método
        factoría L{new_field}.

        @param value: Valor del campo. A partir del valor se interpreta
                    el tipo de dato.
        """
        FIELD.__init__(self, value)

    def AsSQL(self):
        '''
        Representación textual apta para una operación SQL.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> F = Database.FIELD_INTEGER(23)
            >>> print F.AsSQL()
            23

        @return: Representación textual apta para una operación SQL.
        @rtype:  string
        @see: L{FIELD.__str__}
        '''
        return '%d' % self.Value

    def parse(self, s):
        '''
        Interpreta una string intentando obtener el valor
        correspondiente

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> F = Database.new_field(0)
            >>> F.parse('243')
            >>> print F.Value
            243

        @param s: string que representa el valor
        @type  s: string
        '''
        self.Value = int(s)

# class FIELD_LONG(FIELD):
    # """
    # DESCRIPCIÓN
    # ===========

    # Descripción de un campo C{long} de una C{TABLE}.
    # """

    # def __init__(self, value=0):
        # """Constructor.

        # Normalmente no se usa el constructor a pelo, sino el método
        # factoría L{new_field}.

        # @param value: Valor del campo. A partir del valor se interpreta
                    # el tipo de dato.
        # """
        # FIELD.__init__(self, value)

    # def AsSQL(self):
        # '''
        # Representación textual apta para una operación SQL.

        # Ejemplo:

            # >>> from Libreria.Base import Database
            # >>> F = Database.FIELD_LONG(23483L)
            # >>> print F.AsSQL()
            # 23483

        # @return: Representación textual apta para una operación SQL.
        # @rtype:  string
        # @see: L{FIELD.__str__}
        # '''
        # return '%d' % self.Value

    # def parse(self, s):
        # '''
        # Interpreta una string intentando obtener el valor
        # correspondiente

        # Ejemplo de uso:

            # >>> from Libreria.Base import Database
            # >>> F = Database.new_field(0L)
            # >>> F.parse('243034')
            # >>> print F.Value
            # 243034

        # @param s: string que representa el valor
        # @type  s: string
        # '''
        # self.Value = long(s)

class FIELD_DECIMAL(FIELD):
    def __init__(self, value=''):
        super(FIELD_DECIMAL, self).__init__(value)

    def AsSQL(self):
        return str(self.Value)

    def Parse(self, s):
        self.Value = Decimal(s)

class FIELD_FLOAT(FIELD):
    """
    DESCRIPCIÓN
    ===========

    Descripción de un campo C{float} de una C{TABLE}.
    """

    def __init__(self, value=0.0):
        """Constructor.

        Normalmente no se usa el constructor a pelo, sino el método
        factoría L{new_field}.

        @param value: Valor del campo. A partir del valor se interpreta
                    el tipo de dato.
        """
        FIELD.__init__(self, value)

    def AsSQL(self):
        '''
        Representación textual apta para una operación SQL.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> F = Database.FIELD_FLOAT(23.483)
            >>> print F.AsSQL()
            23.483000

        @return: Representación textual apta para una operación SQL.
        @rtype:  string
        @see: L{FIELD.__str__}
        '''
        return '%f' % self.Value

    def parse(self, s):
        '''
        Interpreta una string intentando obtener el valor
        correspondiente

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> F = Database.new_field(0.0)
            >>> F.parse('24.3')
            >>> print F.Value
            24.3

        @param s: string que representa el valor
        @type  s: string
        '''
        self.Value = float(s)

class FIELD_STRING(FIELD):
    """
    DESCRIPCIÓN
    ===========

    Descripción de un campo C{string} de una C{TABLE}.
    """

    def __init__(self, value=''):
        """Constructor.

        Normalmente no se usa el constructor a pelo, sino el método
        factoría L{new_field}.

        @param value: Valor del campo. A partir del valor se interpreta
                    el tipo de dato.
        """
        FIELD.__init__(self, value)

    def AsSQL(self):
        '''
        Representación textual apta para una operación SQL.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> F = Database.FIELD_STRING('PEPE')
            >>> print F.AsSQL()
            'PEPE'

        @return: Representación textual apta para una operación SQL.
        @rtype:  string
        @see: L{FIELD.__str__}
        '''
        return AsString(self.Value)

    def parse(self, s):
        '''
        Interpreta una string intentando obtener el valor
        correspondiente

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> F = Database.new_field('')
            >>> F.parse('Pepe')
            >>> print F.Value
            Pepe

        @param s: string que representa el valor
        @type  s: string
        '''
        if isinstance(s, six.string_types):
            self.Value = s
        elif isinstance(s, six.text_type):
            self.Value = s
        else:
            self.Value = str(s)

class FIELD_DATE(FIELD):
    """
    DESCRIPCIÓN
    ===========

    Descripción de un campo C{date} de una C{TABLE}.
    """

    def __init__(self, value=Fechas.FECHA()):
        """Constructor.

        Normalmente no se usa el constructor a pelo, sino el método
        factoría L{new_field}.

        @param value: Valor del campo. A partir del valor se interpreta
                    el tipo de dato.
        """
        FIELD.__init__(self, value)

    def AsSQL(self):
        '''
        Representación textual apta para una operación SQL.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> F = Database.FIELD_DATE(Database.Fechas.FECHA('16/ene/2005'))
            >>> Database.setType('ODBC')
            >>> print F.AsSQL()
            { d '2005-01-16' }
            >>> Database.setType('ORACLE')
            >>> print F.AsSQL()
            to_date('16/01/2005','DD/MM/YYYY')

        @return: Representación textual apta para una operación SQL.
        @rtype:  string
        @see: L{FIELD.__str__}, L{Libreria.Comun.Fechas.FECHA}
        @attention: El resultado varía segun el tipo de conexión que
                    hayamos establecido.
        '''
        return AsFecha(self.Value)

    def parse(self, s):
        '''
        Interpreta una string intentando obtener el valor
        correspondiente

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> F = Database.new_field(Database.Fechas.FECHA())
            >>> F.parse('23 de enero de 2005')
            >>> print F.Value
            23/1/2005

        @param s: string que representa el valor
        @type  s: string
        '''
        self.Value = Fechas.FECHA(s)

class FIELD_TIMESTAMP(FIELD):
    """
    DESCRIPCIÓN
    ===========

    Descripción de un campo C{timestamp} de una C{TABLE}.
    """

    def __init__(self, value=Fechas.TIMESTAMP()):
        """Constructor.

        Normalmente no se usa el constructor a pelo, sino el método
        factoría L{new_field}.

        @param value: Valor del campo. A partir del valor se interpreta
                    el tipo de dato.
        """
        FIELD.__init__(self, value)

    def AsSQL(self):
        '''
        Representación textual apta para una operación SQL.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> F = Database.FIELD_TIMESTAMP(
            ...         Database.Fechas.TIMESTAMP('16/01/2005 12:34:11')
            ...         )
            >>> print F.AsSQL()
            { ts '2005-01-16 12:34:11' }

        @return: Representación textual apta para una operación SQL.
        @rtype:  string
        @see: L{FIELD.__str__}, L{Libreria.Comun.Fechas.FECHA}
        '''
        return AsTimestamp(self.Value)

    def parse(self, s):
        '''
        Interpreta una string intentando obtener el valor
        correspondiente

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> F = Database.new_field(Database.Fechas.TIMESTAMP())
            >>> F.parse('23/1/2005 12:44:32')
            >>> print F.Value
            23/01/2005 12:44:32

        @param s: string que representa el valor
        @type  s: string
        '''
        self.Value = Fechas.TIMESTAMP(s)

class FIELD_BOOLEAN(FIELD):
    """
    DESCRIPCIÓN
    ===========

    Descripción de un campo booleano (S/N) de una C{TABLE}.
    """

    def __init__(self, value=None):
        """Constructor.

        Normalmente no se usa el constructor a pelo, sino el método
        factoría L{new_field}.

        @param value: Valor del campo. A partir del valor se interpreta
                    el tipo de dato.
        """
        FIELD.__init__(self, value)

    def AsSQL(self):
        '''
        Representación textual apta para una operación SQL.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> F = Database.FIELD_BOOLEAN(True)
            >>> print F
            'S'

        @return: Representación textual apta para una operación SQL.
        @rtype:  string
        @see: L{FIELD.__str__}, L{Libreria.Comun.Fechas.FECHA}
        '''
        if self.Value == None:
            return 'NULL'
        elif self.Value == True:
            return "'S'"
        else:
            return "'N'"

    def parse(self, s):
        '''
        Interpreta una string intentando obtener el valor
        correspondiente

        @param s: string que representa el valor
        @type  s: string
        '''
        s = s.upper()
        if s == 'NULL' or s == 'NONE':
            self.Value = None
        elif s == 'S' or s == 'TRUE':
            self.Value = True
        else:
            self.Value = False


class FIELD_NONE(FIELD):
    """
    DESCRIPCIÓN
    ===========

    Descripción de un campo nulo de una C{TABLE}.
    """

    def __init__(self, value=None):
        """Constructor.

        Normalmente no se usa el constructor a pelo, sino el método
        factoría L{new_field}.

        @param value: Valor del campo. A partir del valor se interpreta
                    el tipo de dato.
        """
        FIELD.__init__(self, value)

    def AsSQL(self):
        '''Retorna la expresión 'NULL'
        @return: la expresión 'NULL'
        @rtype: string
        '''
        return 'NULL'

    def parse(self, s):
        '''
        Al ser una campo nulo, se prohibe la operación parse.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> field = Database.new_field(None)
            >>> try:
            ...     field.parse('tarari que te vi')
            ... except ValueError:
            ...     print 'Te pillé'
            Te pillé

        @param s: string que representa el valor
        @type  s: string
        '''
        raise ValueError('''No se puede convertir la string "%s".
            el campo se ha definido como Nulo''' % s)

def new_field(value):
    """
    Factoría para la construcción de nuevos campos.

    Se utiliza esta función para crear un nuevo objeto, derivado
    del tipo L{FIELD}, adecuado al tipo de dato que se pasa como parámetro.
    por ejemplo, si se pasa un entero, la función retornará un
    objeto de tipo L{FIELD_INTEGER}.

    @param value: Valor a partir del cual se creará el objeto L{FIELD}.
    @see: L{FIELD}
    """
    if value is None:
        return FIELD_NONE(value)
    elif isinstance(value, bool):
        return FIELD_BOOLEAN(value)
    elif isinstance(value, six.integer_types):
        return FIELD_INTEGER(value)
    elif isinstance(value, Decimal):
        return FIELD_DECIMAL(value)
    elif isinstance(value, float):
        return FIELD_FLOAT(value)
    elif isinstance(value, six.string_types):
        return FIELD_STRING(value)
    elif isinstance(value, six.text_type):
        return FIELD_STRING(value)
    elif isinstance(value, (datetime.datetime, Fechas.TIMESTAMP)):
        return FIELD_TIMESTAMP(value)
    elif isinstance(value, datetime.date):
        value = Fechas.FECHA(value)
        return FIELD_DATE(value)
    elif isinstance(value, Fechas.FECHA):
        return FIELD_DATE(value)
    else:
        return FIELD(value)

newField = new_field

def predicate_isnull(fn, v):
    if v:
        return '%s IS NULL' % fn
    else:
        return '%s IS NOT NULL' % fn

def predicate_isnotnull(fn, v):
    if v:
        return '%s IS NOT NULL' % fn
    else:
        return '%s IS NULL' % fn

def predicate_between(fn, v):
    (minimo, maximo) = v
    return '%s BETWEEN %s AND %s' % (fn, new_field(minimo), new_field(maximo))

    

def get_predicate(field__op, value):
    str_safe = lambda s: s.replace("'", "''")
    Mapa_Operadores = {
        'gt': lambda fn, v: '%s > %s' % (fn, new_field(v)),
        'gte':  lambda fn, v: '%s >= %s' % (fn, new_field(v)),
        'lt': lambda fn, v: '%s < %s' % (fn, new_field(v)),
        'lte':  lambda fn, v: '%s <= %s' % (fn, new_field(v)),
        'eq': lambda fn, v: '%s = %s' % (fn, new_field(v)),
        'noteq': lambda fn, v: '%s <> %s' % (fn, new_field(v)),
        'contains': lambda fn, v: "{} LIKE '%%{}%%'".format(fn, str_safe(v)),
        'icontains': lambda fn, v: "UPPER({}) LIKE UPPER('%%{}%%')".format(fn, str_safe(v)),
        'startswith': lambda fn, v: "{} LIKE '{}%%'".format(fn, str_safe(v)),
        'istartswith': lambda fn, v: "UPPER({}) LIKE UPPER('{}%%')".format(fn, str_safe(v)),
        'endswith': lambda fn, v: "{} LIKE '%%{}'".format(fn, str_safe(v)),
        'notendswith': lambda fn, v: "{} NOT LIKE '%%{}'".format(fn, str_safe(v)),
        'iendsswith': lambda fn, v: "UPPER({}) LIKE UPPER('%%{}')".format(fn, str_safe(v)),
        'isnull' : predicate_isnull,
        'isnotnull': predicate_isnotnull,
        'year': lambda fn, v: 'extract(year from %s) = %s' % (fn, new_field(v)),
        'month': lambda fn, v: 'extract(month from %s) = %s' % (fn, new_field(v)),
        'between': predicate_between,
        }
    if '__' in field__op:
        (field_name, op_name) = field__op.split('__', 1)
    else:
        field_name = field__op
        op_name = 'eq'
    functor = Mapa_Operadores.get(op_name)
    return functor(field_name, value)


class TABLE(object):
    """
    DESCRIPCIÓN
    ===========

    Representacion de una tabla de la base de datos.

    RESPONSABILIDADES
    =================

    Las responsabilidades de esta clase son las siguientes:

        - Mantener una conexión con la base de datos en la que reside
          la tabla que está representando (Atributo C{Database}, pasado
          como parámetro C{dbhandle} del L{constructor<__init__>}).

        - Saber acerca de la estructura de la tabla: El nombre de
          la tabla (Atributo C{Name}, pasado como parámetro
          C{name} del L{constructor<__init__>}), el tipo de
          cada columna y que campos son claves
          y cuales no (métodos L{addKey} y L{addField}).

        - Generear, conocida la estructura, las sentencias SQL
          necesarias para insertar, modificar o borrar un registro
          de la tabla (métodos L{getWhereKey}, L{createInsert},
          L{createUpdate} y L{createDelete}.

        - Realizar las operaciones de inserción, modificación
          o borrado en la base de datos: métodos L{Insert},
          L{Update} y L{Delete}.

    EJEMPLO DE USO
    ==============

    Ejemplo de creación, definición y uso de los metodos create*
    de la clase L{TABLE}:

        >>> from Libreria.Base import Database
        >>> DB = Database.DB_ORACLE('scott/tiger@oracle')
        >>> T = Database.TABLE(DB, 'DEPT')
        >>> T.addKey('DEPTNO', 0)
        >>> T.addField('DNAME', 0)
        >>> T.addField('LOC', 0)
        >>> print T.createSelect()
        SELECT DEPTNO, DNAME, LOC
          FROM DEPT
        >>> print T.createSelect(where='DEPTNO=30')
        SELECT DEPTNO, DNAME, LOC
          FROM DEPT
         WHERE DEPTNO=30

    @see: L{DB}, L{FIELD}.
    """
    def __init__(self, dbhandle, name):
        """Constructor.

        Inicialmente la tabla se crea sin ninguna estructura, se
        define la misma mediante los métodos L{addKey} y L{addField}.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> T = Database.TABLE(None, 'ISLA')
            >>> T.addKey('CODIGO', 0)
            >>> T.addField('DESCRIPCION', 0)
            >>> print T.createSelect()
            SELECT CODIGO, DESCRIPCION
              FROM ISLA

        @param dbhandle: Conexión a la base de datos. Se puede usar
                         C{None} para trabajr sin conexión, pero no
                         podremos usar los métodos L{Insert},
                         L{Update} ni L{Delete}.
        @type  dbhandle: Conexión a la base de datos, de tipo L{DB}

        @param name: Nombre de la tabla en el esquema
                     de la base de datos.
        @type  name: string
        """
        self.Name = name
        self.Database = dbhandle
        self.Fields = []
        self.Keys = []
        self.Map = {}

    def NullRow(self):
        """
        Retorna una fila de datos (Un objeto del tipo
        L{Row}), inicialmente con valores Nulos.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> T = Database.TABLE(None, 'ISLA')
            >>> T.addKey('CODIGO', 0)
            >>> T.addField('DESCRIPCION', 0)
            >>> row = T.NullRow()
            >>> print row.Codigo
            None
            >>> print row['DESCRIPCION']
            None
            >>> print row[0], row[1]
            None None


        @return: una fila del tipo L{Row}, ya preparada con la
                 estructura de la tabla y valores nulos.
        @see: L{Row}
        """
        row = Row()
        for name in self.Fields:
            row.addValue(name, None)
        return row

    def setRow(self, row):
        """
        Asigna a cada campo de la tabla el contenido de la fila
        que se le pasa como parámetro C{row}.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> DB = Database.DB_ORACLE('scott/tiger@oracle')
            >>> T = Database.TABLE(None, 'ISLA')
            >>> T.addKey('CODIGO', 0)
            >>> T.addField('DESCRIPCION', '')
            >>> row = T.NullRow()
            >>> row['CODIGO'] = 1
            >>> row['DESCRIPCION'] = 'Tenerife'
            >>> T.setRow(row)
            >>> print T.createUpdate()
            UPDATE ISLA
               SET DESCRIPCION = 'Tenerife'
             WHERE CODIGO = 1

        @attention: Si un campo definido en el objeto L{TABLE}
                    no está definido en el objeto L{Row}, su
                    valor se pondrá a C{None}. si el objeto L{Row}
                    define algún campo que no existe
                    en el objeto L{TABLE}, el campo se ignora.

        @param row: objeto de tipo L{Row}
        @see: L{Row}
        """
        for field in self.Map.keys():
            self.Map[field].Value = row.get(field, None)

    def Debug(self):
        """
        Imprime un volcado C{HTML} del contenido de la Tabla, a
        efectos de depuración.
        """
        print('<table border=1>')
        for index in range(0, len(self.Fields)):
            name = self.Fields[index]
            value = self.Map[name].Value
            IsKey = name in self.Keys
            print('<tr>')
            if IsKey:
                print('<th>PK</th>')
            else:
                print('<td>&nbsp;</td>')
            print('<td>{}</td>'.format(name))
            print('<td>{}</td>'.format(value))
            print('<td>{}</td>'.format(AsHtml(type(value))))
            print('</tr>')
        print('</table>')

    def get(self, fieldname, defaultvalue=None):
        '''
        La función get es similar a la usada para acceder a
        la clase como si fuera un diccionario, pero no eleva excepción
        en caso de que no esté definido el campo, sino que devuelve
        el valor definido por el parámetro C{defaultvalue}.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> Edificio = Database.TABLE(None, 'EDIFICIO')
            >>> Edificio.addKey('Codigo', 1)
            >>> Edificio.addField('Nombre', 'Chrysler Bulding, NY')
            >>> Edificio.addField('Altura', 319)
            >>> print Edificio.get('ALTURA')
            319
            >>> print Edificio.get('NOEXISTE')
            None
            >>> print Edificio.get('NOEXISTE', 28)
            28


        @param fieldname:  El nombre del campo cuyo valor queremos obtener.
        @type fieldname:   string

        @param defaultvalue: Valor de retorno a utilizar en caso de que
                        no esté definido el campo C{key}.

        @return: El valor definido para C{key}, o C{default} si el campo
                 C{key} no existe.
        @rtype: *
        @see: L{__getitem__}
        '''
        if self.Map.has_key(fieldname):
            return self.Map[fieldname].Value
        else:
            return defaultvalue

    def __getitem__(self, field):
        """
        Obtener un valor de la tabla, como si fuera un diccionario.

        @param field: Nombre del campo
        @type field: string
        @return: Valor almacenado
        @raises KeyError: Si el nombre del campo no pertenece a la tabla.
        """
        field = field.upper()
        if self.Map.has_key(field):
            return self.Map[field]
        else:
            raise KeyError(
                'La clave: {} no existe en el objeto TABLE'.format(field)
                )


    def __setitem__(self, field, value):
        """
        Asignar un valor de la tabla, como si fuera un diccionario.

        @param field: Nombre del campo
        @type field: string
        @param value: Valor a almacenar
        @raises KeyError: si el nombre del campo no pertenece a la tabla
        """
        # print 'field, value:', field, value
        field = field.upper()
        if field in self.Fields:
            self.Map[field] = new_field(value)
        else:
            raise KeyError(
                'El campo {} no está definido en la tabla {}'.format(
                    field, self.Name
                    ))

    def setValues(self, **nameargs):
        """
        Asignar valores a

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> Edificio = Database.TABLE(None, 'EDIFICIO')
            >>> Edificio.addKey('Codigo', 0)
            >>> Edificio.addField('Nombre', '')
            >>> Edificio.addField('Altura', 0.0)
            >>> Edificio.setValues(codigo=1, nombre='Empire State', altura=443.2)
            >>> print Edificio['CODIGO']
            1
            >>> print Edificio['NOMBRE']
            'Empire State'
            >>> print Edificio['ALTURA']
            443.200000

        @see: L{__setitem__}
        @see: L{setRow}
        @raise KeyError: Si alguno de los campos indicados no pertecene
                          a la tabla.
        @param nameargs: Diccionario conteniendo la información,
                         siendo las claves/nombre de los parámetros
                         los nombres de los campos, y el
                         contenido/valor de los parámetros
                         los valores de los mismos.
        """
        self._Names = []
        self._Values = []
        for (field, value) in nameargs.items():
            field = field.upper()
            if field in self.Fields:
                self.Map[field].Value = value
            else:
                raise KeyError(
                    'El campo {} no está definido en la tabla {}'.format(
                        field, self.Name
                    ))

    def addField(self, nombre, valor=None):
        """
        Añadir un campo a la definición de la tabla

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> T = Database.TABLE(None, 'ISLA')
            >>> T.addKey('CODIGO', 0)
            >>> T.addField('DESCRIPCION', 0)
            >>> print T.createSelect()
            SELECT CODIGO, DESCRIPCION
              FROM ISLA

        @see: L{new_field}
        @see: L{addKey}
        @see: L{Constructor<__init__>}
        @param nombre: Nombre del campo
        @type nombre:  string
        @param valor: Valor del campo. A partir del tipo de este valor
                      se deducirá el tipo del campo.
        """
        nombre = nombre.upper()
        self.Fields.append(nombre)
        self.Map[nombre] = new_field(valor)
        self.Map[nombre].Value = valor

    def addKey(self, nombre, valor=None):
        """
        Añadir un campo clave a la definición de la tabla.

        Para poder construir las sentencias SQL automáticamente,
        la clase necesita saber que campos forman la clave primaria
        y que campos son dependientes.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> T = Database.TABLE(None, 'ISLA')
            >>> T.addKey('CODIGO', 0)
            >>> T.addField('DESCRIPCION', '')
            >>> print T.createSelect()
            SELECT CODIGO, DESCRIPCION
              FROM ISLA

        @see: L{new_field}
        @see: L{addField}
        @see: L{Constructor<__init__>}
        @param nombre: Nombre del campo
        @type nombre:  string
        @param valor: Valor del campo. A partir del tipo de este valor
                      se deducirá el tipo del campo.
        """
        nombre = nombre.upper()
        self.Keys.append(nombre)
        self.Fields.append(nombre)
        self.Map[nombre] = new_field(valor)
        self.Map[nombre].Value = valor

    def getFields(self):
        """
        Retorna una string con los nombres de los campos
        separados por coma.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> T = Database.TABLE(None, 'Pueblo')
            >>> T.addKey('Codigo', 0)
            >>> T.addField('Nombre', '')
            >>> T.addField('Poblacion', 0.0)
            >>> print T.getFields()
            CODIGO, NOMBRE, POBLACION

        Esta función es usada normalmente por las rutinas create*
        (L{createInsert}, L{createUpdate}, L{createSelect})  para
        crear las sentencias SQL correspondientes.

        @return: los nombres de los campos, separados por coma.
        @rtype: string
        @see: L{getValues}, L{createInsert}, L{createUpdate}, L{createSelect}
        """
        return ', '.join(self.Fields)

    def getValues(self):
        """
        Retorna una string con los valores de los campos
        separados por coma.

        Ejemplo:

            >>> from Libreria.Base import Database
            >>> T = Database.TABLE(None, 'EDIFICIO')
            >>> T.addKey('Codigo', 23)
            >>> T.addField('Edificio', 'Chrysler Building, NY')
            >>> T.addField('Altura', 319)
            >>> print T.getValues()
            23, 'Chrysler Building, NY', 319

        Esta función es usada normalmente por las rutinas create*
        (L{createInsert}, L{createUpdate}, L{createSelect})  para
        crear las sentencias SQL correspondientes.

        @return: los valores de los campos, separados por coma.
        @rtype: string
        @see: L{getFields}, L{createInsert}, L{createUpdate}, L{createSelect}
        """
        return ', '.join([
            str(self.Map[field]) for field in self.Fields
            ])

    def getWhereKey(self):
        """
        Obtiene la clausala C{WHERE} de una sentencia C{SQL}
        que permite localizar el registro de forma inequívoca.

        La clase puede generar el C{WHERE} porque sabe que campos
        son claves y cuales no, si se ha definido correctamente
        la estructura de la tabla con las llamadas L{addKey}
        y L{addField}.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> T = Database.TABLE(None, 'EDIFICIO')
            >>> T.addKey('Codigo', 23)
            >>> T.addField('Edificio', 'Chrysler Building, NY')
            >>> T.addField('Altura', 319)
            >>> print T.getWhereKey()
            CODIGO = 23

        @see: L{getFields}
        @see: L{createInsert}
        @see: L{createUpdate}
        @see: L{createSelect}
        @see: L{createDelete}
        @return: La condición C{WHERE} a aplicar a la
                 sentencia C{SQL} que permite localizar
                 el registro de forma inequívoca.
        @rtype: string
        """
        lista = [
            '{} = {}'.format(field, self.Map[field])
            for field in self.Keys
            ]
        return ' AND '.join(lista)


    def createInsert(self):
        '''
        Devuelve el código C{SQL} necesario para hacer un C{INSERT}.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> T = Database.TABLE(None, 'EDIFICIO')
            >>> T.addKey('Codigo', 23)
            >>> T.addField('Edificio', 'Chrysler Building, NY')
            >>> T.addField('Altura', 319)
            >>> print T.createInsert()
            INSERT INTO EDIFICIO (CODIGO, EDIFICIO, ALTURA)
            VALUES (23, 'Chrysler Building, NY', 319)


        @return: Sentencia SQL
        @rtype: string
        @see: L{Insert}
        '''
        Sql = 'INSERT INTO {} ({})\nVALUES ({})'.format(
            self.Name,
            self.getFields(),
            self.getValues(),
            )
        return Sql

    def Insert(self):
        '''
        Realiza un C{INSERT} en la base de datos, usando los valores
        actuales del objeto.

        @see: L{createInsert}
        @see: L{DB.Execute}
        '''
        Sql = self.createInsert()
        self.Database.Execute(Sql)

    def createUpdate(self):
        '''
        Devuelve el código C{SQL} necesario para hacer un C{UPDATE}.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> T = Database.TABLE(None, 'EDIFICIO')
            >>> T.addKey('Codigo', 23)
            >>> T.addField('Edificio', 'Chrysler Building, NY')
            >>> T.addField('Altura', 319)
            >>> print T.createUpdate()
            UPDATE EDIFICIO
               SET EDIFICIO = 'Chrysler Building, NY',
                   ALTURA = 319
             WHERE CODIGO = 23


        @return: Sentencia SQL
        @rtype: string
        @see: L{Update}
        '''
        lista = [
            '%s = %s' % (field, self.Map[field])
            for field in self.Fields
            if field not in self.Keys
            ] 
        Parejas = ',\n       '.join(lista)
        sql = 'UPDATE {}\n   SET {}\n  WHERE {}'.format(
                self.Name,
                Parejas,
                self.getWhereKey()
                )
        return sql

    def Update(self):
        '''
        Realiza un C{UPDATE} en la base de datos, usando los valores
        actuales del objeto.

        @see: L{createUpdate}
        @see: L{DB.Execute}
        '''
        Sql = self.createUpdate()
        self.Database.Execute(Sql)

    def createDelete(self):
        '''
        Devuelve el código C{SQL} necesario para hacer un C{DELETE}.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> T = Database.TABLE(None, 'EDIFICIO')
            >>> T.addKey('Codigo', 23)
            >>> T.addField('Edificio', 'Chrysler Building, NY')
            >>> T.addField('Altura', 319)
            >>> print T.createDelete()
            DELETE FROM EDIFICIO
             WHERE CODIGO = 23

        @return: Sentencia SQL
        @rtype: string
        @see: L{Delete}
        '''
        Sql = 'DELETE FROM %s\n WHERE %s' % (self.Name, self.getWhereKey())
        return Sql

    def Delete(self):
        '''
        Realiza un C{DELETE} en la base de datos, usando los valores
        actuales del objeto.

        @see: L{createDelete}
        @see: L{DB.Execute}
        '''
        Sql = self.createDelete()
        self.Database.Execute(Sql)

    def ShowSql(self, sql):
        '''
        Muestra la sentencia C{SQL} pasada como el parámetro (C{sql}), en
        formato HTML

        Básicamente se implementa para propositos de depurado.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> T = Database.TABLE(None, 'ISLA')
            >>> T.ShowSql('SELECT * FROM ISLA')
            <p>Tabla: ISLA</p>
            <hr><blockquote>
            <b>SQL:</b> SELECT * FROM ISLA
            </blockquote><hr>

        @param sql: Sentencia SQL
        @return: Sentencia SQL formateada en código HTML
        @rtype: string
        '''
        print('<p>Tabla: {}</p>'.format(self.Name))
        print('<hr><blockquote>')
        print('<b>SQL:</b> {}'.format(sql))
        print('</blockquote><hr>')

    def createSelect(self, where=None, order=None, group=None):
        '''
        Devuelve el código C{SQL} necesario para hacer un C{SELECT}.

        Normalmente, es usada por los métodos L{selectAll},
        L{selectOne} y L{selectMany} para construir la sentencia
        C{SQL} necesaria.

        Ejemplo de uso:

            >>> from Libreria.Base import Database
            >>> T = Database.TABLE(None, 'EDIFICIO')
            >>> T.addKey('Codigo', 23)
            >>> T.addField('Edificio', 'Chrysler Building, NY')
            >>> T.addField('Altura', 319)
            >>> print T.createSelect()
            SELECT CODIGO, EDIFICIO, ALTURA
              FROM EDIFICIO

        Ejemplo de uso con el parametro C{where}
        .
            >>> from Libreria.Base import Database
            >>> T = Database.TABLE(None, 'EDIFICIO')
            >>> T.addKey('Codigo', 23)
            >>> T.addField('Edificio', 'Chrysler Building, NY')
            >>> T.addField('Altura', 319)
            >>> print T.createSelect('CODIGO = 23')
            SELECT CODIGO, EDIFICIO, ALTURA
              FROM EDIFICIO
             WHERE CODIGO = 23

        Ejemplo de uso con el parametro C{order}
            >>> print T.createSelect(order='ALTURA DESC')
            SELECT CODIGO, EDIFICIO, ALTURA
              FROM EDIFICIO
             ORDER BY ALTURA DESC

        @param where: Clausula C{WHERE} para filtrar la consulta
        @type  where:  string
        @param order: Orden en que se deben retornar los resultados,
                      si los hubiera.
        @type  order:  string
        @param group: Agrupamiento a utilizar, si se utilizan funciones
                      de columnas (C{SUM}, C{MAX}, C{MIN}
                      , C{AVERAGE}, C{COUNT} ...).
        @type  group:  string

        @return: Sentencia SQL
        @rtype: string
        @see: L{selectAll}
        @see: L{selectOne}
        @see: L{selectMany}
        '''
        Sql = 'SELECT %s\n  FROM %s' % (self.getFields(), self.Name)
        if where: Sql = '%s\n WHERE %s' % (Sql, where)
        if group: Sql = '%s\n GROUP BY %s' % (Sql, group)
        if order: Sql = '%s\n ORDER BY %s' % (Sql, order)
        if TRON:
            self.ShowSql(Sql)
        return Sql

    def count(self, where=None):
        """
        Obtiene el número de registros en la tabla, atendiendo
        a la condición C{where} si se especifica

        @param where: Condición C{WHERE} a aplicar al C{SELECT}.

        @return: Número de filas que cumplen la condición C{WHERE}
                 especificada, o número total de filas de la tabla
                 si no se especificó la condición C{WHERE}

        @rtype: integer
        """
        Sql = 'Select count(*) as Total from %s' % self.Name
        if where:
            Sql += ' Where %s' % where
        row = self.Database.getRow(Sql)
        return row.TOTAL

    def selectAll(self, where=None, order=None, group=None):
        """
        Seleccciona varias registro de la tabla

        @param where: Condición C{WHERE} a aplicar al C{SELECT}.
        @param order: Descripcion del orden por el que se quieren
                      obtener los resultados
        @param group: En caso de funciones de columna, indica
                      el agrupamiento a utilizar
        @return: Un array de objetos L{Row} por cada fila, o C{None} si
                ninguna fila de la tabla cumpla las condiciones
                especificadas en el parámetro C{WHERE}
        @see: L{Row}
        @see: L{TABLE.selectOne}
        @see: L{TABLE.selectMany}
        """
        Sql = self.createSelect(where, order, group)
        Rows = self.Database.getRows(Sql)
        return Rows

    def selectOne(self, where=None):
        """
        Selecccion un registro de la tabla

        @param where: Condicion C{WHERE} a aplicar al C{SELECT}. Si no se
                      especifica, se intentará localizar por la clave
        @return: Objeto tipo L{Row} conteniendo la fila de resultados
        @see: L{Row}
        @see: L{selectMany}
        @see: L{selectAll}
        """
        if where == None:
            where=self.getWhereKey()
        Sql = self.createSelect(where)
        Row = self.Database.getRow(Sql)
        return Row

    def selectMany(self, numrows, where=None, order=None, group=None):
        """
        Selecccion varias registro de la tabla

        @param numrows: Numero máximo de filas que queremos obtener
        @param where: Concicion C{WHERE} a aplicar al C{SELECT}.
        @param order: Descripcion del orden por el que se quieren
                      obtener los resultados
        @param group: En caso de funciones de columna, indica
                      el agrupamiento a utilizar
        @return: Un array de objetos L{Row} por cada fila, o C{None} si
                 ninguna fila de la tabla cumpla las condiciones
                 especificadas en el parámetro C{WHERE}.

        @see: L{Row}
        @see: L{selectOne}
        @see: L{selectAll}
        """
        Sql = self.createSelect(where, order, group)
        Rows = self.Database.getRows(Sql, numrows)
        return Rows

class OutputBuffer:
    def __init__(self):
        self.kernel = []

    def write(self, s):
        self.kernel.append(s)

    def __str__(self):
        return '\n'.join(self.kernel)


def print_sql(sql, parametros=[], output=None, html_markup=False):
    """Imprime la sentencia C{SQL} que se la pasa como parámetro en forma
    chachi-pilongi, con indentación, estilos, marcas HTML... de
    todo, oiga.

    @param sql: Sentencia C{SQL} a imprimir.
    @param paramentros: Lista de parámetros del SQl (Opcional)
    @param output: Fichero (O similar) de salida en el que se imprimirá el resultado. Por
                   defecto se toma sys.stdout (Opcional)
    """

    if not output or output == True:
        output = sys.stdout
    html_output = html_markup and 'HTTP_USER_AGENT' in os.environ
    if html_output:
        output.write('''<pre style="border:1px solid #AAAAAA;
    background:#F9F9F9;
    margin-left: 35px;
    margin-right: 35px;
    clear:both;"><b style="color:#AAAAAA">--[SQL]----</b><br>\n''')

    sql = str(sql).strip()
    if sql[:7].upper() != 'SELECT ':
        output.write(sql)
    else:
        indentation_table = {
            'SELECT': 0,
            'FROM': 2,
            'WHERE': 1,
            'AND': 3,
            'OR': 4,
            'GROUP BY': 1,
            'ORDER BY': 1,
            'HAVING': 0,
            'LEFT OUTER JOIN': 7, 
            'LEFT_JOIN': 7,
            'ORDER_BY': 1,
            }
        sql = sql.replace('\r\n', ' ')
        sql = sql.replace('\n', ' ')
        sql = sql.replace('LEFT JOIN', 'LEFT_JOIN')
        sql = sql.replace('ORDER BY', 'ORDER_BY')
        sql = ' '.join(sql.split())
        patSQL = re.compile(
            '(SELECT|FROM|LEFT_JOIN|LEFT OUTER JOIN|WHERE|AND|OR|GROUP|ORDER BY|HAVING)'
            , re.IGNORECASE) 
        Words = [s.strip() for s in sql.split() if s.strip()]
        for word in Words:
            if patSQL.match(word):
                sentencia = word.upper()
                if sentencia != 'SELECT':
                    output.write('\n')
                output.write(indentation_table.get(sentencia, 1) * ' ')
                plantilla = '<b>{}</b>' if html_output else '{}'
                sentencia = sentencia.replace('_', ' ')
                output.write(plantilla.format(sentencia))
            else:
                output.write(word)
            output.write(' ')

    if html_output:
        output.write('</big></pre>')
    if parametros:
        if html_output:
            output.write('<p>Parámetros:</p><ol>')
        else:
            output.write('Parámetros:\n')
        for p in parametros:
            if html_output:
                output.write('<li>{}</li>'.format(p))
            else:
                output.write(' * {}'.format(p))
        if html_output:
            output.write('</ol>')
        else:
            output.write('\n')


# 22/02/2005 12:43:50
# Clases para generacion dinámica de SQL
#

class Delete(object):
    """
    El objetivo de esta clase es poder escribir
    sentencias Delete SQL, de forma sencilla.

    Ejemplo de Uso:

        >>> import Database
        >>> Sql = Database.Delete('Isla')
        >>> Sql.Where('id_Isla', 9)
        >>> print Sql
        DELETE FROM Isla
         WHERE Id_Isla = 9
    """

    def __init__(self, table_name):
        self.Tabla = table_name
        self._Where = []


    def Where(self, condicion):
        self._Where = [' WHERE %s' % condicion]
        return self

    def And(self, condicion):
        if self._Where:
            self._Where.append('   AND %s' % condicion)
        else:
            self._Where = [' WHERE %s' % condicion]
        return self

    def __str__(self):
        assert self._Where, 'No se puede hacer un delete sin especificar un predicado'
        return 'DELETE FROM %s\n%s' % (self.Tabla, '\n'.join(self._Where))


class Insert:
    """
    El objetivo de esta clase es poder escribir
    sentencias Insert SQL, de forma sencilla. Permite ir componiendo la
    sentencia mediante distintas llamadas.
    """

    def __init__(self, tabla):
        """Constructor"""
        self.Tabla = tabla
        self._Fields = []
        self._Values = {}

    def Set(self, nombre, valor):
        """Agregar asignaciones de valores.
        """
        nombre = nombre.upper()
        if nombre not in self._Fields:
            self._Fields.append(nombre)
        if str(valor) == '%s':
            self._Values[nombre] = '%s'
        else:
            self._Values[nombre] = new_field(valor)
        return self
    set = Set

    def get(self, nombre):
        nombre = nombre.upper()
        if nombre in self._Fields:
            return self._Values[nombre].Value
        else:
            raise KeyError('Field not found')
            


    def set_literal(self, nombre, valor):
        nombre = nombre.upper()
        if nombre not in self._Fields:
            self._Fields.append(nombre)
        self._Values[nombre] = str(valor) 
        return self


    def JoinAndFormat(self, values=[], sep=', '):
        if values:
            return sep.join([str(x) for x in values])
        else:
            return ''

    def __str__(self):
        """Retorna la sentencia INSERT en forma de string.

        @return: La sentencia SQL construida.
        @rtype: string
        """
        Fields = self._Fields
        Buff = ['INSERT INTO {} ({})'.format(
            self.Tabla, 
            self.JoinAndFormat(Fields)
            )]
        Values = [self._Values[k] for k in self._Fields]
        Buff.append(' VALUES (%s)' % self.JoinAndFormat(Values))
        return '\n'.join(Buff)

    def __unicode__(self):
        """Retorna la sentencia INSERT en forma de unicode.

        @return: La sentencia SQL construida.
        @rtype: unicode
        """
        Fields = self._Fields
        Buff = [u'INSERT INTO %s (%s)' % (self.Tabla, self.JoinAndFormat(Fields))]
        Values = []
        for Nombre in self._Fields:
            Values.append(self._Values[Nombre])
        Buff.append(u'VALUES (%s)' % self.JoinAndFormat(Values))
        return u'\n'.join(Buff)



class Update:
    """
    DESCRIPCIÓN
    ===========

    El objetivo de esta clase es poder escribir
    sentencias Update SQL, de forma sencilla. Permite ir componiendo la
    sentencia mediante distintas llamadas.

        Ejemplo de Uso:

        >>> import Database
        >>> Sql = Database.Update('Isla')
        >>> Sql.Set('Descripcion', 'San Borondon')
        >>> Sql.Where('Isla = 9')
        >>> print Sql
        UPDATE Isla
           SET Descripcion = 'San Borondon'
         WHERE Isla = 9


    """
    def __init__(self, tabla):
        """Constructor
        """
        self.Tabla = tabla
        self._Fields = []
        self._Values = {}
        self._Where = []

    def Set(self, nombre, valor):
        """Agregar asignaciones de valores.
        """
        nombre = nombre.upper()
        if nombre not in self._Fields:
            self._Fields.append(nombre)
        self._Values[nombre] = new_field(valor)
        return self

    set = Set

    def set_literal(self, nombre, valor):
        nombre = nombre.upper()
        if nombre not in self._Fields:
            self._Fields.append(nombre)
        self._Values[nombre] = str(valor) 
        return self

    def SetExpr(self, nombre, expr):
        nombre = nombre.upper()
        if nombre not in self._Fields:
            self._Fields.append(nombre)
        self._Values[nombre] = expr
        return self

    def where(self, condicion):
        """Añadir una condición WHERE a la consulta.

        @param condicion:Condición de seleccion
        @type condicion: string
        """
        if condicion not in self._Where:
            self._Where.append(condicion)
        return self
    Where = where  # PEP8

    def And(self, condicion):
        """Añadir una condición con el conector AND a la consulta.

        @param condicion: Condición de seleccion
        @type condicion: string
        @see: L{Where}, L{Or}
        """
        if self._Where:
            self._Where.append(condicion)
        else:
            self._Where = [condicion]
        return self

    def __str__(self):
        """Retorna la sentencia UPDATE en forma de string.

        @return: La sentencia SQL construida.
        @rtype: string
        """
        if not self._Values:
            raise ValueError('No se ha especificado una clausula Update Válida. Falta los valores de los campos')
        Buff = ['UPDATE %s' % self.Tabla]
        Nombre = self._Fields[0]
        Value = self._Values[Nombre]
        Buff.append('   SET %s = %s' % (Nombre.capitalize(), Value))
        for Nombre in self._Fields[1:]:
            Value = self._Values[Nombre]
            Buff.append('       , %s = %s' % (Nombre.capitalize(), Value))
        Buff.append(' WHERE %s' % self._Where[0])
        for cond in self._Where[1:]:
            Buff.append('   AND %s' % cond)
        return '\n'.join(Buff)

def isnull_functor(sql, field_name, value):
    if value == True:
        sql.And('%s IS NULL' % field_name)
    else:
        sql.And('%s IS NOT NULL' % field_name)


class Select:
    """
    DESCRIPCIÓN
    ===========

    El objetivo de esta clase es poder escribir
    sentencias Select SQL, de forma sencilla. Permite ir componiendo la
    sentencia mediante distintas llamadas.

    Es especialmente apropiada para ser usada cuando tenemos un número
    variable de condiciones de búsqueda, ya que podemos ir añadido
    las condiciones mediante llamadas al método L{And}, sin preocuparnos
    de llamar antes al Where

    Ejemplo de Uso:

        >>> import Database
        >>> Sql = Database.Select('Codigo, Descripcion')
        >>> Sql.From('Isla')
        >>> Sql.Where('Codigo Between 2 and 5')
        >>> print Sql
        SELECT Codigo, Descripcion
          FROM Isla
         WHERE Codigo Between 2 and 5
    """

    def __init__(self, fields='*'):
        """Constructor

        Constructor de la clase Select.

        Ejemplo de uso:

            >>> import Database
            >>> Sql = Database.Select('Codigo, Descripcion')
            >>> Sql.From('Isla')
            >>> print Sql
            SELECT Codigo, Descripcion
              FROM Isla

        @param fields: String con los nombres de los campos que se quieren obtener.
                       por defecto vale '*'
        @type fields: string
        """
        self._Fields = fields
        self._From = None
        self._Joins = []
        self._Where = []
        self._GroupBy = None
        self._OrderBy = None
        self._Limit = None

    @property
    def table(self):
        return self._From

    def add_field(self, field):
        self._Fields += ', %s' % field
        return self

    AddField = add_field
    addField = add_field

    def From(self, tabla):
        """Añadir una o varias tablas a la consulta.

        Ejemplo de uso:

            >>> import Database
            >>> Sql = Database.Select('U.Nombre, U.Apellido1, U.Apellido2, S.Nombre')
            >>> Sql.From('Comun.Usuario U, Comun.Servicio S')
            >>> Sql.Where('U.Id_Servicio = S.Id_Servicio')
            >>> print Sql
            SELECT U.Nombre, U.Apellido1, U.Apellido2, S.Nombre
              FROM Comun.Usuario U, Comun.Servicio S
             WHERE U.Id_Servicio = S.Id_Servicio

        @param tabla: Nombres de la tabla o tablas a consultar, separados por comas
        @type tabla: string
        """
        self._From = tabla
        return self

    def LeftJoin(self, tabla, condicion):
        """Añadir un left join a la consulta.

        Ejemplo de uso:

            >>> import Database
            >>> Sql = Database.Select('U.Nombre, U.Apellido1, U.Apellido2, S.Nombre')
            >>> Sql.From('Comun.Usuario U')
            >>> Sql.LeftJoin('Comun.Servicio S', 'U.Id_Servicio = S.Id_Servicio')
            >>> print Sql
            SELECT U.Nombre, U.Apellido1, U.Apellido2, S.Nombre
              FROM Comun.Usuario U
                   LEFT JOIN Comun.Servicio S ON U.Id_Servicio = S.Id_Servicio

        @see: L{Join}
        @param tabla: Nombres de la tabla con la que se va a hacer el join
        @type tabla: string
        @param condicion:Condición de seleccion del Join
        @type condicion: string
        """

        self._Joins.append('       LEFT JOIN %s ON %s' % (tabla, condicion))
        return self

    def Join(self, tabla, condicion):
        """Añadir un inner join a la consulta.

        Ejemplo de uso:

            >>> import Database
            >>> Sql = Database.Select('U.Nombre, U.Apellido1, U.Apellido2, S.Nombre')
            >>> Sql.From('Comun.Usuario U')
            >>> Sql.Join('Comun.Servicio S', 'U.Id_Servicio = S.Id_Servicio')
            >>> print Sql
            SELECT U.Nombre, U.Apellido1, U.Apellido2, S.Nombre
              FROM Comun.Usuario U
                   JOIN Comun.Servicio S ON U.Id_Servicio = S.Id_Servicio

        @see: L{LeftJoin}
        @param tabla: Nombres de la tabla con la que se va a hacer el join
        @type tabla: string
        @param condicion:Condición de seleccion del Join
        @type condicion: string
        """
        self._Joins.append('       JOIN %s ON %s' % (tabla, condicion))
        return self

    def filter(self, **kwargs):
        tron = kwargs.pop('tron', False)
        for k in kwargs:
            self.And(get_predicate(k, kwargs[k]))

    def Where(self, condicion):
        """Añadir una condición WHERE a la consulta.

        Ejemplo de Uso:

            >>> import Database
            >>> Sql = Database.Select('Id_Servicio, Nombre')
            >>> Sql.From('Comun.Servicio')
            >>> Sql.Where('Id_Servicio = 25')
            >>> print Sql
            SELECT Id_Servicio, Nombre
              FROM Comun.Servicio
             WHERE Id_Servicio = 25

        @see: L{And}, L{Or}
        @param condicion:Condición de seleccion
        @type condicion: string
        """
        if self._Where:
            self._Where.append('   AND %s' % condicion)
        else:
            self._Where = [' WHERE %s' % condicion]
        return self

    def And(self, condicion):
        """Añadir una condición con el conector AND a la consulta.

        Ejemplo de Uso:

            >>> import Database
            >>> Sql = Database.Select('Id_Servicio, Nombre')
            >>> Sql.From('Comun.Servicio')
            >>> Sql.Where('Id_Servicio >= 20')
            >>> Sql.And('Id_Servicio <= 25')
            >>> print Sql
            SELECT Id_Servicio, Nombre
              FROM Comun.Servicio
             WHERE Id_Servicio >= 20
               AND Id_Servicio <= 25

        @param condicion: Condición de seleccion
        @type condicion: string
        @see: L{Where}, L{Or}
        """
        if self._Where:
            self._Where.append('   AND %s' % condicion)
        else:
            self._Where = [' WHERE %s' % condicion]
        return self

    def AndOpenPar(self, condicion=''):
        """Añadir una subcondición, abriendo paréntesis, con el conector AND a la consulta.

        @param condicion: Condición de seleccion
        @type condicion: string
        @see: L{Where}, L{Or}
        """
        if condicion:
            self._Where.append('   AND ( %s' % condicion)
        else:
            self._Where.append('   AND (')
        return self

    def OrOpenPar(self, condicion):
        """Añadir una subcondición, abriendo paréntesis, con el conector AND a la consulta.

        @param condicion: Condición de seleccion
        @type condicion: string
        @see: L{Where}, L{Or}
        """
        self._Where.append('   OR ( %s' % condicion)
        return self

    def ClosePar(self):
        self._Where.append('   )')
        return self

    def Or(self, condition):
        """Añadir una condición con el conector OR a la consulta.

        Ejemplo de Uso:

            >>> import Database
            >>> Sql = Database.Select('Id_Servicio, Nombre')
            >>> Sql.From('Comun.Servicio')
            >>> Sql.Where('Id_Servicio = 20')
            >>> Sql.Or('Id_Servicio = 25')
            >>> print Sql
            SELECT Id_Servicio, Nombre
              FROM Comun.Servicio
             WHERE Id_Servicio = 20
                OR Id_Servicio = 25

        @see: L{Where}, L{And}
        @param condition: Condición de seleccion
        @type condition: string
        """
        if self._Where:
            self._Where.append('    OR %s' % condition)
        else:
            self._Where = [' WHERE %s' % condition]
        return self

    def GroupBy(self, agrupacion):
        self._GroupBy = agrupacion
        return self

    def OrderBy(self, ordenacion):
        """Añadir una clausula de Ordenación a la consulta.

        Ejemplo de Uso:

            >>> import Database
            >>> Sql = Database.Select('Id_Servicio, Nombre')
            >>> Sql.From('Comun.Servicio')
            >>> Sql.OrderBy('Id_Servicio DESC')
            >>> print Sql
            SELECT Id_Servicio, Nombre
              FROM Comun.Servicio
             ORDER BY Id_Servicio DESC

        @param ordenacion: Condición de ordenación
        @type ordenacion: string
        """
        self._OrderBy = ordenacion
        return self

    def Limit(self, n):
        self._Limit = n
        return self

    def __str__(self):
        """Retorna la sentencia SQL en forma de string.

        Retorna la sentencia SQL conpuesta con las funciones L{From},
        L{Join}, L{LeftJoin}, L{Where}, L{And}, L{Or} y L{OrderBy}. Vease
        cualquiera de estas funciones para ver un ejemplo de __str__.

        @return: La sentencia SQL construida.
        @rtype: string
        """
        Buff = ['SELECT %s' % self._Fields]
        Buff.append('  FROM %s' % self._From)
        if self._Joins:
            Buff += self._Joins
        if self._Where:
            Buff += self._Where
        if self._GroupBy:
            Buff.append(' GROUP BY %s' % self._GroupBy)
        if self._OrderBy:
            Buff.append(' ORDER BY %s' % self._OrderBy)
        if self._Limit:
            Buff.append(') WHERE ROWNUM <= %d' % self._Limit)
        return '\n'.join(Buff)


class GRUPO(dict):
    def __init__(self):
        super(GRUPO, self).__init__()
        self._Indices = []

    def __setitem__(self, key, value):
        if key not in self._Indices:
            self._Indices.append(key)
        super(GRUPO, self).__setitem__(key, value)

    def __delitem__(self, key):
        super(GRUPO, self).__delitem__(self, key)
        self._Indices.remove(key)

    def clear(self):
        super(GRUPO, self).clear(self)
        self._Indices = []

    def popitem(self):
        try:
            key = self._Indices[-1]
        except IndexError:
            raise KeyError('dictionary is empty')
        val = self[key]
        del self[key]
        return (key, val)

    def setdefault(self, key, failobj = None):
        result = super(GRUPO, self).setdefault(key, failobj)
        if key not in self._Indices:
            self._Indices.append(key)
        return result


    def update(self, dict):
        super(GRUPO, self).update(self, dict)
        for key in dict.keys():
            if key not in self._Indices: self._Indices.append(key)

    def values(self):
        return [self[k] for k in self._Indices]

    def __iter__(self):
        self._Index = 0
        self._Len = len(self._Indices)
        return self

    def keys(self):
        return self._Indices

    def __next__(self):
        if self._Index >= self._Len:
            raise StopIteration
        key = self._Indices[self._Index]
        self._Index += 1
        return (key, self[key])

    next = __next__


def agrupa(rows, key_name, transform=None):
    result = GRUPO()
    for row in rows:
        if transform:
            key = transform(row, key_name)
        else:
            key = row[key_name]
        result.setdefault(key, []).append(row)
    return result


def get_count(db, table, where='', tron=False):
    sql = Select('Count(*)')
    sql.From(table)
    if where:
        sql.Where(where)
    if tron:
        print_sql(sql, output=tron)
    row = db.getRow(sql)
    if row:
        return row[0]
    else:
        return None


def getEscalar(db, sql):
    row = db.getRow(sql)
    return row[0] if row else 0
get_escalar = getEscalar  # PEP8


_unicode_mapa = {
     33: None,  # exclamation mark
     34: None,  # double quotes
     35: None,  # hash
     36: None,  # dollar
     37: None,  # percent
     38: None,  # ampersand
     39: None,  # simple quote
     40: None,  # open par
     41: None,  # close par
     42: None,  # asterisk
     43: 32,    # plus -> space
     44: None,  # comma
     46: None,  # dot or full stop
     47: 45,    # slash -> hyphen
     58: 45,    # colon -> hyphen
     59: 45,    # semicolon -> hyphen
     60: None,  # open angled bracket
     61: 45,    # equals -> hyphen
     62: None,  # close angled bracket
     63: None,  # question mark
     64: 45,    # @ -> hyphen
     91: None,  # open bracket
     92: 45,    # backslash -> hyphen
     93: None,  # close bracket
     94: 45,    # caret -> hyphen
     95: 45,    # underscore -> hyphen
     96: None,  # grave accent
    123: None,  # open brace
    124: 45,    # pipe -> hyphen
    125: None,  # close brace
    126: 45,    # equivalency sign (~) -> hyphen
    133: 45,    # ellipsis 
    191: None,  # open question mark
    193: 65,
    201: 69,
    205: 73,
    209: 78,
    211: 79,
    218: 85,
    220: 85,
    225: 97,  # a
    233: 101,
    237: 105,
    241: 110,
    243: 111,
    250: 117,
    252: 117,
    8230: 45   # ellipsis
    }

def QuitaAcentos(s, encoding='utf-8'):
    global _unicode_mapa
    if isinstance(s, six.binary_type):
        s = s.decode(encoding)
        s = s.translate(_unicode_mapa)
        return s.encode(encoding)
    else:
        return s.translate(_unicode_mapa)

_mapa_normaliza_texto = dict(zip(
    [ord(x) for x in u'áéíóúüÁÉÍÓÚÜ'],
    [ord(x) for x in 'AEIOUUAEIOUU']
    ))

def normaliza_texto(s):
    global _mapa_normaliza_texto
    if type(s) != six.text_type:
        s = s.decode('utf-8')
    s = s.upper()
    s = s.translate(_mapa_normaliza_texto)
    return s


def escape_string(s):
    s = s.replace('"', '\\"')
    s = s.replace("'", "\\'")
    s = s.replace('\r', '')
    s = s.replace('\t', '\\t')
    words = s.split('\n')
    return '\\n'.join(words)


def encode_value_as_json(value):
    if value is None:
        return 'null'
    elif isinstance(value, (six.string_types, six.text_type)):
        return '"%s"' % escape_string(value)
    elif isinstance(value, bool):
        return 'true' if value else 'false'
    elif isinstance(value, (int, float)):
        return str(value)
    elif isinstance(value, (datetime.datetime, Fechas.TIMESTAMP)):
        return '"%04d-%02d-%02dT%02d:%02d:%02d"' % (
            value.year, value.month, value.day,
            value.hour, value.minute, value.second
            )
    elif isinstance(value, (datetime.date, Fechas.FECHA)):
        return '"%04d-%02d-%02d"' % (value.year, value.month, value.day)
    raise ValueError('Ni idea de lo que es esto: %s' % value)
    return str(value)


def encode_row_as_json(row, indent=4):
    tabs = ' ' * indent
    result = []
    for name, value in row.items():
        result.append('{}"{}": {}'.format(
            tabs,
            name.lower(),
            encode_value_as_json(value)
            ))
    all_values = ',\n'.join(result)
    return '%s{\n%s\n%s}' % (tabs, all_values, tabs)

def pivota(rows, new_columns, aggregate_func=sum, default=0):
    if len(new_columns) < 2:
        raise ValueError('Es obligatorio incluir al menos dos valores para pivotar')
    kernel = {}
    new_rows = []
    a_name = rows[0]._Names[0]
    for row in rows:
        if row[0] not in new_rows:
            new_rows.append(row[0])
        key = (row[0], row[1])
        kernel.setdefault(key, []).append(row[2])
    result = []
    for a in new_rows:
        row = Row([a_name], [a])
        for b in new_columns:
            k = (a, b)
            l = kernel.get(k, [])
            row.addValue(b, aggregate_func(l) if l else default)
        result.append(row)
    return result


def dump(*rows):
    buff = []
    if rows:
        first_row = rows[0]
        cols = first_row.keys()
        num_cols = len(cols)
        widths = [0] * num_cols
        for i, s in enumerate(first_row.keys()):
            widths[i] = len(s)
        for row in rows:
            for i in range(num_cols): 
                v = str(row[i])
                widths[i] = max(widths[i], len(v))
        masks = ['>{:d}s'.format(w) for w in widths]
        sep = ''
        for i, name in enumerate(cols):
            buff.append(sep)
            buff.append(format(name, masks[i]))
            sep = '|'
        buff.append('\n')
        for row in rows:
            sep = ''
            for i in range(num_cols): 
                buff.append(sep)
                v = str(row[i])
                buff.append(format(v, masks[i]))
                sep = '|'
            buff.append('\n')
    return ''.join(buff)
 
  

