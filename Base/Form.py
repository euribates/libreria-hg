#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import cgi
import cgitb
import collections
import decimal
import html
import os
import re
import six
import sys
import types

from Libreria.Comun import Fechas

'''
DESCRIPCIÓN
===========

Formularios en Html (Generación y procesamiento).

GENERACIÓN DE FORMULARIOS
=========================

Para la generación de formularios, las funcionas más utiles son
L{Begin} y L{End}, que imprimen el código HTML de inicio y fin del
formulario. Las funciones L{Tabla}, L{Fila} y L{FinTabla} son solo de
decoración, ya que nos permiten ubicar los controles del
formulario en una sencilla tabla HTML. La función L{261}
puede ser util para localizar problemas.

Para la generación de los controles del formulario, se han definido
una serie de clases, una para cada tipo de control. Todas las clases
de controles derivan de una clase base, L{WIDGET}, por lo que todas
comparten el tener un nombre y un valor (Atributos C{Nombre} y C{Valor},
también accesibles como C{Name} y C{Value}.

Los controles definidos en este módulo son:

    - L{Text}: recuadro de texto
    - L{Password}: Como el anterior, pero oculta los caracteres
                   que se teclean.
    - L{Radio}: Control de opción de tipo redondo (todos los
                controles de este tipo con el mismo nombre
                serán mutuamente excluyentes).
    - L{CheckBox}: Control de opción de tipo cuadrado (los
                   controles de este tipo con el mismo nombre
                   B{no} serán mutuamente excluyentes).
    - L{Label}: Valor de solo lectura. Se muestra y se envia
                pero no puede ser modificado.
    - L{Hidden}: Valor oculto. no se muestra pero se enviará.
    - L{Submit}: Botón de envio de los datos
    - L{Textarea}: Igual que text, pero permite un mayor espacio
                   para escribir texto. Se le pueden definir las
                   columnas y filas que deseemos.
    - L{SELECT}: Un combobox
    - L{IMAGE}: Permite utilizar una imagen como un Submit. De esta forma
                se puede cambiar la apariencia de los botones, si lo
                deseamos. Ademas, se pasarán como datos las coordenadas
                C{X} e C{Y} de la imagen en las cuales se ha pinchado
                con el ratón.
    - L{FILE}: Permite subir un fichero completo. En caso de usar
               este control, se debe especificar el parámetro
               C{enctype} con el valor C{"multipart/form-data"}
               en la llamda al L{Form.Begin}.

RECUPERACION DE LOS DATOS
=========================

    Para recuperar los datos enviados desde un formulario CGI las
    funciones más importantes son L{getValue} y L{getValues}. El caso
    de los ficheros es especial, y require dos funciones adicionales,
    L{getFile} y L{getFilename}
 '''


FORMDATA = cgi.FieldStorage()
method = os.environ.get('REQUEST_METHOD')
POST = FORMDATA if method == 'POST' else None


def set_form_data(d):
    global FORMDATA
    FORMDATA = d


def is_post():
    global method
    return method == 'POST'


def get_method():
    global method
    return method


def has_key(name):
    global FORMDATA
    return name in FORMDATA


def escape(s, quote=None):
    """Retorna la string lista para ser representada como HTML.

    Convierte los caracteres C{&}, C{<} y C{>} en entidades Html,
    de forma que se puedan visualizar correctamente en un navegador. Si
    el parámetro opcional C{quote} es verdadero, tambien se incluye
    en la transformación el caracter doble comillas (").

    Ejemplo de uso:

        >>> import Form
        >>> print(Form.escape('Sea la variable x < 33 y z > 22'))
        Sea la variable x &lt; 33 y z &gt; 22
        >>> print(Form.escape('<hr size="1">', True))
        &lt;hr size=&quot;1&quot;&gt;

    @param s: String de texto a transformar
    @param quote: Indica si debe tambien transformar el caracter
                  doble comillas (C{"})
    @return: La string transformada
    @rtype: string
    """
    return html.escape(str(s), quote)


Escape = escape


def AsFecha(item):
    """Transforma la entrada en un objeto del
    tipo L{Libreria.Comun.Fechas.FECHA}

    Ejemplo:

        >>> import Form
        >>> print(Form.AsFecha(895903.23))
        11/1/1970
        >>> print(Form.AsFecha('14/09/2003'))
        14/9/2003

    @param item: dato a convertir en fecha. Normalmente una string o un float
    @return: una variable de tipo FECHA, o C{None} si la entrada es nula.
    @rtype: L{Libreria.Comun.Fechas.FECHA}
    @see: L{getValue}, L{getValues}
    """
    if item is None:
        return None
    return Fechas.FECHA(item)


def AsTimestamp(item):
    """Transforma la entrada en un objeto del
    tipo L{Libreria.Comun.Fechas.TIMESTAMP}

    Ejemplo:

        >>> import Form
        >>> print(Form.AsFecha(895903.23))
        11/1/1970
        >>> print(Form.AsFecha('14/09/2003'))
        14/9/2003

    @param item: dato a convertir en fecha. Normalmente una string o un float
    @return: una variable de tipo FECHA, o C{None} si la entrada es nula.
    @rtype: L{Libreria.Comun.Fechas.FECHA}
    @see: L{getValue}, L{getValues}
    """
    if item is not None:
        return Fechas.TIMESTAMP(item)
    else:
        return None


def NombrePropio(s):
    '''
    Retorna la frase pasada (normalmente un nombre)
    con las palabras iniciales de cada letra pasadas a Mayúsculas,
    como si fueran nombre propios.

    Ejemplo de uso:

        >>> from Libreria.Base import Form
        >>> print(Form.NombrePropio('la laguna'))
        La Laguna
        >>> print(Form.NombrePropio('CLAUDIA SCHIFFER'))
        Claudia Schiffer

    @return: La string, como si fueran un nombre propio.
    @rtype: string
    '''

    if s:
        return ' '.join([w.capitalize() for w in s.split()])
    else:
        return s


def AsSQLValue(item):
    '''
    apropiada para construir una sentencia SQL. Por ejemplo, si es una
    string, devolverá la string rodeada por un par de caracteres de tipo
    comilla simple, o NULL si la string está vacia.

    @attention: Es mejor utilizar las funciones definidas en el módulo
    L{Libreria.Base.Database}

    Ejemplo de uso:

        >>> from Libreria.Base import Form
        >>> print(Form.AsSQLValue('la laguna'))
        'la laguna'
        >>> print(Form.AsSQLValue(''))
        NULL
        >>> print(Form.AsSQLValue(23))
        23

    @return: Una string, representando el valor, adecuada para ser usada
             en una sentencia SQL
    @rtype: string
    '''
    if item == 0:
        return '0'
    elif item is None:
        return 'NULL'
    elif isinstance(item, (types.IntType, types.LongType)):
        return '%d' % item
    elif isinstance(item, types.FloatType):
        return '%f' % item
    elif isinstance(item, Fechas.FECHA):
        return item.AsOracle()
    else:
        item = str(item).strip().replace("'", "''")
        return "'%s'" % item


def AsString(item):
    '''
    @attention: Usar mejor la función L{AsSQLValue} o, mejor aún,
                 las funciones definidas en el módulo
                 L{Libreria.Base.Database}.
    @rtype: string
    '''
    if item:
        return "'%s'" % item
    else:
        return 'NULL'


def AsInteger(item):
    '''
    @attention: Usar mejor la función int directamente. Es más claro.
    @rtype: int
    '''
    return int(item)


def AsFinancial(f):
    '''
    @attention: Usar mejor la función L{Libreria.Base.View.AsNumero}
    @rtype: string
    '''
    s = '%.2f' % float(f)
    (entero, fraccion) = s.split('.')
    longitud = len(entero)
    resto = len(entero) % 3
    lista = []
    if resto:
        lista.append(entero[:resto])
    for inicio in range(resto, longitud, 3):
        lista.append(entero[inicio:inicio+3])
    resultado = '.'.join(lista) + ',%s' % fraccion
    return resultado


def AsHtml(text):
    '''
    @attention: Usar mejor la función L{Escape}
    @rtype: string
    '''
    text = str(text)
    text = text.replace('&', '&amp;')
    text = text.replace('"', '&quot;')
    text = text.replace('<', '&lt;')
    text = text.replace('>', '&gt;')
    return text


def Debug():
    '''
    Muestra información detallada de las variables pasadas mediante CGI.

    También genera código Python que podria ser válido para
    capturar dichas variables. Funciona mejor si se le pasan datos
    de ejemplo.
    '''
    global FORMDATA
    Code = []
    print('<h2 style="border-bottom: 1px solid black">Form Debug</h2>')
    print('<dl>')
    for name in FORMDATA.keys():
        value = FORMDATA[name]
        if isinstance(value, collections.Iterable):
            print('<dt>Nombre: <b>%s</b></dt>' % name)
            print('<dd><ul>')
            for item in value:
                print('<li>"%s"</li>' % str(item.value))
            print('</ul></dd>')
        else:
            print('<dt><tt>%s</tt></dt>'.format(name))
            print('<dd>{}</dd>'.format(value))
            Code.append('''#~ %s = Form.getValue('%s')\n''' % (
                name.capitalize(),
                name,
                ))
    print('</ul>')
    print('<h2 style="border-bottom: 1px solid black">Código</h2>')
    print('<pre>')
    print(''.join(Code))
    print('</pre>')
    print(
        '<h2 style="border-bottom: 1px solid black">'
        'Información del sistema'
        '</h2>'
        )
    print('<ul>')
    print('<li>sys.version: <tt>', sys.version, '</tt>')
    print('<li>sys.platform: <tt>', sys.platform, '</tt>')
    print('<li>os.getcwd(): <tt>', os.getcwd(), '</tt>')
    print('</ul>')


def getFilename(name, transform=None, default=None):
    '''
    Obtiene el nombre de un fichero que se haya subido mediante
    un formulario. si existe dicho fichero, o C{None} en caso de que
    no exista.

    @param name: nombre CGI
    @type name: string
    @return: Nombre del fichero o None
    @rtype: string
    @see: L{getValue}, L{getValues}, L{getFile}
    '''
    global FORMDATA
    if name not in FORMDATA:
        return default
    fileitem = FORMDATA[name]
    if fileitem.file:
        result = fileitem.filename
        if '\\' in result:
            result = result.split('\\')[-1]
        if transform:
            result = transform(result)
        return result or None
    else:
        return default


get_filename = getFilename  # PEP-8


def getFile(name, default=None):
    '''
    Obtiene el descriptor del fichero que se haya subido mediante
    un formulario. si existe dicho fichero, o C{None} en caso de que
    no exista.

    @param name: nombre CGI
    @type name: string
    @return: Fichero enviado, ya abierto. o C{None}
    @rtype: file
    @see: L{getValue}, L{getValues}, L{getFilename}
    '''
    global FORMDATA
    if name not in FORMDATA:
        return default
    fileitem = FORMDATA[name]
    return fileitem.file if fileitem.file else default


get_file = getFile  # PEP-8


def get_value(name, transform=None, default=None):
    '''Obtiene un valor suministrado por CGI.

    Ejemplo de uso:

        >>> from Libreria.Base import Form
        >>> a = Form.getValue('PEPE')
        >>> print(a)
        None
        >>> a = Form.getValue('PEPE', default="hola")
        >>> print(a)
        hola
        >>> # Simulación del paso de valor CGI
        >>> import cgi
        >>> Form.FORMDATA = {'NUMERO':cgi.MiniFieldStorage('NUMERO', '23')}
        >>> # Fin de simulacion
        >>> f = lambda x: int(x) * 2
        >>> a = Form.getValue('NUMERO', transform=f)
        >>> print(a, type(a))
        46 <type 'int'>


    @param name: Nombre del valor suministrado
    @type name: string

    @param transform: Función de transformación. Si se especifica,
                      se le aplicara esta función al valor proporcionada
                      por CGI, y getValue retornará el resultado de la
                      función (Opcional)

    @param default: Valor por defecto, en caso de que no se haya
                    suministrado ningún valor por CGI (Opcional)

    @return: El valor suministrado por CGI
    @rtype: string, o el tipo que retorne la función C{transform}, si
            procede.
    @see: L{getValues}, L{getFile}, L{getFilename}
    '''
    global FORMDATA
    v = default
    if name in FORMDATA:
        v = FORMDATA[name].value
        if not v:
            return default
        if transform:
            v = transform(v)
    return v


getValue = get_string = get_value  # PEP-8


def get_text(name, default=None):
    '''Always return unicode.
    '''
    value = get_value(name, default=default)
    if isinstance(value, six.binary_type):
        value = value.decode('utf-8')
    return value


def get_integer(name, default=0):
    v = getValue(name, default=default)
    if v in ('', None):
        return default
    else:
        return int(v)


def get_decimal(name, default=decimal.Decimal('0')):
    v = getValue(name, default=None)
    if v is None:
        return default
    v = v.strip()
    if ',' in v:
        v = v.replace('.', '')
        v = v.replace(',', '.')
    return decimal.Decimal(v)


def getFecha(name, default=None, sep='_'):
    '''Obtiene un valor suministrado por CGI, de tipo fecha.

    Esta función está especializada en recoger valores
    de tipo fecha. Por cgi se le puede pasar la fecha como una �nica
    string, en el formato DD/MM/YYYY, o mediante tres valores, uno
    para el dia, otro para el mes y otro para el año. En este
    último caso, los nombres de las variables que se buscaran
    se componen usando el parámetro nombre, el separador
    sep (por defecto, el carcter subrayado _) y los sufijos DIA, MES y ANIO.

    Ejemplo de uso:

        >>> from Libreria.Base import Form
        >>> # Simulación del paso de valor CGI
        >>> import cgi
        >>> Form.FORMDATA['F_DIA'] = cgi.MiniFieldStorage('F_DIA', '23')
        >>> Form.FORMDATA['F_MES'] = cgi.MiniFieldStorage('F_MES', '7')
        >>> Form.FORMDATA['F_ANIO'] = cgi.MiniFieldStorage('F_ANIO', '2005')
        >>> # Fin de simulacion
        >>> f = Form.getFecha('F')
        >>> print(f, type(f))
        23/7/2005 <class 'Libreria.Comun.Fechas.FECHA'>


    @param name: Nombre del valor suministrado
    @type name: string

    @param default: Valor por defecto, en caso de que no se haya
                    suministrado ningún valor por CGI (Opcional)
    @type default: cualquiera

    @param sep: Separador usado para componer los nombres de las variables,
                en caso de que vengan separados los componentes
                día, mes y año.
    @type sep: string

    @return: Una fecha con el valor suministrado por CGI, o el valor
             por defecto si procede.
    @rtype: tipo Libreria.Comun.Fechas.FECHA, o el tipo del valor por defecto.
    @see: L{getValue}, L{getValues}, L{getFile}, L{getTimestamp}
          , L{getFilename}, L{Libreria.Base.Controles.getMultiComboFecha}
    '''
    Fecha = getValue(name, transform=AsFecha, default=None)
    if not Fecha:
        dd = getValue(sep.join([name, 'DIA']), int)
        mm = getValue(sep.join([name, 'MES']), int)
        aaaa = getValue(sep.join([name, 'ANIO']), int)
        if dd and mm and aaaa:
            Fecha = Fechas.FECHA()
            Fecha.setDMA(dd, mm, aaaa)
    return Fecha or default


get_fecha = get_date = getFecha  # PEP-8


def getHora(name='HORA', default=None, sep='_'):
    hh = getValue('%s%sHH' % (name, sep))
    mm = getValue('%s%sMM' % (name, sep))
    if hh and mm:
        result = '%02d:%02d' % (int(hh), int(mm))
    else:
        result = getValue('HORA', default=default)
    return result


get_hora = getHora  # PEP-8


def getFechaHora(name, default=None, sep='_'):
    '''Obtiene un valor suministrado por CGI, de tipo fecha y hora (Timestamp).

    Esta función está especializada en recoger valores de
    tipo L{Libreria.Comun.Fechas.TIMESTAMP}. Por
    cgi se le puede pasar la fecha como una unica string, en el formato
    dia/mes/año hh:mm:ss, o mediante seis valores, para el dia, mes, el año,
    horas, munitus y, opcionalmente, segundos. En este último caso,
    los nombres de las variables que se buscaran se componen usando el
    parámetro nombre, el separador sep (por defecto, el caracter
    subrayado _) y los sufijos DIA, MES y ANIO, HH, MM y SS.

    Ejemplo de uso:

        >>> from Libreria.Base import Form
        >>> # Simulación del paso de valor CGI
        >>> import cgi
        >>> Form.FORMDATA['F_DIA'] = cgi.MiniFieldStorage('F_DIA', '23')
        >>> Form.FORMDATA['F_MES'] = cgi.MiniFieldStorage('F_MES', '7')
        >>> Form.FORMDATA['F_ANIO'] = cgi.MiniFieldStorage('F_ANIO', '2005')
        >>> Form.FORMDATA['F_HH'] = cgi.MiniFieldStorage('F_HH', '12')
        >>> Form.FORMDATA['F_MM'] = cgi.MiniFieldStorage('F_MM', '32')

        >>> # Fin de simulacion
        >>> f = Form.getFechaHora('F')
        >>> print(f, type(f))
        23/07/2005 12:32:30 <class 'Libreria.Comun.Fechas.TIMESTAMP'>


    @param name: Nombre del valor suministrado
    @type name: string

    @param default: Valor por defecto, en caso de que no se haya
                    suministrado ningún valor por CGI (Opcional)
    @type default: cualquiera

    @param sep: Separador usado para componer los nombres de las
                variables, en caso de que vengan separados los
                componentes día, mes y año.
    @type sep: string

    @return: Una fecha con el valor suministrado por CGI, o el valor
             por defecto si procede.
    @rtype: tipo Libreria.Comun.Fechas.TIMESTAMP, o el tipo del
            valor por defecto.
    @see: L{getValue}, L{getValues}, L{getFile}, L{getTimestamp}
          , L{getFilename}, L{Libreria.Base.Controles.getMultiComboFechaHora}
    '''
    FechaHora = getValue(name, transform=AsTimestamp, default=None)
    if not FechaHora:
        dia = getValue(sep.join([name, 'DIA']), transform=int)
        mes = getValue(sep.join([name, 'MES']), transform=int)
        anio = getValue(sep.join([name, 'ANIO']), transform=int)
        horas = getValue(sep.join([name, 'HH']), transform=int)
        minutos = getValue(sep.join([name, 'MM']), transform=int)
        segundos = getValue(sep.join([name, 'SS']), transform=int, default=30)
        if dia and mes and anio:
            FechaHora = Fechas.TIMESTAMP()
            FechaHora.setDMAHMS(dia, mes, anio, horas, minutos, segundos)
    return FechaHora or default


get_fecha_hora = getFechaHora  # PEP-8


def getTimestamp(name, default=None):
    '''Obtiene un valor suministrado por CGI, de tipo timestamp.

    @param name: Nombre del valor suministrado
    @type name: string

    @param default: Valor por defecto, en caso de que no se haya
                    suministrado ningún valor por CGI (Opcional)
    @type default: string o una variable de tipo TIMESTAMP


    @return: Un timestamp con el valor suministrado por CGI, o el valor
             por defecto si procede.
    @rtype: tipo Libreria.Comun.Fechas.TIMESTAMP.
    @see: L{getValue}, L{getValues}, L{getFile}, L{getFecha}
          , L{getFilename}, L{Libreria.Base.Controles.getMultiComboFecha}
    '''
    Timestamp = getValue(name, transform=AsTimestamp)
    return Timestamp or default


get_timestamp = getTimestamp  # PEP-8


def getValues(name, transform=None, default=[]):
    '''Obtiene un lista de valores suministrado por CGI.

    Ejemplo de uso:

        >>> from Libreria.Base import Form
        >>> # Simulación del paso de valor CGI
        >>> import cgi
        >>> Form.FORMDATA = {'NUMEROS':
        ...  [cgi.MiniFieldStorage('NUMERO', '23')
        ...  , cgi.MiniFieldStorage('NUMERO', '12')]
        ...  }
        >>> # Fin de simulacion
        >>> a = Form.getValues('NUMEROS', transform=int)
        >>> print(a, type(a))
        [23, 12] <type 'list'>

    @param name: Nombre del valor suministrado
    @type name: string

    @param transform: Función de transformación. Si se especifica,
                      se le aplicara esta función a cada uno de los
                      valores proporcionada por CGI, y getValue retornará
                      la lista de resultados.
                      (Opcional)

    @return: La lista de valores suministrado por CGI
    @rtype: list
    @see: L{getValue}, L{getFile}, L{getFilename}
    '''
    global FORMDATA

    if name in FORMDATA:
        result = []
        if isinstance(FORMDATA[name], type([])):
            for item in FORMDATA[name]:
                value = item.value
                if transform:
                    value = transform(value)
                result.append(value)
        else:
            value = FORMDATA[name].value
            if transform:
                value = transform(value)
            result.append(value)
        return result
    else:
        return default


get_values = getValues  # PEP-8


def getRango(name):
    '''Obtiene un valor pasado por cgi interpretandolo como un rango.

    Se supone que se usará para obtener uno o dos valores, siendo el
    segundo opcional y separado del primero por cualquiera de los
    caracteres más, menos, barra invertida, asterisco, punto o
    dos puntos.

    En caso de no especificarse el segundo valor, se retornará cero (0).
    '''
    patSeparador = re.compile('[/+*.:~-]')
    s = getValue(name)
    if not s:
        return (None, 0)
    items = patSeparador.split(s)
    n_items = len(items)
    if n_items == 1:
        return (int(items[0]), 0)
    elif n_items == 2:
        return (int(items[0]), int(items[1]))
    else:
        raise ValueError(
            'La expresión {} no es una expresión de rango válida'.format(
                escape(s)
                )
            )


get_rango = getRango  # PEP-8


def getBoolean(name, default=False):
    s = getValue(name, None)
    if s is None:
        return default
    else:
        return s in 'sSyY' or s.lower() == 'true'


get_boolean = getBoolean  # PEP-8


def get_tron(default=False):
    result = getBoolean('tron', default=default)
    if result:
        cgitb.enable()
        sys.stderr = sys.stdout
    return result


def Begin(action, method="POST", **kwargs):
    '''Imprime las marcas HTML de inicio del formulario.

    El parametro C{action} es obligatorio, y debe contener un URL
    (relativo o absoluto) al I{script} que recibirá los
    datos del formulario. El parámetro C{enctype} sólo será
    necesario especificarlo cuando queramos subir un fichero
    completo, mediante el control L{FILE}.

    A efectos de presentación, se engloba todo el
    formulario en un C{div} de clase C{NAVEGACION}. Esto nos
    permite que, cuando se vaya a imprimir, no se represente
    el formulario.

    Ejemplo:

        >>> from Libreria.Base import Form
        >>> Form.Begin('debug.py')
        <form class="form" action="debug.py" method="POST">


    @param action: URL (relativo o absoluto) al I{script} que
                   recibirá los datos del formulario

    @param method: Metodo de codificacion CGI del formulario. El
                   valor por defecto, C{POST}, es el habitual, pero
                   se puede indicar también C{GET}, que pasaría los
                   valores codificados en la propia URL. En caso de
                   que se vaya a suministrar un fichero via cgi, será
                   obligatorio usar C{POST}.

    @param enctype: Tipo de codificación. En caso de que se vaya
                    a suministrar un fichero via cgi, se ha de
                    especificar  como C{enctype}
                    el valor C{"multipart/form-data"}.
    '''
    sys.stdout.write('<form class="%s' % kwargs.pop('klass', 'form'))
    if 'navegation' in kwargs:
        sys.stdout.write(' NAVEGACION')
    sys.stdout.write('" action="%s"' % action)
    sys.stdout.write(' method="%s"' % method)
    if 'identity' in kwargs:
        sys.stdout.write(' id="%s"' % kwargs.pop('identity'))
    if 'klass' in kwargs:
        sys.stdout.write(' class="%{}"' % kwargs.pop('klass'))
    if 'enctype' in kwargs:
        sys.stdout.write(' enctype="%s"' % kwargs.pop('enctype'))
    sys.stdout.write('>\n')


def Tabla(titulo=None, klass='FORM', cancel=''):
    '''
    Imprime el código HTML para poder maquetar el formulario de
    una forma sencilla, en una tabla.

    Ejemplo de uso

        >>> from Libreria.Base import Form
        >>> Form.Tabla('Hola')
        <table class="FORM" border="0">
        <caption class="FORM">Hola</caption>

    @param titulo: Título del formulario (Opcional)
    @see: L{Fila}, L{FinTabla}
    '''

    print('<table class="%s" border="0">' % klass)
    if titulo:
        print('<caption class="FORM">%s</caption>' % titulo)
    if cancel:
        print('''
    <tr>
        <td class="cancel" colspan="2">
            <a href="%s">Cancelar
            <img src="/static/art/cerrar.gif" width="14" height="14"
                border="0" alt="Cancelar"
                title="Cancelar la operación en curso"></a>
        </td>
    </tr>
''' % cancel)
    if get_tron():
        Fila('Trace On', CHECKBOX('tron', 's', checked=True))


def Extra(content, klass="extra"):
    '''Añade dos celdas unidas para poder incluir contenido extra a una tabla.
    '''
    print('<tr><td class="%s" colspan="2">' % klass)
    print(content)
    print('</tr></td>')


def field(f, **kwargs):
    buff = ['<tr>']
    buff.append('<th>')
    if f.flags.required:
        buff.append('<b><label for="{}">{}</label></b><sup>*</sup>'.format(
            f.id, f.label.text
            ))
    else:
        buff.append('<label for="{}">{}</label>'.format(f.id, f.label.text))
    buff.append('</th>')
    buff.append('<td>{}</td>'.format(f(**kwargs)))
    buff.append('</tr>')
    if f.errors:
        buff.append('<tr>')
        buff.append(
            '<td>'
            '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'
            '</td>'
            )
        buff.append('<td><ul class="error list-unstyled">')
        for e in f.errors:
            buff.append('<li>{}</li>'.format(e))
        buff.append('</ul>')
        buff.append('</td>')
        buff.append('</tr>')
    return '\n'.join(buff)


def format_as_label(label, widget):
    if hasattr(widget, 'identity') and widget.identity:
        buff = []
        buff.append('<label for="{}">'.format(widget.identity))
        buff.append(str(label))
        buff.append('</label>')
        buff.append('\n')
        return ''.join(buff)
    else:
        return str(label)


def Fila(label, widget='', comentario=''):
    '''Imprime una fila de la tabla. El primer parámetro (opcional)
    será normalmente una etiqueta. El segundo será el control
    correspondiente. Por ejemplo:

        Form.Fila('nombre', Form.TEXT('NOMBRE'))

    Si queremos incluir un control sin etiqueta, la forma adecuada en
    pasarle como C{label} un valor vacio C{None} o C{""}:

        Form.Fila('', Form.TEXT('NOMBRE'))

    Si no se incluye el segundo parámetro, se considera como una
    sección dentro del formulario, e imprimirá una fila, expandida
    para ocupar las dos columnas

    Si se incluye el tercer parámetro, comentario, (opcional) se
    considera como un comentario adicional y se incluirá a
    continuación del segundo parámetro.

    @param label: Etiqueta asociada al control
    @param widget: Control
    @param comentario: Texto opcional de comentario
    @see: L{Tabla}, L{FinTabla}
    '''
    if label and not widget:
        print(
            '<tr>\n'
            '<td valign="top" class="FORMSEP" colspan="2">'
            '<h3>{}</h3>'
            '</td>\n'
            '</tr>'.format(label)
            )
    else:
        print('<tr>')
        if label:
            print('<th valign="top" class="FORM">')
            print(format_as_label(label, widget))
            print('</th>')
        else:
            print('<td valign="top" class="EMPTY">&nbsp;</td>')
        print('<td valign="top">')
        print(widget or '<b style="color:#861E28">Nulo</b>')
        if comentario:
            print('<span class="COMENTARIO">%s</span>' % comentario)
        print('</td>\n</tr>')


def Filas(label, *args):
    num_widgets = len(args)
    if num_widgets == 0:
        return
    args = list(args)
    first_widget = args.pop(0)
    print('<tr>')
    print(
        '<th valign="top" class="FORM" rowspan="{}">{}</th><td>{}</td>'.format(
            num_widgets,
            label,
            first_widget,
            )
        )
    print('</tr>')
    for widget in args:
        print('<tr><td valign="top">{}</td></tr>'.format(widget))


def FinTabla():
    '''
    Imprime las marcas Html para el final de la tabla
    que contiene los controles del formulario.

    Ejemplo de uso

        >>> from Libreria.Base import Form
        >>> Form.FinTabla()
        </table>

    @see: L{Tabla}, L{Fila}
    '''
    print('</table>')


def End():
    '''Imprime las marcas HTML de fin del formulario.

    Ejemplo:

        >>> from Libreria.Base import Form
        >>> Form.End()
        </form>
    '''
    print('</form>')


WIDGET_EMPTY = '<font color="#7C3836"><i>&lt;Vacío&gt;</i></font>'


class HtmlTag(object):
    __slots__ = ['TagName', '_Events', '_Klass']

    def __init__(self):
        self.TagName = self.__class__.__name__
        try:
            self._Klass = set([])
        except NameError:
            import sets
            self._Klass = sets.Set([])
        self._Events = {}

    def setKlass(self, klass):
        self._Klass.add(klass)

    def getKlass(self):
        if self._Klass:
            return ' class="%s"' % (' '.join(self._Klass))
        else:
            return ''

    Klass = property(getKlass, setKlass)

    def getEvents(self):
        if self._Events:
            return ''.join([
                ' %s="javascript:%s"' % x for x in self._Events.items()
                ])
        else:
            return ''

    Events = property(getEvents)

    def setOnLoad(self, code):
        self._Events['onLoad'] = code

    def getOnLoad(self):
        return self._Events.get('onLoad')

    onLoad = property(getOnLoad, setOnLoad)

    def setOnUnload(self, code):
        self._Events['onUnload'] = code

    def getOnUnload(self):
        return self._Events.get('onUnload')

    onUnload = property(getOnUnload, setOnUnload)

    def setOnFocus(self, code):
        self._Events['onFocus'] = code

    def getOnFocus(self):
        return self._Events.get('onFocus')

    onFocus = property(getOnFocus, setOnFocus)

    def setOnBlur(self, code):
        self._Events['onBlur'] = code

    def getOnBlur(self):
        return self._Events.get('onBlur')

    onBlur = property(getOnBlur, setOnBlur)

    def setOnMouseOver(self, code):
        self._Events['onMouseOver'] = code

    def getOnMouseOver(self):
        return self._Events.get('onMouseOver')

    onMouseOver = property(getOnMouseOver, setOnMouseOver)

    def setOnMouseOut(self, code):
        self._Events['onMouseOut'] = code

    def getOnMouseOut(self):
        return self._Events.get('onMouseOut')

    onMouseOut = property(getOnMouseOut, setOnMouseOut)

    def setOnClick(self, code):
        self._Events['onClick'] = code

    def getOnClick(self):
        return self._Events.get('onClick')

    onClick = property(getOnClick, setOnClick)

    def setOnMouseDown(self, code):
        self._Events['onMouseDown'] = code

    def getOnMouseDown(self):
        return self._Events.get('onMouseDown')

    onMouseDown = property(getOnMouseDown, setOnMouseDown)

    def setOnMouseUp(self, code):
        self._Events['onMouseUp'] = code

    def getOnMouseUp(self):
        return self._Events.get('onMouseUp')

    onMouseUp = property(getOnMouseUp, setOnMouseUp)

    def setOnMouseMove(self, code):
        self._Events['onMouseMove'] = code

    def getOnMouseMove(self):
        return self._Events.get('onMouseMove')

    onMouseMove = property(getOnMouseMove, setOnMouseMove)

    def setOnKeyDown(self, code):
        self._Events['onKeyDown'] = code

    def getOnKeyDown(self):
        return self._Events.get('onKeyDown')

    onKeyDown = property(getOnKeyDown, setOnKeyDown)

    def setOnKeyUp(self, code):
        self._Events['onKeyUp'] = code

    def getOnKeyUp(self):
        return self._Events.get('onKeyUp')

    onKeyUp = property(getOnKeyUp, setOnKeyUp)

    def setOnKeyPress(self, code):
        self._Events['onKeyPress'] = code

    def getOnKeyPress(self):
        return self._Events.get('onKeyPress')

    onKeyPress = property(getOnKeyPress, setOnKeyPress)

    def setOnResize(self, code):
        self._Events['onResize'] = code

    def getOnResize(self):
        return self._Events.get('onResize')

    onResize = property(getOnResize, setOnResize)


HTMLTAG = HtmlTag  # PEP8


class Div(HtmlTag):

    __slots__ = ['Text']

    def __init__(self, text):
        super(Div, self).__init__()
        self.Text = text

    def __str__(self):
        return '<div%s%s>%s</div>' % (self.Klass, self.Events, self.Text)


DIV = Div  # PEP8


class Span(HtmlTag):

    __slots__ = ['Text']

    def __init__(self, text):
        super(Span, self).__init__()
        self.Text = text

    def __str__(self):
        return '<span%s%s>%s</span>' % (self.Klass, self.Events, self.Text)


SPAN = Span  # pep8


class Widget(HtmlTag):
    '''
    DESCRIPCIÓN
    ===========

    Clase base para derivar los demas controles. Principlamente define
    los atributos C{Nombre} y C{Valor}, y define propiedades para poder
    acceder tambien cono C{Name} y C{Value}.

    Se ha transladado también el atributo C{disabled}, aunque no todos las
    clases derivadas trabajen con el.
    '''

    __slots__ = ['Nombre', 'Valor', 'Extra', 'Disabled', 'Identity', 'Klass']

    def __init__(self, nombre, valor, identity='', klass=''):
        '''
        Constructor.

        @param nombre: Nombre CGI del widget
        @param valor: Valor del control
        '''
        super(Widget, self).__init__()
        self.Nombre = nombre
        self.Valor = valor
        self.Extra = ''
        self.Disabled = False
        self.set_identity(identity)
        self.Klass = klass

    def set_identity(self, identity):
        if identity:
            self.Identity = identity
        else:
            self.Identity = '{}_{}'.format(
                self.__class__.__name__.lower(),
                self.Nombre.lower(),
                )

    def get_identity(self):
        return self.Identity

    identity = property(get_identity, set_identity)

    def setDisabled(self, flag=True):
        '''
        Desabilita el control
        @param flag: Si es C{True}, o si se omite el parámetro, se
                     desabilita el control. Si vale C{False}, se habilita el
                     Control.
        '''
        self.Disabled = flag

    def setName(self, nombre):
        '''
        función de acceso para la propiedad Name (Sinónimo de Nombre).
        '''
        self.Nombre = nombre

    def getName(self):
        '''
        función de acceso para la propiedad Name (Sinónimo de Nombre).
        @return: El atributo Nombre
        @rtype: string
        '''
        return self.Nombre

    Name = name = property(getName, setName)

    def setValue(self, value, extra=None):
        '''
        función de acceso para la propiedad Value (Sinónimo de Valor).
        '''
        self.Valor = value
        if extra:
            self.Extra = extra

    def getValue(self):
        '''
        función de acceso para la propiedad Value (Sinónimo de Valor).
        @return: El atributo Valor
        @rtype: *
        '''

        return self.Valor

    Value = property(getValue, setValue)

    def Write(self):
        '''
        Imprime el control, en forma de código HTML.

        Depende de que los controles derivados implementen
        correctamente el método C{__str__}.
        '''
        sys.stdout.write(str(self))

    def showValue(self):
        '''
        Devuelve una representación textual del valor del control, o
        la variable L{WIDGET_EMPTY} si el valor no está definido.

        @return: representación textual del valor del control
        @rtype: string
        '''
        if self.Valor is None:
            return WIDGET_EMPTY
        else:
            return str(self.Valor)

    def loadFromCGI(self, default=None):
        '''
        Intenta leer el valor desde los datos `pasados por cgi.

        El nombre del control debe coincidir con el nombre
        pasado por CGI.

        @param default: Valor por defecto a aplicar si no se puede obtener
                        el valor via CGI.
        '''
        self.Valor = getValue(self.Nombre, default)


WIDGET = Widget


class Label(Widget):
    '''
    DESCRIPCIÓN
    ===========

    Obsoleto.
    '''
    __slots__ = ['Text', 'Obligatorio', 'ForItem']

    def __init__(self, text, **kwargs):
        obligatorio = kwargs.pop('obligatorio', None)
        identity = kwargs.pop('identity', '')
        klass = kwargs.pop('klass', '')
        for_item = kwargs.pop('for_item', '')
        super(Label, self).__init__(text, None, identity, klass)
        self.Text = text
        self.Obligatorio = obligatorio
        self.ForItem = for_item

    def __str__(self):
        lista = ['<label']
        if self.ForItem:
            lista.append(' for="%s"' % self.ForItem)
        if self.Identity:
            lista.append(' id="%s"' % self.Identity)
        if self.Klass:
            lista.append(' class="%s"' % self.Klass)
        lista.append('>')
        if self.Obligatorio:
            lista.append('<b>%s</b>' % self.Text)
        else:
            lista.append(self.Text)
        lista.append('</label>')
        return ''.join(lista)


LABEL = Label  # PEP8


class Hidden(Widget):
    """
    DESCRIPCIÓN
    ===========

    Clase para representar controles de tipo HIDDEN (Ocultos)

    Los campos ocultos permite incluir datos en el formulario que no
    serán presentados al usuario. Esto es particularmente útil en
    formularios extensos, que hayan de ser representados con
    varias páginas HTML. Las entradas del usuario del formulario anterior
    pueden ser incluidas en el siguiente, sin que el usuario lo perciba.
    También puede ser util para definir variables internas, que
    el usuario no necesite conocer.

    @Note: Las variables son ocultas unicamente en el sentido de que no son
    mostradas por el navegador, pero cualquier puede visualizarlas
    simplemente viendo el código fuente de la página.

    Ejemplo de uso:

        >>> from Libreria.Base import Form
        >>> print(Form.Hidden('GATO', 'TOTORO'))
        <input type="hidden" name="GATO" value="TOTORO">

    """

    __slots__ = []

    def __str__(self):
        '''
        Representación en formato HTML del control HIDDEN
        @return: Html del control HIDDEN
        @rtype: string
        '''
        result = ['<input type="hidden" name="%s"' % self.Nombre]
        if self.Identity:
            result.append(' id="%s"' % self.Identity)
        if self.Klass:
            result.append(' class="%s"' % self.Klass)
        if self.Valor is None:
            result.append(' value=""')
        else:
            result.append(' value="%s"' % self.Valor)
        result.append('>')
        return ''.join(result)


HIDDEN = Hidden  # PEP8


class Submit(Widget):
    """
    DESCRIPCIÓN
    ===========

    Clase para representar controles de tipo Submit (Botones)

    Un control de tipo C{Submit} representa un botón que permite
    enviar a procesar el formulario. El c{Valor} debe entenderse aqui
    como el texto del boton. Se pueden utilizar varios botones, ya
    que solo se enviará la pareja Nombre/Valor del boton que se pulse.
    Esto permite que el programa que recibe los datos actue de una
    manera o de otra según cual haya sido el botón pulsado.

    Ejemplo de uso:

        >>> from Libreria.Base import Form
        >>> print(Form.Submit('DELETE', 'Borrar'))
        <input type="submit" name="DELETE" value="Borrar">

    @see: L{IMAGE}
    """
    __slots__ = []

    def __init__(self, name='ok', value=None, **kwargs):
        '''Constructor.

        Además de los valores normales, C{Nombre} y C{Valor}, se puede
        especificar si el botón estará habilitado o no. Por defecto,
        estará habilitado. Si se quiere desabilitar posteriormente a la
        creación, se puede usar el método L{setDisabled}.
        '''
        self.Disabled = kwargs.get('disabled', False)
        identity = kwargs.get('identity', '')
        klass = kwargs.get('klass', '')
        Widget.__init__(self, name, value, identity=identity, klass=klass)

    def __str__(self):
        '''
        Representación en formato HTML del control Submit

        @return: Html del control Submit
        @rtype: string
        '''
        buff = ['<input type="submit"']
        if self.Nombre:
            buff.append(' name="{}"'.format(self.Nombre))
        if self.Valor:
            buff.append(' value="{}"'.format(self.Valor))
        if self.Disabled:
            buff.append(' disabled')
        if self.Identity:
            buff.append(' id="{}"'.format(self.Identity))
        if self.Klass:
            buff.append(' class="{}"'.format(self.Klass))
        buff.append('>')
        return ''.join(buff)


SUBMIT = Submit  # PEP8


class Number(Widget):

    def __init__(self, name, value=None, **kwargs):
        identity = kwargs.pop('identity', '')
        klass = kwargs.pop('klass', 'form-control')
        super(Number, self).__init__(
            name, value,
            identity=identity,
            klass=klass,
            )
        self.size = kwargs.pop('size', 5)
        self.disabled = kwargs.pop('disabled', False)
        self.max_length = kwargs.pop('max_length', None)
        self.autofocus = kwargs.pop('autofocus', None)
        self.placeholder = kwargs.pop('placeholder', '')

    def __str__(self):
        lista = ['<input type="number" step="1"']
        lista.append(' name="%s"' % self.Nombre)
        if self.Valor is not None:
            lista.append(' value="%s"' % escape(str(self.Valor), True))
        if self.Identity:
            lista.append(' id="%s"' % self.Identity)
        if self.Klass:
            lista.append(' class="%s"' % self.Klass)
        lista.append(' size="%d"' % self.size)
        if self.disabled:
            lista.append(' disabled="true"')
        if self.max_length:
            lista.append(' maxlength="{}"'.format(self.max_length))
        if self.autofocus:
            lista.append(' autofocus="{}"'.format(self.autofocus))
        if self.placeholder:
            lista.append(' placeholder="{}"'.format(self.placeholder))
        lista.append('>')
        return ''.join(lista)


class Text(Widget):
    """
    DESCRIPCIÓN
    ===========

    Clase para representar controles de tipo TEXT.

    Este control proporcional una entrada de texto de una sola
    línea. El parámetro C{valor} especifica el valor inicial.
    El atributo C{size} es una sugerencia del número de caracteres
    que se espera contenga el control. C{maxlength}, si se define,
    especifica el tamaño máximo, en caracteres, del control.

    Ejemplo de uso:

        >>> from Libreria.Base import Form
        >>> txt = str(Form.Text('nombre', '', size=12))
        >>> assert txt.startswith('<input')
        >>> assert 'type="text"' in txt
        >>> assert 'name="nombre"' in txt
        >>> assert 'value=""' in txt
        >>> assert 'size="12"' in txt
        >>> assert txt.endswith('>')

    @see: L{Password}, L{TextArea}
    """
    __slots__ = ['Size', 'MaxLength', 'autofocus', 'placeholder']

    def __init__(self, name, value=None, **kwargs):
        '''Constructor.

        Además de los valores normales, C{Nombre} y C{Valor}, se puede
        especificar si el control estará habilitado o no. (Por defecto,
        no lo estará) con el parámetro ``disabled``. Además, se puede
        especificar el tamaño recomendado del control con ``size``,
        siendo este el número más o menos aproximado de caracteres que
        se espera contenga. El parámetro ``maxlength``, si se define,
        especifica el tamaño máximo, en caracteres, que acepta el
        control. Otros parámetros aceptados son ``autofocus`` y
        ``placeholder``.

        @Note: No se recominda confiar ciegamente en el
        atributo C{maxlength}, ya que algunos navegadores pueden
        ignorarlo. Es mejor verificar la longitud del dato (así como
        cualquier otra cosa verificable) en los
        programas que procesan los datos.
        '''
        identity = kwargs.pop('identity', '')
        klass = kwargs.pop('klass', 'form-control')
        super(Text, self).__init__(name, value, identity=identity, klass=klass)
        self.Size = kwargs.pop('size', 35)
        self.Disabled = kwargs.pop('disabled', False)
        self.MaxLength = kwargs.pop('maxlength', None)
        self.autofocus = kwargs.pop('autofocus', None)
        self.placeholder = kwargs.pop('placeholder', '')

    def __str__(self):
        '''
        Representación en formato HTML del control Text.

        @return: Html del control Text
        @rtype: string
        '''
        lista = ['<input type="text"%s' % self.Events]
        lista.append(' name="%s"' % self.Nombre)
        if self.Valor is not None:
            lista.append(' value="%s"' % escape(str(self.Valor), True))
        if self.Identity:
            lista.append(' id="%s"' % self.Identity)
        if self.Klass:
            lista.append(' class="%s"' % self.Klass)
        lista.append(' size="%d"' % self.Size)
        if self.Disabled:
            lista.append(' disabled="true"')
        if self.MaxLength:
            lista.append(' maxlength="{}"'.format(self.MaxLength))
        if self.autofocus:
            lista.append(' autofocus="{}"'.format(self.autofocus))
        if self.placeholder:
            lista.append(' placeholder="{}"'.format(self.placeholder))
        lista.append('>')
        return ''.join(lista)


TEXT = Text  # PEP8


class Password(Widget):
    """
    DESCRIPCION
    ===========

    Clase para representar controles de tipo L{Password}.

    El control L{Password} es una variante del L{Text}. La única
    diferencia es que los caracteres de entrada son enmascarados,
    normalmente como usa serie de asteriscos, para proteger
    la información suministrada de ser leida por un observador casual.

    Los datos son trasmitidos por la red
    en claro, asi que es una forma B{muy poco fiable} de obtener
    datos confidenciales.

    Ejemplo de uso:

        >>> from Libreria.Base import Form
        >>> txt = str(Form.Password('pwd', 'hola', size=12))
        >>> assert txt.startswith('<input ')
        >>> assert 'type="password"' in txt
        >>> assert 'autocomplete="off"' in txt
        >>> assert 'name="pwd"' in txt
        >>> assert 'value="hola"' in txt
        >>> assert 'size="12"' in txt
        >>> assert txt.endswith('>')

    @see: L{TEXT}
    """

    __slots__ = ['Size']

    def __init__(self, name, value=None, **kwargs):
        '''Constructor.

        Además de los valores normales, C{Nombre} y C{Valor}, se puede
        especificar si el control estará habilitado o no. (Por defecto,
        no lo estará). Además, se puede especificar el tamaño del control,
        siendo este el número más o menos aproximado de caracteres que
        se espera contenga.
        '''
        size = kwargs.pop('size', 35)
        disabled = kwargs.pop('disabled', False)
        identity = kwargs.pop('identity', '')
        klass = kwargs.pop('klass', 'form-control')
        Widget.__init__(self, name, value, identity=identity, klass=klass)
        self.Size = size
        self.Disabled = disabled

    def __str__(self):
        '''
        Representación en formato HTML del control L{PASSWORD}.

        @return: Html del control TEXT
        @rtype: string
        '''
        lista = ['<input type="password" autocomplete="off"']
        lista.append(' name="%s"' % self.Nombre)
        if self.Valor is not None:
            lista.append(' value="%s"' % self.Valor)
        lista.append(' size="%d"' % self.Size)
        if self.Disabled:
            lista.append(' Disabled="true"')
        if self.Identity:
            lista.append(' id="%s"' % self.Identity)
        if self.Klass:
            lista.append(' class="%s"' % self.Klass)
        lista.append('>')
        return ''.join(lista)


PASSWORD = Password  # PEP8


class TextArea(Widget):
    """
    DESCRIPCIÓN
    ===========

    Clase para representar controles de tipo TEXTAREA.

    Este control proporciona una entrada de texto de varias líneas. El texto
    inicial se representa como el contenido de los tags (Es decir, es el texto
    entre el <TEXTAREA> y </TEXTAREA>), y no debe contener ninguna etiqueta
    HTML.

    Los atributos de C{filas} y C{columnas} especifican las filas y columnas
    visibles, y proporcionan más una guia al usuario que una restricción. En
    teoría no hay límite al tamaño del texto que se puede introducir, pero en
    la práctica la mayoría de los navegadores imponen un límite que puede
    variar entre los 32 y 64 I{kilobytes}. Si se quiere imponer un límite
    máximo, se recomienda controlarlo en el programa CGI que procesa los datos
    del formulario.

    @see: L{TEXT}
    """

    __slots__ = ['Rows', 'Cols', 'Wrap', 'placeholder']

    def __init__(self, name, value=None, **kwargs):
        '''Constructor.

        Además de los valores normales, C{Nombre} y C{Valor}, se puede
        especificar si el control estará habilitado o no. (Por defecto,
        no lo estará). Además, se puede especificar el tamaño del control,
        mediante los parámetros C{filas} y C{columnas}.
        '''
        identity = kwargs.pop('identity', '')
        klass = kwargs.pop('klass', '')
        Widget.__init__(self, name, value, identity=identity, klass=klass)
        self.Rows = kwargs.get('rows', kwargs.pop('filas', 4))
        self.Cols = kwargs.get('cols', kwargs.pop('columnas', 35))
        self.Wrap = kwargs.pop('wrap', 'SOFT')
        self.Disabled = kwargs.pop('disabled', False)
        self.placeholder = kwargs.pop('placeholder', '')

    def __str__(self):
        '''
        Representación en formato HTML del control TEXTAREA.

        @return: Html del control TEXTAREA
        @rtype: string
        '''
        lista = ['<textarea name="{}"'.format(self.Nombre)]
        lista.append(' rows="{}" cols="{}"'.format(self.Rows, self.Cols))
        if self.Wrap:
            lista.append(' wrap="{}"'.format(self.Wrap))
        if self.Disabled:
            lista.append(' disabled="True"')
        if self.Identity:
            lista.append(' id="{}"'.format(self.Identity))
        if self.Klass:
            lista.append(' class="{}"'.format(self.Klass))
        if self.placeholder:
            lista.append(' placeholder="{}"'.format(self.placeholder))
        lista.append('>')
        if self.Valor:
            lista.append(AsHtml(self.Valor))
        lista.append('</textarea><br>')
        return ''.join(lista)

    def setWrap(self, wrap):
        '''Wrap es un atributo de Textarea definido inicialmente
        por Netscape, y asumido también por el MS Explorer. No se
        ha incorporado al estándar HTML 4.0, por lo que no se garantiza
        su comportamiento. Los posibles valores del
        atributo son:

          - C{OFF}: No se produce saltos de línea automáticos y
                    el texto es enviado exactamente como
                    fue escrito.

          - C{SOFT}: Se usa salto de línea automático solo para
                     mostrar el texto en pantalla, el texto
                     realmente es enviado como una sola línea.
                     (Por defecto)

          - C{HARD}: Se usa y envía el texto con saltos automáticos
                     de línea.

        '''
        self.Wrap = wrap


TEXTAREA = TextArea  # PEP8


class Select(Widget):
    '''
    DESCRIPCIÓN
    ===========

    Clase para representar controles de tipo SELECT (Combobox)

    El control SELECT contiene una serie de opciones para que el usuario
    seleccione una.

    @todo: En la implementación actual, solo se permite que
    se seleccione una, pero HTML permite especificar controles SELECT
    múltiples. Por el momento, no han sido necesarios, y por eso no
    se ha implementado esta posibilidad.
    '''

    __slots__ = ['Dict', 'Keys', 'Size', 'Multiple', 'Style', 'disabled']

    def __init__(self, name, selected=None, **kwargs):
        '''Constructor.

        Permite pasar un diccionario con los valores iniciales de la
        lista. Se usara cada clave del diccionario como valor
        de cada opción, y se mostrará el contenido del diccionario
        bajo dicho índice como descripción.

        @param name: Nombre CGI.
        @param dict: Diccionario con los valores de selección.
        @param Selected: Valor seleccionado por defecto.
        '''
        dictionary = kwargs.pop('dictionary', None)
        identity = kwargs.pop('identity', '')
        klass = kwargs.pop('klass', '')
        Widget.__init__(self, name, selected, identity=identity, klass=klass)
        self.reset(dictionary)
        if dictionary:
            self.sort()

    def enable(self):
        self.disabled = False

    def disable(self):
        self.disabled = True

    def showValue(self):
        '''Se redefine el método showValue porque, es este caso concreto,
        queremos como representación del valor la descripción, y no el
        código, que sería lo que mostraría la implementación por
        defecto de L{WIDGET}.
        '''
        if self.Valor in self.Dict:
            return str(self.Dict[self.Valor])
        else:
            return None

    def reset(self, dictionary=None):
        '''Inicializar el control, reseteando todas las variables internas
        del objeto.
        '''
        self.Dict = dictionary if dictionary else {}
        self.Keys = list(self.Dict.keys())
        self.Size = None
        self.Multiple = None
        self.Valor = None
        self.Style = None
        self.disabled = None

    def add_option(self, name, value, selected=False):
        '''Añadir una opción a la lista de opciones del Combo.

        @param name: Nombre de la opción
        @param value: Descripción de la opción
        @param selected: Si es C{True}, esta opcion pasa a ser la seleccionada.
        '''
        self.Keys.append(name)
        self.Dict[name] = value
        if selected:
            self.setSelected(name)

    addOption = add_option

    def set_selected(self, clave):
        '''Definir el valor seleccionado dentro de la lista
        de valores del control. Otra forma de establecer
        el valor.

        @param clave: Nombre de la opción a ser seleccionada.
        '''
        self.Valor = clave

    setSelected = set_selected

    def set_order(self, lista):
        '''
        Definir el orden a seguir al mostrar la lista de opciones
        del control.

        @param lista: Lista de los nombres de las opciones, en el
                      orden deseado.
        '''
        self.Keys = lista

    SetOrder = set_order

    def load_from_dict(self, dictionary):
        '''Cargar los valores de la lista de opciones
        desde un diccionario.

        @param dict: Diccionaro con los valores de la lista de opciones
        '''
        self.Dict = dictionary
        self.Keys = self.Dict.keys()
        self.Keys.sort()

    LoadFromDict = load_from_dict  # PEP8

    def load_from_array(self, array):
        '''Cargar los valores de la lista de opciones
        desde una lista (Los valores y las descripciones
        coincidirán).

        @param array: Lista con los valores de la lista de opciones
        '''
        self.Keys = array
        self.Dict = {}
        for item in array:
            self.Dict[item] = item

    LoadFromArray = load_from_array  # PEP8

    def __setitem__(self, key, value):
        '''Permite usar a la lista de los valores como un diccionario.

        Ejemplo de uso:

            >>> from Libreria.Base import Form
            >>> cbColor = Form.SELECT(
            ...       'color',
            ...       dictionary={1:'azul', 2:'rojo', 3:'verde'}
            ...       )
            >>> cbColor[3] = 'green'
            >>> print(cbColor)
            <select name="color">
            <option value="1">azul</option>
            <option value="2">rojo</option>
            <option value="3">green</option>
            </select>

        @param key: Nombre de la opción.
        @param value: Descripción de la opción.
        '''
        if key not in self.Keys:
            self.Keys.append(key)
        self.Dict[key] = value

    def keys(self):
        return self.Keys

    def sort(self):
        '''Ordena los valores que se utilizan como claves.
        '''
        self.Keys.sort()

    def __str__(self):
        '''
        Representación en formato HTML del control SELECT.

        @return: Html del control SELECT
        @rtype: string
        '''
        Select = ['<select name="%s"' % self.Nombre]
        if self.Identity:
            Select.append(' id="%s"' % self.Identity)
        if self.Klass:
            Select.append(' class="%s"' % self.Klass)
        if self.Style:
            Select.append(' style="%s"' % self.Style)
        if self.disabled:
            Select.append(' disabled="disabled"')
        Select.append('>')
        lista = [''.join(Select)]
        if self.Valor is None:
            for k in self.Keys:
                lista.append(
                    '<option value="{}">{}</option>'.format(
                        k,
                        self.Dict[k]
                        )
                    )
        else:
            for k in self.Keys:
                if k == self.Valor:
                    lista.append(
                        '<option value="{}" selected>{}</option>'.format(
                            k,
                            self.Dict[k]
                            )
                        )
                else:
                    lista.append(
                        '<option value="{}">{}</option>'.format(
                            k,
                            self.Dict[k]
                            )
                        )
        lista.append('</select>')
        return '\n'.join(lista)


SELECT = Select  # PEP8


class CheckBox(Widget):
    '''
    DESCRIPCIÓN
    ===========

    Control de opción de tipo Checkbox o cuadrado.

    Los controles L{Radio} y L{Checkbox} proporcionan interruptores,
    que pueden ser activados o desactivados por el usuario. Los dos
    tipos difieren en que los botones de tipo L{Radio} están agrupados
    (se agrupan especificando el mismo nombre para todos los
    controles), de forma que de todo el grupo sólo uno de los controles
    puede estar activo a la vez. Los interruptores de tipo L{Checkbox},
    por el contrario, son independientes entre si, aun con el mismo
    nombre (Si se especifican varios L{Checkbox} con el mismo nombre, y
    el usuario seleciona más de uno, el resultado sera una lista de
    todos los valores, a leer con L{getValues}).

    El atribito booleano C{checked} especifica si el control está
    inicialmente seleecionado.
    '''

    __slots__ = ['Checked', 'Label']

    def __init__(self, name, value=None, **kwargs):
        '''Constructor

        Además de los valores normales, C{Nombre} y C{Valor}, se puede
        especificar si el control estará habilitado o no. (Parámtro
        C{disabled}; por defecto, no lo estará). Además, opcionalmente,
        se puede especificar si el control debe  aparecer marcado
        (Parámetro C{checked}) y también se le puede indicar una
        etiqueta de texto que se mostrará a la vez que el control.
        '''
        identity = kwargs.pop('identity', '')
        klass = kwargs.pop('klass', '')
        Widget.__init__(self, name, value, identity=identity, klass=klass)
        self.Checked = kwargs.pop('checked', False)
        self.Label = kwargs.pop('label', '')
        self.Disabled = kwargs.pop('disabled', None)

    def showValue(self):
        '''Se redefine el método showValue para
        retornar solo Si/No.
        '''
        if (not self.Valor) or self.Valor == 'N' or self.Valor == 'False':
            return 'No'
        else:
            return 'Sí'

    def getValue(self):
        '''Se redefine el método getValue para
        trabajar sólo con valores booleanos.
        '''

        if (not self.Valor) or self.Valor == 'N' or self.Valor == 'False':
            result = False
        else:
            result = True
        return result

    def __str__(self):
        '''
        Representación en formato HTML del control CHECKBOX.

        @return: Html del control CHECKBOX
        @rtype: string
        '''
        lista = ['<input type="checkbox" name="%s"' % self.Nombre]
        lista.append(self.Events)
        if self.Valor:
            lista.append(' value="{}"'.format(self.Valor))
        if self.Checked:
            lista.append(' checked')
        if self.Disabled:
            lista.append(' disabled="True"')
        if self.Identity:
            lista.append(' id="{}"'.format(self.Identity))
        if self.Klass:
            lista.append(' class="{}"'.format(self.Klass))
        lista.append('>')
        if self.Label:
            if self.Identity:
                lista.append(
                    '&nbsp;<label for="{}">{}</label>'.format(
                        self.Identity,
                        self.Label
                        )
                    )
            else:
                lista.append(
                    '&nbsp;<label for="{}">{}</label>'.format(
                        self.Name,
                        self.Label
                        )
                    )
        return ''.join(lista)


CHECKBOX = CheckBox  # PEP8


class Radio(Widget):
    '''
    DESCRIPCIÓN
    ===========

    Control de opción de tipo Radio o redondo

    Los controles L{Radio} y L{Checkbox} proporcionan interruptores,
    que pueden ser activados o desactivados por el usuario. Los dos
    tipos difieren en que los botones de tipo L{Radio} están agrupados
    (se agrupan especificando el mismo nombre para todos los
    controles), de forma que de todo el grupo sólo uno de los controles
    puede estar activo a la vez. Los interruptores de tipo L{Checkbox},
    por el contrario, son independientes entre si, aun con el mismo
    nombre (Si se especifican varios L{Checkbox} con el mismo nombre, y
    el usuario seleciona más de uno, el resultado sera una lista de
    todos los valores, a leer con L{getValues}).

    El atribito booleano C{checked} especifica si el control está
    inicialmente seleecionado.
    '''

    __slots__ = ['Checked', 'Label']

    def __init__(self, name, value=None, **kwargs):
        '''Constructor

        Además de los valores normales, C{Nombre} y C{Valor}, se puede
        especificar si el control estará habilitado o no. (Parámtro
        C{disabled}; por defecto, no lo estará). Además, opcionalmente,
        se puede especificar si el control debe  aparecer marcado
        (Parámetro C{checked}) y también se le puede indicar una
        etiqueta de texto que se mostrará a la vez que el control.
        '''
        identity = kwargs.pop('identity', '')
        klass = kwargs.pop('klass', '')
        Widget.__init__(self, name, value, identity=identity, klass=klass)
        self.Checked = kwargs.pop('checked', False)
        self.Label = kwargs.pop('label', '')
        self.Disabled = kwargs.pop('disabled', None)

    def __str__(self):
        '''
        Representación en formato HTML del control RADIO.

        @return: Html del control RADIO
        @rtype: string
        '''
        lista = ['<input type="radio" name="%s"' % self.Nombre]
        lista.append(self.Events)
        if self.Valor:
            lista.append(' value="%s"' % self.Valor)
        if self.Disabled:
            lista.append(' disabled="True"')
        if self.Checked:
            lista.append(' checked')
        if self.Identity:
            lista.append(' id="%s"' % self.Identity)
        if self.Klass:
            lista.append(' class="%s"' % self.Klass)
        lista.append('>')
        if self.Label:
            if self.Identity:
                lista.append('&nbsp;<label for="{}">{}</label>'.format(
                    self.Identity,
                    self.Label,
                    ))
            else:
                lista.append('&nbsp;<label for="{}">{}</label>'.format(
                    self.Name,
                    self.Label
                    ))
        return ''.join(lista)


RADIO = Radio  # PEP8


class Image(Widget):
    '''
    DESCRIPCIÓN
    ===========

    Permite utilizar una imagen como un botón de L{Submit}.
    De esta forma se puede cambiar la apariencia de los botones, si lo
    deseamos. El parámetro C{url} es obligatorio e indica la URL de la
    imagen que se usara. El parámetro C{alt} es opcional y proporciona
    una descripción textual de apoyo, para aquellos usuarios que
    no puedan visualizar el gráfico.

    Cuando se pulsa un botón de este tipo, se pasan como datos
    las coordenadas C{X} e C{Y} de la imagen en las cuales se
    ha pinchado con el ratón.

    Hay más informacion sobre el uso de botones gráficos en:
    U{http://www.htmlhelp.com/feature/art3c.htm}

    Ejemplo de uso

        >>> from Libreria.Base import Form
        >>> img = Form.Image('BOTON', '/static/art/up.gif')
        >>> print(img)
        <input type="image" name="BOTON" src="/static/art/up.gif" border="0">

    @see: L{Submit}
    '''

    __slots__ = ['URL', 'Alt']

    def __init__(self, name, url, alt="", identity='', klass=''):
        '''El valor en el caso de este control es el
        URL de la imagen que se desea como botón
        o área clickable. Sin embargo, los valores que se
        pasaran
        '''
        Widget.__init__(self, name, None, identity=identity, klass=klass)
        self.URL = url
        self.Alt = alt

    def __str__(self):
        '''
        Representación en formato HTML del control IMAGE.

        @return: Html del control IMAGE
        @rtype: string
        '''
        buff = ['<input type="image" name="%s" src="%s" border="0"' % (
                self.Nombre,
                self.URL)]
        if self.Alt:
            buff.append(' alt="%s"' % self.Alt)
        if self.Identity:
            buff.append(' id="%s"' % self.Identity)
        if self.Klass:
            buff.append(' class="%s"' % self.Klass)
        buff.append('>')
        return ''.join(buff)


IMAGE = Image  # PEP8


class File(Widget):
    '''
    DESCRIPCIÓN
    ===========

    La clase L{File} permite a los usuarios subir ficheros desde su
    ordenador o la red hacia el servidor. El atributo C{valor} significa
    en este caso el nombre inicial del archivo, pero la mayoría de
    los navegadores, si no todos, ignoran este valor por razones de
    seguridad, asi que en esta implementación el valor se ignora.

    El parámetro C{accept} permite especificar, en forma de valores
    MIME separados por coma, el tipo de archivos que la aplicación
    acepta, lo que permite al navegador filtrar los ficheros que
    le muestra al usuario. La mayoría de los navegadores entiende
    el atributo C{accept}

    Ejemplo de uso:

        >>> from Libreria.Base import Form
        >>> txt = str(Form.File('archivo'))
        >>> assert txt.startswith('<input ')
        >>> assert 'type="file"' in txt
        >>> assert 'name="archivo"' in txt
        >>> assert 'value=""' in txt
        >>> assert txt.endswith('>')

    '''

    __slots__ = ['accept']

    def __init__(self, nombre, valor='', accept='', identity='', klass=''):
        '''Constructor.

        Además de los valores normales, C{Nombre} y C{Valor} (este
        último no tiene ningún efecto), se puede especificar
        en el parámetro accept, una string que consista
        en una lista separada por comas de los tipos MIME
        que la aplicación acepta.

        Ejemplo de uso:

            >>> from Libreria.Base import Form
            >>> f = Form.FILE('ARCHIVO', accept='application/rtf')


        @param nombre: Nombre CGI
        @param valor: Se ignora
        @param accept: Lista, separada por comas, de los tipos MIME aceptados.
                '''
        Widget.__init__(self, nombre, valor, identity=identity, klass=klass)
        self.accept = accept

    def __str__(self):
        '''
        Representación en formato HTML del control FILE.

        @return: Html del control FILE
        @rtype: string
        '''
        result = ['<input type="file" name="{}" value="{}"'.format(
            self.Nombre, self.Valor)
            ]
        if self.accept:
            result.append(' accept="{}"'.format(self.accept))
        if self.Identity:
            result.append(' id="{}"'.format(self.Identity))
        if self.Klass:
            result.append(' class="{}"'.format(self.Klass))
        result.append('>')
        return ''.join(result)


FILE = File  # PEP8


def Boton(action, title, method='POST', force_uppercase=True, **parms):
    """Devuelve un formulario HTML en el cual el único control
    visible es el botón de enviar o I{submit}. El resto de
    los valores irán en campos C{HIDDEN}.

    @param action: Script action del formuario

    @param title: Leyenda del botón.

    @param method: El método usado para enviar los
                   datos (C{POST}/C{GET}). Por
                   defecto C{POST}

    @param force_uppercase: Forzar nombres de parámetros en
                            mayúsculas: Por defecto C{True}

    @param parms: secuencia de valores nombre=valor, que se
                traducirán en campos C{HIDDEN} dentro del
                formulario. Todos los nombres de los campos
                se pasarán a mayúsculas, a no ser que se indique
                lo contrario con el parámetro C{force_uppercase}.
                los valores permanecen inalterados.

    @return: Una string conteniendo el código C{HTML} del Formulario.
    @rtype: string
    """
    buff = [
        '<form action="{}" method="{}" '
        'style="margin: 0" class="FORMULARIO">'.format(
            action,
            method,
            )
        ]
    if force_uppercase:
        for (k, v) in parms.items():
            buff.append('<input type="hidden" name="%s" value="%s">' % (
                k.upper(),
                v,
                ))
    else:
        for (k, v) in parms.items():
            buff.append('<input type="hidden" name="%s" value="%s">' % (k, v))
    buff.append('<input type="submit" name="OK" value="%s">' % title)
    buff.append('</form>')
    return '\n'.join(buff)


#  Googles ReCaptcha V2


def get_recaptcha():
    recaptcha_response = get_value('g-recaptcha-response')
    remote_addr = os.environ.get('REMOTE_ADDR')
    real_ip = os.environ.get('HTTP_X_REAL_IP', remote_addr)
    return (recaptcha_response, real_ip)
