#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import six
import pytest

from .Database import Row

@pytest.fixture
def eiffel():
    row = Row()
    row.add_value('Nombre', 'Torre Eiffel')
    row.add_value('Altura', 300)
    return row


def test_row_as_repr(eiffel):
    s = repr(eiffel)
    if six.PY2:
        assert s == "Row(['nombre', 'altura'], [u'Torre Eiffel', 300])"
    else:
        assert s == "Row(['nombre', 'altura'], ['Torre Eiffel', 300])"


def test_row_act_as_a_dict(eiffel):
    assert eiffel['nombre'] == 'Torre Eiffel'
    assert eiffel['altura'] == 300
    assert 'not_included' not in eiffel


def test_row_act_as_a_dict_case_insensitive(eiffel):
    assert eiffel['nombre'] == eiffel['Nombre']
    assert eiffel['nombre'] == eiffel['NOMBRE']
    assert eiffel['altura'] == eiffel['Altura']
    assert eiffel['altura'] == eiffel['ALTURA']


def test_row_as_sequence(eiffel):
    assert eiffel[0] == 'Torre Eiffel'
    assert eiffel[1] == 300


def test_row_length(eiffel):
    assert len(eiffel) == 2


@pytest.fixture
def met():
    row = Row()
    row.add_value('Nombre', 'Metropolitan Opera House')
    row.add_value('Aforo', 3800)
    row.add_value('Ciudad', 'New York')
    row.add_value('latitud', 40.772778)
    row.add_value('longitud', -73.984167)
    return row


def test_row_slice_1_4(met):
    subrow = met[1:4]
    assert len(subrow) == 3
    assert subrow.aforo == subrow[0] == 3800
    assert subrow.ciudad == subrow[1] == 'New York'
    assert subrow.latitud == subrow[2] == 40.772778


def test_row_slice_0_3(met):
    subrow = met[:3]
    assert len(subrow) == 3
    assert subrow.nombre == subrow[0] == 'Metropolitan Opera House'
    assert subrow.aforo == subrow[1] == 3800
    assert subrow.ciudad == subrow[2] == 'New York'


def test_row_slice_3_forward(met):
    subrow = met[3:]
    assert len(subrow) == 2
    assert subrow.latitud == subrow[0] == 40.772778
    assert subrow.longitud == subrow[1] == -73.984167


def test_row_create_without_parameters():
    row = Row()
    assert len(row) == 0


def test_create_just_names():
    row = Row(['name','secret_identity'])
    assert len(row) == 2
    assert row.name is None
    assert row.secret_identity is None


def test_create_with_names_and_values():
    row = Row(['name','secret_identity'], ['Superman', 'Clark Kent'])
    assert len(row) == 2
    assert row.name == 'Superman'
    assert row.secret_identity == 'Clark Kent'


def test_create_with_named_parameters():
    row = Row(name='Superman', secret_identity='Clark Kent')
    assert len(row) == 2
    assert row.name == 'Superman'
    assert row.secret_identity == 'Clark Kent'


@pytest.fixture
def spiderman():
    return Row(
        ['name','secret_identity'],
        ['Spiderman', 'Peter Parker'],
        )

def test_row_update(spiderman):
    assert len(spiderman) == 2
    spiderman.update({'name':'Scarlet Spider', 'alias':'Ben Reilly'})
    assert len(spiderman) == 3
    assert spiderman.name == 'Scarlet Spider'
    assert spiderman.secret_identity == 'Peter Parker'
    assert spiderman.alias == 'Ben Reilly'


def test_update_case_insensitive(spiderman):
    spiderman.update({'name': 'Scarlet Spider'})
    assert spiderman['name'] == spiderman.NAME == 'Scarlet Spider'
    spiderman.update({'NamE':'Webhead'})
    assert spiderman['name'] == spiderman.NAME == 'Webhead'
    spiderman.update({'NAME':'Spidey'})
    assert spiderman['name'] == spiderman.NAME == 'Spidey'


def test_row_reset(spiderman):
    assert len(spiderman) == 2
    assert 'name' in spiderman
    assert 'secret_identity' in spiderman
    spiderman.Reset()
    assert len(spiderman) == 0
    assert 'name' not in spiderman
    assert 'secret_identity' not in spiderman


def test_row_add_value():
    row = Row()
    row.addValue('Name', 'Superman')
    row.addValue('Secret_Identity', 'Clark Kent')
    assert len(row) == 2
    assert row.name == 'Superman'
    assert row.secret_identity == 'Clark Kent'


def test_row_get_value(spiderman):
    assert spiderman.getValue('secret_identity'), 'Peter Parker'
    assert spiderman.getValue('name') == 'Spiderman'
    with pytest.raises(ValueError):
        spiderman.getValue('non_existing_key')  


def test_row_get(spiderman):
    '''Prueba del metodo get(nombre[, valor por defecto]) de ROW.
    '''
    assert spiderman.get('name') == 'Spiderman'
    assert spiderman.get('secret_identity') == 'Peter Parker'
    assert spiderman.get('Non_Exists') == None
    assert spiderman.get('Non_Exists', 'Default') == 'Default'


def test_row_set_values():
    row = Row()
    row.setValues(name='Batman', secret_identity='Bruce Wayne')
    assert row.name == 'Batman'
    assert row.secret_identity == 'Bruce Wayne'


def test_row_keys(spiderman):
    keys = spiderman.keys()
    assert set(keys) == set(['NAME', 'SECRET_IDENTITY'])


@pytest.fixture
def superman():
    return Row(
        name='Superman',
        secret_identity='Clark Kent',
        headquarters='Fortress of Solitude',
        )


def test_row_getitem(superman):
    '''Testing method __getitem__(name) of Row.
    '''
    assert superman['NAME'] == 'Superman'
    assert superman['Name'] == 'Superman'
    assert superman['name'] == 'Superman'
    assert superman[0] == 'Superman'
    assert superman['SECRET_IDENTITY'] == 'Clark Kent'
    assert superman['Secret_Identity'] == 'Clark Kent'
    assert superman['secret_identity'] == 'Clark Kent'
    assert superman[1] == 'Clark Kent'
    assert superman['HEADQUARTERS'] == 'Fortress of Solitude'
    assert superman['Headquarters'] == 'Fortress of Solitude'
    assert superman['headquarters'] == 'Fortress of Solitude'
    assert superman[2] == 'Fortress of Solitude'


def test_row_setitem_using_names():
    row = Row(['name','secret_identity'])
    row['name'] = 'Green Lantern'
    row['secret_identity'] = 'Hal Jordan'
    assert row.name == 'Green Lantern'
    assert row.secret_identity == 'Hal Jordan'


def test_row_setitem_using_indexes():
    row = Row(['name','secret_identity'])
    row[0] = 'Flash'
    row[1] = 'Barry Allen'
    assert row.name == 'Flash'
    assert row.secret_identity == 'Barry Allen'


def test_row_compare(superman, spiderman):
    spider_clon = Row(name='Spiderman', secret_identity='Peter Parker')
    assert spiderman == spider_clon
    assert superman != spiderman
    assert spiderman != superman
    assert superman == superman
    assert spiderman == spiderman


def test_row_getattr(superman):
    assert superman.NAME == 'Superman'
    assert superman.Name == 'Superman'
    assert superman.name == 'Superman'
    assert superman.SECRET_IDENTITY == 'Clark Kent'
    assert superman.Secret_Identity == 'Clark Kent'
    assert superman.secret_identity == 'Clark Kent'
    with pytest.raises(AttributeError):
        superman.NON_EXISTS


def test_row_str(superman):
    s = str(superman)
    assert 'Superman' in s
    assert 'Clark Kent' in s
    assert 'Fortress of Solitude' in s


def test_row_slices():
    r = Row(a=1, b=2, c=3)
    assert r[0:2] == Row(a=1, b=2)
    assert r[:1] == r[0:1] == Row(a=1)
    assert r[1:] == r[1:3] == Row(b=2, c=3)
    assert r[1:2] == Row(b=2)


def test_row_len(superman, spiderman):
    assert len(spiderman) == 2
    assert len(superman) == 3

