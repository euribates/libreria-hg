#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import re
import os
import six

_mapa_as_inicial = {
    225: 65,  # á --> A
    233: 69,  # é --> E
    237: 73,  # í --> I
    243: 79,  # ó --> O
    250: 85,  # ú --> U
    193: 65,  # Á --> A
    201: 69,  # É --> E
    205: 73,  # Í --> I
    211: 79,  # Ó --> O
    218: 85,  # Ú --> U
    }


def as_inicial(texto):
    global _mapa_as_inicial
    if not texto:
        return ''
    if not isinstance(texto, six.text_type):
        texto = texto.decode('utf-8')
    return texto[0].upper().translate(_mapa_as_inicial)


def set_initial_lower(s):
    if s:
        initial = s[0]
        if initial != initial.lower():
            s = initial.lower() + s[1:]
    return s


def set_initial_upper(s):
    if s:
        initial = s[0]
        if initial != initial.upper():
            s = initial.upper() + s[1:]
    return s


_pat_hora = re.compile(r'(\d\d?):(\d\d)')


def hour_to_integer(s):
    global _pat_hora
    s = s.strip()
    match = _pat_hora.match(s)
    if match:
        hours = int(match.group(1))
        if hours > 23:
            raise ValueError('El valor indicado no es una hora')
        minutes = int(match.group(2))
        if minutes > 59:
            raise ValueError('El valor indicado no es una hora')
        return hours * 60 + minutes
    else:
        raise ValueError('El valor indicado no es una hora')


_slugify_mapa = {
    32: 45,    # space -> hyphen
    33: None,  # exclamation mark
    34: None,  # double quotes
    35: None,  # hash
    36: None,  # dollar
    37: None,  # percent
    38: None,  # ampersand
    39: None,  # simple quote
    40: None,  # open par
    41: None,  # close par
    42: None,  # asterisk
    43: 45,    # plus -> hyphen
    44: None,  # comma
    46: None,  # dot or full stop
    47: 45,    # slash -> hyphen
    58: 45,    # colon -> hyphen
    59: 45,    # semicolon -> hyphen
    60: None,  # open angled bracket
    61: 45,    # equals -> hyphen
    62: None,  # close angled bracket
    63: None,  # question mark
    64: 45,    # @ -> hyphen
    91: None,  # open bracket
    92: 45,    # backslash -> hyphen
    93: None,  # close bracket
    94: 45,    # caret -> hyphen
    95: 45,    # underscore -> hyphen
    96: None,  # grave accent
    123: None,  # open brace
    124: 45,    # pipe -> hyphen
    125: None,  # close brace
    126: 45,    # equivalency sign (~) -> hyphen
    133: 45,    # ellipsis
    191: None,  # open question mark
    193: 65,
    201: 69,
    205: 73,
    209: 78,
    211: 79,
    218: 85,
    220: 85,
    225: 97,  # a
    233: 101,
    237: 105,
    241: 110,
    243: 111,
    250: 117,
    252: 117,
    8230: 45   # ellipsis
    }


_slugify_pat_multiple_hyphens = re.compile('--+')


def slugify(s):
    global _slugify_pat_multiple_hyphens, _slugify_mapa
    s = s.lower()
    s = s.replace('ñ', 'ni')
    s = s.replace('€', '-euros')
    # logger.debug('s antes   = "{}"'.format(s))
    s = s.translate(_slugify_mapa)
    # logger.debug('s después = "{}"'.format(s))
    s = ''.join([_ for _ in s if ord(_) < 129])
    s = _slugify_pat_multiple_hyphens.sub('-', s)
    return s.encode('ascii')


def as_safe_filename(filename):
    fn, ext = os.path.splitext(filename)
    return '{}{}'.format(
        slugify(fn).decode('ascii'),
        ext.lower(),
        )


def is_image(filename):
    (_, ext) = filename.rsplit('.', 1)
    return ext.lower() in ('png', 'gif', 'jpg')
