#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import

import os
from six.moves import StringIO


class BASE_MENU(object):

    CurrentAction = ''
    Expandir_Todo = False

    def __init__(self, parent, titulo, url=''):
        self.Parent = parent
        self.Titulo = titulo
        self.URL = url
        self.Activo = False
        if url:
            self.Selected = (BASE_MENU.CurrentAction == url)

    def setActivo(self, flag=True):
        self.Activo = flag
        if self.Parent:
            self.Parent.setActivo(flag)

    def setExpandir(self, flag=True):
        BASE_MENU.Expandir_Todo = flag

    def setSelected(self, value):
        self._Selected = value
        if self._Selected:
            self.setActivo(True)

    def getSelected(self):
        return self._Selected

    Selected = property(getSelected, setSelected)


class MENU(BASE_MENU):

    def __init__(self, titulo='', width=150, url=''):
        if not titulo:
            titulo = 'MENÚ'
        if 'SCRIPT_NAME' in os.environ:
            BASE_MENU.CurrentAction = os.path.split(
                os.environ['SCRIPT_NAME']
                )[1]
            Query_String = os.environ.get('QUERY_STRING', '')
            if Query_String:
                BASE_MENU.CurrentAction = '?'.join([
                    BASE_MENU.CurrentAction,
                    Query_String,
                    ])
        else:
            BASE_MENU.CurrentAction = ''
        BASE_MENU.__init__(self, None, titulo)
        self.SubMenus = []
        self.Width = width
        self.Align = 'LEFT'
        self.URL = url
        self.ImagenCerrar = '<img src="/static/art/cerrar_menu.png">'
        self.ImagenAbrir = '<img src="/static/art/abrir_menu.png">'

    def __iter__(self):
        self.index = 0
        self.top = len(self.SubMenus)
        return self

    def next(self):
        if self.index < self.top:
            result = self.SubMenus[self.index]
            self.index += 1
            return result
        else:
            raise StopIteration

    def __str__(self):
        buff = StringIO()
        buff.write('<p class="menu">{}</p>\n'.format(self.Titulo))
        buff.write('<ul class="SUBMENU">\n')
        for submenu in self.SubMenus:
            buff.write(str(submenu))
        buff.write('</ul>\n')
        return buff.getvalue()

    def dump(self, titulo=None):
        if titulo:
            print('<p>{}'.format(titulo))
        print('<ul>')
        for submenu in self.SubMenus:
            print('<li><font size="+1"><b>')
            print('<a href="{}">{}</a>'.format(submenu.URL, submenu.Titulo))
            print('</b></font></li>')
            print('<ul>')
            for menuitem in submenu.MenuItems:
                print('<li><a href="{}">{}</a>'.format(
                    menuitem.URL,
                    menuitem.Titulo
                    ))
            print('</ul>')
        print('</ul>')

    def debug(self):
        print('<ul>')
        for submenu in self.SubMenus:
            print('<li><font size="+1"><b>')
            print('<a href="{}">{}</a>'.format(submenu.URL, submenu.Titulo))
            print('<br>Activo: {}'.format(submenu.Activo))
            print('<br>Su padre es: {}'.format(submenu.Parent.Titulo))
            print('<br>CurrentAction: {}'.format(submenu.CurrentAction))
            print('</b></font></li>')
            print('<ul>')
            for menuitem in submenu.MenuItems:
                print('<li><a href="{}">{}</a>'.format(
                    menuitem.URL,
                    menuitem.Titulo,
                    ))
                print('<br>Activo: {}'.format(menuitem.Activo))
                print('<br>Su padre es: {}'.format(menuitem.Parent.Titulo))
            print('</ul>')
        print('</ul>')


class SUBMENU(BASE_MENU):
    def __init__(self, parent, titulo, url, icono=''):
        BASE_MENU.__init__(self, parent, titulo, url)
        self.Icono = icono
        self.MenuItems = []
        if isinstance(parent, MENU):
            self.Parent.SubMenus.append(self)
        if isinstance(parent, SUBMENU):
            self.Parent.MenuItems.append(self)

    def __str__(self):
        buff = StringIO()
        if self.Selected:
            buff.write('<li class="SUBMENU SELECTED">%s</li>\n' % self.Titulo)
        elif isinstance(self.Parent, MENU):
            buff.write(
                '<li class="SUBMENU">'
                '<a href="{}" class="SUBMENU">{}</a>'
                '</li>\n'.format(self.URL, self.Titulo)
                )
        else:
            buff.write(
                '<li class="SUBSUBMENU">'
                '<a href="{}" class="SUBSUBMENU">{}</a>'
                '</li>\n'.format(self.URL, self.Titulo)
                )
        if self.MenuItems:
            buff.write('<ul class="list list-group">\n')
            for menuitem in self.MenuItems:
                buff.write(str(menuitem))
            buff.write('</ul>\n')
        buff.seek(0)
        return buff.read()

    def dump(self, titulo=''):
        print('<p>')
        print('<div align="CENTER">')
        print(
            '<table border="0" width="450"'
            ' cellpadding="2" bgcolor="#FDFFF3">'
            )
        if self.Icono:
            print(self.Icono)
        if titulo:
            print('<caption>%s</caption>' % titulo)
        for menuitem in self.MenuItems:
            print('<tr>')
            print(
                '<td align="CENTER"'
                ' valign="TOP"'
                ' style="border:hover:1px solid blue">'
                )
            print('<font size="+2">')
            print('<a href="{}">{}</a>'.format(menuitem.URL, menuitem.Titulo))
            print('</font>')
            if isinstance(menuitem, MENUITEM) and menuitem.Icono:
                print('<br><a href="{}">{}</a>'.format(
                    menuitem.URL,
                    menuitem.Icono,
                    ))
            print('</td>')
            if isinstance(menuitem, MENUITEM) and menuitem.Desc:
                print('<td align="LEFT" valign="TOP"><font size="+1">')
                print(menuitem.Desc)
                print('</td>')
            else:
                print('<td>&nbsp;</td>')
            print('</tr>')
        print('</table>')
        print('</div>')


class MENUSEP(BASE_MENU):
    def __init__(self, parent, desc=''):
        BASE_MENU.__init__(self, parent, desc, url=None)
        if isinstance(parent, MENU):
            self.Parent.SubMenus.append(self)
        if isinstance(parent, SUBMENU):
            self.Parent.MenuItems.append(self)

    def __str__(self):
        if self.Titulo:
            return '<li class="MENUSEP">%s</li>\n' % self.Titulo
        else:
            return '<li class="MENUSEP"><hr></li>\n'


class MENUITEM(BASE_MENU):
    def __init__(self, parent, titulo, url, desc='', icono='', on_active=None):
        BASE_MENU.__init__(self, parent, titulo, url)
        self.Desc = desc
        self.Icono = icono
        self.Parent.MenuItems.append(self)
        self.MenuItems = False
        self.On_Active = on_active

    def __str__(self):
        buff = []
        if self.Selected:
            buff.append(
                '<li class="list-group-item menu-item disabled">'
                '{}'
                '</li>'.format(self.Titulo)
                )
            if self.On_Active and callable(self.On_Active):
                buff.append(self.On_Active())
        else:
            buff.append(
                '<li class="list-group-item menu-item">'
                '<a href="{}" class="MENUITEM">'
                '{}'
                '</a></li>\n'.format(
                    self.URL,
                    self.Titulo
                    )
                )
        return '\n'.join(buff)


class MENUFORM(BASE_MENU):

    def __init__(self, parent, titulo, url, queryname='Q', **params):
        BASE_MENU.__init__(self, parent, titulo, url)
        self.QueryName = queryname
        self.Params = params
        if isinstance(parent, MENU):
            self.Parent.SubMenus.append(self)
        if isinstance(parent, SUBMENU):
            self.Parent.MenuItems.append(self)
        self.MenuItems = False

    def __str__(self):
        buff = ['<li class="MENUITEM">']
        buff.append('<form action="%s" class="FORM">' % self.URL)
        for (name, value) in self.Params.items():
            buff.append(
                '<input type="HIDDEN" name="{}" value="{}">'.format(
                    name,
                    value,
                    )
                )
        buff.append(self.Titulo)
        buff.append('<br>')
        buff.append('<input type="TEXT" name="{}" size="7">'.format(
            self.QueryName
            ))
        buff.append('<input type="SUBMIT" name="OK" value="OK">')
        buff.append('</form>')
        return '\n'.join(buff)


class MENUADVICE(BASE_MENU):

    def __init__(self, parent, titulo, texto):
        BASE_MENU.__init__(self, parent, titulo, '')
        self.Texto = texto
        if isinstance(parent, MENU):
            self.Parent.SubMenus.append(self)
        if isinstance(parent, SUBMENU):
            self.Parent.MenuItems.append(self)
        self.MenuItems = []

    def __str__(self):
        return '''<li class="MENUADVICE">
<div class="advice_titulo">%s</div>
<div class="advice_texto">%s</div>
</li>''' % (self.Titulo, self.Texto)


class MENU_HORIZONTAL(MENU):

    def __str__(self):
        Script_Name = os.environ.get('SCRIPT_NAME', '')
        buff = [
            '<table class="NAVEGACION menu_horizontal"'
            ' border="0" cellspacing="0" cellpadding="0">'
            ]
        submenu = None
        buff.append('<tr>')
        if Script_Name.endswith('index.py'):
            buff.append('<th class="left_on">Inicio</th>')
            Next_Style = 'middle_off_left'
        else:
            buff.append(
                '<th class="left_off">'
                '<a class="menu" href="index.py">Inicio</a>'
                '</th>'
                )
            Next_Style = ''
        cols = len(self.SubMenus)
        for sm in self.SubMenus:
            if sm.Activo:
                submenu = sm
                buff.append('<th class="middle_on">')
                Next_Style = 'middle_off_left'
                if sm.URL.endswith(Script_Name):
                    buff.append(sm.Titulo)
                else:
                    buff.append('<a class="menu" href="%s">%s</a>'.format(
                        sm.URL,
                        sm.Titulo,
                        ))
            else:
                if Next_Style:
                    buff.append('<th class="%s">' % Next_Style)
                    Next_Style = ''
                else:
                    buff.append('<th class="middle_off">')
                buff.append('<a href="%s">%s</a>' % (sm.URL, sm.Titulo))
            buff.append('</th>')
        if Next_Style:
            buff.append('<th class="right_on">&nbsp;</th>')
        else:
            buff.append('<th class="right_off">&nbsp;</th>')
        buff.append('<th class="right_fade">&nbsp;</th>')
        buff.append('</tr>')
        buff.append('<tr>')

        buff.append('<td class="menuitem" colspan="%s">' % (cols+3))
        if submenu:
            if submenu.MenuItems:
                ancho = int(round(100. / float(len(submenu.MenuItems))))
            else:
                ancho = 125
            for menuitem in submenu.MenuItems:
                buff.append(
                    '<div style="text-align: center;'
                    ' border: 0px solid red; width: {}%%;'
                    ' float:left">'.format(ancho)
                    )
                if Script_Name.endswith(menuitem.URL):
                    buff.append(menuitem.Titulo)
                else:
                    buff.append('<a href="{}">{}</a>'.format(
                        menuitem.URL,
                        menuitem.Titulo,
                        ))
                buff.append('</div>')
        buff.append('</tr>')
        buff.append('</table>')
        return '\n'.join(buff)


class MENU_CSS(MENU):
    def __str__(self):
        buff = StringIO.StringIO()
        buff.write('<ul id="nav">\n')
        for submenu in self.SubMenus:
            buff.write('<li><a href="%s">%s</a>\n' % (
                submenu.URL,
                submenu.Titulo
                ))
            if submenu.MenuItems:
                buff.write('\t<ul>\n')
                for item in submenu.MenuItems:
                    buff.write('\t<li><a href="%s">%s</a>\n' % (
                        item.URL,
                        item.Titulo
                        ))
                    if item.MenuItems:
                        buff.write('\t\t<ul>\n')
                        for leaf in item.MenuItems:
                            buff.write('\t\t<li><a href="%s">%s</a>\n' % (
                                item.URL,
                                item.Titulo
                                ))
                        buff.write('\t\t</li>\n')
                        buff.write('\t\t</ul>\n')
                buff.write('\t</li>\n')
                buff.write('\t</ul>\n')
        buff.write('</ul>\n')
        buff.seek(0)
        return buff.read()
