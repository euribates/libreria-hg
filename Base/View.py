#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import

import re
import datetime
import six
from functools import partial

from markdown2 import markdown
from jinja2 import Template, Environment
from Libreria.Comun import Fechas
from Libreria.Comun import numerals

if six.PY2:
    def memoize(f):
        """ Memoization decorator for functions taking one or more arguments. """
        class memodict(dict):
            def __init__(self, f):
                self.f = f
            def __call__(self, *args):
                return self[args]
            def __missing__(self, key):
                ret = self[key] = self.f(*key)
                return ret
        return memodict(f)
else:
    from functools import lru_cache
    memoize = lru_cache(maxsize=256)



def as_mes(num_mes, size=0):
    if size:
        return Fechas.MESES[num_mes][0:size]
    else:
        return Fechas.MESES[num_mes]


def escape(s):
    if not s:
        return ''
    s = str(s)
    s = s.replace('&', '&amp;')
    s = s.replace('<', '&lt;')
    s = s.replace('>', '&gt;')
    return s


def AsAviso(texto):
    """retorna un texto de aviso con un estilo determinado.
    @param texto: Texto del aviso
    @type texto: string
    """
    return u'''
<p align="RIGHT">
<div style="border: 0px solid blue;
    width: 160px;
    float: right;
    margin: 3px;
    padding: 6px;
    background:URL(/static/art/obras.png);">
<div style="background: URL(/static/art/aviso.png) left top no-repeat #F9DA5F;
    text-align:center;
    height: 26px;"><b>A T E N C I Ó N</b>
</div>

<div style="background:#F9DA5F;
    padding: 2px;">
%s
</div>
</div>
</p>
''' % texto


def AsLink(url, desc=''):
    if url:
        if desc:
            return u'<a href="%s">%s</a>' % (url, desc)
        else:
            return u'<a href="%s">%s</a>' % (url, url)
    else:
        return desc


def as_fecha(f):
    if not f:
        return ''
    return '{}/{}/{}'.format(
        f.day,
        Fechas.MESES[f.month][0:3],
        f.year,
        )


def AsFecha(f, default='---'):
    if f:
        f = Fechas.FECHA(f)
        return u'<i style="color: #AAAAAA;">(%s)</i>' % f
    else:
        return default


def as_timestamp(ts, full_text=False):
    if ts:
        ts = Fechas.TIMESTAMP(ts)
        if full_text:
            return u'{:02d}/{:02d}/{:04d}, ' \
                   u'a las {:02d}:{:02d}'.format(
                        ts.TS[2], ts.TS[1], ts.TS[0],
                        ts.TS[3], ts.TS[4]
                        )
        else:
            return str(ts)
    else:
        return u'---'

AsTimestamp = as_timestamp  # Retrocompatibilidad

def AsHora(i):
    try:
        i = int(i)
        es_positivo = (abs(i) == i)
        horas  = abs(i) // 60
        minutos = abs(i) % 60
        if es_positivo:
            return u'<b class="HORA">{:02d}:{:02d}</b>'.format(horas, minutos)
        else:
            return u'<b class="HORA ENNEGATIVO">-{:02d}:{:02d}</b>'.format(horas, minutos)
    except ValueError: # No es un entero, seguramente una string en la forma HH:MM
        return u'<b class="HORA">%s</b>' % i


def as_filesize(d):
    '''retorna una descripcion textual del tamaño del fichero, redondeaado
    a las unidades más cercanas.
    '''
    if d < 1024:
        return '%d Bytes' % d
    elif d < 1048576:
        return '%.2f KB.' % (float(d) / 1024.0)
    elif d < 1073741824:
        return '%.2f MB.' % (float(d) / 1048576.0)
    else:
        return '%.2f GB.' % (float(d) / 1073741824.0)


AsFilesize = as_filesize


def as_entero(n):
    try:
        n = int(n)
        return u'<tt class="entero">%d</tt>' % n
    except ValueError:
        return u'<b class="error">Nan</b>: "%s" no es un entero' % escape(n)


def AsNumero(n, html_markup=True):
    import math
    (fraccion, entero) = math.modf(float(n))
    entero = int(entero)
    s = str(entero)
    if len(s) > 12:
        s = s[:-12]+'.'+s[-12:-9]+'.'+s[-9:-6]+'.'+s[-6:-3]+'.'+s[-3:]
    elif len(s) > 9:
        s = s[:-9] + '.' + s[-9:-6] + '.' + s[-6:-3] + '.' + s[-3:]
    elif len(s) > 6:
        s = s[:-6] + '.' + s[-6:-3] + '.' + s[-3:]
    elif len(s) > 3:
        s = s[:-3] + '.' + s[-3:]
    result = s
    fraccion = round(fraccion * 100)
    result = u'%s,%02d' % (result, fraccion)
    if html_markup:
        result = u'<span align="RIGHT"><tt>%s</tt></span>' % result
    return result


def AsNumeroEntero(n):
    import math
    s = str(n)
    if len(s) > 12:
        s = s[:-12]+'.'+s[-12:-9]+'.'+s[-9:-6]+'.'+s[-6:-3]+'.'+s[-3:]
    elif len(s) > 9:
        s = s[:-9] + '.' + s[-9:-6] + '.' + s[-6:-3] + '.' + s[-3:]
    elif len(s) > 6:
        s = s[:-6] + '.' + s[-6:-3] + '.' + s[-3:]
    elif len(s) > 3:
        s = s[:-3] + '.' + s[-3:]
    result = s
    return result


def as_tabla(tabla, headers=[], klass='table'):
    if tabla:
        Buffer = [u'<table class="%s">' % klass]
        fila_zero = tabla[0]

        if not headers and hasattr(fila_zero, 'keys'):
            headers = fila_zero.keys()
        if headers:
            Buffer.append(u'<thead><tr>')
            for nombre in headers:
                Buffer.append(u'<th>%s</th>' % nombre)
            Buffer.append(u'</tr></thead>')
        for fila in tabla:
            Buffer.append(u'<tr>')
            for celda in fila:
                if celda is None:
                    Buffer.append(u'\t<td valign="top"><i style="color:#663333">Null</i></td>')
                else:
                    Buffer.append(u'\t<td valign="top">%s</td>' % celda)
            Buffer.append(u'</tr>')
        Buffer.append(u'</table>')
        return '\n'.join(Buffer)
    else:
        return ''
as_table = as_tabla
AsTabla = as_tabla
AsRows = as_tabla


def as_boolean(v):
    if v and v != 'N':
        return u'<b style="color:#18442D;">Si</b>'
    else:
        return u'<b style="color:#321A26;">No</b>'

AsBoolean = as_boolean # PEP8


def as_email(email):
    if email:
        return u'<a href="mailto:%s" style="font-style:sans-serif;">%s</a>' % (email, email)
    else:
        return u'<i>Desconocido</i>'

AsEmail = as_email


def AsNumeralEuros(n):
    d = '%.2f' % float(n)
    (i, f) = [int(x) for x in d.split('.')]
    if f:
        return u'%s euros con %s céntimos' % (numerals.numeral(i), numerals.numeral(f))
    else:
        return u'%s euros' % numerals.numeral(i)


def AsRecuento(n, singular, plural):
    return singular % {'n':n} if n == 1 else plural % {'n':n}


def as_importe(importe, divisa="&euro;"):
    return u'%.2f&nbsp;%s' % (importe, divisa)

AsImporte = as_importe # PEP8

#
# Añadido por Enrique para tema de conversion texto ascci 2 HTML
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#

def translate(text, pre=0):
    prog = re.compile(r'\b(http|ftp|https)://\S+(\b|/)|\b[-.\w]+@[-.\w]+')
    i = 0
    list = []
    while 1:
        m = prog.search(text, i)
        if not m:
            break
        j = m.start()
        list.append(escape(text[i:j]))
        i = j
        url = m.group(0)
        while url[-1] in '();:,.?\'"<>':
            url = url[:-1]
        i = i + len(url)
        url = escape(url)
        if not pre:
            if ':' in url:
                repl = '<a href="%s">%s</a>' % (url, url)
            else:
                repl = '<a href="mailto:%s">%s</a>' % (url, url)
        else:
            repl = url
        list.append(repl)
    j = len(text)
    list.append(escape(text[i:j]))
    return ''.join(list)


def emphasize(line):
    return re.sub(r'\*([a-zA-Z]+)\*', r'<i>\1</i>', line)


def text2html(body, flow=0):
    res = []
    atstart = 1
    quoting = 0
    rulechars = '-_~=*#>'
    RElinesplit = re.compile(r'\r?\n')
    REleadingspace = re.compile(' +')
    if len(body) > 10 and body[0:10] == '<x-flowed>':
        body = body[10:]
        flow = 1
    for line in RElinesplit.split(body):
        line = line.expandtabs()
        if not line.strip():
            res.append('<P>')
            atstart = 1
        else:
            if line[0] == '>': # quoted text
                if len(line)>1 and line[1] == ' ':
                    line = line[2:]
                else:
                    line = line[1:]
                if not quoting:
                    res.append('<blockquote>')
                    quoting = 1
                    if not flow and len(line)>1 and line[-1] == ' ':
                        flow = 1
                if not lines.strip():
                    res.append('<P>')
                    atstart = 1
                    continue
            elif quoting:
                res.append('</blockquote>')
                quoting = 0

            if '/' in line or '@' in line:
                line = translate(line, 0)
            elif '<' in line or '&' in line:
                line = escape(line)

            if line[0] == ' ':
                m = REleadingspace.match(line)
                n = m.end() - m.start()
                if not atstart:
                    res.append('<br>')
                    atstart = 1
                line = n * '&nbsp;' + line[m.end():]
            elif line[0] in rulechars:
                if re.match('^['+rulechars+']{4,}\\s*$', line):
                    line = '<hr>'
                elif not atstart:
                    res.append('<br>')
                    atstart = 1
            if '*' in line:
                line = emphasize(line)
            res.append(line)
            atstart = 0
            if not flow or line[-1] != ' ':
                res.append('<br>')
                atstart = 1
    return '\n'.join(res)


def AsHtml(texto):
    return text2html(texto)


def as_row(row, titulo=''):
    buff = ['<table class="browser">']
    if titulo:
        buff.append('<caption>%s</caption>' % titulo)
    for (name, value) in row.items():
        buff.append('<tr>')
        buff.append('<th>%s</th>' % name)
        buff.append('<td>%s</td>' % value)
        buff.append('</tr>')
    buff.append('</table>')
    return '\n'.join(buff)


def as_iban(code):
    return ' '.join([
        code[0:4], code[4:8], code[8:12],
        code[12:16], code[16:20], code[20:24],
        ])


def as_multiples_enlaces(texto, patrones_a_enlazar):
    for (patron, url) in patrones_a_enlazar:
        texto = texto.replace(patron, '<a href="%s">%s</a>' % (url, patron))
    return texto


def as_resumen_texto(texto, num_words=20):
    words = texto.split()
    if len(words) > num_words:
        return ' '.join(words[0:num_words]) + '...'
    else:
        return ' '.join(words)

# wtforms

def as_errors(items):
    if items:
        buff=['<ul class="error list-unstyled">']
        for msg in items:
            buff.append('<li>{}</li>'.format(msg))
        buff.append('</ul>')
        return ''.join(buff)
    else:
        return '<br>'


def as_field_errors(form, field_name):
    if field_name in form.errors:
        return as_errors([msg for msg in form.errors[field_name]])
    else:
        return ''


def as_form(form, action, label='Enviar', method='POST', title='', klass='form-horizontal'):
    from wtforms import DateField

    buff = [
        '<form method="{}" '
        ' enctype="multipart/form-data"'
        ' class="{}"'
        ' action="{}">'.format(method, klass, action)
        ]
    if title:
        buff.append('<h3>{}</h3>'.format(title))
    if hasattr(form, 'template'):
        env = Environment()
        env.filters['as_errors'] = lambda field: as_errors(field.errors)
        t = env.from_string(form.template)
        buff.append(t.render(form=form, label=label))
    else:
        for field in form:
            if field.type == 'HiddenField':
                buff.append(str(field))
                continue
        # buff.append(u'<div class="form-group">')
            buff.append('<div class="form-group">')
            buff.append('<label for="{}" class="col-sm-2 control-label">'.format(
                field.id
                ))
            if field.flags.required:
                buff.append('<b>{}</b><sup>*</sup>'.format(field.label.text))
            else:
                buff.append(field.label.text)
            buff.append('</label>')
            buff.append('<div class="col-sm-10">')
            if isinstance(field, DateField):
                buff.append(unicode(field(class_="form-control datepicker")))
            else:
                buff.append(unicode(field(class_="form-control")))
            buff.append(as_field_errors(form, field.name))
            if hasattr(field, 'description'):
                buff.append('<span class="form-field-description">')
                buff.append(field.description)
                buff.append('</span>')
            buff.append('</div>')
            buff.append('</div><!-- de form-group -->')
        buff.append('<div class="form-group">')
        buff.append(' <div class="col-sm-offset-2 col-sm-10">')
        buff.append(
            ' <button type="submit" class="btn btn-primary">'
            '{}'
            '</button>'.format( label)
            )
        buff.append(' </div>')   
        buff.append('</div><!-- de form-group -->')
    buff.append(u'</form>')        
    return u'\n'.join(buff)


def as_badge(num):
    return '<span class="badge">{}</span>'.format(num)


def as_month_name(num):
    return Fechas.MESES[num]


def button(url, desc, klass='default'):
    return '<a role="button" class="btn btn-{k}" href="{u}">{d}</a>'.format(
        k=klass,
        d=desc,
        u=url,
        )


def as_fecha_futura(fecha, ref=None):
    if isinstance(fecha, Fechas.FECHA):
        fecha = fecha.as_date()
    if not ref:
        ref = datetime.date.today()
    delta = fecha - ref
    if delta.days < 0:
        return ''
    elif delta.days == 0:
        return 'hoy'
    elif delta.days == 1:
        return 'mañana'
    elif delta.days == 2:
        return 'pasado mañana'
    return 'dentro de {} días'.format(delta.days)


_mapa_get_inicial = {
    193: 65,  # A acentuada mayúscula -> A
    201: 69,  # E acentuada mayúscula -> E
    205: 73,  # I acentuada mayúscula -> I
    211: 79,  # O acentuada mayúscula -> O
    218: 85,  # U acentuada mayúscula -> U
    220: 85,  # U con diéresis -> U
    }


def get_inicial(word):
    '''Espera un texto en unicode o codificado utf-8, devuelve ASCII.
    '''
    global _mapa_get_inicial
    if type(word) != six.text_type:
        word = word.decode('utf-8')
    inicial = word[0].upper().translate(_mapa_get_inicial)
    return inicial.encode('utf-8')


def as_markdown(s):
    if type(s) != six.text_type:
        s = s.decode('utf-8')
    result = markdown(s, extras=['tables', 'footnotes'])
    if '<table' in result:
        result = result.replace('<table', '<table class="table"')
    return result


def as_hora(num_minutes):
    if num_minutes < 0 or num_minutes > 1439:
        raise ValueError('El número de minutos es incorrecto')
    horas = num_minutes // 60
    minutos = num_minutes % 60
    return '{:02d}:{:02d}'.format(horas, minutos)


def as_unbreakable(s):
    return '&nbsp;'.join(str(s).split())


_ordinals = [
    ('primero', 'primera'),
    ('segundo', 'segunda'),
    ('tercero', 'tercera'),
    ('cuarto', 'cuarta'),
    ('quinto', 'quinta'),
    ('sexto', 'sexta'),
    ('séptimo', 'séptima'),
    ('octavo', 'octava'),
    ('noveno', 'novena'),
    ('décimo', 'décima'),
    ('undécimo', 'undécima'),
    ('duodécimo', 'duodécima'),
    ]


def as_ordinal(num, is_fem=False):
    assert num > 0
    pos = 1 if is_fem else 0
    index = num - 1
    if num < 13:
        return _ordinals[index][pos]
    elif num == 18:
        return 'décimoctava' if is_fem else 'décimoctavo'

    elif num < 20:
        index -= 10
        return 'décimo{}'.format(_ordinals[index][pos])
    elif num == 20:
        return 'vigésima' if is_fem else 'vigésimo'
    elif num < 30:
        index -= 20
        return 'vigésimo {}'.format(_ordinals[index][pos])
    elif num == 30:
        return 'trigésima' if is_fem else 'trigésimo'
    elif num < 40:
        index -= 30
        return 'trigésimo {}'.format(_ordinals[index][pos])
    else:
        return "{}{}".format(
            num,
            'ª' if is_fem else 'º',
            )


class AsIconDays:

    def __init__(self):
        self.hoy = datetime.date.today()
        self._icons = [
            '\u2610',  # Empty box 
            '\u2680', '\u2681', '\u2682',  # Dice numer 1-3
            '\u2683', '\u2684', '\u2685',  # Dice numer 4-6
            ]

    def __call__(self, fecha):
        delta = fecha - self.hoy
        if delta.days < 0:
            return '\u2611'  # Checked box
        elif 0 <= delta.days < 7:
            return self._icons[delta.days]
        else:
            return '\u25A3'  # White square containing black small square


def as_list_errors(l):
    if l:
        buff = ['<ul class="errors">']
        for item in l:
            buff.append('<li>{}</li>'.format(item))
        buff.append('</ul>')
        return ''.join(buff)
    else:
        return ''


def as_fa_ico(kind, size=''):
    if size:
        return '<i class="fa fa-{} fa-{}" aria-hidden="true"></i>'.format(kind, size)
    else:
        return '<i class="fa fa-{}" aria-hidden="true"></i>'.format(kind)

as_play_ico = partial(as_fa_ico, 'play')

as_stop_ico = partial(as_fa_ico, 'stop')

as_pause_ico = partial(as_fa_ico, 'pause')

as_clock_ico = partial(as_fa_ico, 'clock-o')
