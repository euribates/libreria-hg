#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import re
import six

if six.PY3:
    from html import escape
else:
    from cgi import escape


class ErrorValidacion(Exception):

    def __init__(self, mensaje, extra=''):
        self.Mensaje = mensaje
        self.extra = extra
        self.Severity = False
        Exception.__init__(self, mensaje)

    def __str__(self):
        if self.extra:
            return '{}: {}'.format(self.Mensaje, escape(str(self.extra)))
        else:
            return self.Mensaje


class ErrorValorNulo(ErrorValidacion):
    pass


class ErrorValorIncorrecto(ErrorValidacion):
    pass


class IntegridadReferencial(ErrorValidacion):
    pass


class Prerequisito(ErrorValidacion):
    def __init__(self, mensaje, value=None):
        ErrorValidacion.__init__(self, mensaje, value)
        self.Severity = True


def MaxSize(value, size, mensaje):
    """
    Validacion de longitud máxima.

    Comprueba que un valor determinado, o bien sea nulo, o bien que su longitud
    sea inferior o igual al límite indicado en el parámetro size. Si no se
    verifica la condición, se elevara una excepción del tipo
    L{ErrorValidacion}.

    @param value: valor a comprobar
    @type value: string
    @param size: límite máximo de tamaño que puede alcanzar value
    @type value: int
    @param mensaje: Mensaje de error a retornar en la excepción, en
                    caso de fallo
    @type mensaje: string
    """
    if value and len(str(value)) > size:
        raise ErrorValidacion(mensaje, value)


def MinSize(value, size, mensaje):
    """
    Validacion de longitud mínima.

    Comprueba que un valor determinado, o bien sea nulo, o bien que su longitud
    sea superior o igual al límite indicado en el parámetro size. Si no se
    verifica la condición, se elevara una excepción del tipo
    L{ErrorValidacion}.

    @param value: valor a comprobar
    @type value: string
    @param size: límite mínimo de tamaño que puede alcanzar value
    @type value: int
    @param mensaje: Mensaje de error a retornar en la
                    excepción, en caso de fallo
    @type mensaje: string
    """
    if value and len(str(value)) < size:
        raise ErrorValidacion(mensaje, value)


def MayorQueCero(value, mensaje=None, name=None):
    mensaje = mensaje or 'No se aceptan valores negativos: [{}]'
    if value <= 0:
        raise ErrorValorIncorrecto(mensaje, name)


def obligatorio(value, mensaje='No se admiten valores nulos', extra=None):
    try:
        if len(value) == 0:
            raise ErrorValorNulo(mensaje, extra=extra)
    except TypeError:
        if value is None or value == {} or value == '':
            raise ErrorValorNulo(mensaje, extra=extra)


NoNulo = obligatorio


def NoVacio(value, name='', mensaje='No se admiten valores vacios'):
    if name:
        mensaje = ':'.join([name, mensaje])
    if not value:
        raise ErrorValorNulo(mensaje)


def verdadero(flag, mensaje):
    if not flag:
        raise ErrorValidacion(mensaje)


def EnRango(valor, minimo, maximo):
    if minimo <= valor <= maximo:
        return
    msg = 'El valor [{}] no está en del rango permitido: [{}..{}]'.format(
        valor, minimo, maximo
        )
    raise ErrorValorIncorrecto(msg)


def PasswordSegura(value, msg=None):
    msg = msg or 'Contraseña insegura: debe tener 6 caracteres o más'
    if value and len(value) < 6:
        raise ErrorValorIncorrecto(msg)


def SonIguales(valor1, valor2, msg='Los valores indicados no coinciden.'):
    if valor1 != valor2:
        raise ErrorValorIncorrecto(msg)


def DBActiva(db, msg='Imposible conectar con la base de datos'):
    if db is None:
        raise Prerequisito(msg)


def UsuarioValidado(sesion):
    NoNulo(sesion, 'Operación Prohibida para usuarios no identificados')


def CorreoElectronico(email, msg='El formato del email es incorrecto'):
    if not email:
        return
    if email.count('@') != 1:
        extra_msg = '{}: falta el caracter @'.format(msg)
        raise ErrorValorIncorrecto(extra_msg)
    (username, hostname) = email.split('@')
    if hostname.count('.') == 0:
        raise ErrorValorIncorrecto(msg)
    if ' ' in username or ' ' in hostname:
        extra_msg = '{}: {}'.format(msg, 'No puede contener espacios')
        raise ErrorValorIncorrecto(extra_msg)


correo_electronico = CorreoElectronico


_iban_length = {
    'AL': 28, 'AD': 24, 'AT': 20, 'AZ': 28, 'BE': 16, 'BH': 22,
    'BA': 20, 'BG': 22, 'BR': 29, 'CR': 21, 'HR': 21, 'CY': 28,
    'CZ': 24, 'DK': 18, 'DO': 28, 'EE': 20, 'FO': 18, 'FI': 18,
    'FR': 27, 'GE': 22, 'DE': 22, 'GI': 23, 'GR': 27, 'GL': 18,
    'HU': 28, 'IS': 26, 'IE': 22, 'IL': 23, 'IT': 27, 'KZ': 20,
    'KW': 30, 'LV': 21, 'LB': 28, 'LI': 21, 'LT': 20, 'LU': 20,
    'MK': 19, 'MT': 31, 'MR': 27, 'MU': 30, 'MC': 27, 'MD': 24,
    'ME': 22, 'NL': 18, 'NO': 15, 'PS': 29, 'PL': 28, 'PK': 24,
    'PT': 25, 'RO': 24, 'SM': 27, 'SA': 24, 'RS': 22, 'SK': 24,
    'SI': 19, 'ES': 24, 'SE': 24, 'CH': 21, 'TN': 24, 'TR': 26,
    'AE': 23, 'GB': 22, 'VG': 24
}


def IBAN(code):
    global _iban_length
    # Official validation algorithm:
    # https://en.wikipedia.org/wiki/International_Bank_Account_Number#Validating_the_IBAN
    # Comprobar la longitud del código IBAN, que depende del pais
    value = code.replace(' ', '')  # Ignoramos espacios, si los hubiera
    country_code = value[:2]
    if country_code not in _iban_length:
        raise ErrorValidacion(
            code,
            'Código de país desconocido: %s' % country_code
            )
    if _iban_length[country_code] != len(value):
        raise ErrorValidacion(
            code,
            'Longitud incorrecta para el país %s.' % country_code
            )

    # Pasar los 4 primeros caracteres al final
    value = value[4:] + value[:4]

    # 3. Reemplazar letras por números siguiendo el esquema
    # A = 10, B = 11, ..., Z = 35.
    value_digits = ""
    for x in value:
        ord_value = ord(x)
        if 48 <= ord_value <= 57:  # 0 - 9
            value_digits += x
        elif 65 <= ord_value <= 90:  # A - Z
            value_digits += str(ord_value - 55)
        else:
            raise ErrorValidacion(
                code,
                'El caracter %s no es válido para un IBAN.' % x
                )

    if int(value_digits) % 97 != 1:
        raise ErrorValidacion(
            code,
            'No es un código IBAN válido.'
            )


def NIF(value):
    '''comprueba la letra del NIF. No aplicable a personas jurídicas.
    Los NIF deben tener una letra al final. El CIF (antiguo, pero
    muchas empresas todavÍa lo tienen) tiene una letra al inicio y puede
    ser que tenga una al final, o no.

    Si Todo va bien, devuelve el dato original, o corregido si es un error
    menor. En caso de error, eleva una excepción de tipo ErrorValidacion:
    '''
    if not value:
        return value
    value = str(value).strip().upper()
    if len(value) != 9:
        raise ErrorValidacion(value, 'La longitud debe ser de 9 caracteres')

    Tabla = "TRWAGMYFPDXBNJZSQVHLCKE"
    patNIF = re.compile(r'(\d{8})([TRWAGMYFPDXBNJZSQVHLCKE])')
    match = patNIF.match(value)
    if match:
        letra = match.group(2)
        numero = int(match.group(1))
        if letra != Tabla[numero % 23]:
            raise ErrorValidacion(value, 'El NIF no es correcto')
        return value

    patCIF = re.compile(r'([ABCDEFGHJKLMNPQRSUVW])(\d{7})(\d|[JABCDEFGHI])')
    match = patCIF.match(value)
    if match:
        primero = match.group(1)
        numero = match.group(2)
        control = match.group(3)
        pesos = [2, 1, 2, 1, 2, 1, 2]
        digitos = [int(x) for x in numero]
        acc = sum([
            ((x*y) % 10)+((x * y) // 10)
            for x, y in zip(digitos, pesos)
            ])
        dc = (10 - (acc % 10)) % 10
        if control == 'JABCDEFGHI'[dc]:
            return value
        elif int(control) == dc:
            return value
        raise ErrorValidacion(value, 'El CIF no es correcto')

    patNIE = re.compile(r'([XYZ])(\d{7})([TRWAGMYFPDXBNJZSQVHLCKE])')
    match = patNIE.match(value)
    if match:
        primero = match.group(1)
        if primero == 'X':
            numero = int(match.group(2))
        elif primero == 'Y':
            numero = int('1' + match.group(2))
        else:
            numero = int('2' + match.group(2))
        control = match.group(3)
        if control != Tabla[numero % 23]:
            raise ErrorValidacion(value, u'El NIE no es correcto')
        return value
    raise ErrorValidacion(value, u'El NIF/CIF/NIE no es correcto')


# Agrupar varias validaciones por nombres


class Validator():

    def __init__(self, data=None):
        self.kernel = {}
        self.data = {}
        if data:
            self.data.update(data)
        self.errors = {}

    def __len__(self):
        return len(self.errors)

    def __nonzero__(self):
        return len(self.errors) == 0  # Inverted logic; True if no errors

    def validate(self, name, functor, *args, **kwargs):
        self.kernel.setdefault(name, []).append((functor, args, kwargs))

    def check(self, name, validate_function, value, *args, **kwargs):
        try:
            validate_function(value, *args, **kwargs)
        except ErrorValidacion as err:
            self.errors.setdefault(name, []).append(str(err))

    def is_valid(self, **kwargs):
        self.data.update(kwargs)
        for name in self.kernel:
            value = self.data.get(name, None)
            for validate_function, args, kwargs in self.kernel[name]:
                self.check(name, validate_function, value, *args, **kwargs)
        return self.errors == {}

    def list_errors(self, name):
        return self.errors.get(name, [])

    def __str__(self):
        buff = ['--[validator]----------------------------------']
        buff.append('Kernel size: {}'.format(len(self.kernel)))
        for i, name in enumerate(self.kernel):
            buff.append(' {}) {}'.format(i, name))
        buff.append('Data size: {}'.format(len(self.data)))
        for name in self.data:
            buff.append('  {}: {}'.format(name, self.data[name]))
        buff.append('--[end validator]------------------------------')
        buff.append('Errores: {}'.format(len(self.errors)))
        for name in self.errors:
            buff.append('  {}'.format(name))
            for err in self.errors[name]:
                buff.append('    - {}'.format(err))
        buff.append('bool(self): {}'.format(bool(self)))
        return '\n'.join(buff)
