#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import six
import sys
import codecs
import types

if six.PY2:
    def _b(s):
        return s
    def _u(t):
        return t.decode('utf-8')
else:
    def _b(s):
        return s.encode('utf-8')
    def _u(t):
        return t

class RTF(object):

    def __init__(self, salida=None):
        self.close_on_exit = True
        if isinstance(salida, six.string_types):
            self.Out = codecs.open(salida, 'w', 'cp1250')
        elif hasattr(salida, 'write'):
            self.Out = salida
        else:
            self.Out = sys.stdout
            self.close_on_exit = False

    def __del__(self):
        if self.close_on_exit:
            self.Out.close()

    def Begin(self, titulo='Sin título', creator='RTF.py'):
        self.Creator = _u(creator)
        self.Titulo = _u(titulo)
        self.Out.write(u'{\\rtf1\\ansi\\deff0\\deftab720\n')
        self.Out.write(u'{\\fonttbl\n')
        self.Out.write(u'{\\f0\\fnil Times New Roman;}\n')
        self.Out.write(u'{\\f1\\fnil\\fcharset2 Symbol;}\n')
        self.Out.write(u'{\\f2\\fswiss\\fprq2 System;}\n')
        self.Out.write(u'{\\f3\\fnil MS Sans Serif;}\n')
        self.Out.write(u'{\\f4\\fswiss\\fprq2 Arial;}}\n')
        self.Out.write(u'{\\colortbl\\red0\\green0\\blue0;}\n')
        self.Out.write(u'\\deflang1034\n')
        self.Out.write(u'\\pard\\plain\\f3\\fs22\n')

    def Header(self, subtitulo=''):
        self.subtitulo = _u(subtitulo)
        self.Out.write(
              u"{\\header\\pard\\plain\\s15\\widctlpar\\tqc\\tx4419"
              u"\\tqr\\tx8504\\adjustright \\fs20\\lang3082\\cgrid"
              )
        self.Out.write(u"{\\f4\\fs18 %s \\tab \\tab {\\b %s}\\par }" % (
            self.Creator,
            self.Titulo,
            ))
        if self.subtitulo:
           self.Out.write(u"{\\f4\\fs18\\tab\\tab %s\\par }" % self.subtitulo)
        self.Out.write(u'}\n')

    def End(self):
        self.Out.write(u'\n}\n')
        self.Out.close()

    def p(self, text):
        text = _u(text)
        self.Out.write(u'{} \\par\n'.format(text))

    def b(self, text):
        return u'\\b {}\\b0 '.format(_u(text))

    def i(self, text):
        return u'\\i {}\\i0 '.format(_u(text))

    def h1(self, text):
        self.Out.write(u'\\f4\\fs60 %s\\plain\\par\n' % _u(text))

    def h2(self, text):
        self.Out.write(u'\\f4\\fs48 %s\\plain\\par\n' % _u(text))

    def h3(self, text):
        self.Out.write(u'\\f4\\fs36 %s\\plain\\par\n' % _u(text))

    def h4(self, text):
        self.Out.write(u'\\f4\\fs32 %s\\plain\\par\n' % _u(text))


