#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import pytest
import unittest
from . import filters


class AsInicial(unittest.TestCase):

    def test_known_values(self):
        self.assertEqual(filters.as_inicial('Hola'), 'H')
        self.assertEqual(filters.as_inicial('hola'), 'H')
        self.assertEqual(filters.as_inicial('Mundo'), 'M')
        self.assertEqual(filters.as_inicial('mundo'), 'M')

    def test_empty(self):
        self.assertEqual(filters.as_inicial(None), '')
        self.assertEqual(filters.as_inicial(''), '')

    def test_acentos_a(self):
        self.assertEqual(filters.as_inicial('árbol'), 'A')
        self.assertEqual(filters.as_inicial('Árbol'), 'A')

    def test_acentos_e(self):
        self.assertEqual(filters.as_inicial('énfasis'), 'E')
        self.assertEqual(filters.as_inicial('Énfasis'), 'E')

    def test_acentos_i(self):
        self.assertEqual(filters.as_inicial('índice'), 'I')
        self.assertEqual(filters.as_inicial('Índice'), 'I')

    def test_acentos_o(self):
        self.assertEqual(filters.as_inicial('óvulo'), 'O')
        self.assertEqual(filters.as_inicial('Óvulo'), 'O')

    def test_acentos_u(self):
        self.assertEqual(filters.as_inicial('único'), 'U')
        self.assertEqual(filters.as_inicial('Único'), 'U')

    def test_acentos_enie(self):
        self.assertEqual(filters.as_inicial('ñu'), 'Ñ')
        self.assertEqual(filters.as_inicial('Ñu'), 'Ñ')


class AsSafeFilename(unittest.TestCase):

    def test_uso(self):
        self.assertEqual(
            filters.as_safe_filename('Hola, mundo  que tal?.rtf'),
            'hola-mundo-que-tal.rtf',
            )

class SetInitialLower(unittest.TestCase):

    def test_set_initial_lower(self):
        self.assertEqual(filters.set_initial_lower('hola'), 'hola')
        self.assertEqual(filters.set_initial_lower('Hola'), 'hola')
        self.assertEqual(filters.set_initial_lower('Zorglub'), 'zorglub')

    def test_set_initial_lower_only_touchs_first_letter(self):
        self.assertEqual(filters.set_initial_lower('HOLA'), 'hOLA')


class SetInitialUpper(unittest.TestCase):

    def test_set_initial_upper(self):
        self.assertEqual(filters.set_initial_upper('hola'), 'Hola')
        self.assertEqual(filters.set_initial_upper('Hola'), 'Hola')
        self.assertEqual(filters.set_initial_upper('hOLA'), 'HOLA')
        self.assertEqual(filters.set_initial_upper('zorglub'), 'Zorglub')

    def test_set_initial_upper_only_touchs_first_letter(self):
        self.assertEqual(filters.set_initial_upper('hOLa'), 'HOLa')


class HourToInteger(unittest.TestCase):

    def test_known_values(self):
        self.assertEqual(filters.hour_to_integer('00:00'), 0)
        self.assertEqual(filters.hour_to_integer('00:01'), 1)
        self.assertEqual(filters.hour_to_integer('00:59'), 59)
        self.assertEqual(filters.hour_to_integer('01:00'), 60)
        self.assertEqual(filters.hour_to_integer('01:01'), 61)
        self.assertEqual(filters.hour_to_integer('08:30'), 510)
        self.assertEqual(filters.hour_to_integer('11:59'), 719)
        self.assertEqual(filters.hour_to_integer('12:00'), 720)
        self.assertEqual(filters.hour_to_integer('12:01'), 721)
        self.assertEqual(filters.hour_to_integer('23:59'), 1439)

    def test_hour_wrong_value(self):
        self.assertRaises(ValueError, filters.hour_to_integer, 'WTF!')

    def test_wrong_hour(self):
        self.assertRaises(ValueError, filters.hour_to_integer, '24:00')
        self.assertRaises(ValueError, filters.hour_to_integer, '24:01')
        self.assertRaises(ValueError, filters.hour_to_integer, '44:02')
    
    def test_wrong_minutes(self):
        self.assertRaises(ValueError, filters.hour_to_integer, '07:60')
        self.assertRaises(ValueError, filters.hour_to_integer, '07:61')
        self.assertRaises(ValueError, filters.hour_to_integer, '07:89')


class Slugify(unittest.TestCase):

    def test_hola(self):
        self.assertEqual(filters.slugify('holá'), b'hola')

    def test_arbol(self):
        self.assertEqual(filters.slugify('árbol'), b'arbol')

    def test_vocals(self):
        self.assertEqual(filters.slugify('áéíóúü-ÁÉÍÓÚÜ'), b'aeiouu-aeiouu')
    
    def test_enies(self):
        self.assertEqual(filters.slugify('año'), b'anio')
        self.assertEqual(filters.slugify('AÑO'), b'anio')

    def test_parentesis(self):
        self.assertEqual(filters.slugify('(hola)'), b'hola')

    def test_corchetes(self):
        self.assertEqual(filters.slugify('[hola]'), b'hola')

    def test_llaves(self):
        self.assertEqual(filters.slugify('[hola]'), b'hola')

    def test_pipe(self):
        self.assertEqual(filters.slugify('hola|mundo'), b'hola-mundo')

    def test_space(self):
        self.assertEqual(filters.slugify('hola mundo'), b'hola-mundo')

    def test_symbols(self):
        self.assertEqual(filters.slugify('!"#$%&\'*,.'), b'')

    def test_plus(self):
        self.assertEqual(filters.slugify('hola+mundo'), b'hola-mundo')

    def test_hyphen(self):
        self.assertEqual(filters.slugify('hola-mundo'), b'hola-mundo')

    def test_slash(self):
        self.assertEqual(filters.slugify('hola/mundo'), b'hola-mundo')

    def test_backslash(self):
        self.assertEqual(filters.slugify('hola\\mundo'), b'hola-mundo')

    def test_numbers(self):
        self.assertEqual(filters.slugify('0123456789'), b'0123456789')

    def test_lowercase(self):
        self.assertEqual(
            filters.slugify('abcdefghijklmnñopqrstuvwxyz'), 
            b'abcdefghijklmnniopqrstuvwxyz'
            )

    def test_uppercase(self):
        self.assertEqual(
            filters.slugify('ABCDEFGHIJKLMNÑOPQRSTUVWXYZ'), 
            b'abcdefghijklmnniopqrstuvwxyz'
            )

    def test_colon(self):
        self.assertEqual(filters.slugify('hola:mundo'), b'hola-mundo')

    def test_semicolon(self):
        self.assertEqual(filters.slugify('hola;mundo'), b'hola-mundo')

    def test_angled_brackets(self):
        self.assertEqual(filters.slugify('<hola mundo>'), b'hola-mundo')

    def test_equals(self):
        self.assertEqual(filters.slugify('hola=mundo'), b'hola-mundo')

    def test_question_marks(self):
        self.assertEqual(filters.slugify('¿hola?'), b'hola')

    def test_exclamation_marks(self):
        self.assertEqual(filters.slugify('¡hola!'), b'hola')

    def test_at(self):
        self.assertEqual(filters.slugify('hola@mundo'), b'hola-mundo')

    def test_caret(self):
        self.assertEqual(filters.slugify('hola^mundo'), b'hola-mundo')

    def test_underscore(self):
        self.assertEqual(filters.slugify('hola_mundo'), b'hola-mundo')

    def test_grave_accent(self):
        self.assertEqual(filters.slugify('hola`mundo'), b'holamundo')

    def test_equivalency_sign(self):
        self.assertEqual(filters.slugify('hola~mundo'), b'hola-mundo')

    def test_multiple_hyphens(self):
        self.assertEqual(filters.slugify('hola-~mundo'), b'hola-mundo')
        self.assertEqual(filters.slugify('hola-------mundo'), b'hola-mundo')
        self.assertEqual(filters.slugify('hola-~-=-_-^-@-mundo'), b'hola-mundo')

    def test_euro(self):
        self.assertEqual(filters.slugify('23€'), b'23-euros')

    def test_ellipsis(self):
        self.assertEqual(filters.slugify('hola…mundo'), b'hola-mundo')

    def test_ejemplo(self):
        self.assertEqual(
            filters.slugify('Charla: ¿Hablamos bien los canarios?'), 
            b'charla-hablamos-bien-los-canarios'
            )

image_filenames = [
    'imagen.png', 'imagen.jpg', 'imagen.gif', 
    'imagen.PNG', 'imagen.JPG', 'imagen.GIF',
    'nombre.con.muchos.puntos.jpg',
    ]
@pytest.fixture(params=image_filenames, ids=image_filenames)
def image_name(request):
    return request.param


def test_is_image_gif(image_name):
    assert filters.is_image(image_name)

def test_is_not_image_pdf():
    assert filters.is_image('ejemplo.pdf') is False

def test_is_not_image_DOC():
    assert filters.is_image('ejemplo.DOC') is False


if __name__ == '__main__':
    pytest.main()
