#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import cgitb
import os
import sys
import datetime
import traceback
from cgi import escape
from functools import partial
from pprint import pformat
from six.moves import StringIO
from six.moves.urllib.parse import quote

import six

from Libreria import config

'''
DESCRIPCIÓN
===========

Generación de HTML.

'''


VERSION = '18.1'

ERROR_ID_CODE = '4458c1ac-dfec-44d7-b9bc-a2a1173e283b'

PANIC_ID_CODE = 'dd108fed-13a2-4b89-8cc3-4edcd138fb78'


if six.PY2:
    __metaclass__ = type


def env_is_development():
    return config.DEBUG


class ObjectDict(dict):
    """Makes a dictionary behave like an object, with attribute-style access.
    """
    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)

    def __setattr__(self, name, value):
        self[name] = value


class HtmlError(Exception):
    def __init__(self, text, ext):
        Exception.__init__(self, '%s\n<br>%s' % (text, ext))


def printf(s, *args, **kwargs):
    end = kwargs.pop('end', '\n')
    if isinstance(s, bytes):
        s = s.decode('utf-8')
    else:
        s = str(s)
    if args or kwargs:
        print(s.format(*args, **kwargs), end=end)
    else:
        print(s, end=end)


def print_panic_code():
    global PANIC_ID_CODE
    printf(
        '<span id="error_code" data-code="{0}">Error code: {0}</span>',
        PANIC_ID_CODE
        )


def print_error_code():
    global ERROR_ID_CODE
    printf(
        '<span id="error_code" data-code="{0}">' 'Error code: {0}' '</span>',
        ERROR_ID_CODE
        )


def version():
    return 1


HEADER_ENDS = False

_Stack = []
_Top_Stack = 0
_Java_Scripts = []
_Styles = []


def addStyle(url, media='screen'):
    global _Styles
    _Styles.append((url, media))


def getStyles():
    global _Styles
    return _Styles


def add_javascript(code, first=False, inline=False):
    global _Java_Scripts
    if inline:
        url = '''
<script type="text/javascript">
//<![CDATA[
%s
//]]>
</script>
''' % code
    else:
        url = '<script language="Javascript" src="%s"></script>' % code
    if first:
        _Java_Scripts.insert(0, url)
    else:
        _Java_Scripts.append(url)


addJavaScript = add_javascript


def getJavaScripts():
    global _Java_Scripts
    return _Java_Scripts


def Header(extra=None, charset="utf-8"):
    global HEADER_ENDS
    if not HEADER_ENDS:
        sys.stderr = sys.stdout
        sys.stdout.write(
            "Content-type: text/html; charset={0}\n".format(charset)
            )
        if extra is not None:
            sys.stdout.write('{}\n'.format(extra))
        sys.stdout.write('\n')
    HEADER_ENDS = True


simple_header = Header


def Debug():
    sys.stderr = sys.stdout
    print("Content-type: text/html", end='\n\n')

    print('<html><head><title>Debug mode</title></head>')
    print('<body>')
    print('<h3>Variables de entorno</h3>')
    print('<ul>')
    for k in os.environ.keys():
        print('<li><b>{}</b>: {}'.format(k, os.environ[k]))
    print('</ul>')
    print('</body>')
    print('</html>')


def push(item):
    global _Stack, _Top_Stack
    _Stack.append(item)
    _Top_Stack = _Top_Stack + 1


def pop():
    global _Stack, _Top_Stack
    _Top_Stack = _Top_Stack - 1
    sys.stdout.write(_Stack[_Top_Stack])
    del _Stack[_Top_Stack]


_300_status_desc = {
     "301": 'Moved Permanently',
     "302": 'Found',
     "303": 'See Other',
     "304": 'Not Modified',
     "305": 'Use Proxy',
     "307": 'Temporary Redirect',
     }


def Redirect(url, extra='', status='302'):
    global _300_status_desc
    sys.stdout.write('Location: {}\n'.format(url))
    if status != '302':
        sys.stdout.write('Status: {status} {desc}\n'.format(
            status=status,
            desc=_300_status_desc.get(
                status, 'Redirection {}'.format(status)
                )
            ))
    if extra:
        sys.stdout.write('{}\n'.format(extra))
    sys.stdout.write('\n')


def SimpleBegin(titulo):
    printf('''
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>{titulo}</title>
<link rel="stylesheet" type="text/css" href="/static/art/printer.css">
<script language="Javascript" src="/static/lib/jquery-3.1.1.min.js"></script>
<script language="Javascript" src="/static/lib/jquery.ui.js"></script>
<script language="Javascript" src="/static/js/bootstrap.min.js"></script>
<script language="Javascript" src="/static/js/intranet.js"></script>
</head>
<body bgcolor="#FFFFFF">
''', titulo=titulo)


simple_begin = SimpleBegin


def SimpleEnd():
    printf('</body>')
    printf('</html>')


simple_end = SimpleEnd


def Begin(titulo, style='', onload='', onunload='', extra='', meta=[]):
    '''Imprime el código generico de inicio para un documento Html.

    Esta función probablemente será reescrita en otros módulos que
    importen Html, para adecuarse a las necesidades de cada aplicación.

    @param titulo: Título de la página html.
    '''
    global _Styles
    global _Java_Scripts
    printf('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">')
    printf('<html><head>')
    for m in meta:
        printf(m)
    printf('<title>{}</title>', titulo)
    if style:
        printf(
            '<link rel="stylesheet" type="text/css" '
            'media="screen,projection" href="{}">',
            style
            )
    if _Styles:
        for (url, media) in _Styles:
            printf(
                '<link rel="stylesheet" type="text/css" '
                'media="{}" href="{}">',
                media,
                url,
                )
    for linea in _Java_Scripts:
        printf(linea)
    if extra:
        printf(extra)
    printf('</head>')
    printf('<Body bgcolor="#FFFFFF"', end='')
    if onload:
        printf(' onload="{}"'.format(onload), end='')
    if onunload:
        printf(' onunload="{s}"'.format(onunload), end='')
    printf('>')
    printf('<h3 align="center">{0}</h3>', titulo)


begin = Begin


def End():
    printf('</body>')
    printf('</html>')


end = End


def TraceBack(msg=None, extra=None):
    (tipo, valor) = sys.exc_info()[:2]
    if msg:
        printf('<p class="SUPERNOTA">{}', msg)
    printf('<p class="NOTA">{}<br>Tipo: <b><i>{}</i></b></p>', valor, tipo)
    if extra:
        printf('<tt>{}</tt>', extra)
    printf('<p><b>Traza de ejecución:</b>')
    printf('<hr noshadow size="1" width="450"></p>')
    printf('<pre>')
    printf('{}:', tipo)
    traceback.print_exc(file=sys.stdout)
    printf('</pre>')


def get_panic_styles():
    return '''<style>
body {
    background-color: linen;
}

div.panic-header {
    border-bottom: 1px solid silver;
    line-height: 64px;
    }

div.panic-header img {
    float: left;
    width: 64px;
    height: 64px;
    }

h1 {
    padding-top: 2px;
    margin-top: 0;
    color: maroon;
    margin-left: 72px;
    font-size: 32px;
}

h2 {
    font-family: helvetica,airal,sans-serif;
    font-size: 24px;
}

h3 {
    font-family: helvetica,airal,sans-serif;
    font-size: 20px;
    font-weight: lighter;
}
</style>'''


def get_panic_icon():
    return (
        '<img src="data:image/png;base64,'
        'iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAA'
        'CqaXHeAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz'
        'AAALEwAACxMBAJqcGAAACi9JREFUeNrVm2tsXM'
        'UVx39z971e21lfJ21sg+M4CSYkJIEmRYlYSqlU'
        '+i6tQCWiKhSUohAoUmlpC+IRAUWqVIUAqXi0Ug'
        'slBfVDaVGMigJ0KU2VpgkhAScxxXGME0NY39ix'
        '9713+mHXiXfnrvd1Te2R/GWu9+78z5zzP/9zZl'
        'YwzSMSDp4PXAEsBxYDXcBcwFnwr2ngJHAI6AUO'
        'ADv1kNEznesT0wT6G8B64BqbXvkC8JweMl6csQ'
        'aIhIMNwGZgE+CYpg3LAI8B9+ghY3RGGCASDs4B'
        'tgHX8smO7cBGPWScquUlWo3gHwCMWsBLKZFSVv'
        'PRawEjt4ZP1gMi4eAFwBtAsGpfzpjgX4HEQWz4'
        'LZxOiccFmlbVkgzgUj1kvDPtBoiEg3cBNVndNE'
        '0aVr6Cu3F11hjRPt7fuQ6PO4bfW7URAO7WQ8aD'
        '0xYCkXDw5VrBA3jm33QGPIDD38H8VY8TjQtS6Z'
        'pe/UAkHOyeFg+IhIM9uRxe05A4aVr7PsIRUJ71'
        'v/4FMtG91PslDkdN/HxIDxnn2+YBkXDwAzvAA/'
        'g6NluCB2hZ/QTxuCSRolpinBhduTXXboDczrfa'
        'kricOv62DUUfu+o60TtvJJYQZDI1f1trbu3VGy'
        'AXT7bsPEDd4i0lbT5v2WZM00MsUbMXTHhCd1UG'
        'yLH9lbZJTt9SPM1fKf1/Dj+tFz1KPKnVSogT48'
        'oclvJJMJfnD9oFXkpJ40VhnIHl+fNmkuTIO3iC'
        'qywI8XNkom/bQYgTY5mVTijmAW/YqVmdTV9TwA'
        'MY721jcPd6a0K8+CnicUkyZUsoFMWkFZG3QbvA'
        'm2aG+sW/UL0iZTB08H4cDDH+/iMqIdYvpnnhDU'
        'QTgnTGlqUErWSzsChsDDt339OyibrOzcr80H82'
        'MjK4nWA9aJpEX3sU4ZqTb7zUCL3dXXjdcQJ+EM'
        'KWUAhOLqAKPWCbneAlbvwdP1PrgOgHDB/bjt8L'
        'Dgdomsbou7eo7ulqpHXVI8STwi5CVDBqBfW8rS'
        'Wtv/MhhOZT5k/suQmvR+DznN3VzGg3ycgu5X8D'
        '51yDJ7Cc8RhkMrZwwbU5rFl+mvRgs63VuqsFX8'
        'v3lenYyTcYHzlA+2d/j8CclBHijB25haZL9oDI'
        'd8y2NU9y5G+XkHSDV5N2hMJm4PbCENhkJ/5Al0'
        'U0yQzH991GXd0cvPO+imfe18/8uZu/iNDijPc/'
        'rmaRwHk0d95oJyFuyguBXA/PtjaWo34d7jkhZX'
        '6k/zmSsaO4GLGwTRSkJN5/D2b8uPJ87gX3kTG9'
        'xBNgmjWHgiOH+YwHrLcv7ZnULdliCfD4/jvxec'
        'Ahkurz9BhgIjTB6OEfqYrNGaBt1RY7CXH9ZAPY'
        '1b3F8+nrcPo7lfnhnofRRAy/ByBh0SI6a5TMyM'
        'skh/9VhBCX5Yqlmr3gGgAt17e3TfIGFqmix0wZ'
        'DPU+ejbtOR0WHmCAzJKiEIKxwxuQpmqo1tW/JZ'
        'HEjpKZSDh4vkb20MKW4VtwD8JRr8x/uPdWvG6B'
        'x1VczEgKwKQ/IDrwlIVCXERzx/V2lcxXaGRPbG'
        'xgvib859yqTCdO7cc4sQOvR+KYtPFm4mS+l8Q/'
        'ApmPKNZ3NzKldr3nXvhLpPQRT9bsBcs1ssdVtd'
        'f6Sx4F4VRFz77b8LrB7czffSG0ksWp5tAYfXej'
        'Vc3M/JVbiCUEyVRNy16s2dHwEP6VeJq/pMyPDb'
        '5I4vRB/F61pDVTBSWHloRJwuhMJIx0kzRUQqw/'
        '52q8gaXEEqKWtNilkT2orI34znvcIh8mOb7vdj'
        'wuEwvOQ8r8rct6hwpECMHYoZstjdOy5hkScUhU'
        'HwpzNdRT2soUb/NVuAJqIjF6tyAYwVesz59JWH'
        'hEERDpY0SPPal+d6CDppxCrJIQnTUdjZmmSb2V'
        '6EmPcqLnYbxuabn7WcCnCyYyaiaYNKJ9dyFTqo'
        'Kct/RupPQRS1anEGsygL/9pwhngzL/0f47cDuZ'
        'Mu1lrwPkG40p3FjTJKOHbldDxNWYJcS4IFmFQq'
        'zaAFJrxN/+Y2U+OdpDZOBPStpTVx4rcIB48RCY'
        'MJnxZ5Kn9loTYsOFxKtQiJqyFeVWe0u2glARnt'
        'h7Kx5Xqd0HhFsthkoYQAjBWM8NINUlt61+knhM'
        'VkqIaSfZaynzK0p7vmWWLe7oUDexkX00Bkp3cu'
        'OD20gNv5LTDpL06JtgJstY8gDRgd/gP/cHBSXz'
        'EvTO6xkZ+B1ul8RZHrWfFJFw8FXg8krS3pyL38'
        'RRt1Sp9Y90d+ESH1Pvr+mEt/QaTEnTun6Ff2Qm'
        'ypEdi/C6YtT5ylrDaxrZC0kVpL1vquCB4d6tkI'
        'ng85QHXpoS4VmKS78aXG0V0ZHQBKM9t1kIRD+t'
        'q7ZWohB7NbK3scpMexkClmnvNB/2PITXLXGVqS'
        'p87Q8SXPMP6pc+QdOaPbj0b4Pwlm2EtPEiSUMl'
        'xEDrt/A1rCCeLIsQD2jAzrKrvfafozkb1Wpv/0'
        '9wOjJ43WW2rp0t+BdM0viaG//C+8DRUL4XCMHY'
        'oe+pxCk0Wtc8TSJJyWJJSrlbK/cennQ0UbfgTm'
        'U+dfoQxsDz+EqlvYIen5KOnAGE5qWiSyvpQcb7'
        '1d6js64TveN64iV6iM2Xndo9EXgvlE57j1jOD+'
        '65GZdLlk57k1Xw+BHLlpiZiiGlWREhxo7ei5n8'
        '2KKHeD+mzPYQi3jBS5OF0HN/WU17J3aQOH2AOi'
        '9oFUgqmRok2rc1v3L8770IRitueWua'
        'yekjd1j0EOtpWfWrqU6Zn88rwCPhYNqqMyylZM'
        '5n/o3Dv6iAxRP0vtSJwzGOxw3VtOod7jZcDctI'
        'Du/CNEdLCqGiHpWWfGr1X/Holyp9pr5XL0XG31'
        'VOmfWQISioBB8Dflj4cve87yjgs4Yx6Qj9BVsu'
        'my6oWRmgeZotGyxtq5/mvZ1rcbvAd/ZQ5Q9KCy'
        'Z3XDSSn/ZAX3sY4ZrLbB5DezcydvyP1NdJXE4B'
        'kgX6ZUZ/XjGUu3u7PS++/MtnPXiA+nOvI5nK3j'
        'Uwpdw1Ad6qGtyYz8wnq47LmTQysQE0TeDQQBNi'
        'Q9FyOHdufuampUgPET22ZXaDj5/go7fvxO+VuJ'
        'w8q4eMg8XbsGf5YJjcLREpJaY4F9PdhphlziDN'
        'NInRvWiONB4XzLv8lFBp0toAlpekaj2JscrxNt'
        '3/Kfm9UsrlQoh39JAhSxogZ4SaL0XPoPGsHjK+'
        'a2mcqT6Vu2R45SwGLoGjeshYWNQ7Sr0hEg4eJn'
        't6JGYh+AE9ZLRPGR7lvCkSDiZzqlHMIvBiQu5O'
        'WUuUAV7oIcMN9M2ynReRvwdLGqCiHY2Eg/uBC2'
        'e4AfqmivmKPaBAKK0Afj3D2X5hJR+o9kdT64DX'
        'qfFc0ebxeT1kvFbph6o9GfqnHjJcwEMzAPgzOb'
        'J7vSqRVOu3/x9/OLkL2FCo7T9xAxT0E6b7p7PZ'
        'Zobkrskl7YwwQIEx7P7x9EvA83rIeNb2OmG6/T'
        'QSDnYBVwErgHZgJTDVCchu4EPgLWCHHrK4H2Pj'
        '+B+KmOqV1LoXPAAAAABJRU5ErkJggg==">')


def Panic(err=None, extra=None, debug=False):
    global HEADER_ENDS
    sys.stderr = sys.stdout = sys.__stdout__

    if debug or env_is_development():
        simple_header()
        simple_begin('Panic! - %s' % err)
        print_panic_code()
        print(get_panic_icon())
        h1('Panic! <small>Error crítico</small>')
        h2(err)
        if extra:
            h4('Error en el código de la aplicación')
            print('<div class="extra">{}</dii>'.format(extra))
        h4('Traceback')
        printf('<blockquote><pre>')
        printf('<pre>')
        traceback.print_exc(file=sys.stdout)
        printf('</pre></blockquote>')
        simple_end()
    else:
        print('HTTP/1.1 500 Internal Server Error', end='\n\n')
    sys.exit(0)


panic = Panic  # Old code pre PEP8 calls Panic


def simple_page(title, msg):
    simple_header(charset="utf-8")
    simple_begin(title)
    h1(title)
    h2(msg)
    simple_end()


def error(msg, extra=None):
    Header()
    begin('¡Error! - {}'.format(msg))
    h1('¡Error!')
    h2('Mensaje: {}', msg)
    if extra:
        if isinstance(extra, str):
            blockquote(extra)
        else:
            TraceBack(msg, extra)
        printf('<pre><code>')
        printf('--[traceback]----------------------------------------------')
        traceback.print_exc(file=sys.stdout)
        printf('--[fin de traceback]---------------------------------------')
        printf('</code></pre>')
    print_error_code()
    end()
    sys.exit(-2)


Error = error  # Old code pre PEP8 calls Error


def printMensajeErrorNormal(e, tron=False):
    print('''
<div class="error_validacion">

<h2>Se ha producido un error de validación</h2>

<p>Se ha producido un error de validación. Esto significa
que alguno de los valores introducidos
no es correcto.</p>

<p>La causa del error parece ser la siguiente:</p>

<p class="causa">%s</p>
''' % escape(str(e)))
    if tron:
        print('<h3>Los datos de la excepción son los siguientes</h3>')
        print('<ul>')
        print('<li>Classname: <b>%s</b></li>' % e.__class__.__name__)
        print('<li>Value: <b>%s</b></li>' % escape(str(e)))
        if hasattr(e, 'Mensaje'):
            print('<li>Mensaje: <b>%s</b></li>' % escape(str(e.Mensaje)))
        if hasattr(e, 'Severity'):
            print('<li>Severity: <b>%s</b></li>' % escape(str(e.Severity)))
        print('</ul>')
        print('<h3>Variables de entorno</h3>')
        keys = os.environ.keys()
        keys.sort()
        print('<dl>')
        for k in keys:
            print('<dt><b>%s</b></dt><dd>%s</dd>' % (k, os.environ[k]))
        print('</dl>')
    print('</div>')


def printMensajeErrorGrave(e, tron=False):
    print('<div class="error_validacion">')
    print('<h2>Página web temporalmente inaccesible</h2>')
    print('<h4>Rogamos disculpen las molestias</h3>')
    if tron:
        print('<h3>Los datos de la excepción son los siguientes</h3>')
        print('<ul>')
        print('<li>Classname: <b>%s</b></li>' % e.__class__.__name__)
        print('<li>Value: <b>%s</b></li>' % escape(str(e)))
        print('<li>Mensaje: <b>%s</b></li>' % escape(str(e.Mensaje)))
        print('<li>Severity: <b>%s</b></li>' % escape(str(e.Severity)))
        print('</ul>')
    print('</div>')


def ErrorValidacion(err, tron=False, send_message=True, debug=False):
    sys.stderr = sys.stdout = sys.__stdout__
    simple_header()
    addStyle('/static/css/error_validacion.css')
    if hasattr(err, 'Severity') and err.Severity:
        simple_begin('Intranet - Página web temporalmente inaccesible')
        printMensajeErrorGrave(str(err), tron)
    else:
        simple_begin('Intranet - Error de validación - %s' % err)
        printMensajeErrorNormal(str(err), tron)
    print_error_code()
    printf('''
<h2 class="error_title">Se ha producido un error de validación</h2>
<img src="/static/art/danger.png" width="80" height="80"
 alt="Error" class="error_image">
<p>Se ha producido un error de validación. Esto significa
que alguno de los valores introducidos
no es correcto.</p>''')

    if debug:
        h2('Traceback')
        printf('<blockquote><pre>')
        traceback.print_exc()
        printf('</pre></blockquote>')
    SimpleEnd()
    sys.exit(0)


error_validadion = ErrorValidacion


def aviso(msg, extra=None):
    Header()
    begin('Atención - ' + msg)
    h1('ATENCIÓN')
    h2('Mensaje: %s' % msg)
    if extra:
        blockquote(extra)
    end()
    sys.exit(0)


Aviso = aviso  # PEP-8


def print_head_level(level, text, *args, **kwargs):
    assert 1 <= level <= 6
    tag = 'h{0:d}'.format(level)
    klass = kwargs.pop('klass', '')
    if klass:
        printf('<{tag} class="{cls}">', tag=tag, cls=klass, end='')
    else:
        printf('<{tag}>', tag=tag, end='')
    kwargs['end'] = ''
    printf(text, *args, **kwargs)
    printf('</{}>', tag)


h1 = partial(print_head_level, 1)
h2 = partial(print_head_level, 2)
h3 = partial(print_head_level, 3)
h4 = partial(print_head_level, 4)
h5 = partial(print_head_level, 5)
h6 = partial(print_head_level, 6)


def blockquote(item):
    printf('<blockquote>\n{}\n</blockquote>', item)


def hr():
    printf('<hr size="1" noshadow>')


def Small(text):
    return '<small>%s</small>' % text


def Big(text):
    return '<big>%s</big>' % text


def p(texto, klass='PARRAFO'):
    if isinstance(texto, list):
        for linea in texto:
            printf('<p class="{}">{}</p>', klass, linea)
    else:
        printf('<p class="{}">{}</p>', klass, texto)


def div(texto, klass='', identity=''):
    printf(
        '<div{0}{1}>',
        ' class="{}"'.format(klass) if klass else '',
        ' id="{}"'.format(identity) if identity else '',
        )
    printf(texto)
    printf('</div>')


def QuitaAcentos(s):
    if type(s) is not six.text_type:
        s = s.decode('utf-8')
    tabla = {
        ord(a): ord(b)
        for a, b in zip('ÁÉÍÓÚáéíóúÜü', 'AEIOUaeiouUu')
        }
    return s.translate(tabla)


def b(txt):
    return '<b>{}</b>'.format(txt)


def a(url, txt='', klass=''):
    txt = txt or url
    if klass:
        return '<a href="{}" class="{}">{}</a>'.format(url, klass, txt)
    else:
        return '<a href="{}">{}</a>'.format(url, txt)


def li(text, klass=''):
    if klass:
        return '<li class="%s">%s</li>' % (klass, text)
    else:
        return '<li>%s</li>' % text


def ul(*items, **kw):
    klass = kw.pop('klass', None)
    line = ['<ul']
    if klass:
        line.append(' class="{}"'.format(klass))
    _id = kw.pop('id', None)
    if _id:
        line.append(' id="{}"'.format(_id))
    line.append('>')
    printf(''.join(line))
    for i in items:
        printf(li(i))
    printf('</ul>')


def fa(name):  # Font Awesome:
    return '<i class="fa fa-{}" aria-hidden="true"></i>'.format(name)


def dt(name, definition):
    printf('<dt>{}</dt> <dd>{}</dd>', name, definition)


def ol(*items):
    printf('<ol>')
    for i in items:
        printf(li(i))
    printf('</ol>')


def tt(s, klass=None):
    """Imprime la variable pasada como parámetro C{s}, rodeada
    de los etiquetas HTML usadas para indicar texto
    de teletipo (Fuente monoespaciada), <tt> y </tt>.

    @param s: Variable a imprimir
    @param klass: Atributo C{class} de la etiqueta <tt> (Opcional).
    """
    if klass:
        printf('<tt class="{}">{}</tt>'.format(klass, s))
    else:
        printf('<tt>{}</tt>'.format(s))


def table(caption=None, border=0, width='100%'):
    printf('<table border={} width="{}"> '.format(border, width))
    if caption:
        print ('<caption>{}</caption>'.format(caption))
    push('</table>')


# def end(tag=None):
#     if tag:
#         print('</{}>'.format(tag))
#     else:
#         pop()


def th(item):
    printf('<th>{}</th>', item)


def thtd(header, data):
    return '\n'.join([
        '<tr>',
        '\t<th align="right">{}</th>'.format(header),
        '\t<td align="left">{}</td>'.format(
            '<i class="undefined">No definido</i>' if data is None else data
            ),
        '</tr>',
        ])


def td(item):
    return '<td>{}</td>'.format(item)


def tr(*items):
    buff = ['<tr valign="top">']
    buff.extend(['\t<td>{}</td>'.format(_) for _ in items])
    buff.append('</tr>')
    return '\n'.join(buff)


def table_section(text, numcols):
    printf('<tr bgcolor="99AA99">')
    printf('<td colspan="{}">{}</td>', numcols, text)
    printf('</tr>')


def thead(*items):
    buff = ['<thead><tr>']
    buff.extend(['\t<th>{}</th>'.format(_) for _ in items])
    buff.append('</tr></thead>')
    return '\n'.join(buff)


class HTMLTAG:

    def Html(self):
        return str(self)

    def Print(self):
        printf(str(self))

    def Write(self):
        sys.stdout.write(str(self))

    def _clears(self):
        self.Stack = []

    def _adds(self, s):
        try:
            self.Stack.append(s)
        except AttributeError:
            self.Stack = [s]

    def _gets(self, sep=''):
        return sep.join(self.Stack)


class Href(HTMLTAG):

    def __init__(self, url, text=None, param=None, title=None, klass=''):
        self.Text = text
        self.Url = url
        if self.Url:
            self.Url = self.Url.replace('&', '&amp;')
        if self.Url and not self.Text:
            self.Text = self.Url
        self.Title = title
        self.klass = klass
        self.Param = param
        self.Target = '_top'

    def setTarget(self, target):
        self.Target = target

    def __str__(self):
        self.Stack = []
        url = self.Url
        if self.Param:
            url = url + '?%s' % self.Param
        if self.Url:
            self._adds('<a href="%s"' % url)
            self._adds(' target="%s"' % self.Target)
            if self.klass:
                self._adds(' class="%s"' % self.klass)
            if self.Title:
                self._adds(' title="%s"' % self.Title)
            self._adds('>%s</a>' % self.Text)
        else:
            self._adds(self.Text)
        return self._gets()


class DL(HTMLTAG):

    def __init__(self, hash={}):
        self.Hash = hash

    def __setitem__(self, key, value):
        self.Hash[key] = value

    def __str__(self):
        self._adds('<dl>')
        for k in self.Hash.keys():
            self._adds('<dt><tt>%s</tt>' % k)
            self._adds('<dd>%s' % self.Hash[k])
        self._adds('</dl>')
        return self._gets()


class UL(HTMLTAG):

    def __init__(self, list=[], klass='', identity=''):
        self.List = list
        self.Klass = klass
        self.Identity = identity

    def append(self, item):
        self.List.append(item)

    def __str__(self):
        self._adds('<ul')
        if self.Klass:
            self._adds(' class="%s"' % self.Klass)
        if self.Identity:
            self._adds(' id="%s"' % self.Identity)
        self._adds('>\n')
        for i in self.List:
            self._adds('<li>%s</li>\n' % i)
        self._adds('</ul>\n')
        return self._gets()


class Img(HTMLTAG):
    '''Clase para el tag IMG de Html.

    Esta clase permite definir tags de imágenes según la norma Html,
    permitiendo modificar las atributos, como Border o Alt durante la
    ejecución.
    '''

    def __init__(self, src, w, h, alt, **kwargs):
        self.Src = src
        self.Width = w
        self.Height = h
        self.Alt = alt
        self.Title = kwargs.get('title', '')
        self.Border = kwargs.get('border', 0)
        self.UseMap = kwargs.get('usemap')
        self.Klass = kwargs.get('klass', '')
        self.Identity = kwargs.get('identity', '')
        self.HSpace = kwargs.get('hspace')

    def setBorder(self, w=2):
        self.Border = w

    def setHSpace(self, hspace=2):
        self.HSpace = hspace

    def __str__(self):
        self._clears()
        self._adds(
            '<img src="{}"'
            ' width="{}"'
            ' height="{}"'
            ' alt="{}"'
            ' border="{}"'.format(
                self.Src,
                self.Width,
                self.Height,
                self.Alt,
                self.Border
                )
            )
        if self.Klass:
            self._adds(' class="%s"' % self.Klass)
        if self.Title:
            self._adds(' title="%s"' % self.Title)
        if self.Identity:
            self._adds(' id="%s"' % self.Identity)
        if self.UseMap:
            self._adds(' usemap="#%s"' % self.UseMap)
        if self.HSpace:
            self._adds(' hspace="%s"' % self.HSpace)
        self._adds('>')
        return self._gets()


IMG = Img


def img(src, w, h, alt, border=0):
    IMG(src, w, h, alt, border).Write()


# Tablas


class TD(HTMLTAG):

    def __init__(self, value, align=None, valign=None, bgcolor=None):
        self.Value = value
        self.BGColor = bgcolor
        self.Align = align
        self.VAlign = valign

    def __str__(self):
        self._adds('<td')
        if self.Align:
            self._adds(' align="%s"' % self.Align)
        if self.VAlign:
            self._adds(' Valign="%s"' % self.VAlign)
        if self.BGColor:
            self._adds(' bgcolor="%s"' % self.BGColor)
        self._adds('>%s</td>' % self.Value)
        return self._gets()


class TR(HTMLTAG):

    def __init__(self, *items):
        self.Items = []
        for item in items:
            self.append(TD(item))

    def append(self, value):
        self.Items.append(TD(value))

    def __str__(self):
        self._adds('<tr>')
        for i in self.Items:
            self._adds(str(i))
        self._adds('</tr>\n')


class TABLE(HTMLTAG):

    def __init__(self, border=1, caption=None):
        self.Border = border
        self.Caption = caption
        self.Rows = []

    def TR(self, *values):
        row = TR()
        for item in values:
            row.append(TD(item))

    def __str__(self):
        self._adds('<table border="{}">'.format(self.Border))
        if self.Caption:
            self._adds('<Caption>{}</Caption>\n'.format(self.Caption))
        for row in self.Rows:
            self._adds(str(row))
        self._adds('</table>\n')
        return self._gets()


def get_base_path():
    full_path = os.getcwd()
    if 'cgi-bin' in full_path:
        return full_path.split('cgi-bin')[0]
    elif 'wwwroot' in full_path:
        return full_path.split('wwwroot')[0]
    else:
        return full_path


def getDocumentRoot():
    '''Rotorna el directorio absoluto donde están los contenidos
    estáticos: wwwroot.
    '''
    return os.path.join(get_base_path(), 'wwwroot')


def get_cgi_bin():
    '''Rotorna el directorio absoluto donde están los contenidos
    estáticos: wwwroot.
    '''
    return os.path.join(get_base_path(), 'cgi-bin')


def includeFile(filename):
    if not os.path.exists(filename):
        if filename[0] == '/' or filename[0] == '\\':
            filename = os.path.join(getDocumentRoot(), filename[1:])
    if os.path.exists(filename):
        with open(filename, 'r') as f:
            print(f.read())
    else:
        raise HtmlError(
            'Error al incluir un archivo',
            'No puedo encontrar el archivo de texto {}'.format(filename)
            )


include_file = includeFile  # Pep8


def join(*directorios):
    result = os.path.join(getDocumentRoot(), *directorios)
    return os.path.normpath(result)


def get_script_name():
    return os.environ.get('SCRIPT_NAME', '')


def get_path_info():
    result = os.environ.get('PATH_INFO', '')
    script_name = get_script_name()
    if result and result.startswith(script_name):
        result = result[len(script_name):]
    return result


def getRelativePath(camino):
    '''Devuelve la ruta relativa a partir de la ruta absoluta.

    Si tenemos el nombre completo de un fichero que este en el directorio
    ofrecido por el servidor web, esta función nos retorna el path relativo,
    es decir, la ruta relativa a partir del directorio retornado
    por L{getDocumentRoot}

    @return: ruta relativa al directorio raiz de la web
    @rtype: string
    '''
    if not camino.endswith(os.path.sep):
        camino += os.path.sep
    camino = camino.replace(getDocumentRoot(), '')
    return camino


def trace_on():
    sys.stderr = sys.stdout
    cgitb.enable(display=1)
    return True


def trace_off():
    sys.stderr = sys.__stderr__
    cgitb.enable(display=0)
    return False


def Check_CGI_Traceback():
    '''Activa/Desactiva la traza de las aplicaciones.

    Si la variable de entorno `PYTHON_CGITB_ENABLE` no está
    definida, o está definida con un valor distinto de `0`, se
    activa el módulo cgitb, en caso contrario se
    desactiva. Tambien se activa si en la *query string* se
    especifica `tron=s`, o si a nivel global se ha definido
    como verdadera la constante `TRON`.

    '''
    flag = any([
        os.environ.get('PYTHON_CGITB_ENABLE', '0') != '0',
        'tron=s' in os.environ.get('QUERY_STRING', ''),
        globals().get('TRON', False),
        ])
    return trace_on() if flag else trace_off()


class StyleSheets(object):
    '''Contenedor de Hojas de estilo.
    '''

    def __init__(self):
        self.Estilos = []

    def _link(self, url, media=''):
        if media:
            return '<link rel="stylesheet"'  \
                   ' type="text/css" media="{}"'  \
                   ' href="{}">'.format(media, url)
        else:
            return '<link rel="stylesheet"'  \
                   ' type="text/css"'  \
                   ' href="{}">'.format(url)

    def add(self, url, media=''):
        self.Estilos.append(self._link(url, media))

    def __str__(self):
        return '\n'.join(self.Estilos)


def show(name):
    import cgi
    import inspect
    print('<p>Variable <b><tt>{}</tt></b>:'.format(name))
    for t in inspect.stack()[1:]:
        (f_frame, f_filename, f_line_number,
            f_function_name, context, index) = t
        if name in f_frame.f_locals:
            v = f_frame.f_locals[name]
            print('({})'.format(cgi.escape(str(type(v)))))
            print(pformat(v))
        else:
            print('Error: variable <i>No definida</i>')
    print('</p>')


class Alert:

    def __init__(self, msg, klass='info'):
        self.msg = msg
        self.klass = klass

    def __str__(self):
        return (
            '<div class="alert alert-{cls}" role="alert">\n'
            '{msg}\n'
            '</div>'.format(
                cls=self.klass,
                msg=self.msg,
                )
            )


def alert(msg, *args, **kwargs):
    end = kwargs.pop('end', '\n')
    klass = kwargs.pop('klass', 'info')
    msg = str(msg)
    if args or kwargs:
        msg = msg.format(*args, **kwargs)
    print(Alert(msg, klass), end=end)


class Button:

    def __init__(self, url, text, **kwargs):
        self.url = url
        self.text = text
        klass = kwargs.pop('klass', 'info')
        self.klass = "btn btn-{}".format(klass)
        self.identity = kwargs.pop('identity', None)
        self.disabled = kwargs.pop('disabled', False)
        self.icon = kwargs.pop('icon', '')
        self.attrs = kwargs.copy()

    def __str__(self):
        if self.disabled:
            buff = [
                '<button type="button"'
                ' class="{}"'
                ' disabled="disabled">'.format(self.klass),
                self.icon,
                '&nbsp;' if self.icon else '',
                str(self.text),
                '</button>',
                ]
        else:
            if self.attrs:
                args = '&'.join([
                    '{}={}'.format(
                        name,
                        quote(str(self.attrs[name])),
                        )
                    for name in self.attrs
                    ])
                url = '{}?{}'.format(self.url, args)
            else:
                url = self.url
            buff = [
                '<a role="button" class="{}"'.format(self.klass),
                ' rel="nofollow" href="{}"'.format(url),
                ' id="{}"'.format(self.identity) if self.identity else '',
                '>',
                self.icon,
                '&nbsp;' if self.icon else '',
                str(self.text),
                '</a>',
                ]
        return ''.join(buff)


def button(url, text='', **kwargs):
    return str(Button(url, text, **kwargs))


FA_PLUS = FA_ADD = '<i class="fa fa-plus-square-o" aria-hidden="true"></i>'

add_button = partial(button, icon=FA_ADD, klass='info')

FA_MINUS = '<i class="fa fa-minus-square-o" aria-hidden="true"></i>'
minus_button = partial(button, icon=FA_MINUS, klass='info')

FA_EDIT = '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>'
edit_button = partial(button, icon=FA_EDIT, klass='info')

FA_DELETE = '<i class="fa fa-trash" aria-hidden="true"></i>'
delete_button = partial(button, icon=FA_DELETE, klass='danger')

FA_CANCEL = '<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>'
cancel_button = partial(button, icon=FA_CANCEL, klass='warning')

FA_EYE = '<i class="fa fa-eye" aria-hidden="true"></i>'
view_button = partial(button, icon=FA_EYE, klass='info')

FA_UP = '<i class="fa fa-chevron-circle-up" aria-hidden="true"></i>'
up_button = partial(button, icon=FA_UP, klass='info')

FA_DOWN = '<i class="fa fa-chevron-circle-down" aria-hidden="true"></i>'
down_button = partial(button, icon=FA_DOWN, klass='info')

FA_RIGHT = '<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>'
right_button = partial(button, icon=FA_RIGHT, klass='info')

FA_LEFT = '<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>'
left_button = partial(button, icon=FA_LEFT, klass='info')

FA_CAMERA = '<i class="fa fa-camera" aria-hidden="true"></i>'
camera_button = partial(button, icon=FA_CAMERA, klass='info')

FA_LOCK = '<i class="fa fa-lock" aria-hidden="true"></i>&nbsp;'
lock_button = partial(button, icon=FA_LOCK, klass='warning')

FA_CALENDAR = '<i class="fa fa-calendar-plus-o" aria-hidden="true"></i>'
calendar_button = partial(button, icon=FA_CALENDAR, klass='info')

FA_PDF = '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>'
pdf_button = partial(button, icon=FA_PDF, klass='info')

FA_PLAY = '<i class="fa fa-play" aria-hidden="true"></i>'
play_button = partial(button, icon=FA_PLAY, klass='info')


def mas_info(url):
    return (
        '<p class="right"><a href="{url}"'
        ' class="btn btn-warning btn-xs" role="button"'
        ' title="Más información">{btn}</a></p>'.format(
            url=url,
            btn=FA_PLUS,
            )
        )


def well(texto):
    print('<div class="well">{}</div>'.format(texto))


class CaptureOutput:

    def __init__(self, list_output=None):
        self.list_output = [] if list_output is None else list_output

    def __enter__(self):
        self._sys_stdout_backup = sys.stdout
        sys.stdout = self.stream = StringIO()
        return self.stream

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout = self._sys_stdout_backup
        self.list_output.append(self.stream.getvalue())
        return False

    def getvalue(self):
        return '\n'.join(self.list_output)


class PanelBase:

    def __init__(self, title, klass='default'):
        self.title = title
        self.klass = klass
        self.footer = None

    def __enter__(self):
        print('<div class="panel panel-{}">'.format(self.klass))
        print(' <div class="panel-heading">')
        print('  <h3 class="panel-title">{}</h3>'.format(self.title))
        print(' </div><!-- de panel-heading -->')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.footer:
            print('<div class="panel-footer">')
            print(self.footer)
            print('</div>')
        print('</div><!-- de panel -->')

    def print(self, s, *args, **kwargs):
        printf(s, *args, **kwargs)

    def p(self, txt, klass=''):
        if klass:
            print('<p class="{}">{}</p>'.format(klass, txt))
        else:
            print('<p>{}</p>'.format(txt))


class Panel(PanelBase):

    def __enter__(self):
        super(Panel, self).__enter__()
        print(' <div class="panel-body">')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(' </div><!-- de panel-body -->')
        super(Panel, self).__exit__(exc_type, exc_val, exc_tb)
        return False


class ListPanel(PanelBase):

    def __enter__(self):
        super(ListPanel, self).__enter__()
        print(' <ul class="list-group">')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(' </ul><!-- de list-group -->')
        super(ListPanel, self).__exit__(exc_type, exc_val, exc_tb)
        return False

    def li(self, s):
        print('  <li class="list-group-item">{}</li>'.format(s))


class TablePanel(PanelBase):

    def __enter__(self):
        super(TablePanel, self).__enter__()
        print(' <table class="table table-striped">')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(' </table><!-- de list-group -->')
        super(TablePanel, self).__exit__(exc_type, exc_val, exc_tb)
        return False

    def td(self, *args):
        print('<tr>')
        for a in args:
            print('<td>{}</td>'.format(a))
        print('</tr>')

    def thtd(self, *args):
        if args:
            print('<tr>')
            print('<th>{}</th>'.format(args[0]))
            for a in args[1:]:
                print('<td>{}</td>'.format(a))
            print('</tr>')


# Barra de navegacion por años


def nav_bar_year(url, active_year, **kwargs):
    today = datetime.date.today()
    from_year = kwargs.get('from_year', today.year)
    to_year = kwargs.get('to_year', from_year - 5)
    field_name = kwargs.get('field_name', 'anio')
    if from_year < to_year:
        from_year, to_year = to_year, from_year
    if not from_year >= active_year >= to_year:
        raise ValueError("El año activo cae fuera del rango")
    buff = []
    buff.append('''
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button"
         class="navbar-toggle collapsed"
         data-toggle="collapse"
         data-target="#navbar-years"
         aria-expanded="false">
        <span class="sr-only">Activar navegación</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-years">
      <ul class="nav navbar-nav">
    ''')
    for y in range(from_year, to_year-1, -1):
        buff.append('<li class="active">' if y == active_year else '<li>')
        buff.append('<a href="{url}">{txt}</a>'.format(
            url='{}?{}={}'.format(url, field_name, y),
            txt=str(y)
            ))
        buff.append('</a>')
        buff.append('</li>\n')
    buff.append('</ul>\n')
    buff.append('''
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
''')
    return ''.join(buff)


def alfabar(clavs=None):
    if clavs is None:
        clavs = []
    buff = [
        '<nav aria-label="Acceso por inicial">\n',
        '<ul class="pagination pagination-sm">\n',
        ]
    for letra in 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZ':
        if letra in clavs:
            buff.append('<li>')
        else:
            buff.append('<li class="disabled">')
        buff.append('<a href="#{0}">{0}</a>'.format(letra))
        buff.append('</li>\n')
    buff.append('</ul>\n')
    buff.append('</nav>\n')
    return ''.join(buff)
